<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Dbiz
 * @package     Dbiz_Banner
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 * General form
 *
 * @category   Dbiz
 * @package    Dbiz_Banner
 * @author     Pham Hoang Tan
 */
class Dbiz_Banner_Block_Adminhtml_Banner_Add_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('banner_form', array('legend'=>Mage::helper('banner')->__('General Information')));
        $fieldset->addField('name', 'text', array(
            'label'     => Mage::helper('banner')->__('Title'),
            'title'     => Mage::helper('banner')->__('Title'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'name',
        ));
        $fieldset->addField('position', 'select', array(
            'label'     => Mage::helper('banner')->__('Position'),
            'title'     => Mage::helper('banner')->__('Position'),
			'class'     => 'required-entry',
            'name'      => 'position',
            'values'    => Mage::getSingleton('banner/config_source_position')->toOptionArray(),
        ));
		
		$dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
		$fieldset->addField('start_date', 'date', array(
		  'name'   			=> 'start_date',
		  'label'  			=> Mage::helper('banner')->__('Start Date'),
		  'title'  			=> Mage::helper('banner')->__('Start Date'),
		  'class'    		=> 'validate-date',
          'required'  		=> true,
		  'time' 			=> false,
		  'image'  			=> $this->getSkinUrl('images/grid-cal.gif'),
		  'input_format' 	=> Varien_Date::DATETIME_INTERNAL_FORMAT,
		  'format'       	=> $dateFormatIso
		));	
		
		$fieldset->addField('end_date', 'date', array(
		  'name'   			=> 'end_date',
		  'label'  			=> Mage::helper('banner')->__('End Date'),
		  'title'  			=> Mage::helper('banner')->__('End Date'),
		  'class'    		=> 'validate-date',
		  'required'  		=> false,
		  'time' 			=> false,
		  'image'  			=> $this->getSkinUrl('images/grid-cal.gif'),
		  'input_format' 	=> Varien_Date::DATETIME_INTERNAL_FORMAT,
		  'format'       	=> $dateFormatIso
		));
		       
        $fieldset->addField('sort_order', 'text', array(
            'label'     => Mage::helper('banner')->__('Sort By'),
            'title'     => Mage::helper('banner')->__('Sort By'),
            'required'  => false,
            'name'      => 'sort_order',
        ));
        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('banner')->__('Is Active'),
            'title'     => Mage::helper('banner')->__('Is Active'),
            'name'      => 'is_active',
            'values'    => Mage::getSingleton('adminhtml/system_config_source_yesno')->toOptionArray(),
        ));
        
        $storeList = array(array('value' => 0, 'label' => $this->__('Select All Store')));
        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('stores', 'multiselect', array(
                'label'     => Mage::helper('banner')->__('Visible In'),
                'title'     => Mage::helper('banner')->__('Visible In'),
                'required'  => true,
                'name'      => 'stores[]',
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(),
            ));
        }
        else {
            $fieldset->addField('stores', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
        }
        if( Mage::getSingleton('adminhtml/session')->getBannerData() ) {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getBannerData());
            Mage::getSingleton('adminhtml/session')->setBannerData(null);
        }
        return parent::_prepareForm();
    }
}
