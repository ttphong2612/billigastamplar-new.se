<?php

class Dbiz_Banner_Block_Adminhtml_Banner_Renderer_EndDate extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$value =  $row->getData($this->getColumn()->getIndex());
		if (!empty($value)) {
			$value = date('d/m/Y', strtotime($value));
		}
		return '<span style="color:red;">'.$value.'</span>';
	}
}
?>