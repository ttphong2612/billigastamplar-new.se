<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Dbiz
 * @package     Dbiz_Banner
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Base Banner Block Content Partner
 *
 * @category   Dbiz
 * @package    Dbiz_Banner
 * @author     Pham Hoang Tan
 */
class Dbiz_Banner_Block_Content_Partner extends Dbiz_Banner_Block_Banner {
    protected $_position = 'CONTENT_PARTNER';
    
    public function _getCollectionAllPage(){ 	
    	$result = array();   
		$collection_1 = $this->_getCollection($this->_position);
		if(count($collection_1) > 0) {
			foreach ($collection_1 as $item) {
				$result[$item->getBannerId()] = $item;
			}
		}
		
		$collection_2 = $this->_getCollectionPage($this->_position);
		if(count($collection_2) > 0) {
			foreach ($collection_2 as $item) {
				$result[$item->getBannerId()] = $item;
			}
		}
		
		return $result;
    }
    
    protected function _getCollectionPage($position = null) {
        $storeId = Mage::app()->getStore()->getId();
        
        $collection = Mage::getModel('banner/banner')->getCollection()
                ->addEnableFilter($this->_isActive);
        if (!Mage::app()->isSingleStoreMode()) {
            $collection->addStoreFilter($storeId);
        }
		
        if (Mage::registry('current_category')) {
            $collection->addPageFilter();
        } elseif (Mage::app()->getFrontController()->getRequest()->getRouteName() == 'cms') {
            $pageId = Mage::getBlockSingleton('cms/page')->getPage()->getPageId();
            $collection->addPageFilter($pageId);
        }

        if ($position) {
            $collection->addPositionFilter($position);
        } elseif ($this->_position) {
            $collection->addPositionFilter($this->_position);
        }
        return $collection;      
    }
   
}