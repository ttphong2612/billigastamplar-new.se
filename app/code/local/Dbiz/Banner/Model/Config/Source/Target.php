<?php
class Dbiz_Banner_Model_Config_Source_Target {
	
    const BLANK     = '_blank';
    const SELF      = '_self';
    const PARENT    = '_parent'; 

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        return array(
            array('value' => self::BLANK, 'label'=>Mage::helper('adminhtml')->__('blank')),
            array('value' => self::SELF, 'label'=>Mage::helper('adminhtml')->__('self')),
            array('value' => self::PARENT, 'label'=>Mage::helper('adminhtml')->__('parent'))
        );
    }
}
