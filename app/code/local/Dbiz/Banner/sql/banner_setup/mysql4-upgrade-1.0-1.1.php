<?php
$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();
$installer->getConnection()->addColumn(
    $this->getTable('banner/banner'),
    'start_date',
    'datetime NOT NULL'
);

$installer->getConnection()->addColumn(
    $this->getTable('banner/banner'),
    'end_date',
    'datetime NULL'
);

$installer->endSetup();