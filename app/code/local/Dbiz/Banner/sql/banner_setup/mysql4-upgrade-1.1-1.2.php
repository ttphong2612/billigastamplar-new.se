<?php

$installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */

$installer->startSetup();
$installer->run("

ALTER TABLE `{$this->getTable('banner/banner')}`
   ADD INDEX `position` (`position`),
   ADD INDEX `start_date` (`start_date`),
   ADD INDEX `end_date` (`end_date`),
   ADD INDEX `sort_order` (`sort_order`);

ALTER TABLE `{$this->getTable('banner/banner_image')}`
   ADD INDEX `label` (`label`),
   ADD INDEX `position` (`position`);

ALTER TABLE `{$this->getTable('banner/banner_category')}`
   ADD INDEX `category_id` (`category_id`);

ALTER TABLE `{$this->getTable('banner/banner_page')}`
   ADD INDEX `page_id` (`page_id`);

ALTER TABLE `{$this->getTable('banner/banner_store')}`
   ADD INDEX `store_id` (`store_id`);

");

$installer->endSetup();
