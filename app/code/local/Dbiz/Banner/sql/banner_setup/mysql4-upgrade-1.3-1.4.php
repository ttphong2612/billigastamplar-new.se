<?php
$installer = $this;
$installer->startSetup();
$installer->run("
	ALTER TABLE `{$this->getTable('banner/banner_image')}`
		ADD COLUMN `target` ENUM('_self','_blank') NULL DEFAULT '_self' AFTER `position`;
");

$installer->endSetup();