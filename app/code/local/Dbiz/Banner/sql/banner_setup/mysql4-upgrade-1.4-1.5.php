<?php
$installer = $this;
$installer->startSetup();
$installer->run("
ALTER TABLE `dbiz_banner_banner_image` ADD COLUMN `image_text` VARCHAR(255) NULL;
");

$installer->endSetup();