<?php

class Dbiz_CategoryExtend_Block_Homepage extends Mage_Core_Block_Template {

    protected $_displayAttribute = 'category_homepage_display';
    protected $_moduleName = 'Dbiz_CategoryExtend';
    protected $_postionAttribute = 'category_homepage_position';

    public function getCategoryShowInHomepage($anpha = true) {
        if (Mage::helper('core')->isModuleEnabled($this->_moduleName)) {
            $categories = Mage::getModel('cateextend/categories')->getCategoriesByCutomAtribute(false, $this->_displayAttribute,$this->_postionAttribute);

            return $categories;
        }
    }

}
