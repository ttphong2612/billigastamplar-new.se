<?php

class Dbiz_CategoryExtend_Helper_Data extends Mage_Core_Helper_Abstract {

    public function setAnphabeForArray($data) {
        $result = array();
        foreach ($data as $item)
            $result[$item->getName()] = $item;
        ksort($result);
        return $result;
    }

}
