<?php

class Dbiz_CategoryExtend_Model_Categories extends Mage_Core_Model_Abstract {

    public function getCategoriesByCutomAtribute($anpha, $displayAtribute, $postionAtribute) {

        try {
            $result = array('result' => true, 'dataType' => 'array', 'data' => null);
            $categories = Mage::getModel('catalog/category')->getCollection()
                    ->addAttributeToSelect(array('description', 'image', 'url', 'name'))
                    ->addAttributeToFilter($displayAtribute, array('eq' => 1))
                    ->addAttributeToFilter('is_active', 1)
                    ->addAttributeToSort($postionAtribute, 'asc')
            ;
            if ($categories->count() < 0)
                throw new Exception('Not found any category !');
            if ($anpha)
                $result['data'] = Mage::helper('cateextend')->setAnphabeForArray($categories);
            else
                $result['data'] = $categories;
            return $result;
        } catch (Exception $ex) {
            Mage::log($ex->getMessage(), Zend_Log::ERR);
            $result['result'] = false;
            return $result;
        }
    }

}
