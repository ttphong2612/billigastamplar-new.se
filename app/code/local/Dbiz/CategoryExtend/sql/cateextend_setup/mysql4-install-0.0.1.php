<?php

$installer = $this;
$installer->startSetup();
$entityTypeId = $installer->getEntityTypeId('catalog_category');
$attributeSetId = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$installer->addAttribute('catalog_category', 'category_homepage_display', array(
    'type' => 'int',
    'label' => 'Display in Homepage',
    'input' => 'select',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'visible' => true,
    'required' => false,
    'user_defined' => false,
    'default' => 0,
    'source' => 'eav/entity_attribute_source_boolean',
    'group' => "General Information"
));


$installer->addAttributeToGroup(
        $entityTypeId, $attributeSetId, $attributeGroupId, 'category_homepage_display', '11'
);

$attributeId = $installer->getAttributeId($entityTypeId, 'category_homepage_display');

$installer->run("
INSERT INTO `{$installer->getTable('catalog_category_entity_int')}`
(`entity_type_id`, `attribute_id`, `entity_id`, `value`)
    SELECT '{$entityTypeId}', '{$attributeId}', `entity_id`, '1'
        FROM `{$installer->getTable('catalog_category_entity')}`;
");

Mage::getModel('catalog/category')
        ->load(1)
        ->setImportedCatId(0)
        ->setInitialSetupFlag(true)
        ->save();

Mage::getModel('catalog/category')
        ->load(2)
        ->setImportedCatId(0)
        ->setInitialSetupFlag(true)
        ->save();

$installer->endSetup();
