<?php

$this->startSetup();
$this->removeAttribute('catalog_category', 'category_homepage_position');
$this->addAttribute(Mage_Catalog_Model_Category::ENTITY, 'category_homepage_position', array(
    'group'         =>  "General Information",
    'input'         => 'text',
    'type'          => 'int',
    'label'         => 'Visible position',
    'default'       =>  "",
    'visible'       => true,
    'user_defined'  =>  true,
    'required'      => false,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
));
 
$this->endSetup();
