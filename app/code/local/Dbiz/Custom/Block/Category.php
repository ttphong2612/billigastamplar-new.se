<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Dbiz
 * @package     Dbiz_Banner
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Base Banner Block
 *
 * @category   Dbiz
 * @package    Dbiz_Banner
 * @author     Pham Hoang Tan
 */
class Dbiz_Custom_Block_Category extends Mage_Catalog_Block_Product_Abstract 
{
	 /**
     * Product Collection
     *
     * @var array product collection
     */
    protected $productCateCollection;
	
	/**
     * Retrieve loaded category collection
     *
     * @return array product collection
     */
    protected function _getProductCollection()
    {
        if (is_null($this->productCateCollection)) {
            $layer = $this->getLayer();
			$category = $layer->getCurrentCategory();
			$_subCategories = $category->getChildrenCategories();
			
			foreach($_subCategories as $_subCategory){
				$products = array();
				$productsCollection = $_subCategory->getProductCollection()
							 ->addAttributeToSelect('*') // add all attributes - optional
							 ->addAttributeToFilter('status', 1) // enabled
							 ->addAttributeToFilter('visibility', 4) //visibility in catalog,search
							 ->setOrder('position', 'ASC'); //sets the order by price
				foreach($productsCollection as $product){
					$products[] = $product;
				}			
				$this->productCateCollection[] = array('products' => $products, 
													   'category_id' => $_subCategory->getId(), 
													   'name'  => $_subCategory->getName() );
				
			}
        }
        return $this->productCateCollection;
    }
	
	/**
     * Need use as _prepareLayout - but problem in declaring collection from
     * another block (was problem with search result)
     */
    protected function _beforeToHtml()
    {
        // called prepare sortable parameters
        $collection = $this->_getProductCollection();
        return parent::_beforeToHtml();
    }
	
	 /**
     * Retrieve loaded category collection
     *
     * @return array
     */
    public function getLoadedProductCollection()
    {
        return $this->_getProductCollection();
    }
	
	/**
     * Get catalog layer model
     *
     * @return Mage_Catalog_Model_Layer
     */
    public function getLayer()
    {
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('catalog/layer');
    }
	
	/**
     * Retrieve block cache tags based on product collection
     *
     * @return array
     */
    public function getCacheTags()
    {
        return array_merge(
            parent::getCacheTags(),
            $this->getItemsTags($this->_getProductCollection())
        );
    }
	
	public function getMaxProductInSubCate()
	{
		$cateProducts = $this->getLoadedProductCollection();
		$maxTotal = 0;
		foreach($cateProducts as $cateProduct){
			if($maxTotal < count($cateProduct['products'])){
				$maxTotal = count($cateProduct['products']);
			}
		}
		return $maxTotal;
	}
}
