<?php

/**
 * @category    DBIZ
 * @package     DBIZ Stamp Home Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net>
 * @modified    Tan Pham
 */
class Dbiz_StamphomeProducts_Block_Adminhtml_Edit_Grid extends Mage_Adminhtml_Block_Widget_Grid {

	public function __construct() {
		parent::__construct();

		$this->setId('is_stamphome');
		$this->setDefaultSort('entity_id');
		$this->setUseAjax(true);

		$this->setRowClickCallback('productRowClick');
		$this->setCheckboxCheckCallback('registerProduct');
		$this->setRowInitCallback('productRowInit');
	}

	public function getProduct() {
		return Mage::registry('product');
	}

	protected function _getStore() {
		$storeId = (int) $this->getRequest()->getParam('store', 0);
		return Mage::app()->getStore($storeId);
	}

	protected function _addColumnFilterToCollection($column) {
		if ($this->getCollection()) {
			if ($column->getId() == 'websites') {
				$this->getCollection()->joinField('websites', 'catalog/product_website', 'website_id', 'product_id=entity_id', null, 'left');
			}
		}

		if ($column->getId() == "stamphome") {
			$productIds = $this->_getSelectedProducts();
			if (empty($productIds)) {
				$productIds = 0;
			}
			if ($column->getFilter()->getValue()) {
				$this->getCollection()->addFieldToFilter('entity_id', array('in' => $productIds));
			} elseif (!empty($productIds)) {
				$this->getCollection()->addFieldToFilter('entity_id', array('nin' => $productIds));
			}
		} else {
			parent::_addColumnFilterToCollection($column);
		}

		return $this;
	}

	protected function _prepareCollection() {

		$store = $this->_getStore();
		$collection = Mage::getModel('catalog/product')->getCollection()
			->addAttributeToSelect('name')
			->addAttributeToSelect('sku')
			->addAttributeToSelect('dbiz_stamphome_product')
			->addAttributeToSelect('type_id')
			->addAttributeToFilter('visibility', array('nin' => array(1, 3)));

		if ($store->getId()) {
			//$collection->setStoreId($store->getId());
			$collection->addStoreFilter($store);
			$collection->joinAttribute('custom_name', 'catalog_product/name', 'entity_id', null, 'inner', $store->getId());
			$collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner', $store->getId());
			$collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', 1, 'inner', $store->getId());
			$collection->joinAttribute('price', 'catalog_product/price', 'entity_id', null, 'left', $store->getId());
		} else {
			$collection->addAttributeToSelect('price');
			$collection->addAttributeToSelect('status');
			$collection->addAttributeToSelect('visibility');
		}

		$action_name = $this->getRequest()->getActionName();

		if ($action_name == 'exportCsv' || $action_name == 'exportXml') {
			$collection->addAttributeToFilter('is_stamphome', array('eq' => true));
		}

		$collection->joinField('position', 'stamphomeproducts/product_position', 'position', 'product_id=entity_id', null, 'left');
		$this->setCollection($collection);
		parent::_prepareCollection();
		$this->getCollection()->addWebsiteNamesToResult();

		return $this;
	}

	protected function _prepareColumns() {

		$action_name = $this->getRequest()->getActionName();
		if ($action_name != 'exportCsv' && $action_name != 'exportXml') {

			$this->addColumn('stamphome', array(
				'header_css_class' => 'a-center',
				'type' => 'checkbox',
				'name' => 'stamphome',
				'values' => $this->_getSelectedProducts(),
				'align' => 'center',
				'index' => 'entity_id'
			));
		}

		$this->addColumn('entity_id', array(
			'header' => Mage::helper('catalog')->__('ID'),
			'sortable' => true,
			'width' => '50px',
			'index' => 'entity_id',
			'type' => 'number'
		));

		$this->addColumn('name', array(
			'header' => Mage::helper('catalog')->__('Name'),
			'index' => 'name',
			'width' => '250px',
			'renderer' => 'stamphomeproducts/adminhtml_edit_renderer_name',
		));

		$this->addColumn('type', array(
			'header' => Mage::helper('catalog')->__('Type'),
			'width' => '120px',
			'index' => 'type_id',
			'type' => 'options',
			'options' => Mage::getSingleton('catalog/product_type')->getOptionArray(),
		));

		$this->addColumn('sku', array(
			'header' => Mage::helper('catalog')->__('SKU'),
			'width' => '100',
			'index' => 'sku'
		));

		$this->addColumn('visibility', array(
			'header' => Mage::helper('catalog')->__('Visibility'),
			'width' => '120px',
			'index' => 'visibility',
			'filter' => false,
			'renderer' => 'stamphomeproducts/adminhtml_edit_renderer_visibility',
		));

		if (!Mage::app()->isSingleStoreMode()) {
			$this->addColumn('websites', array(
				'header' => Mage::helper('catalog')->__('Websites'),
				'width' => '140px',
				'sortable' => false,
				'index' => 'websites',
				'type' => 'options',
				'options' => Mage::getModel('core/website')->getCollection()->toOptionHash(),
			));
		}

		$store = $this->_getStore();
		$this->addColumn('price', array(
			'header' => Mage::helper('catalog')->__('Price'),
			'type' => 'price',
			'currency_code' => $store->getBaseCurrency()->getCode(),
			'index' => 'price',
		));
		
		$this->addColumn('status', array(
			'header' => Mage::helper('catalog')->__('Status'),
			'width' => '100px',
			'index' => 'status',
			'type' => 'options',
			'options' => Mage::getSingleton('catalog/product_status')->getOptionArray(),
			'renderer' => 'stamphomeproducts/adminhtml_edit_renderer_status',
		));

		$this->addColumn('position', array(
			'header' => Mage::helper('catalog')->__('Position'),
			'width' => '1',
			'type' => 'number',
			'index' => 'position',
			'editable' => true,
		));

		$this->addExportType('*/*/exportCsv', Mage::helper('stamphomeproducts')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('stamphomeproducts')->__('Excel XML'));

		return parent::_prepareColumns();
	}

	public function getGridUrl() {
		return $this->getUrl('*/*/grid', array('_current' => true));
	}

	protected function _getSelectedProducts($json = false) {
		$temp = $this->getRequest()->getPost('stamphome_ids');
		$store = $this->_getStore();

		if ($temp) {
			parse_str($temp, $stamphome_ids);
		}

		$_prod = Mage::getModel('catalog/product')->getCollection()
			->joinAttribute('is_stamphome', 'catalog_product/is_stamphome', 'entity_id', null, 'left', $store->getId())
			->addAttributeToFilter('is_stamphome', '1');

		$products = $_prod->getColumnValues('entity_id');
		$selected_products = array();


		if ($json == true) {
			foreach ($products as $key => $value) {
				$selected_products[$value] = '1';
			}
			return Zend_Json::encode($selected_products);
		} else {

			foreach ($products as $key => $value) {
				if ((isset($stamphome_ids[$value])) && ($stamphome_ids[$value] == 0)) {
					
				} else {
					$selected_products[$value] = '0';
				}
			}

			if (isset($stamphome_ids)) {
				foreach ($featured_ids as $key => $value) {
					if ($value == 1) {
						$selected_products[$key] = '0';
					}
				}
			}

			return array_keys($selected_products);
		}

		return $products;
	}

	//add javascript before/after grid html
	protected function _afterToHtml($html) {
		return $this->_prependHtml() . parent::_afterToHtml($html) . $this->_appendHtml();
	}

	private function _prependHtml() {
		$html = '<script type="text/javascript">
//<![CDATA[
    categoryForm = new varienForm("stamphome_edit_form");
	categoryForm.submit= function (url) {
	    this._submit();
        return true;
    };

    function getParamValue(element, checked){
        var value = "0_0";
        if(checked){
		    value = "1" + "_" + ((element.positionElement && element.positionElement.value != "") ? element.positionElement.value : "0");
		}
		return value;
    }

    function registerProduct(grid, element, checked){

        if(!element.positionElement){
            return;
        }

		if(checked){
		    element.positionElement.disabled = false;
		} else{
		    element.positionElement.disabled = true;
		}

		checkBoxes.set(element.value, getParamValue(element, checked));

		$("in_stamphome_products").value = checkBoxes.toQueryString();
	   	grid.reloadParams = {"stamphome_ids":checkBoxes.toQueryString()};
    }

    function positionChange(event){
        var element = Event.element(event);
        if(element && element.checkboxElement && element.checkboxElement.checked){
            checkBoxes.set(element.checkboxElement.value, getParamValue(element.checkboxElement, true));
            $("in_stamphome_products").value = checkBoxes.toQueryString();
        }
    }

    var tabIndex = 1000;
    function productRowInit(grid, row){
        var checkbox = $(row).getElementsByClassName("checkbox")[0];
        var position = $(row).getElementsByClassName("input-text")[0];
        if(checkbox && position){
            checkbox.positionElement = position;
            position.checkboxElement = checkbox;
            position.disabled = !checkbox.checked;
            position.tabIndex = tabIndex++;
            Event.observe(position,"keyup",positionChange);
        }
    }

    function categorySubmit(url) {
    	var params = {};
        var fields = $("stamphome_edit_form").getElementsBySelector("input", "select");
        categoryForm.submit();
    }
    
    function productRowClick(grid, event){
    	var trElement = Event.findElement(event, "tr");
    	var isInput   = Event.element(event).tagName == "INPUT";
    	if(trElement){
    	    var checkbox = Element.getElementsBySelector(trElement, "input");
    	    if(checkbox[0]){
    	        var checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                grid.setCheckboxChecked(checkbox[0], checked);
            }
    	}
    }
//]]>
</script>';

		return $html;
	}

	private function _appendHtml() {
		$html = '<script type="text/javascript">	
			var checkBoxes = $H();
        </script>';

		return $html;
	}

}
