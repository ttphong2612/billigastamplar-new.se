<?php

/**
 * @category    DBIZ
 * @package     DBIZ Stamp Home Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net>
 * @modified    Tan Pham
 */
class Dbiz_StamphomeProducts_Block_Adminhtml_Edit_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

	protected $_values;

	/**
	 * Renders grid column
	 *
	 * @param   Varien_Object $row
	 * @return  string
	 */
	public function render(Varien_Object $row) {

		$this->_values = Mage::getModel('catalog/product_status')->getOptionArray();

		$html = $this->_values[$row->getData($this->getColumn()->getIndex())];

		return $html;
	}

}
