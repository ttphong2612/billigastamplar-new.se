<?php

/**
 * @category    DBIZ
 * @package     DBIZ Stamp Home Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net>
 * @modified    Tan Pham
 */
class Dbiz_StamphomeProducts_Block_Listing extends Mage_Catalog_Block_Product_Abstract {
	/*
	 * Check sort option and limits set in System->Configuration and apply them
	 * Additionally, set template to block so call from CMS will look like {{block type="stamphomeproducts/listing"}}
	 */

	public function __construct() {

		$this->setTemplate('dbiz/stamphomeproducts/block_stamphome_products.phtml');
		$this->setLimit((int) Mage::getStoreConfig("stamphomeproducts/cmspage/number_of_items"));
		$sort_by = Mage::getStoreConfig("stamphomeproducts/cmspage/product_sort_by");
		$this->setItemsPerRow((int) Mage::getStoreConfig("stamphomeproducts/cmspage/number_of_items_per_row"));

		switch ($sort_by) {
			case 0:
				$this->setSortBy("rand()");
				break;
			case 1:
				$this->setSortBy("created_at desc");
				break;
			case 2:
				$this->setSortBy("position asc");
				break;
			default:
				$this->setSortBy("rand()");
		}
	}

	/*
	 * Load stamphome products collection
	 */

	protected function _beforeToHtml() {
		$collection = Mage::getResourceModel('catalog/product_collection');

		$attributes = Mage::getSingleton('catalog/config')
			->getProductAttributes();

		$collection->addAttributeToSelect($attributes)
			->addMinimalPrice()
			->addFinalPrice()
			->addTaxPercents()
			->addAttributeToFilter('is_stamphome', 1, 'left')
			->addStoreFilter()
			->getSelect()->order($this->getSortBy())->limit($this->getLimit());

		$collection->joinField(
			'position', 'stamphomeproducts/product_position', 'position', 'product_id=entity_id'
		);

		$collection->getSelect()->order($this->getSortBy())->limit($this->getLimit());
		
		Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($collection);
	//	Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

		$this->_productCollection = $collection;

		$this->setProductCollection($collection);
		return parent::_beforeToHtml();
	}

	protected function _toHtml() {

		if (!$this->helper('stamphomeproducts')->getIsActive()) {
			return '';
		}

		return parent::_toHtml();
	}

	/*
	 * Return label for CMS block output
	 * */

	protected function getBlockLabel() {
		return $this->helper('stamphomeproducts')->getCmsBlockLabel();
	}

}
