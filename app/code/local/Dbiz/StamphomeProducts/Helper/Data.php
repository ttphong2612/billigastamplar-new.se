<?php

/**
 * @category    DBIZ
 * @package     DBIZ Stamp Home Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net>
 * @modified    Tan Pham
 */
class Dbiz_StamphomeProducts_Helper_Data extends Mage_Core_Helper_Abstract {

	const PATH_PAGE_HEADING = 'stamphomeproducts/standalone/heading';
	const PATH_CMS_HEADING = 'stamphomeproducts/cmspage/heading_block';
	const DEFAULT_LABEL = 'Stamp Products';

	public function getCmsBlockLabel() {
		$configValue = Mage::getStoreConfig(self::PATH_CMS_HEADING);
		return strlen($configValue) > 0 ? $configValue : self::DEFAULT_LABEL;
	}

	public function getPageLabel() {
		$configValue = Mage::getStoreConfig(self::PATH_PAGE_HEADING);
		return strlen($configValue) > 0 ? $configValue : self::DEFAULT_LABEL;
	}

	public function getIsActive() {
		return (bool) Mage::getStoreConfig('stamphomeproducts/general/active');
	}

}
