<?php

/**
 * @category    DBIZ
 * @package     DBIZ Stamp Home Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net>
 * @modified    Tan Pham
 */
class Dbiz_StamphomeProducts_Model_Observer {

	public function afterSaveProduct($observer) {
		$product = $observer->getProduct();
		if (!$product->getData('dbiz_stamphome_product_position')) {
			$product->setData('dbiz_stamphome_product_position', 0);
		}

		Mage::getResourceSingleton('stamphomeproducts/product_position')->saveProductPosition($product);
	}

}
