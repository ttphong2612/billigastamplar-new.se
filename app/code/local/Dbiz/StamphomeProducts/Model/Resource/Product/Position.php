<?php

/**
 * @category    DBIZ
 * @package     DBIZ Stamp Home Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net>
 * @modified    Tan Pham
 */
class Dbiz_StamphomeProducts_Model_Resource_Product_Position extends Mage_Core_Model_Resource_Db_Abstract {

	protected function _construct() {
		$this->_init('stamphomeproducts/product_position', null);
	}

	public function saveProductPosition($product) {
		if (!$product->getId()) {
			return $this;
		}

		$deleteCondition = array('product_id=?' => $product->getId());
		$this->_getWriteAdapter()->delete($this->getMainTable(), $deleteCondition);

		$data = array(
			'product_id' => $product->getId(),
			'position' => $product->getDbizStamphomeProductPosition()
		);

		$this->_getWriteAdapter()->insert($this->getMainTable(), $data);

		return $this;
	}

}
