<?php

/**
 * @category    DBIZ
 * @package     DBIZ Stamp Home Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net>
 * @modified    Tan Pham
 */
class Dbiz_StamphomeProducts_Model_System_Config_Source_Sort {
	/*
	 * Prepare data for System->Configuration dropdown
	 */

	public function toOptionArray() {
		return array(
			0 => Mage::helper('adminhtml')->__('Random'),
			1 => Mage::helper('adminhtml')->__('Last Added'),
			2 => Mage::helper('adminhtml')->__('Position'),
		);
	}

}
