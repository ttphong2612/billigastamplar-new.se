<?php

/**
 * @category    DBIZ
 * @package     DBIZ Stamp Home Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net>
 * @modified    Tan Pham
 */
class Dbiz_StamphomeProducts_IndexController extends Mage_Core_Controller_Front_Action {
	/*
	 * Check settings set in System->Configuration and apply them for stamphome-products page
	 * */

	public function indexAction() {

		if (!Mage::helper('stamphomeproducts')->getIsActive()) {
			$this->_forward('noRoute');
			return;
		}

		$template = Mage::getConfig()->getNode('global/page/layouts/' . Mage::getStoreConfig("stamphomeproducts/standalone/layout") . '/template');

		$this->loadLayout();

		$this->getLayout()->getBlock('root')->setTemplate($template);
		$this->getLayout()->getBlock('head')->setTitle($this->__(Mage::getStoreConfig("stamphomeproducts/standalone/meta_title")));
		$this->getLayout()->getBlock('head')->setDescription($this->__(Mage::getStoreConfig("stamphomeproducts/standalone/meta_description")));
		$this->getLayout()->getBlock('head')->setKeywords($this->__(Mage::getStoreConfig("stamphomeproducts/standalone/meta_keywords")));

		$breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs');
		$breadcrumbsBlock->addCrumb('stamphome_products', array(
			'label' => Mage::helper('stamphomeproducts')->__(Mage::helper('stamphomeproducts')->getPageLabel()),
			'title' => Mage::helper('stamphomeproducts')->__(Mage::helper('stamphomeproducts')->getPageLabel()),
		));

		$this->renderLayout();
	}

}
