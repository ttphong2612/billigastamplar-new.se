<?php
/**
 * @category    DBIZ
 * @package     DBIZ Stamp Home Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net>
 * @modified    Tan Pham
 */

$installer = $this;
/* @var $installer Mage_Eav_Model_Entity_Setup */

$installer->startSetup();


$installer->addAttribute('catalog_product', 'is_stamphome', array(
        'group'             => 'General',
        'type'              => 'int',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Is Product Min Price',
        'input'             => 'boolean',
        'class'             => '',
        'source'            => '',
        'is_global', Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'default'           => '0',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'apply_to'          => 'simple,configurable,virtual,bundle,downloadable',
        'is_configurable'   => false,
        'used_in_product_listing', '1'
    ));

$installer->updateAttribute('catalog_product', 'is_stamphome', 'used_in_product_listing', '1');
$installer->updateAttribute('catalog_product', 'is_stamphome', 'is_global', '0');

$installer->endSetup();