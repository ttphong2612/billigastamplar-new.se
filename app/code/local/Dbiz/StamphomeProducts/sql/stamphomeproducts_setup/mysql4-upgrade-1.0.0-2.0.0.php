<?php
/**
 * @category    DBIZ
 * @package     DBIZ Stamp Home Products
 * @author      Domagoj Potkoc, Inchoo Team <web@inchoo.net>
 * @modified    Tan Pham
 */

$installer = $this;
/* @var $installer Mage_Eav_Model_Entity_Setup */

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('stamphomeproducts/product_position'))
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 10, array('unsigned' => true,))
//    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_INTEGER, 2)
    ->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, 4);

$installer->getConnection()
    ->createTable($table);

$installer->endSetup();