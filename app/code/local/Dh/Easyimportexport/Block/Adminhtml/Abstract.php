<?php 
abstract class Dh_Easyimportexport_Block_Adminhtml_Abstract extends Mage_Adminhtml_Block_Template
{
	protected $_actions;
	protected $_currentAction;
	protected $_error=null;
	protected $_messages=null;
	protected $_validation_result=false;
	
	public function getCurrentAction(){
		if(!$this->_currentAction){
			if(Mage::getSingleton('core/session')->hasActionkey()){
				$model=Mage::getModel(Mage::getSingleton('core/session')->getActionkey());
				if($model instanceof Dh_Easyimportexport_Model_Action_Abstract){
					$this->_currentAction = $model;
				} else {
					Mage::getSingleton('core/session')->unsActionkey();
					return $this->getCurrentAction();
				}
			} else {
				if(!$this->_actions || !is_array($this->_actions)){
					$this->getAllActions();
				}
				if(Mage::app()->getRequest()->getParam('actionKey') && isset($this->_actions[Mage::app()->getRequest()->getParam('actionKey')])){
					$model=Mage::getModel(Mage::app()->getRequest()->getParam('actionKey'));
					if($model instanceof Dh_Easyimportexport_Model_Action_Abstract){
						$this->_currentAction = $model;
					} else {
						throw new Exception($this->__('Requested action is not valid.'));
					}
				} else {
					throw new Exception($this->__('No current action set.'));
				}
			}
		}
		return $this->_currentAction;
	}
	
	public function getStepsBlock(){
		return $this->getChild('easyimportexport_steps');
	}
	
	public function getCurrentStep(){
		return $this->getStepsBlock()->getCurrentStep();
	}
	
	public function setError($msg){
		$this->_error=$msg;
		if($this->getStepsBlock()->getCurrentStep()>1){
			$this->getStepsBlock()->setCurrentStep($this->getStepsBlock()->getCurrentStep()-1);
		}
	}
	
	public function getError(){
		return $this->_error;
	}
	
	public function addMessage($msg){
		if(!$this->_messages){
			$this->_messages=array();
		}
		if(is_array($msg)){
			foreach($msg as $_msg){
				$this->_messages[]=$_msg;
			}
		} else {
			$this->_messages[]=$msg;
		}
	}
	
	public function getMessages(){
		return $this->_messages;
	}
	
	public function fileIsValid(){
		return $this->_validation_result;
	}
	
	abstract public function init_action();
	
	abstract protected function getAllActions();
}
?>