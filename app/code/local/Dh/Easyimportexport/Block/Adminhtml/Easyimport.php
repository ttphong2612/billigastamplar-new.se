<?php 
class Dh_Easyimportexport_Block_Adminhtml_Easyimport extends Dh_Easyimportexport_Block_Adminhtml_Abstract
{
	public function init_action(){
		try{
			switch($this->getCurrentStep()){
				case 1:
					try{
						$currentAction=$this->getCurrentAction();
						$currentAction->setParam('all_attr', false, true);
						$currentAction->setParam('all_resources_attr', false, true);
						$currentAction->setParam('all_required_attr', false, true);
					} catch(Exception $e){}
				break;
				case 2:
					if(Mage::app()->getRequest()->getParam('actionKey')){
						Mage::getSingleton('core/session')->setActionkey(Mage::app()->getRequest()->getParam('actionKey'));
					}
					$this->getCurrentAction()->step2();
				break;
				
				case 3:
					$path = Mage::getBaseDir('var').DS.'import'.DS;
					$currentAction=$this->getCurrentAction();
					$request=Mage::app()->getRequest();
					//var_dump($request->getParam('existing_file'));
					if ($request->getParam('existing_file')===0 || $request->getParam('existing_file')==='0'){
						if(isset($_FILES['file_upload']['name']) && $_FILES['file_upload']['name'] != '') {
				            $uploader = new Varien_File_Uploader('file_upload');
				            $uploader->save($path, $_FILES['file_upload']['name']);
				            Mage::unregister('easyimportexport_file');
				            Mage::register('easyimportexport_file', $path.$_FILES['file_upload']['name']);
						} else {
							throw new Exception($this->__('No file sent'));
						}
		            } else if($request->getParam('existing_file') && $request->getParam('file_choose')) {
		            	Mage::unregister('easyimportexport_file');
			            Mage::register('easyimportexport_file', $path.$request->getParam('file_choose'));
		            } else if($this->getCurrentAction()->getParam('import_file')){
		            	Mage::unregister('easyimportexport_file');
			            Mage::register('easyimportexport_file', $currentAction->getParam('import_file'));
		            } else {
		            	throw new Exception($this->__('No file choosed'));
		            }
					$currentAction->setParam('import_file', Mage::registry('easyimportexport_file'));
		            $messages=array();
		            $currentAction->setImportDatas(array());
		            $currentAction->setParam('treated_skus', array(), false);
		            $currentAction->setParam('missing_fields_to_fill', array(), false);

		            $this->getCurrentAction()->step3();
		        break;
		        
				case 4:
					$this->getCurrentAction()->step4();
				break;
				
				case 5:
					$this->getCurrentAction()->step5();
				break;
			}
		} catch(Exception $e){
			$this->setError($e->getMessage());
		}
	}
	public function getAllActions(){
		if(!$this->_actions){
			$this->_actions=array(	'easyimportexport/action_import_updateprice'	=>	$this->__('Update prices'),
									'easyimportexport/action_import_updatecustom'	=>	$this->__('Update custom fields'),
									'easyimportexport/action_import_updatemedia'	=>	$this->__('Update images'),
									'easyimportexport/action_import_updatestock'	=>	$this->__('Update stocks'),
									'easyimportexport/action_import_updatelinks'	=>	$this->__('Update links'),
									'easyimportexport/action_import_updatetiers'	=> 	$this->__('Update tier prices'),
									'easyimportexport/action_import_addproduct'		=>	$this->__('Add new products')
			);
		}
		return $this->_actions;
	}
	protected $_filesList;
	public function getImportFilesList(){
		if(!isset($this->_filesList)){
			$dir = opendir(Mage::getBaseDir('var').DS.'import'.DS);
			$this->_filesList=array();
			while($file = readdir($dir)) {
				if($file != '.' && $file != '..' && !is_dir($dirname.$file))
				{
					$this->_filesList[]=$file;
				}
			}
			closedir($dir);
			sort($this->_filesList);
		}
		return $this->_filesList;
	}
}
?>