<?php 
abstract class Dh_Easyimportexport_Model_Action_Abstract extends Mage_Core_Model_Abstract
{
	const CONFIG_FILE_PATH='var/config/easyimportexport.config';
	const DATE_FORMAT='YYYY-MM-dd HH:mm:ss';
	const STATUS_SUCCESS=1;
	const STATUS_FATAL=-2;
	const STATUS_ERROR=-1;
	const STATUS_END=0;
	const STATUS_WARNING=-3;
	protected $_attributes;
	protected $_values;
	protected $_validHelper;
	protected $_image_fields=array('image', 'thumbnail', 'small_image');
	protected $_errors=array();
	protected $_warnings=array();
	protected $_wrongFields=array();
	protected $_status=Dh_Easyimportexport_Model_Action_Abstract::STATUS_SUCCESS;
	protected $_log_file;

	public function __construct(){
		$this->_validHelper=Mage::helper('easyimportexport/validation');
		if(!is_dir('var/config')){
			mkdir('var/config');
		}
		parent::__construct();
	}
	/**
	 * 
	 * Retrieve the informations to display if this action is selected.
	 */
	public function getAdditionalInfos(){
		return 'Required fields are : '.implode($this->getRequiredFields(), ', ').'.';
	}
	/**
	 * 
	 * Generate html field for an attribute
	 * @param string $name
	 * @param int $attributeId
	 * @param string $input_type
	 * @param mixed $value
	 * @param Mage_Catalog_Model_Resource_Eav_Attribute $resource
	 * @param bool $readOnly
	 * @return string
	 */
	public function renderField($name, $attributeId, $input_type='text', $value='', $resource=false, $readOnly=false, $attributeCode)
    {
    	$field_html='';
    	if(is_array($value)){
    		$values=implode(',', $value);
    	}
		if($input_type=='textarea'){
			$field_html.='<textarea rows="3" name="'.$name.'">'.(is_array($value) ? $values : $value).'</textarea>';
		} else if($input_type=='select' || $input_type=='multiselect' || ($resource && $resource->usesSource())){
			$options=Mage::getModel('catalog/product_attribute_api')->options($attributeId);
			$field_html.='<select name="'.$name.'" ';
			if($input_type=='multiselect'){
				$field_html.='multiple="multiple"';
			}
			if($readOnly){
				$field_html.=' readonly="readonly" ';
			}
			$field_html.='>';
			foreach ($options as $option) {
				$field_html.='<option value="'.$option['label'].'"';
				if((is_array($value) && in_array($option['label'], $value)) || $value==$option['label']){
					$field_html.=' selected="selected" ';
				}
				$field_html.='>'.$option['label'].'</option>';
			}
			$field_html.='</select>';
		} else if($attributeCode=='attribute_set'){
			if(empty($value)){
				$value='Default';
			}
			$field_html.='<select name="'.$name.'" ';
			if($readOnly){
				$field_html.=' readonly="readonly" ';
			}
			$field_html.='>';
			$attributeSets=Mage::getModel('catalog/product_attribute_set_api')->items();
			foreach($attributeSets as $attributeSet){
				$field_html.='<option value="'.$attributeSet['name'].'"';
				if($value==$attributeSet['name']){
					$field_html.=' selected="selected" ';
				}
				$field_html.='>'.$attributeSet['name'].'</option>';
			}
			$field_html.='</select>';
		} else if($attributeCode=='type'){
			if(empty($value)){
				$value='Simple Product';
			}
			$field_html.='<select name="'.$name.'" ';
			if($readOnly){
				$field_html.=' readonly="readonly" ';
			}
			$field_html.='>';
			$types=Mage::getModel('catalog/product_type_api')->items();
			foreach($types as $type){
				$field_html.='<option value="'.$type['label'].'"';
				if($value==$type['label']){
					$field_html.=' selected="selected" ';
				}
				$field_html.='>'.$type['label'].'</option>';
			}
			$field_html.='</select>';
		} else {
			$field_html.='<input type="text" name="'.$name.'" ';
			if($readOnly){
				$field_html.=' readonly="readonly" ';
			} else if(!empty($value)){
				$field_html.=' value="'.(is_array($value) ? $values : $value).'" ';
			}
			$field_html.='/>';
		}
		return $field_html;
    }
	/**
	 * 
	 * Set the value of one attribute in the _values array.
	 * @param string $code
	 * @param mixed $value
	 */
	protected function setAttr($code, $value){
    	$key=array_search($code, $this->_attributes);
    	if($key!==false){
    		$this->_values[$key]=$value;
    	} else {
    		$this->_attributes[]=$code;
    		$this->_values[]=$value;
    	}
    }
    /**
     * 
     * Retrieve the value of one attribute in the _values array.
     * @param string $attr
     */
    protected function getAttributeByName($attr){
    	$key=array_search($attr, $this->_attributes);
    	if($key!==false){
    		return $this->_values[$key];
    	} else {
    		/*var_dump($this->_attributes);
    		var_dump($attr);
    		var_dump($key);
    		var_dump($this->_values);*/
    		return null;
    	}
    }
    /**
     * 
     * Actions to run on step 2.
     */
    public function step2(){}
    /**
     * 
     * Actions to run on step 3.
     */
	public function step3(){@unlink($this->getLogFilename());}
	/**
     * 
     * Actions to run on step 4.
     */
	public function step4(){}
	/**
     * 
     * Actions to run on step 5.
     */
	public function step5(){@unlink($this->getLogFilename());}
    /**
     * 
     * Evaluate the validity of one line of the import file.
     * @param int $line
     */
    abstract public function validate($line);
	/**
	 * 
	 * Retrieve fields which have to be processed.
	 */
	abstract public function getRequiredFields();
    /**
     * 
     * Process one line of the datas.
     * @param int $line
     */
    abstract public function import($line);
    /**
     * 
     * Process media's datas.
     * @param Mage_Catalog_Model_Product $product
     * @param array[string]=mixed $datas
     */
    protected function importImages($product, $datas){
		$mediaSet = false;
		foreach ( $this->_image_fields as $mediaAttributeCode ) {
			if (isset ( $datas [$mediaAttributeCode] )) {
				$mediaSet = true;
				break;
			}
		}
		if ($mediaSet) {
			$mediaGalleryBackendModel = Mage::getModel ( 'catalog/resource_eav_attribute' )->loadByCode ( $product->getEntityTypeId (), 'media_gallery' )->getBackend ();
			$arrayToMassAdd = array ();
			foreach ( $this->_image_fields as $mediaAttributeCode ) {
				if (isset ( $datas [$mediaAttributeCode] )) {
					$file = trim ( $datas [$mediaAttributeCode] );
					$oldMd5 = false;
					$newMd5 = true;
					if ($product->getData ( $mediaAttributeCode ) != 'no_selection') {
						$oldMd5 = md5 ( file_get_contents ( Mage::getBaseDir ( 'media' ) . DS . 'catalog' . DS . 'product' . $product->getData ( $mediaAttributeCode ) ) );
						$newMd5 = md5 ( file_get_contents ( Mage::getBaseDir ( 'media' ) . DS . 'import' . DS . $datas [$mediaAttributeCode] ) );
					}
					if (! empty ( $file ) && ! $mediaGalleryBackendModel->getImage ( $product, $file ) && $oldMd5 != $newMd5) {
						$arrayToMassAdd [] = array ('file' => trim ( $file ), 'mediaAttribute' => $mediaAttributeCode );
					}
				}
			}
			try {
				$addedFilesCorrespondence = $mediaGalleryBackendModel->addImagesWithDifferentMediaAttributes ( $product, $arrayToMassAdd, Mage::getBaseDir ( 'media' ) . DS . 'import' . DS, false, false );
			} catch ( Exception $e ) {
				$this->addError('Warning ! Failed to save images. ' . $e->getMessage ());
				return;
			}
			foreach ( $this->_image_fields as $mediaAttributeCode ) {
				$addedFile = '';
				if (isset ( $datas [$mediaAttributeCode . '_label'] )) {
					$fileLabel = trim ( $datas [$mediaAttributeCode . '_label'] );
					if (isset ( $datas [$mediaAttributeCode] )) {
						$keyInAddedFile = array_search ( $datas [$mediaAttributeCode], $addedFilesCorrespondence ['alreadyAddedFiles'] );
						if ($keyInAddedFile !== false) {
							$addedFile = $addedFilesCorrespondence ['alreadyAddedFilesNames'] [$keyInAddedFile];
						}
					}
					
					if (! $addedFile) {
						$addedFile = $product->getData ( $mediaAttributeCode );
					}
					if ($fileLabel && $addedFile) {
						$mediaGalleryBackendModel->updateImage ( $product, $addedFile, array ('label' => $fileLabel ) );
					}
				}
			}
			$product->save ();
		}
	}
	/**
	 * 
	 * Retrieve datas to process during the import.
	 */
	public function getImportDatas(){
		return Mage::getSingleton('core/session')->getImportdatas();
	}
	/**
	 * 
	 * Set datas to process during the import.
	 * @param array[int]=array[string]=mixed $datas
	 * @throws Exception
	 */
	public function setImportDatas($datas){
		if(is_array($datas)){
			Mage::getSingleton('core/session')->setNbimportdatas(count($datas));
			return Mage::getSingleton('core/session')->setImportdatas($datas);
		} else {
			throw new Exception('Invalid import datas.');
		}
	}
	/**
	 * 
	 * Set the summary of the import
	 * @param unknown_type $summary
	 */
	public function setSummary($summary){
		return Mage::getSingleton('core/session')->setEasyimportexportsum($summary);
	}
	/**
	 * 
	 * Retrieve the summary of the import
	 */
	public function getSummary(){
		return Mage::getSingleton('core/session')->getEasyimportexportsum();
	}
	/**
	 * 
	 * Keep a parameter in session
	 * @param string $code
	 * @param mixed $value
	 * @param bool $persistent if true, datas will also be keep in a file
	 */
	static public function setParam($code, $value, $persistent=true){
		Mage::getSingleton('core/session')->setData('Easyimport_export_param_'.$code, $value);
		if($persistent){
			$config=json_decode(file_get_contents(self::CONFIG_FILE_PATH), true);
			
			$config[$code]=$value;
			$config_file=fopen(Dh_Easyimportexport_Model_Action_Abstract::CONFIG_FILE_PATH, 'w');
			fwrite($config_file, json_encode($config));
			fclose($config_file);
		}
	}
	/**
	 * 
	 * Retrieve the value of a parameter set with setParam
	 * @param unknown_type $code
	 */
	static public function getParam($code){
		if(Mage::getSingleton('core/session')->hasData('Easyimport_export_param_'.$code)){
			return Mage::getSingleton('core/session')->getData('Easyimport_export_param_'.$code);
		} else {
			$config=json_decode(file_get_contents(self::CONFIG_FILE_PATH), true);
			if(isset($config[$code])){
				return $config[$code];
			} else {
				return null;
			}
		}
	}
	/**
	 * 
	 * Log error
	 * @param string $msg
	 */
	protected function addError($msg){
		$this->_errors[]=$msg;
		if($this->_status==self::STATUS_SUCCESS || $this->_status==self::STATUS_WARNING){
			$this->_status=self::STATUS_ERROR;
		}
	}
	/**
	 * 
	 * Retrieve all errors logged.
	 * @param int $line
	 */
	public function getErrorsString($line){
		if(count($this->_errors)){
			if(!$this->_log_file){
				$this->_log_file=fopen($this->getLogFilename(), 'a');
			}
			fwrite($this->_log_file, 'Line '.$line.' : '.implode("\n".'Line '.$line.' : ', $this->_errors)."\n");
			return '<p class="error">Line '.$line.' : '.implode('</p><p class="error">Line '.$line.' : ', $this->_errors).'</p>';
		} else {
			return '';
		}
	}
	/**
	 * 
	 * Log fields with value to change
	 * @param string $msg
	 */
	protected function addWrongField($name, $attributeId, $attributeCode, $input_type='text', $value='', $resource=false){
		if(!isset($this->_wrongFields[$attributeId])){
			$this->_wrongFields[$attributeId]=$attributeCode.' : '.$this->renderField($name, $attributeId, $input_type, $value, $resource);
		}
	}
	/**
	 * 
	 * Retrieve all errors logged.
	 * @param int $line
	 */
	public function getWrongFieldsString($line){
		if(count($this->_wrongFields)){
			return '<p class="quickfix" id="quickfix_'.$line.'"><a onclick="$jQuery(\'#quickfix_'.$line.' span\').toggle();return false;" href="#">Quick fix the line '.$line.' : </a><br /><span>'.implode('<br />', $this->_wrongFields).'</span></p>';
		} else {
			return '';
		}
	}
	/**
	 * 
	 * Log warning
	 * @param string $msg
	 */
	protected function addWarning($msg){
		$this->_warnings[]=$msg;
		if($this->_status==self::STATUS_SUCCESS){
			$this->_status=self::STATUS_WARNING;
		}
	}
	/**
	 * 
	 * Retrieve all warnings logged.
	 * @param int $line
	 */
	public function getWarningsString($line){
		if(count($this->_warnings)){
			if(!$this->_log_file){
				$this->_log_file=fopen($this->getLogFilename(), 'a');
			}
			fwrite($this->_log_file, 'Line '.$line.' : Warning ! '.implode("\n".'Line '.$line.' : Warning ! ', $this->_warnings)."\n");
			return '<p class="warning">Line '.$line.' : Warning ! '.implode('</p><p class="warning">Line '.$line.' : Warning ! ', $this->_warnings).'</p>';
		} else {
			return '';
		}
	}
	/**
	 * 
	 * Retrieve the name of the log file
	 */
	public function getLogFilename(){
		return 'var/tmp/easyimportexport_error_log.txt';
	}
	/**
	 * 
	 * Retrieve a line of datas from the given file
	 * @param int $line
	 * @param resource $file
	 * @param int $currentLine
	 */
	protected function fetchLine($line, &$file, $currentLine=2){
		for($i=$currentLine;$i<$line;$i++){
			$this->_values=fgetcsv($file);
		}
		return fgetcsv($file);
	}
	/**
	 * 
	 * Retrieve all the stores name and id
	 */
	protected  function getStoresInfos() {
		$collection=Mage::getModel('core/store')->getCollection();
		$stores_codes=array();
		foreach ($collection as $store){
			$stores_codes[]=array('name'=>$store->getName(), 'id'=>$store->getId(), 'code'=>$store->getCode());
		}
		$stores_codes[]=array('name'=>'Default Values', 'id'=>null);
		return $stores_codes;
	}
	
	protected function processCategoryPath(&$rowDatas){
		/*---------- <Category path handling> ----------*/
		$session=Mage::getSingleton('core/session');
		$path_cache=$session->hasPatchcache() ? $session->getPatchcache() : array();
		if(isset($rowDatas['category_path_by_names']) && !empty($rowDatas['category_path_by_names'])){
			$category_paths=explode(',', $rowDatas['category_path_by_names']);
			unset($rowDatas['category_path_by_names']);
			$rowDatas['categories']=array();
			$cat_resource=Mage::getModel('catalog/resource_eav_mysql4_category');
			foreach($category_paths as $path){
				if(isset($path_cache[$path])){
					$rowDatas['categories'][]=$path_cache[$path];
				} else {
					$complete_path=$path;
					$path=explode('|', $path);
					$level=1;
					$parent_id=null;
					$current_path_ids='1';
					$current_path_names='';
					foreach($path as $cat_name){
						
						if($parent_id){$current_path_ids.='/'.$parent_id;}//we keep the current path_ids in order to be able to create the missing categories if needed (see below)
						if(!empty($current_path_names)){$current_path_names.='|';}
						$current_path_names.=$cat_name;
						
						if(isset($path_cache[$current_path_names])){
							$found_cat=Mage::getModel('catalog/category')->load($path_cache[$current_path_names]);
						} else {
							//selection of the category by its name, level, and parent_id (if it's not the first one (level 1))
						    $categories=Mage::getModel('catalog/category')->getCollection();
						    if($cat_name!='Default Category' || $level>1){
						    	$categories->addAttributeToFilter('name', $cat_name);
						    }
						    $categories->addAttributeToFilter('level', $level);
						    if($parent_id){
						    	$categories->addAttributeToFilter('path', array('like' => $current_path_ids.'%'));
						    }
						    if($categories->count()){
						    	$found_cat=$categories->getFirstItem();
						    } else {
						    	$found_cat=false;
						    }
						}
					    if($found_cat){ //we found our category
				    		if($level>=count($path)){
				    			//if it's the last segment of the path, we add the product to the category...
				    			$rowDatas['categories'][]=$found_cat->getId();
				    			$path_cache[$complete_path]=$found_cat->getId();
				    		} else {
				    			//else we continue
				    			$path_cache[$current_path_names]=$found_cat->getId();
				    			$parent_id=$found_cat->getId();
				    		}
					    } else {
					    	//creation of the missing category
					    	$new_cat= Mage::getModel('catalog/category');
					    	$cat_datas=array('name'=>$cat_name, 'attribute_set_id'=> 3, 'is_active'=>1, 'include_in_menu' => 0, 'parent_id' => $parent_id);
					    	$new_cat->setData($cat_datas);
					    	$new_cat->save();
					    	/*//we set the path with the current path_ids
					    	$new_cat->setPath($current_path_ids.'/'.$new_cat->getId());
					    	$new_cat->setLevel($level);
					    	$new_cat->save();*/
					    	$cat_resource->changeParent($new_cat, Mage::getModel('catalog/category')->load($parent_id));
					    	if($level>=count($path)){
					    		$rowDatas['categories'][]=$new_cat->getId();
					    		$path_cache[$complete_path]=$new_cat->getId();
					    	} else {
					    		$parent_id=$new_cat->getId();
					    		$path_cache[$current_path_names]=$new_cat->getId();
					    	}
					    }
					    $level++;
					}
				}
			}
			$session->setPathcache($path_cache);
		}
		/*---------- </Category path handling> ----------*/
	}
}
?>