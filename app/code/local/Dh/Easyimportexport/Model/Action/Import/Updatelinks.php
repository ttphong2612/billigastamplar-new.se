<?php 
class Dh_Easyimportexport_Model_Action_Import_Updatelinks extends Dh_Easyimportexport_Model_Action_Abstract
{
	public function validate($line){
		if(!is_array($messages)){
			$messages = array();
		}
		$file=fopen($this->getParam('import_file'), 'r');
		$this->_attributes = fgetcsv($file);
		if($this->_attributes){
			$importdatas=is_array($this->getImportDatas()) ? $this->getImportDatas() : array();
			$treatedSkus=is_array($this->getParam('treated_skus')) ? $this->getParam('treated_skus') : array();
			$this->_values=$this->fetchLine($line, $file);
			if(!$this->_values){
				if(feof($file)){
					return self::STATUS_END;
				} else {
					$this->addError('Corrupted datas.');
					return self::STATUS_FATAL;
				}
			}
			$sku=$this->getAttributeByName('sku');
			$skus['related_products_sku']=$this->getAttributeByName('related_products_sku');
			$skus['crosssell_products_sku']=$this->getAttributeByName('crosssell_products_sku');
			$skus['upsell_products_sku']=$this->getAttributeByName('upsell_products_sku');
			if(in_array($sku, $treatedSkus)){
				$this->addWarning('Duplicated sku "'.$sku.'".');
			} else {
				$treatedSkus[]=$sku;
			}
			foreach ($skus as $code=>$values){
				if($values){
					$values=explode(',', $values);
					if(count($values)){
						$i=1;
						foreach($values as $value){
							if($sku==$value){
								$this->addWarning('A product cannot be linked to itself. ('.$code.' column, value '.$i.')');
							} else {
								if(!$this->_validHelper->skuExistsCheck($value)){
									$this->addWarning('Unknown linked sku "'.$value.'". ('.$code.' column, value '.$i.')');
								} else {
									$importdatas[$line-2][$code][]=$value;
								}
							}
							$i++;
						}
					}
				}
			}
			if(count($importdatas[$line-2])){
				$product=Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
				if(!$product){
					$this->addError('Unknown sku "'.$sku.'".');
				} else {
					$importdatas[$line-2]['id']=$product->getId();
				}
			} else {
				$this->addError('No valid datas found.');
			}
			$this->setParam('treated_skus', $treatedSkus, false);
		} else {
			$this->addError('Incorrect file.');
		}
		if($this->_status==self::STATUS_SUCCESS || $this->_status==self::STATUS_WARNING){
			$this->setImportdatas($importdatas);
			$this->setSummary(count($importdatas).' product\'s links will be updated.');
		}
		return $this->_status;
	}
	
	public function getRequiredFields(){
		return array('sku', 'related_products_sku', 'crosssell_products_sku', 'upsell_products_sku');
	}
	
	public function import($lineNb){
		$datas=$this->getImportdatas();
		for($i=0;$i<$lineNb;$i++){
			next($datas);
		}
		$lineNb=key($datas);
		if(isset($datas[$lineNb])){
			$product=Mage::getModel('catalog/product')->load($datas[$lineNb]['id']);
			if($product){
				if(count($datas[$lineNb]['related_products_sku'])){
					$link_array=array();
					foreach($datas[$lineNb]['related_products_sku'] as $related_sku){
						$product2link=Mage::getModel('catalog/product')->loadByAttribute('sku', $related_sku);
						$link_array[$product2link->getId()]=array('position'=> 0);
					}
					$product->setRelatedLinkData($link_array);
				}
				if(count($datas[$lineNb]['crosssell_products_sku'])){
					$link_array=array();
					foreach($datas[$lineNb]['crosssell_products_sku'] as $related_sku){
						$product2link=Mage::getModel('catalog/product')->loadByAttribute('sku', $related_sku);
						$link_array[$product2link->getId()]=array('position'=> 0);
					}
					$product->setCrossSellLinkData($link_array);
				}
				if(count($datas[$lineNb]['upsell_products_sku'])){
					$link_array=array();
					foreach($datas[$lineNb]['upsell_products_sku'] as $related_sku){
						$product2link=Mage::getModel('catalog/product')->loadByAttribute('sku', $related_sku);
						$link_array[$product2link->getId()]=array('position'=> 0);
					}
					$product->setUpSellLinkData($link_array);
				}
				$product->save();
			} else {
				$this->addError('Product not found.');
			}
		} else {
			$this->addError('Invalid datas.');
		}
		return $lineNb;
	}
	
	public function getAdditionalInfos(){
		$required_fields_array=$this->getRequiredFields();
		unset($required_fields_array[0]);
		return 'Required fields are : sku, and at least one of those : '.implode($required_fields_array, ', ').'.';
	}
}
?>