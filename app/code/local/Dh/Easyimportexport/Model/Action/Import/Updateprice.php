<?php 
class Dh_Easyimportexport_Model_Action_Import_Updateprice extends Dh_Easyimportexport_Model_Action_Abstract
{
	public function validate($line){
		$file=fopen($this->getParam('import_file'), 'r');
		$this->_attributes = fgetcsv($file);
		if($this->_attributes){
			$missing_fields=array_diff($this->getRequiredFields(), $this->_attributes);
			if(count($missing_fields)){
				foreach ($missing_fields as $missing_field){
					$this->addError('"'.$missing_field.'" field is missing.');
				}
				return self::STATUS_FATAL;
			} else {
				$importdatas=is_array($this->getImportDatas()) ? $this->getImportDatas() : array();
				$treatedSkus=is_array($this->getParam('treated_skus')) ? $this->getParam('treated_skus') : array();
				$this->_values=$this->fetchLine($line, $file);
				if(!$this->_values){
					if(feof($file)){
						return self::STATUS_END;
					} else {
						$this->addError('Corrupted datas.');
						return self::STATUS_FATAL;
					}
				}
				$sku=$this->getAttributeByName('sku');
				$price=$this->getAttributeByName('price');
				$websitesCode = $this->getAttributeByName('websites');
				
				if(in_array($sku, $treatedSkus)){
					$this->addError('Duplicated sku "'.$sku.'".');
				} else {
					$treatedSkus[]=$sku;
				}
				
				$product=Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
				
				if(!$product){
					$this->addError('Unknown sku "'.$sku.'".');
				} else {
					$lineArray =array('id'=>$product->getId(), 'price'=>$price);
					if($websitesCode){
						$websiteIds = $product->getWebsiteIds();
						$websiteId = Mage::getModel('core/website')->load($websitesCode)->getId();
						if(!in_array($websiteId, $websiteIds)){
							$this->addError('Unknown website code "'.$websitesCode.'".');
						}else{
							$lineArray['websites'] = $websiteId;
						}
					}
					$importdatas[$line-2]= $lineArray;
				}
				
				if(!($this->_validHelper->numberCheck($price) && $price>=0)){
					$this->addError('Price is not a valid number "'.$price.'".');
				}
				$this->setParam('treated_skus', $treatedSkus, false);
				$this->setParam('missing_fields_to_fill', $missing_fields_to_fill, false);
			}
		} else {
			$this->addError('Incorrect file.');
			return self::STATUS_FATAL;
		}
		if($this->_status==self::STATUS_SUCCESS || $this->_status==self::STATUS_WARNING){
			$this->setImportdatas($importdatas);
			$this->setSummary(count($importdatas).' product\'s prices will be updated.');
		}
		return $this->_status;
	}
	
	public function getRequiredFields(){
		return array('sku', 'price');
	}
	
	public function import($lineNb){
		$datas=$this->getImportdatas();
		for($i=0;$i<$lineNb;$i++){
			next($datas);
		}
		$lineNb=key($datas);
		
		if(isset($datas[$lineNb])){
			$product = Mage::getModel('catalog/product');
			if($datas[$lineNb]['websites']){
				$websiteObj = new Mage_Core_Model_Website();
				$websiteObj->load($datas[$lineNb]['websites']);
				$storeIds = $websiteObj->getStoreIds();
				if (count($storeIds)) {
					foreach ($storeIds as $_eachStoreId) {
						$product->setStoreId($_eachStoreId)
								->load($datas[$lineNb]['id']);
		 
						$oldPrice = $product->getPrice();
						if ($oldPrice != $datas[$lineNb]['price']) {
							$product->setPrice($datas[$lineNb]['price']);
							$product->save();
						}
					}
				}
			}else{
				$product=Mage::getModel('catalog/product')->load($datas[$lineNb]['id']);
				if($product){
					$product->setPrice($datas[$lineNb]['price'])->save();
				} else {
					$this->addError('Product not found.');
				}
			}
			
		} else {
			$this->addError('Invalid datas.');
		}
		return $lineNb;
	}
}
?>