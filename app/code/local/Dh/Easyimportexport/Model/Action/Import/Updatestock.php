<?php 
class Dh_Easyimportexport_Model_Action_Import_Updatestock extends Dh_Easyimportexport_Model_Action_Abstract
{
	//protected $_field_desc=array();
	public function validate($line){
		$file=fopen($this->getParam('import_file'), 'r');
		$this->_attributes = fgetcsv($file);
		if($this->_attributes){
			$importdatas=is_array($this->getImportDatas()) ? $this->getImportDatas() : array();
			$treatedSkus=is_array($this->getParam('treated_skus')) ? $this->getParam('treated_skus') : array();
			$this->_values=$this->fetchLine($line, $file);
			if(!$this->_values){
				if(feof($file)){
					return self::STATUS_END;
				} else {
					$this->addError('Corrupted datas.');
					return self::STATUS_FATAL;
				}
			}
			$sku=$this->getAttributeByName('sku');
			$qty=$this->getAttributeByName('qty');
			if(in_array($sku, $treatedSkus)){
				$this->addError('Duplicated sku "'.$sku.'".');
			} else {
				$treatedSkus[]=$sku;
			}
			
			$product=Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
			if(!$product){
				$this->addError('Unknown sku "'.$sku.'".');
			} else {
				/*$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
				var_dump($stockItem);*/
				$required_fields_array=$this->getRequiredFields();
				unset($required_fields_array[0]);
				foreach ($required_fields_array as $code){
					$attr=$this->getAttributeByName($code);
					if($attr!==false && $attr!==null && $attr!==''){
						$importdatas[$line-2][$code]=$attr;
					}
				}
				if(count($importdatas[$line-2])){
					$importdatas[$line-2]['id']=$product->getId();
				} else {
					$this->addError('No valid datas found.');
				}
			}
			$this->setParam('treated_skus', $treatedSkus, false);
		} else {
			$this->addError('Incorrect file.');
			return self::STATUS_FATAL;
		}
		if($this->_status==self::STATUS_SUCCESS || $this->_status==self::STATUS_WARNING){
			$this->setImportdatas($importdatas);
			$this->setSummary(count($importdatas).' product\'s prices will be updated.');
		}
		return $this->_status;
	}
	
	public function getRequiredFields(){
		//$this->_field_desc=array(null, null, Mage::helper('catalog')->__('Qty for Item\'s Status to become Out of Stock'), null, Mage::helper('catalog')->__('Qty Uses Decimals'), null,  null, Mage::helper('catalog')->__('Minimum Qty Allowed in Shopping Cart'), null, null,  null, null, null, null, null);
		return array('sku', 'qty', 'min_qty', 'use_config_min_qty', 'is_qty_decimal', 'backorders', 'use_config_backorders', 'min_sale_qty', 'use_config_min_sale_qty', 'max_sale_qty', 'use_config_max_sale_qty', 'is_in_stock', 'use_config_notify_stock_qty', 'manage_stock', 'use_config_manage_stock', 'stock_status_changed_automatically', 'type_id');
	}
	
	public function import($lineNb){
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		$datas=$this->getImportdatas();
		for($i=0;$i<$lineNb;$i++){
			next($datas);
		}
		$lineNb=key($datas);
		if(isset($datas[$lineNb])){
			$product=Mage::getModel('catalog/product')->load($datas[$lineNb]['id']);
			if($product){
				$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
				if($stockItem){
					$stock=$stockItem->getData();
					foreach($stock as $code=>$value){
						if(isset($datas[$lineNb][$code])){
							//var_dump($code.' '.$datas[$lineNb][$code].' '.$value);
							$stockItem->setData($code, $datas[$lineNb][$code]);
						}
					}
					$stockItem->save();//var_dump($stockItem->getData());
				} else {
					$this->addError('Cannot load stock item.');
				}
				
			} else {
				$this->addError('Product not found.');
			}
		} else {
			$this->addError('Invalid datas.');
		}
		return $lineNb;
	}
	
	public function getAdditionalInfos(){
		$required_fields_array=$this->getRequiredFields();
		unset($required_fields_array[0]);
		return 'Required fields are : sku, and at least one of those : <ul><li>'.implode($required_fields_array, '</li><li>').'</li></ul>';
	}
}
?>