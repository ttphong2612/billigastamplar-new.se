<?php

class Dh_Smtp_Model_Email extends Mage_Core_Model_Email {

    public function send() {
        if (Mage::getStoreConfigFlag('system/smtp/disable')) {
            return $this;
        }
		
        $mail = new Zend_Mail();

        if (strtolower($this->getType()) == 'html') {
            $mail->setBodyHtml($this->getBody());
        } else {
            $mail->setBodyText($this->getBody());
        }

        $mail->setFrom($this->getFromEmail(), $this->getFromName())
                ->addTo($this->getToEmail(), $this->getToName())->setSubject($this->getSubject());
        $ssl = Mage::getStoreConfig('smtp/params/smtp_ssl');
        if (Mage::getStoreConfig('smtp/params/smtp_enabled')) {
            $config = array('port' => Mage::getStoreConfig('smtp/params/smtp_port'));
            if (!empty($ssl)) {
                $config ['ssl'] = $ssl;
            }
            $username = Mage::getStoreConfig('smtp/params/smtp_username');
            $password = Mage::getStoreConfig('smtp/params/smtp_password');
            if (!empty($username) && !empty($password)) {
                $config ['auth'] = 'login';
                $config ['username'] = $username;
                $config ['password'] = $password;
            }
            $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
            try {
                $mail->send($transport);
            } catch (Exception $e) {
                Mage::logException($e);
                $logMail = new Zend_Mail();
                $logMail->setFrom($this->getFromEmail(), $this->getFromName());
                $logMail->addTo(Mage::getStoreConfig('smtp/params/smtp_logmail'), 'Admin');
                $logMail->setSubject('SMTP fail');
                $logMail->setBodyText('An error occured while sending a mail via smtp : ' . $e->getMessage());
                $logMail->send();
                $mail->send();
            }
        } else {
            $mail->send();
        }

        return $this;
    }

}

?>
