<?php
class Dh_Smtp_Model_Email_Template extends Mage_Core_Model_Email_Template {
	/**
     * Send mail to recipient
     *
     * @param   array|string       $email        E-mail(s)
     * @param   array|string|null  $name         receiver name(s)
     * @param   array              $variables    template variables
     * @return  boolean
     **/
    public function send($email, $name = null, array $variables = array())
    {
        if (!$this->isValidForSend()) {
            Mage::logException(new Exception('This letter cannot be sent.')); // translation is intentionally omitted
            return false;
        }
        
        $emails = array_values((array)$email);
        $names = is_array($name) ? $name : (array)$name;
        $names = array_values($names);
        foreach ($emails as $key => $email) {
            if (!isset($names[$key])) {
                $names[$key] = substr($email, 0, strpos($email, '@'));
            }
        }

        $variables['email'] = reset($emails);
        $variables['name'] = reset($names);

        ini_set('SMTP', Mage::getStoreConfig('system/smtp/host'));
        ini_set('smtp_port', Mage::getStoreConfig('system/smtp/port'));

        $mail = $this->getMail();

        $setReturnPath = Mage::getStoreConfig(self::XML_PATH_SENDING_SET_RETURN_PATH);
        switch ($setReturnPath) {
            case 1:
                $returnPathEmail = $this->getSenderEmail();
                break;
            case 2:
                $returnPathEmail = Mage::getStoreConfig(self::XML_PATH_SENDING_RETURN_PATH_EMAIL);
                break;
            default:
                $returnPathEmail = null;
                break;
        }

        if ($returnPathEmail !== null) {
            $mailTransport = new Zend_Mail_Transport_Sendmail("-f".$returnPathEmail);
            Zend_Mail::setDefaultTransport($mailTransport);
        }

        foreach ($emails as $key => $email) {
            $mail->addTo($email, '=?utf-8?B?' . base64_encode($names[$key]) . '?=');
        }

        $this->setUseAbsoluteLinks(true);
        $text = $this->getProcessedTemplate($variables, true);

        if($this->isPlain()) {
            $mail->setBodyText($text);
        } else {
            $mail->setBodyHTML($text);
        }

        $mail->setSubject('=?utf-8?B?' . base64_encode($this->getProcessedTemplateSubject($variables)) . '?=');
        $mail->setFrom($this->getSenderEmail(), $this->getSenderName());

        try {
        	if(Mage::getStoreConfig('smtp/params/smtp_enabled')){
        		$config = array('port' => Mage::getStoreConfig('smtp/params/smtp_port'));
        		$ssl=Mage::getStoreConfig('smtp/params/smtp_ssl');
		        if(!empty($ssl)){
		        	$config['ssl']=$ssl;
		        }
		        $username=Mage::getStoreConfig('smtp/params/smtp_username');
		        $password=Mage::getStoreConfig('smtp/params/smtp_password');
		        if(!empty($username) && !empty($password)){
		        	$config['auth']='login';
		        	$config['username']=$username;
		        	$config['password']=$password;
		        }
				$transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);
				try {
                                        
					$mail->send($transport);
                                        
                                        
				} catch(Exception $e){
                                        Mage::log($config);
                                        Mage::log($mail);
					Mage::logException($e);
					
                                        $logMail = new Zend_Mail();
					$logMail->setFrom($this->getFromEmail(), $this->getFromName());
					$logMail->addTo(Mage::getStoreConfig ( 'smtp/params/smtp_logmail' ), 'Admin');
					$logMail->setSubject(Mage::getStoreConfig ( 'general/store_information/name' ).' : Magento SMTP fail');
					$logMail->setBodyText('An error occured while sending a mail via smtp : '.$e->getMessage());
					$logMail->send();
					$mail->send();
				}
        	} else {
        		$mail->send();
        	}
            
            $this->_mail = null;
        }
        catch (Exception $e) {
            $this->_mail = null;
            Mage::logException($e);
            return false;
        }

        return true;
    }
}
?>