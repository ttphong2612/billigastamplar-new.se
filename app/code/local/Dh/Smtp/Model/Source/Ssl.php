<?php 
class Dh_Smtp_Model_Source_Ssl extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {
	public function getAllOptions(){
		return array(array('label'=>'TLS', 'value'=>'tls'), array('label'=>'SSL', 'value'=>'ssl'));
	}
	
	public function toOptionArray(){
		return $this->getAllOptions();
	}
}
?>