<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SmtpController
 *
 * @author Administrator
 */
class Dh_Smtp_SmtpController extends Mage_Adminhtml_Controller_action {
    
    public function testAction () {
        $email = Mage::getModel("core/email");
        $email->setSubject("Test connection email");
        $email->setBody("You have successfully configured your SMTP connection.");
        $email->setFrom(Mage::getStoreConfig('smtp/params/smtp_username',Mage::app()->getStore()));
        $email->setToName("Tester Magento Connection")
            ->setToEmail($this->getRequest()->getParam("to"));
        
        $email->send();
        
        echo json_encode(array("result"=>"ok"));
    }
}

?>
