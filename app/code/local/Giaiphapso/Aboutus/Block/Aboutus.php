<?php
/**
* Example View block
*
* @codepool   Local
* @category   Fido
* @package    Fido_Example
* @module     Example
*/
class Giaiphapso_Aboutus_Block_Aboutus extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }

     public function getAboutus()
     {
     	 return Mage::getModel('aboutus/aboutus')
            ->getCollection()
            ->addFieldToFilter('store_id', Mage::app()->getStore()->getId())
            ->addFieldToFilter("status", 1);
    }
}