<?php
class Giaiphapso_Aboutus_Block_Adminhtml_Aboutus extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_aboutus';
        $this->_blockGroup = 'aboutus';
        $this->_headerText = Mage::helper('aboutus')->__('Item Manager');
        $this->_addButtonLabel = Mage::helper('aboutus')->__('Add Item');
        parent::__construct();
    }
}