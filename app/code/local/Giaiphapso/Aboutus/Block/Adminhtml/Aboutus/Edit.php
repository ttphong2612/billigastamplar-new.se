<?php

    class Giaiphapso_Aboutus_Block_Adminhtml_Aboutus_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
    {

        public function __construct()
        {
            parent::__construct();

            $this->_objectId = 'id';
            $this->_blockGroup = 'aboutus';
            $this->_controller = 'adminhtml_aboutus';

            $this->_updateButton('save', 'label', Mage::helper('aboutus')->__('Save Item'));
            $this->_updateButton('delete', 'label', Mage::helper('aboutus')->__('Delete Item'));
        }

        public function getHeaderText()
        {
            if( Mage::registry('aboutus_data') && Mage::registry('aboutus_data')->getId() ) {
                return Mage::helper('aboutus')->__("Title : %s", $this->htmlEscape(Mage::registry('aboutus_data')->getTitle()));
            } else {
                return Mage::helper('aboutus')->__('Add Aboutus');
            }
        }
}