<?php

    class Giaiphapso_Aboutus_Block_Adminhtml_Aboutus_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
    {
        /**
        * Load Wysiwyg on demand and Prepare layout
        */
        protected function _prepareLayout()
        {
            parent::_prepareLayout();
            if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
                $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
                 $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
            }
        }

        protected function _prepareForm()
        {
            $form = new Varien_Data_Form();
            $this->setForm($form);
            $fieldset = $form->addFieldset('aboutus_form', array('legend'=>Mage::helper('aboutus')->__('Item information')));

            $fieldset->addField('title', 'text', array(
                'label'     => Mage::helper('aboutus')->__('Title'),
                'class'     => 'required-entry',
                'required'  => true,
                'name'      => 'title',
            ));

            $fieldset->addField('content', 'editor', array(
                'name'      => 'content',
                'label' => Mage::helper('aboutus')->__('Content'),
                'style' => 'width:700px; height:500px;',
                'config'    => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
                'wysiwyg' => true,
                'required' => true, 
                'style'    => 'height:150px;width:700px;',
            ));


            $fieldset->addField('status', 'select', array(
                'label'     => Mage::helper('aboutus')->__('Status'),
                'name'      => 'status',
                'values'    => array(
                    array(
                        'value'     => 1,
                        'label'     => 'Enabled',
                    ),

                    array(
                        'value'     => 0,
                        'label'     => 'Disabled',
                    )
                ),
            ));

            $fieldset->addField('store_id', 'select', array(
                'name'      => 'store_id',
                'label'     => Mage::helper('aboutus')->__('Store'),
                'required'  => true,
                'class'     => 'required-entry',
                'values'      => Mage::getModel('adminhtml/system_store')->getStoreValuesForForm()
            ));

            if ( Mage::getSingleton('adminhtml/session')->getAboutusData() )
            {
                $form->setValues(Mage::getSingleton('adminhtml/session')->getAboutusData());
                Mage::getSingleton('adminhtml/session')->setAboutusData(null);
            } elseif ( Mage::registry('aboutus_data') ) {
                $form->setValues(Mage::registry('aboutus_data')->getData());
            }

            return parent::_prepareForm();
        }
}