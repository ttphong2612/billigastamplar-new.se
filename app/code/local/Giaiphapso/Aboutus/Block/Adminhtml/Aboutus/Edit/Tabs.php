<?php

class Giaiphapso_Aboutus_Block_Adminhtml_Aboutus_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
 
    public function __construct()
    {
        parent::__construct();
        $this->setId('aboutus_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('aboutus')->__('News Information'));
    }
 
    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('aboutus')->__('Item Information'),
            'title'     => Mage::helper('aboutus')->__('Item Information'),
            'content'   => $this->getLayout()->createBlock('aboutus/adminhtml_aboutus_edit_tab_form')->toHtml(),
        ));
       
        return parent::_beforeToHtml();
    }
}