<?php

class Giaiphapso_Aboutus_IndexController extends Mage_Core_Controller_Front_Action {
 
    protected function _initAction()
    {
        
    }   
   
    public function indexAction()
    {
            $this->loadLayout();
            $this->renderLayout();
    }
    
    public function saveaboutusAction () {
        $session            = Mage::getSingleton('core/session');
        $content = $this->getRequest()->getPost("content");
        
        try {
            if (!$this->getRequest()->getPost("content") || trim($this->getRequest()->getPost("content")) == "") {
                Mage::throwException($this->__('Vraag is niet valide.'));
            }
            $aboutus = Mage::getModel('aboutus/aboutus');
            $aboutus->setContent($content);
            $aboutus->setDate(date("Y-m-d H:i:s"));
            $aboutus->save();
            $session->addSuccess($this->__('Bedankt voor uw vraag. Wij nemen binnen 2 werkdagen contact met u op.'));
        }
        catch (Mage_Core_Exception $e) {
            $session->addException($e, $this->__('Er was een probleem met uw vraag: %s', $e->getMessage()));
        }
        $this->_redirectReferer();
    }
}