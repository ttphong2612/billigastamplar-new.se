<?php
$installer = $this;
 
$installer->startSetup();
 
$installer->run("
 
DROP TABLE IF EXISTS {$this->getTable('aboutus')};
CREATE TABLE {$this->getTable('aboutus')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,          
  `content` text NOT NULL,
  `status` int(1) NOT NULL default 0,
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`store_id`) REFERENCES `{$this->getTable('core_store')}` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
    ");
 
$installer->endSetup();

?>