<?php  

    class Giaiphapso_Marketing10_Block_Adminhtml_Marketing10 extends Mage_Adminhtml_Block_Widget_Grid_Container
    {
        public function __construct()
        {
            $this->_controller = 'adminhtml_marketing10';
            $this->_blockGroup = 'marketing10';
            $this->_headerText = Mage::helper('marketing10')->__('Item Manager');
            parent::__construct();
            $this->_removeButton('add');
        }

}