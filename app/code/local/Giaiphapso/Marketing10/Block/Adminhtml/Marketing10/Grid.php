<?php

    class Giaiphapso_Marketing10_Block_Adminhtml_Marketing10_Grid extends Mage_Adminhtml_Block_Widget_Grid
    {
        public function __construct()
        {
            parent::__construct();
            $this->setId('marketing10Grid');
            // This is the primary key of the database
            $this->setDefaultSort('id');
            $this->setDefaultDir('DESC');
            $this->setSaveParametersInSession(true);
        }

        protected function _prepareCollection()
        {
            $rule_id = 1;
            
            $collection = Mage::getModel('marketing10/statistics')->getCollection();
            $collection->join('salesrule/coupon',"`salesrule/coupon`.rule_id = $rule_id AND `salesrule/coupon`.coupon_id = `main_table`.coupon_id");
            $this->setCollection($collection);
            return parent::_prepareCollection();
        }

        protected function _prepareColumns()
        {
            $this->addColumn('id', array(
                'header'    => Mage::helper('marketing10')->__('ID'),
                'align'     =>'center',
                'width'     => '50px',
                'index'     => 'id',
            ));

            $this->addColumn('email', array(
                'header'    => Mage::helper('marketing10')->__('Email'),
                'align'     => 'left',
                'width'     => '120px',
                'default'   => '--',
                'index'     => 'email',
            ));

            $this->addColumn('code', array(
                'header'    => Mage::helper('marketing10')->__('Coupon code'),
                'align'     => 'left',
                'width'     => '100px',
                'index'     => 'code',
            ));

            $this->addColumn('times_used', array(
                'header'    => Mage::helper('marketing10')->__('Used'),
                'align'     => 'center',
                'width'     => '80px',
                'index'     => 'times_used',
                'type'      => 'options',
                'options'   => array(
                    1 => 'Yes',
                    0 => 'No',
                ),
            ));

            $this->addColumn('created_at', array(
                'header'    => Mage::helper('marketing10')->__('Created At'),
                'align'     => 'center',
                'type'      => 'date',
                'width'     => '120px',
                'index'     => 'created_at',
                'gmtoffset' => false
            ));
            
            $this->addColumn('expiration_date', array(
                'header'    => Mage::helper('marketing10')->__('Expiration date'),
                'align'     => 'center',
                'type'      => 'date',
                'width'     => '120px',
                'index'     => 'expiration_date',
                'gmtoffset' => false
            ));
            
            $this->addColumn('store_id', array(
                'header'    => Mage::helper('aboutus')->__('Store'),
                'align'     => 'center',
                'width'     => '160px',
                'index'     => 'store_id',
                'type'        => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
                => array($this, '_filterStoresCondition'),
            ));

            $this->addExportType('*/*/exportCsv', Mage::helper('marketing10')->__('CSV'));
            $this->addExportType('*/*/exportXml', Mage::helper('marketing10')->__('XML'));

            return parent::_prepareColumns();
        }

        protected function _filterStoresCondition($collection, $column)
        {
            if (!$value = $column->getFilter()->getValue()) {
                return;
            }

            $this->getCollection()->addFieldToFilter('store_id', array('finset' => $value));
        }

        protected function _prepareMassaction()
        {
            $this->setMassactionIdField('id');
            $this->getMassactionBlock()->setFormFieldName('marketing10');

            $this->getMassactionBlock()->addItem('delete', array(
                'label'    => Mage::helper('marketing10')->__('Delete'),
                'url'      => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('marketing10')->__('Are you sure?')
            ));
            
            $this->getMassactionBlock()->addItem('sendemail', array(
                'label'    => Mage::helper('marketing10')->__('Send Coupon Notify'),
                'url'      => $this->getUrl('*/*/massSendEmail'),
                'confirm'  => Mage::helper('marketing10')->__('Are you sure?')
            ));
            
            $this->getMassactionBlock()->addItem('expiredate', array(
                'label'=> Mage::helper('catalog')->__('Set expire date'),
                'url'  => $this->getUrl('*/*/massExpireDate', array('_current'=>true)),
                'additional' => array(
                    'expire' => array(
                        'name' => 'expire_date',
                        'type'     => 'date',
                        'class'    => 'required-entry',
                        'label' => Mage::helper('catalog')->__('Expire date'),
                        'gmtoffset' => true,
                        'image'    => '/skin/adminhtml/default/default/images/grid-cal.gif',
                        'format'    => '%d/%m/%Y'
                    )
                )
            ));
            
            return $this;
        }
}