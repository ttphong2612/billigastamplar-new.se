<?php   
class Giaiphapso_Marketing10_Block_Init extends Mage_Core_Block_Template{   

    public function __construct()
    {      
        $websiteId = Mage::app()->getWebsite()->getId(); 
        
        $arrOffWeb = array(SE_WEBSITE_ID,DK_WEBSITE_ID);
       // $promotion = array(DK_WEBSITE_ID, FR_WEBSITE_ID);
        $arrCmsKey = array('promotion','standard-stempler/promotion','stockstamp-deeplink/promotion');
        //check promotion from cms page for promotion & stock stamp deeplink
		if( in_array(Mage::getSingleton('cms/page')->getIdentifier(), $arrCmsKey) ){ 
			// CMS Url key = promotion
			$this->setTemplate('marketing10/popup.phtml');   
		//check for all page + all site except dk+us
		}else{
			 if( ! in_array($websiteId, $arrOffWeb) ){
				$mk10 = Mage::getModel('core/cookie')->get('mk10');
				if( in_array($websiteId, array(UK_WEBSITE_ID ) )){ // On one visit every 3 days
					if( empty($mk10) ){
						Mage::getModel('core/cookie')->set('mk10', time(), 3*24*60*60);    
						$this->setTemplate('marketing10/popup.phtml');
					}
				}
			}
		}
       
    }
}