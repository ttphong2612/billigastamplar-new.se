<?php
    class Giaiphapso_Marketing10_Helper_Helper extends Mage_Core_Helper_Abstract
    {
        public function calculationNumberDays($date1, $date2)
        {
            $timeDiff    = abs($date2 - $date1);
            $numberDays  = $timeDiff/86400;  // 86400 seconds in one day

            // and you might want to convert to integer
            return $numberDays;
        }

        public function sendMail($fromName, $fromEmail, $email, $subject, $content)
        {
            $mail = new Zend_Mail('UTF-8');

            $mail->addTo($email, $email);
            $mail->setBodyHtml($content);
            $mail->setSubject($subject);
            $mail->setFrom($fromEmail, $fromName);
            $mail->send();    
        }

        public function translate($storeId, $expiryDate, $couponCode=null, $dayAfter, $bold=false)
        {   
            $prefix = '';
            $subfix = '';

            if( $bold ){
                $prefix = '<b>';
                $subfix = '</b>';    
            }

            $content = '';
            switch( $storeId ){
                /* case 5: 
                case 6:
                    $content = "Dear, <br/><br/>$dayAfter days ago we send you an e-mail with your 35% discount Voucher for Stamp-factory.com<br/><br/>We can see you have not yet used this 35% discount Voucher: <span style='color:red'>$couponCode</span><br/><br/>Please let us help you if we did something wrong or misunderstood you.<br/><br/>We are here to help you, and hope you will use your 35% discount in good time before it expire on <span style='color:red'>$expiryDate</span><br/><br/><br/>Best regards<br/>Stamp-factory.com<br/>again, again & again<br/><br/><br/>OBS: did you know there are no limit on your order, on content or product type, in fact no limits at all.<br/><br/>";
                    break;
                case 8: 
                    $content = "Geachte, <br/><br/>$dayAfter dagen geleden hebben wij u een e-mail gestuurd met uw 35% kortingsbon voor Stamp-factory.com<br/><br/>We zien dat u nog geen gebruik gemaakt hebt van deze 35% korting Voucher: <span style='color:red'>$couponCode</span><br/><br/>Laat ons u helpen als we iets verkeerd hebben gedaan of u misschien verkeerd begrepen.<br/><br/>Wij zijn hier om u te helpen, en hopen dat u uw 35% korting tijdig zal gebruiken vóór het verstrijken op <span style='color:red'>$expiryDate</span><br/><br/><br/>.Met vriendelijke groet<br/>Stamp-factory.com<br/>opnieuw, opnieuw en opnieuw<br/><br/><br/>OBS: wist u dat er geen limiet op uw bestelling is, op de inhoud of het type product, in feite helemaal geen grenzen.<br/><br/>";
                    break;
                case 9: 
                case 16: 
                    $content = "Cher, <br/><br/>Il y a $dayAfter jours vous avez reçu un e-mail avec un avoir de 35% sur votre première commande à Stamp-factory.com.<br/><br/>Vous ne l'avez, à ce jour, pas  utilisé <span style='color:red'>$couponCode</span><br/><br/>Merci de nous signaler si cette proposition ne vous semble pas claire<br/><br/>Nous sommmes à votre disposition pour tout renseignement complémentaire<br/><br/>Nous expérons vous voir bénéficier de cette promotion.<br/><br/><br/>Cordialement,<br/>Stamp-factory.com<br/>encore, encore et encore<br/><br/><br/>PS: savez-vous qu'il n'y a pas de limite à vos commandes,  contenus ou produits – en fait aucune limite.<br/><br/>";
                    break;
                case 12:
                    $content = "Sehr geehrter, <br/><br/>Vor $dayAfter Tagen schicken wir Ihnen eine E-Mail mit Ihren 35% Rabatt-Gutschein für Stempel-factory.com.<br/><br/>Es kann uns klar sein, dass Sie noch nicht diesen 35% Rabatt verwendet haben Voucher: <span style='color:red'>$couponCode</span><br/><br/>Bitte lassen Sie uns helfen, wenn wir etwas falsch gemacht oder falsch verstanden haben.<br/><br/>Wir sind hier, um Ihnen zu helfen, und hoffen, dass Sie Ihre 35% Ermäßigung rechtzeitig nutzen, bevor es auf <span style='color:red'>$expiryDate</span> verfallen.<br/><br/><br/>Mit freundlichen Grüßen<br/>Stamp-factory.com<br/>wieder, wieder und wieder<br/><br/><br/>OBS: Wussten Sie, es gibt keine Begrenzung auf Ihrer Bestellung, auf der Inhalte oder Produkttyp, in der Wirklichkeit es keine Grenzen überhaupt gibt.<br/><br/>";
                    break;
                case 14:
                    $content = "Dear, <br/><br/>Hace $dayAfter días que le enviaremos un correo electrónico con su cupón de 35% de descuento para stamp-factory.com.<br/><br/>Podemos ver que aún no ha utilizado este 35% de descuento Por favor Voucher: <span style='color:red'>$couponCode</span><br/><br/>Permítanos ayudarle si hicimos algo mal o mal entendido.<br/><br/>Estamos aquí para ayudarle y esperamos que va a utilizar el 35% de descuento en buen tiempo antes de que expira en la <span style='color:red'>$expiryDate</span>.<br/><br/><br/>Saludos cordiales<br/>Stamp-factory.com<br/>de nuevo, una y otra vez<br/><br/><br/>OBS: ¿sabía usted que no hay límite en su orden, el contenido o el tipo de producto, de hecho, no hay límites en absoluto<br/><br/>";
                    break;
                case 2:
                    $content = "Kära, <br/><br/>För $dayAfter dagar sedan skickade vi ett mail till dig med din rabattkupong på 35%.<br/><br/>Vi kan se att du ännu inte har utnyttjat denna Voucher: <span style='color:red'>$couponCode</span> rabatt på 35%.<br/><br/>Vänligen låt oss hjälpa dig om vi gjort något fel eller missförstått dig.<br/><br/>Vi finns här för att hjälpa dig, och hoppas att du kommer utnyttja din rabatt på 35% innan dess att tiden för erbjudandet löper ut <span style='color:red'>$expiryDate</span>.<br/><br/><br/>Vänliga hälsningar<br/>Stamp-factory.com<br/>igen, igen & igen<br/><br/><br/>OBS: Visste du att din beställning inte innehåller någon begränsning - varken vad gäller innehålls- eller produkttyp – faktiskt inga begränsningar alls.";
                    break;
                case 1:
                    if( $dayAfter == 5 ){
                        $content = "Hej, <br/><br/>For 5 dage siden sendte vi dig en Voucher med 35% rabat <span style='color:red'>$couponCode</span><br/><br/>Vi kan se du endnu ikke har brugt den og vil blot spørge om der er noget vi kan hjælpe med?. Måske er der noget vi har misforstået ? Eller var der noget der ikke virkede ?<br/><br/>Vi er her for at hjælpe dig, så rabatten kan blive udnyttet inden den udløber om 5 dage.<br/><br/><br/>Med venlig hilsen<br/>Stamp-factory.com<br/>again, again & again";    
                    }
                    elseif( $dayAfter == 9 ){
                        $content = "Hej, <br/><br/>For 9 dage siden sendte vi dig en Voucher med 35% rabat <span style='color:red'>$couponCode</span><br/><br/>Vi kan se du endnu ikke har brugt den og vil blot spørge om der er noget vi kan hjælpe med ?<br/><br/>Vi er her for at hjælpe!, så den kan blive udnyttet inden den udløber om 24 timer<br/><br/><br/>Med venlig hilsen<br/>Stamp-factory.com<br/>again, again & again";    
                    }
                    elseif( $dayAfter == 10 ){
                        $content = "Hej, <br/><br/>For 10 dage siden sendte vi dig en Voucher med 35% rabat <span style='color:red'>$couponCode</span><br/><br/>I dag udløber den, MEN du kan stadig nå at bruge den, hvis du blot indløser den inden kl. 24:00 I aften.<br/><br/>Vi vil rigtigt gerne at du har en god oplevelse, og du modtager den rabat vi har lovet dig.<br/><br/><br/>Med venlig hilsen<br/>Stamp-factory.com<br/>again, again & again";    
                    }else{
                        $content = "Hej, <br/><br/>For 5 dage siden sendte vi dig en Voucher med 35% rabat <span style='color:red'>$couponCode</span><br/><br/>Vi kan se du endnu ikke har brugt den og vil blot spørge om der er noget vi kan hjælpe med?. Måske er der noget vi har misforstået ? Eller var der noget der ikke virkede ?<br/><br/>Vi er her for at hjælpe dig, så rabatten kan blive udnyttet inden den udløber om 5 dage.<br/><br/><br/>Med venlig hilsen<br/>Stamp-factory.com<br/>again, again & again";    
                    }
                    break; */
                default:         
                    $content = "Dear, <br/><br/>$dayAfter days ago we send you an e-mail with your 35% discount Voucher for Bestpriceforfubberstamps.com <br/><br/>We can see you have not yet used this 35% discount Voucher: <span style='color:red'>$couponCode</span><br/><br/>Please let us help you if we did something wrong or misunderstood you.<br/><br/>We are here to help you, and hope you will use your 35% discount in good time before it expire on <span style='color:red'>$expiryDate</span><br/><br/><br/>Best regards<br/>bestpriceforfubberstamps.com<br/>again, again & again<br/><br/><br/>OBS: did you know there are no limit on your order, on content or product type, in fact no limits at all.<br/><br/>";
                    break;
            }

            return $prefix . $content . $subfix;
        }

        public function getFromEmail( $storeId )
        {
            $fromEmail = Mage::getStoreConfig('trans_email/ident_support/email');
            
            switch( $storeId ){
                case 5: 
                    $fromEmail = "england@stamp-factory.com";
                    break;
                case 6: 
                case 16:
                case 14:
                    $fromEmail = "usa@stamp-factory.com";
                    break;
                case 8: 
                    $fromEmail = "holland@stamp-factory.com";
                    break;
                case 9: 
                    $fromEmail = "france@stamp-factory.com";
                    break;
                case 12:
                    $fromEmail = "germany@stamp-factory.com";
                    break;
                case 2:
                    $fromEmail = "sverige@stamp-factory.com";
                    break;
                case 1:
                    $fromEmail = "info@bestpriceforfubberstamps.com ";
                    break;
                default:         
                    $fromEmail = "denmark@stamp-factory.com";
                    break;
            }
            
            return $fromEmail;
        }
    }
