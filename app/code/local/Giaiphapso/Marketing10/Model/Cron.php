<?php

    class Giaiphapso_Marketing10_Model_Cron
    {	
        public function checkUseCoupon()
        {             
            $helper  = Mage::helper('marketing10/helper');
            $rule_id = 1;

            $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $query = "SELECT c.email, c.store_id, s.code, s.created_at FROM salesrule_coupon s, c_marketing10_statistics c WHERE s.rule_id=$rule_id AND s.times_used=0 AND s.coupon_id = c.coupon_id";
            $items = $readConnection->fetchAll($query);

            if( count($items) ){
                foreach($items as $item){
                    $numberDays = $helper->calculationNumberDays(strtotime($item['created_at']), strtotime(date('Y-m-d h:i:s')));
                    
                    if( $numberDays <= 10 )
                    {
                        $expiryDate = date('Y-m-d h:i:s',strtotime($item['created_at']) + 60*60*24*10); 
                        $email     = $item['email'];
                        $subject   = Mage::helper('marketing10')->__('Voucher from bestpriceforrubberstamps.com');
                        $fromName  = 'Bestpriceforrubberstamps.com';
                        $fromEmail = $helper->getFromEmail( $item['store_id'] );
                        $dayAfter  = 5;
                        
                        //if( $email == 'phong.tran@giaiphapso.com' ){
                        
                        // Check after 5 day wich customer does not use, send email remind
                        if( $numberDays >= 5 && $numberDays < 6 )
                        {
                            $dayAfter = 5;
                        }
                        // Check after 9 day wich customer does not use, send email remind
                        elseif( $numberDays > 8  && $numberDays <= 9 )
                        {
                            $dayAfter = 9;
                        }
                        // Check after 10 day wich customer does not use, send email remind
                        elseif( $numberDays > 9 && $numberDays <= 10 )
                        {
                            $dayAfter = 10;
                        }
                        else
                        {
                            continue;
                        }
                        
                        $content = $helper->translate($item['store_id'], $expiryDate, $item['code'], $dayAfter, false);
                        $helper->sendMail($fromName, $fromEmail, $email, $subject, $content);
                        //}
                    }
                }
            }

            return $this;
        }

        public function reminderAtExpireDay()
        {               
            $helper  = Mage::helper('marketing10/helper');
            $rule_id = 1;

            $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $query = "SELECT c.email, c.store_id, s.code, s.created_at FROM salesrule_coupon s, c_marketing10_statistics c WHERE s.rule_id=$rule_id AND s.times_used=0 AND s.coupon_id = c.coupon_id";
            $items = $readConnection->fetchAll($query);

            if( count($items) ){
                foreach($items as $item){
                    // Check after 5 day wich customer does not use, send email remind
                    $numberDays = $helper->calculationNumberDays(strtotime($item['created_at']), strtotime(date('Y-m-d h:i:s')));
                    if( $numberDays >= 9 && $numberDays < 10 )
                    {
                        $expiryDate = date('Y-m-d h:i:s',strtotime($item['created_at']) + 60*60*24*10); 

                        $content   = $helper->translate($item['store_id'], $expiryDate, 10, $item['code'], true);
                        $email     = $item['email'];
                        $subject   = Mage::helper('marketing10')->__('Voucher from bestpriceforrubberstamps.com');
                        $fromEmail = $helper->getFromEmail( $item['store_id'] );
                        $fromName  = 'Bestpriceforrubberstamps.com';

                        $helper->sendMail($fromName, $fromEmail, $email, $subject, $content);
                    }
                }
            }

            return $this;
        } 
}