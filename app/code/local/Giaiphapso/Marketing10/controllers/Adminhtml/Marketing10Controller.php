<?php
    class Giaiphapso_Marketing10_Adminhtml_Marketing10Controller extends Mage_Adminhtml_Controller_Action
    {
        public function indexAction()
        {
            $this->loadLayout();
            $this->_title($this->__("Marketing10"));
            $this->_addContent($this->getLayout()->createBlock('marketing10/adminhtml_marketing10'));
            $this->renderLayout();
        }

        public function exportCsvAction() {
            $fileName = 'marketing10.csv';
            $content = $this->getLayout ()->createBlock ( 'marketing10/adminhtml_marketing10_grid' )->getCsv();

            $this->_sendUploadResponse ( $fileName, $content );
        }

        public function exportXmlAction() {
            $fileName = 'marketing10.xml';
            $content = $this->getLayout ()->createBlock ( 'marketing10/adminhtml_marketing10_grid' )->getXml();

            $this->_sendUploadResponse ( $fileName, $content );
        }

        protected function _sendUploadResponse($fileName, $content, $contentType = 'application/octet-stream') {
            $response = $this->getResponse ();
            $response->setHeader ( 'HTTP/1.1 200 OK', '' );
            $response->setHeader ( 'Pragma', 'public', true );
            $response->setHeader ( 'Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true );
            $response->setHeader ( 'Content-Disposition', 'attachment; filename=' . $fileName );
            $response->setHeader ( 'Last-Modified', date ( 'r' ) );
            $response->setHeader ( 'Accept-Ranges', 'bytes' );
            $response->setHeader ( 'Content-Length', strlen ( $content ) );
            $response->setHeader ( 'Content-type', $contentType );
            $response->setBody ( $content );
            $response->sendResponse ();
            die ();
        }

        public function massDeleteAction() {
            $ids = $this->getRequest ()->getParam ( 'marketing10' );
            
            if (! is_array ( $ids )) {
                Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'adminhtml' )->__ ( 'Please select item(s)' ) );
            } else {
                try {
                    $m10Statistics = Mage::getModel ( 'marketing10/statistics' );
                    $coupon        = Mage::getModel ( 'salesrule/coupon' );
                    foreach ( $ids as $id ) {
                        $m10Statistics->load ( $id );
                        $m10Statistics->delete ();
                        
                        $coupon->load( $m10Statistics->getData('coupon_id') );
                        $coupon->delete ();
                    }
                    Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'adminhtml' )->__ ( 'Total of %d record(s) were successfully deleted', count ( $ids ) ) );
                } catch ( Exception $e ) {
                    Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
                }
            }
            $this->_redirect ( '*/*/index' );
        }
        
        public function massExpireDateAction()
        {
            $ids = $this->getRequest ()->getParam ( 'marketing10' );
            
            if (! is_array ( $ids )) {
                Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'adminhtml' )->__ ( 'Please select item(s)' ) );
            } else {
                try {
                    $couponids = array();
                    $data = $this->getRequest()->getPost();
                    $data = $this->_filterDates($data, array('expire_date'));
                    $collection = Mage::getModel('marketing10/statistics')->getCollection()->addFieldToFilter('id', array('in' => $data['marketing10']));
                    foreach($collection as $coupon){ 
                        array_push($couponids, $coupon->getCouponId());
                    }
                    $sql = 'UPDATE salesrule_coupon SET expiration_date = "'.$data['expire_date'].'" WHERE coupon_id IN ('.implode(',', $couponids).')';
                    $connection = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $connection->query($sql);
                    Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'adminhtml' )->__ ( 'Total of %d record(s) were successfully update', count ( $couponids ) ) );
                } catch ( Exception $e ) {
                    Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
                }
            }
            $this->_redirect ( '*/*/index' );
        }
        
        public function massSendEmailAction()
        {
            set_time_limit(0);
            ini_set('memory_limit','2000M');
            $storeId = 1;
            //template email voucher notify id = 9
            $templateId = 9;
            $ids = $this->getRequest ()->getParam ( 'marketing10' );
            $paramName = 'filter';
            $sessionParamName = 'marketing10Grid'.$paramName;
            $filter = Mage::getSingleton('adminhtml/session')->getData($sessionParamName);
            if (!is_null($filter))  {
                //decode the filter
                $filter = Mage::helper('adminhtml')->prepareFilterString($filter);
                //store id = 1 denmark
                $storeId = $filter['store_id'];
            }
            //dk site
            if($storeId == 1){
                $templateId = 9;
            //
            }elseif($storeId == 2){
                $templateId = 10;
            }
            if (! is_array ( $ids )) {
                Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'adminhtml' )->__ ( 'Please select item(s)' ) );
            } else {
                try {
                    $sender  = array(
                        'name' => Mage::getStoreConfig('general/store_information/name', $storeId),
                        'email' => Mage::getStoreConfig('trans_email/ident_support/email', $storeId)
                    );
                    $collection = Mage::getModel('marketing10/statistics')->getCollection()->addFieldToFilter('id', array('in' => $ids));
                    $collection->join('salesrule/coupon',"`salesrule/coupon`.coupon_id = `main_table`.coupon_id");
                    
                    foreach($collection as $coupon){
                        $recepientEmail = $coupon->getEmail();
                        $vars = array(
                            'coupon' => $coupon->getCode(),
                        );
                        Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $recepientEmail , '', $vars, $storeId);
                    }
                }catch ( Exception $e ) {
                    Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
                }
            }
            $this->_redirect ( '*/*/index' );
        }
}