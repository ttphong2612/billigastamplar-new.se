<?php
    class Giaiphapso_Marketing10_IndexController extends Mage_Core_Controller_Front_Action{
        const INTERVAL_EXPIRE_DATE = 10;
        public function IndexAction() {
            return;
        }

        /*Use for test*/
        public function TestAction() {
            // PHẢI ĐẶT ĐIỀU KIỆN CHỈ CHẠY ĐỐI VỚI EMAIL PHONG.TRAN@GIAIPHAPSO.COM
            /*$model = Mage::getModel('marketing10/cron');
            $model->checkUseCoupon();*/
        }

        /*Use for test*/
        public function SendfromlistAction()
        {
            /*$helper  = Mage::helper('marketing10/helper');
            $storeId = Mage::app()->getStore()->getId();

            $emails  = array('tom_beke@hotmail.com', 'jiwi_de_kiwi@hotmail.com', 'misseslies@hotmail.com', 'pompoen_tom@hotmail.com', 'l_zaimi@hotmail.com', 'corneliskelly@gmail.com', 'beletluc@hotmail.com', 'gretelverboomen@hogmail.com', 'Kristel.bourgeois@gmail.com', 'jorien_baeten@hotmail.com', 'niekverslype@gmail.com', 'b_pyl@yahoo.co.uk', 'jwm@aibv.eu', 'usler-sahin@live.nl', 'bo@boco.dk', 'ttn244_dl@yahoo.com');
            foreach( $emails as $email ){

                // Validate email
                if( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
                    echo 0;
                    return;
                }
                // Add to newsletter list
                $this->_autoSubscribe($email);
                // Create Coupon
                $rule_id = 4;
                $generator = Mage::getModel('salesrule/coupon_massgenerator');
                $data = array(
                    'max_probability'   => .25,
                    'max_attempts'      => 10,
                    'uses_per_customer' => 1,
                    'uses_per_coupon'   => 1,
                    'qty'               => 1, //number of coupons to generate
                    'prefix'            => 'MK10-', //number of coupons to generate
                    'length'            => 12, //length of coupon string
                    //'to_date'           => date('Y-m-d h:i:s'), //ending date of generated promo                
                    'format'            => Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_ALPHANUMERIC,
                    'rule_id'           => $rule_id //the id of the rule you will use as a template
                );
                $generator->validateData($data);
                $generator->setData($data);
                $generator->generatePool();

                // Send Coupon Code to customer & Store to C_MARKETING1-_STATISTICS table
                $resource = Mage::getSingleton('core/resource');
                $readConnection = $resource->getConnection('core_read');
                $query  = 'SELECT coupon_id, code FROM salesrule_coupon WHERE rule_id = '. "'$rule_id'" . ' ORDER BY coupon_id DESC LIMIT 1';
                $result = $readConnection->fetchAll($query);

                // Store to C_MARKETING1-_STATISTICS table
                $writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
                $sql = "INSERT INTO c_marketing10_statistics(email, coupon_id, store_id) VALUES('$email',". $result[0]['coupon_id'] .", $storeId)";
                $writeConnection->query($sql);   

                // Send Code to email of customer
                $content   = $this->__('Dear, <br/><br/>Thank you for asking for <span style="color:red">%s</span> discount at your purchase here at Stamp-factory.com.<br/><br/>Here is your Voucher <span style="color:red">%s</span> which you have to use when you enter the basket.<br/><br/>This Voucher will be active for the next <span style="color:red">%s</span> days, and can be used one time.<br/><br/>We hope you will contact us if any questions or you need any help. Enjoy your visit here at Stamp-factory.com.<br/><br/><br/>Best regards<br/>Stamp-factory.com<br/>again, again & again<br/><br/><br/>OBS: did you know there are no limit on your order, on content or product type, in fact no limits at all.<br/><br/>', "35%", $result[0]['code'], "10");
                $content   .= '<p style="color: red;">Beste klant. Wij hebben een probleem gehad met het voucher systeem sinds afgelopen maandag. Wij verontschuldigen ons voor het ongemak en doen u hierbij een nieuwe voucher toekomen</p>';
                $fromName  = 'Stamp-factory.com';
                $fromEmail = Mage::getStoreConfig('trans_email/ident_support/email');
                $email     = $email;
                $subject   = $this->__('Voucher from stamp-factory.com');

                $helper->sendMail($fromName, $fromEmail, $email, $subject, $content);
            }

            return;*/
        }

        public function InputemailAction() {
            
            $helper  = Mage::helper('marketing10/helper');
            $storeId = Mage::app()->getStore()->getId();

            $email   = $this->getRequest()->getParam('email');
            $email = trim($email);
            // Validate email
            if( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
                echo 0;
                return;
            }

            // Add to newsletter list
            $this->_autoSubscribe($email);

            // Create Coupon
            $rule_id = 1;
            $generator = Mage::getModel('salesrule/coupon_massgenerator');
            $date = Mage::getModel('core/date')->date('Y-m-d');
            $to_date = Mage::getModel('core/date')->date('Y-m-d', strtotime($date . "+".self::INTERVAL_EXPIRE_DATE." days"));
            $data = array(
                'max_probability'   => .25,
                'max_attempts'      => 10,
                'uses_per_customer' => 1,
                'uses_per_coupon'   => 1,
                'qty'               => 1, //number of coupons to generate
                'prefix'            => 'MK10-', //number of coupons to generate
                'length'            => 12, //length of coupon string
                //'to_date'           => date('Y-m-d h:i:s'), //ending date of generated promo                
                'format'            => Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_ALPHANUMERIC,
                'rule_id'           => $rule_id, //the id of the rule you will use as a template
                'to_date'           => $to_date
            );
            $generator->validateData($data);
            $generator->setData($data);
            $generator->generatePool();

            // Send Coupon Code to customer & Store to C_MARKETING1-_STATISTICS table
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');
            $query  = 'SELECT coupon_id, code FROM salesrule_coupon WHERE rule_id = '. "'$rule_id'" . ' ORDER BY coupon_id DESC LIMIT 1';
            $result = $readConnection->fetchAll($query);

            // Store to C_MARKETING1-_STATISTICS table
            $writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
            $sql = "INSERT INTO c_marketing10_statistics(email, coupon_id, store_id) VALUES('$email',". $result[0]['coupon_id'] .", $storeId)";
            $writeConnection->query($sql);   

            // Send Code to email of customer
			
			$domainName = preg_replace('#^https?://#', '', rtrim(Mage::getBaseUrl(),'/'));
			
            $content   = $this->__('Dear, <br/><br/>Thank you for asking for <span style="color:red">%s</span> discount at your purchase here at %s.<br/><br/>Here is your Voucher <span style="color:red">%s</span> which you have to use when you enter the basket.<br/><br/>This Voucher will be active for the next <span style="color:red">%s</span> days, and can be used one time.<br/><br/>We hope you will contact us if any questions or you need any help. Enjoy your visit here at %s.<br/><br/><br/>Best regards<br/>%s<br/>again, again & again<br/><br/><br/>OBS: did you know there are no limit on your order, on content or product type, in fact no limits at all.<br/><br/>', "35%", $domainName, $result[0]['code'], "10", $domainName, $domainName);
            $fromName  = $domainName;
            $fromEmail = Mage::getStoreConfig('trans_email/ident_support/email');
            $subject   = $this->__('Voucher from %s',$domainName);

            $helper->sendMail($fromName, $fromEmail, $email, $subject, $content);

            echo $result[0]['code']; // Return coupon code
            
            // Store in session
            Mage::getSingleton('core/session')->setM10VoucherCode($result[0]['code']);
            
            return;
        }

        protected function _autoSubscribe($email)
        {
            $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($email);
			try{
				if($subscriber->getStatus() != Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED &&
					$subscriber->getStatus() != Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED) {
					$subscriber->setImportMode(true)->subscribe($email);
				}
		
			}catch(Exception $e){
				var_dump($e->getMesasge());exit;
			}
        }
}