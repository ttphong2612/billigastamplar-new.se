<?php
class Giaiphapso_Question_Block_Adminhtml_Question extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_question';
        $this->_blockGroup = 'question';
        $this->_headerText = Mage::helper('question')->__('Item Manager');
        $this->_addButtonLabel = Mage::helper('question')->__('Add Item');
        parent::__construct();
    }
}