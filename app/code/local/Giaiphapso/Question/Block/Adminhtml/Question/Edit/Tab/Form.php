<?php

class Giaiphapso_Question_Block_Adminhtml_Question_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('question_form', array('legend'=>Mage::helper('question')->__('Item information')));
       
        $fieldset->addField('content', 'editor', array(
            'name'      => 'content',
            'label' => Mage::helper('question')->__('Question'),
            'title' => Mage::helper('question')->__('Question'),
            'style' => 'width:700px; height:500px;',
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            'wysiwyg' => true,
            'required' => true, 
            'style'    => 'height:150px;width:700px;',
        ));
       
        $fieldset->addField('answer', 'editor', array(
            'name'      => 'answer',
            'label' => Mage::helper('question')->__('Answer'),
            'title' => Mage::helper('question')->__('Answer'),
            'style' => 'width:700px; height:500px;',
            'config' => Mage::getSingleton('cms/wysiwyg_config')->getConfig(),
            'wysiwyg' => true,
            'required' => true, 
            'style'    => 'height:150px;width:700px;',
        ));
        
        $fieldset->addField('status', 'select', array(
                'label'     => Mage::helper('question')->__('Status'),
                'name'      => 'status',
                'values'    => array(
                    array(
                        'value'     => 1,
                        'label'     => 'Enabled',
                    ),

                    array(
                        'value'     => 0,
                        'label'     => 'Disabled',
                    )
                ),
            ));

            $fieldset->addField('store_id', 'select', array(
                'name'      => 'store_id',
                'label'     => Mage::helper('question')->__('Store'),
                'required'  => true,
                'class'     => 'required-entry',
                'values'      => Mage::getModel('adminhtml/system_store')->getStoreValuesForForm()
            ));
       
        if ( Mage::getSingleton('adminhtml/session')->getQuestionData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getQuestionData());
            Mage::getSingleton('adminhtml/session')->setQuestionData(null);
        } elseif ( Mage::registry('question_data') ) {
            $form->setValues(Mage::registry('question_data')->getData());
        }
        return parent::_prepareForm();
    }
}