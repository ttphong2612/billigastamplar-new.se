<?php

    class Giaiphapso_Question_Block_Adminhtml_Question_Grid extends Mage_Adminhtml_Block_Widget_Grid
    {
        public function __construct()
        {
            parent::__construct();
            $this->setId('questionGrid');
            // This is the primary key of the database
            $this->setDefaultSort('id');
            $this->setDefaultDir('ASC');
            $this->setSaveParametersInSession(true);
        }

        protected function _prepareCollection()
        {
            $collection = Mage::getModel('question/question')->getCollection();
            $this->setCollection($collection);
            return parent::_prepareCollection();
        }

        protected function _prepareColumns()
        {
            $this->addColumn('id', array(
                'header'    => Mage::helper('question')->__('ID'),
                'align'     =>'right',
                'width'     => '50px',
                'index'     => 'id',
            ));

            $this->addColumn('content', array(
                'header'    => Mage::helper('question')->__('Question'),
                'align'     => 'left',
                'width'     => '120px',
                'default'   => '--',
                'index'     => 'content',
            ));

            $this->addColumn('answer', array(
                'header'    => Mage::helper('question')->__('Answer'),
                'align'     => 'left',
                'width'     => '120px',
                'default'   => '--',
                'index'     => 'answer',
            )); 

            $this->addColumn('status', array(
                'header'    => Mage::helper('question')->__('Status'),
                'align'     => 'center',
                'width'     => '80px',
                'index'     => 'status',
                'type'      => 'options',
                'options'   => array(
                    1 => 'Enabled',
                    0 => 'Disabled',
                ),
            ));

            $this->addColumn('store_id', array(
                'header'    => Mage::helper('question')->__('Store'),
                'align'     => 'center',
                'width'     => '160px',
                'index'     => 'store_id',
                'type'        => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
                => array($this, '_filterStoresCondition'),
            ));

            $this->addColumn('action',
                array(
                    'header'    =>  Mage::helper('question')->__('Action'),
                    'align'     => 'center',
                    'width'     => '100',
                    'type'      => 'action',
                    'getter'    => 'getId',
                    'actions'   => array(
                        array(
                            'caption'   => Mage::helper('question')->__('Edit'),
                            'url'       => array('base'=> '*/*/edit'),
                            'field'     => 'id'
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true,
            ));

            return parent::_prepareColumns();
        }

        public function getRowUrl($row)
        {
            return $this->getUrl('*/*/edit', array('id' => $row->getId()));
        }

        protected function _prepareMassaction()
        {
            $this->setMassactionIdField('id');
            $this->getMassactionBlock()->setFormFieldName('question');

            $this->getMassactionBlock()->addItem('delete', array(
                'label'    => Mage::helper('question')->__('Delete'),
                'url'      => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('question')->__('Are you sure?')
            ));

            $statuses = Mage::getSingleton('question/question')->getOptionArray();

            array_unshift($statuses, array('label'=>'', 'value'=>''));
            $this->getMassactionBlock()->addItem('status', array(
                'label'=> Mage::helper('aboutus')->__('Change status'),
                'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
                'additional' => array(
                    'visibility' => array(
                        'name' => 'status',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => Mage::helper('question')->__('Status'),
                        'values' => $statuses
                    )
                )
            ));
            return $this;
        }
        
        protected function _filterStoresCondition($collection, $column)
        {
            if (!$value = $column->getFilter()->getValue()) {
                return;
            }

            $this->getCollection()->addFieldToFilter('store_id', array('finset' => $value));
        }
}