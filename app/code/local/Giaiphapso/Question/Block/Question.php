<?php
/**
* Example View block
*
* @codepool   Local
* @category   Fido
* @package    Fido_Example
* @module     Example
*/
class Giaiphapso_Question_Block_Question extends Mage_Core_Block_Template
{
    public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }

     public function getQuestion()
     {
     	 return Mage::getModel('question/question')
            ->getCollection()
            ->addFieldToFilter('store_id', Mage::app()->getStore()->getId())
            ->addFieldToFilter("status", 1);
    }
}