<?php

class Giaiphapso_Question_IndexController extends Mage_Core_Controller_Front_Action {
 
    protected function _initAction()
    {
        
    }   
   
    public function indexAction()
    {
            $this->loadLayout();
            $this->renderLayout();
    }
    
    public function savequestionAction () {
        $session            = Mage::getSingleton('core/session');
        $content = $this->getRequest()->getPost("content");
        
        try {
            if (!$this->getRequest()->getPost("content") || trim($this->getRequest()->getPost("content")) == "") {
                Mage::throwException($this->__('Vraag is niet valide.'));
            }
            $question = Mage::getModel('question/question');
            $question->setContent($content);
            $question->setDate(date("Y-m-d H:i:s"));
            $question->save();
            $session->addSuccess($this->__('Bedankt voor uw vraag. Wij nemen binnen 2 werkdagen contact met u op.'));
        }
        catch (Mage_Core_Exception $e) {
            $session->addException($e, $this->__('Er was een probleem met uw vraag: %s', $e->getMessage()));
        }
        $this->_redirectReferer();
    }
}