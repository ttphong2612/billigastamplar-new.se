<?php
$installer = $this;
 
$installer->startSetup();
 
$installer->run("
 
-- DROP TABLE IF EXISTS {$this->getTable('question')};
CREATE TABLE {$this->getTable('question')} (
  `id` int(11) unsigned NOT NULL auto_increment,
  `content` text NOT NULL,
  `answer` text,
  `date` datetime NOT NULL,
  `status` int(1) NOT NULL default 0,
  `store_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`store_id`) REFERENCES `{$this->getTable('core_store')}` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
    ");
 
$installer->endSetup();

?>