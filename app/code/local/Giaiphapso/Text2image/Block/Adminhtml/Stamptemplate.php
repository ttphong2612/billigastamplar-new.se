<?php
class Giaiphapso_Text2image_Block_Adminhtml_Stamptemplate extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{   
		$this->_controller = "adminhtml_stamptemplate";
		$this->_blockGroup = 'text2image';
		$this->_headerText = Mage::helper("text2image")->__("Manage Stamptemplate");
	    parent::__construct();
	    
        if (true ) { 
            $this->_updateButton('add', 'label', Mage::helper('text2image')->__('Add New Stamptemplate'));
        } else {
            $this->_removeButton('add');
        }
	}
	/**
	 * Check permission for passed action
	 *
	 * @param string $action
	 * @return bool
	 */
	protected function _isAllowedAction($action)
	{
	    return Mage::getSingleton('admin/session')->isAllowed('text2image/adminhtml/stamptemplate/' . $action);
	}
}