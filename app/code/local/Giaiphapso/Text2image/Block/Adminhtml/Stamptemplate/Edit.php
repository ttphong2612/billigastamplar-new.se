<?php 

class Giaiphapso_Text2image_Block_Adminhtml_Stamptemplate_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'template_id';
        $this->_blockGroup = 'text2image';
        $this->_controller = 'adminhtml_stamptemplate';
        
        parent::__construct();
        //fixbug missing back button (same sort_order with reset button) 
        $this->_updateButton('back','sort_order',10);
        
        if ($this->_isAllowedAction('save')) {
            $this->_updateButton('save', 'label', Mage::helper('text2image')->__('Save Stamptemplate'));
            $this->_addButton('saveandcontinue', array(
                    'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
                    'onclick'   => 'saveAndContinueEdit(\''.$this->_getSaveAndContinueUrl().'\')',
                    'class'     => 'save',
            ), -100);
            
        } else {
            $this->_removeButton('save');
        }
        
        if ($this->_isAllowedAction('delete')) {
            $this->_updateButton('delete', 'label', Mage::helper('text2image')->__('Delete Page'));
        } else {
            $this->_removeButton('delete');
        }
        
    }
    
    public function getHeaderText()
    {
        if( Mage::registry('text2image_stamptemplate') && Mage::registry('text2image_stamptemplate')->getId() ) {
            return Mage::helper('text2image')->__("Edit Stamptemplate '%s'", $this->htmlEscape(Mage::registry('text2image_stamptemplate')->getTitle()));
        } else {
            return Mage::helper('text2image')->__('Add Stamptemplate');
        }
    }
    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('text2image/adminhtml/stamptemplate/' . $action);
    }
    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
                '_current'   => true,
                'back'       => 'edit',
                'active_tab' => '{{tab_id}}'
        ));
    }
    /**
     * Prepare layout
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _prepareLayout()
    {
        $tabsBlock = $this->getLayout()->getBlock('text2image/adminhtml_stamptemplate_edit_tabs');
        if ($tabsBlock) {
            $tabsBlockJsObject = $tabsBlock->getJsObjectName();
            $tabsBlockPrefix   = $tabsBlock->getId() . '_';
        } else {
            $tabsBlockJsObject = 'stamptemplate_tabsJsTabs';
            $tabsBlockPrefix   = 'stamptemplate_tabs_';
        }
        
        $this->_formScripts[] = "
        function toggleEditor() {
            if (tinyMCE.getInstanceById('stamptemplate_content') == null) {
                tinyMCE.execCommand('mceAddControl', false, 'stamptemplate_content');
            } else {
                tinyMCE.execCommand('mceRemoveControl', false, 'stamptemplate_content');
            }
        }
        
        function saveAndContinueEdit(urlTemplate) {
            var tabsIdValue = " . $tabsBlockJsObject . ".activeTab.id;
            var tabsBlockPrefix = '" . $tabsBlockPrefix . "';
            if (tabsIdValue.startsWith(tabsBlockPrefix)) {
                tabsIdValue = tabsIdValue.substr(tabsBlockPrefix.length)
            }
            var template = new Template(urlTemplate, /(^|.|\\r|\\n)({{(\w+)}})/);
            var url = template.evaluate({tab_id:tabsIdValue});
            editForm.submit(url);
        }
        ";
        return parent::_prepareLayout();
    }
}