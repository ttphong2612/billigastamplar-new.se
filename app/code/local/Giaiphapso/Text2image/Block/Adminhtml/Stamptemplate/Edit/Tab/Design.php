<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Giaiphapso_Text2image_Block_Adminhtml_Stamptemplate_Edit_Tab_Design
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public $templateModel;
    public $rowNum = 0;
    public $heightUsed = 0;
    
    public function __construct()
    {
        parent::__construct();
        $this->setShowGlobalIcon(true);
        $this->setTemplate('text2image/design.phtml');
    }
    public function _prepareLayout()
    {
       
        return parent::_prepareLayout();
    }
    
    protected function _prepareForm()
    {
        /* @var $this->templateModel Giaiphapso_Text2image_Model_Stamptemplate */
        $this->templateModel = Mage::registry('text2image_stamptemplate');
        $this->generateAllTextLine();
        $this->setData('model',$this->templateModel);
    }
    public function generateAllTextLine(){
        $arrayObject = array();
        $allTextLine = '';
        $allEditorLine = '';
        $allHiddenField = '';
        if($this->templateModel->getData('elements')){
            $textLines = Mage::helper('core')->jsonDecode($this->templateModel->getData('elements'));
            if(is_array($textLines)){
                foreach($textLines as $id => $objectLine){
                    $this->rowNum++;
                    $allTextLine .= $this->_generateTextLine($id, $objectLine);
                    $allEditorLine .= $this->_generateEditorLine($id, $objectLine);
                    $allHiddenField .= $this->_generateHiddenField($id, $objectLine);
                }
            }
        }
        $this->allEditorLine = $allEditorLine;
        $this->allTextLine = $allTextLine;
        $this->allHiddenField = $allHiddenField;
    }
    public function generateImage(Giaiphapso_Text2image_Model_Textplate $line){
        $image = '<div >';
        
        
    }
    protected  function _generateHiddenField($id, $objectLine){
        $fieldHTML = '<div id="hidden_'.id.'">';
        
        $fieldHTML .= '<input type="hidden" id="hidden_text_'+id+'" name="data[text][]" value="'.$objectLine['text'].'" />';
        $fieldHTML .= '<input type="hidden" id="hidden_font_'+id+'" name="data[font][]" value="'.$objectLine['font'].'" />';
        $fieldHTML .= '<input type="hidden" id="hidden_size_'+id+'" name="data[size][]" value="'.$objectLine['size'].'" />';
        $fieldHTML .= '<input type="hidden" id="hidden_type_'+id+'" name="data[type][]" value="'.$objectLine['type'].'" />';
        $fieldHTML .= '<input type="hidden" id="hidden_left_'+id+'" name="data[left][]" value="'.$objectLine['left'].'" />';
        $fieldHTML .= '<input type="hidden" id="hidden_top_'+id+'" name="data[top][]" value="'.$objectLine['top'].'" />';
        $fieldHTML .= '<input type="hidden" id="hidden_width_'+id+'" name="data[width][]" value="'.$objectLine['width'].'" />';
        $fieldHTML .= '<input type="hidden" id="hidden_height_'+id+'" name="data[height][]" value="'.$objectLine['height'].'" />';
        $fieldHTML .= '<input type="hidden" id="hidden_style_'+id+'" name="data[style][]" value="'.$objectLine['style'].'" />';
        $fieldHTML .= '<input type="hidden" id="hidden_color_'+id+'" name="data[color][]" value="'.$objectLine['color'].'" />';
        $fieldHTML .= '</div>';
        return $fieldHTML;
    }
    protected  function _generateEditorLine($id, $objectLine){
        $htmlEditorLine = '';
        if($objectLine['type'] == 'text'){
            //generate input text
            $htmlEditorLine .= '<div id="line'.$id.'" class="text_design_input">';
            $htmlEditorLine .= '<div class="inputtext_template">
                                    <input id="'.$id.'" type="text" name="text" title="Text line '.$id.'" value="'.$objectLine['text'].'">
                                </div>';
            //generate select font
            $htmlEditorLine .= '<div class="inputfont">';
            $htmlEditorLine .= $this->getFontSelect('font'.$id, 'font' , $objectLine['font']);             
            $htmlEditorLine .= '</div>';
            //generate select size
            $htmlEditorLine .= '<div class="inputsize">';
            $htmlEditorLine .= $this->getSizeSelect('size'.$id, 'size' , $objectLine['size']);
            $htmlEditorLine .= '</div>';
            //generate checkbox font style
            $checkedBold = $checkedItalic = $checkedUnderline = '';
            if(strpos($objectLine['style'],'b') !== false){
                $checkedBold = 'checked="checked"';
            }
            if(strpos($objectLine['style'],'i') !== false){
                $checkedItalic = 'checked="checked"';
            }
            if(strpos($objectLine['style'],'u') !== false){
                $checkedUnderline = 'checked="checked"';
            }
            $htmlEditorLine .= '<div class="checkboxtext">';
            $htmlEditorLine .= '<input type="checkbox" name="data[bold][]" title="Bold line '.$id.'" '. $checkedBold . '>';
            $htmlEditorLine .= '<input type="checkbox" name="data[italic][]" title="Italic line '.$id.'" '. $checkedItalic . '>';
            $htmlEditorLine .= '<input type="checkbox" name="data[underline][]" title="Underline line '.$id.'" '. $checkedUnderline . '>';
            $htmlEditorLine .= '</div>';
            //generate alignment
            $htmlEditorLine .= '<div class="standarcen1">';
            $htmlEditorLine .= '<a id="'.$id.'" href="javascript:void(0)">';
            $htmlEditorLine .= '<span class="alginment justifyleft"></span></a>
                                <a href="javascript:void(0)"><span class="alginment justifycenter active"></span></a>
                                <a href="javascript:void(0)"><span class="alginment justifyright"></span></a>
                               </div>';
            //generate distance 2 character
            /*$htmlEditorLine .= '<div class="standarcen1">';
            $htmlEditorLine .= '<a id="'.$id.'" href="javascript:void(0)">';
            $htmlEditorLine .= '<span class="alginment justifyleft"></span></a>
                                <a href="javascript:void(0)"><span class="alginment justifycenter active"></span></a>
                                <a href="javascript:void(0)"><span class="alginment justifyright"></span></a>
                               </div>';*/
            //generate delete button
            $htmlEditorLine .= '<div class="standarcheck"><a id="'.$id.'" href="javascript:void(0)"></a></div>';
            $htmlEditorLine .= '</div>';
            
        }
        return $htmlEditorLine;
            
    }
    protected function _generateTextLine($id, $objectLine){
        $textline = '';
        if($objectLine['type'] == 'text'){
            $textline .= '<div id="txt_'.$id.'" class="text_image" style="position:absolute;top:'.$objectLine['top'].'px;left:'.$objectLine['left'].'px">';
            $textBase64 = base64_encode($objectLine['text']);
            $textline .= '<img id="img_'.$id.'" src="'.$this->getBaseUrl().'text2image/image/text/'.$objectLine['font'].'/'.$objectLine['size'].'/1.3/'.$objectLine['color'].'/'.$textBase64.'/'.$objectLine['style'].'"></div>';
        }elseif($objectLine['type'] == 'image'){
            $textline .= '<div id="txt_'.$id.'" class="text_image" style="position:absolute;top:'.$objectLine['top'].'px;left:'.$objectLine['left'].'px">';
            $textline .= '<img id="img_'.$id.'" src="'.$this->getBaseUrl().'text2image/image/thumbnail/'.$this->templateModel->width.'/'.$objectLine['text'].'"></div>';
        }
        return $textline; 
    }
    public function getTextAlignButton($id, $type, $value){
        return '<a href="#">'.$type.'</a>';
    }
    public function getTextInput($id, $value=''){
        
        return '<input type="text" name="text[]" value="'. $value .'" title="Text line '. $id .'" />'; 
    }
    public function getFontStyleCheckbox($id, $value='', $type){
    
        return '<input type="checkbox" name="'. $type .'[]" value="'. $value .'" title="Bold line '. $id .'" />';
    }
    public function getFontSelect($id, $name , $value=''){
        $select = Mage::app()->getLayout()->createBlock('core/html_select')
                ->setName($name)
                ->setId($id)
                ->setTitle('Font for line '. $id)
                ->setValue($value)
                ->setOptions(Mage::helper('text2image')->getFontList());
                return $select->getHtml();
    }
    public function getSizeSelect($id, $name, $value=''){
        $select = Mage::app()->getLayout()->createBlock('core/html_select')
        ->setName($name)
        ->setId($id)
        ->setTitle('Size for line '.$id)
        ->setValue($value)
        ->setOptions(Mage::helper('text2image')->getSizeList());
        return $select->getHtml();
    }
    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('text2image')->__('Design');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('text2image')->__('Design');
    }

    /**
     * Returns status flag about this tab can be showen or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('text2image/stamptemplate/' . $action);
    }
}
