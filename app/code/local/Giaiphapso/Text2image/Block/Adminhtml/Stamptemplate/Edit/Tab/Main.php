<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Cms page edit form main tab
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Giaiphapso_Text2image_Block_Adminhtml_Stamptemplate_Edit_Tab_Main
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _prepareForm()
    {   
        /* @var $model Mage_Cms_Model_Page */
        $model = Mage::registry('text2image_stamptemplate');
        /*
         * Checking if user have permissions to save information
         */
        if ($this->_isAllowedAction('save')) {
            $isElementDisabled = false;
        } else {
            $isElementDisabled = true;
        }


        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('stamptemplate_');

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('text2image')->__('Stamptemplate Information')));

        if ($model->getId()) {
            $fieldset->addField('template_id', 'hidden', array(
                'name' => 'template_id',
            ));
        }
       
        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'label'     => Mage::helper('text2image')->__('Stamptemplate Title'),
            'title'     => Mage::helper('text2image')->__('Stamptemplate Title'),
            'required'  => true,
            'disabled'  => $isElementDisabled
        ));

        $event = $fieldset->addField('textplate_size', 'select', array(
                'name'      => 'textplate_size',
                'label'     => Mage::helper('text2image')->__('Stamptemplate size'),
                'title'     => Mage::helper('text2image')->__('Stamptemplate size'),
                'required'  => true,
                'options'   => array(''=>'') +  $model->getAvailableSize(),
                'disabled'  => $isElementDisabled,
                'onchange'  => 'ReloadTextplate(this.value)',
        ));
        $event->setAfterElementHtml("<script type=\"text/javascript\">
                        function ReloadTextplate(textplate_size){
                            var reloadurl = '". $this->getUrl('text2image/adminhtml_ajax/gettextplate/')."';
                            reloadurl += 'textplate_size/' + textplate_size;
                            new Ajax.Request(reloadurl, {
                                method: 'get',
                                onLoading: function (transport) {
                                   
                                },
                                onComplete: function(transport) {
                                    $('stamptemplate_product_id').innerHTML = '';
                                    var json = transport.responseText.evalJSON();
                                    json.each(function(element){
                                        $('stamptemplate_product_id').options.add(new Option(element.label, element.value));
                                    });
                                }
                            });
                            var widthheight = $('stamptemplate_textplate_size').select('option[selected]')[0].innerHTML;
                            widthheight = widthheight.toLowerCase();
                            var arr = widthheight.split('x'); 
                            arr[0] = arr[0]*3.77952775905;
                            arr[1] = arr[1]*3.77952775905;
                            $('texplate_design_content').setStyle({
                        		width: arr[0] + 'px',
                                height: arr[1] + 'px',
                        	});
                            width_template = arr[0];
                            height_template = arr[1];
                        }
                    </script>");     
        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $field = $fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('text2image')->__('Store View'),
                'title'     => Mage::helper('text2image')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
                'disabled'  => $isElementDisabled,
            ));
            $renderer = $this->getLayout()->createBlock('adminhtml/store_switcher_form_renderer_fieldset_element');
            $field->setRenderer($renderer);
        }
        else {
            $fieldset->addField('store_id', 'hidden', array(
                'name'      => 'stores[]',
                'value'     => Mage::app()->getStore(true)->getId()
            ));
            $model->setStoreId(Mage::app()->getStore(true)->getId());
        }
        //multi product
        $fieldset->addField('product_id', 'select', array(
                'name'      => 'product_id[]',
                'label'     => Mage::helper('text2image')->__('Apply for product(s)'),
                'title'     => Mage::helper('text2image')->__('Apply for product(s)'),
                'required'  => true,
                'values'    => $model->getProductTextplateForForm($model->getData('textplate_size')),
                'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('datefield', 'select', array(
                'label'     => Mage::helper('text2image')->__('Is Date Field'),
                'title'     => Mage::helper('text2image')->__('Is Date Field'),
                'name'      => 'datefield',
                'required'  => true,
                'options'   => $model->getAvailableStatuses(),
                'disabled'  => $isElementDisabled,
        ));
        $fieldset->addField('is_active', 'select', array(
            'label'     => Mage::helper('text2image')->__('Status'),
            'title'     => Mage::helper('text2image')->__('Stamptemplate Status'),
            'name'      => 'is_active',
            'required'  => true,
            'options'   => $model->getAvailableStatuses(),
            'disabled'  => $isElementDisabled,
        ));
        if (!$model->getId()) {
            $model->setData('is_active', $isElementDisabled ? '0' : '1');
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('text2image')->__('Stamptemplate Information');
    }

    /**
     * Prepare title for tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('text2image')->__('Stamptemplate Information');
    }

    /**
     * Returns status flag about this tab can be shown or not
     *
     * @return true
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Returns status flag about this tab hidden or not
     *
     * @return true
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Check permission for passed action
     *
     * @param string $action
     * @return bool
     */
    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('text2image/stamptemplate/' . $action);
    }
}
