<?php
class Giaiphapso_Text2image_Block_Adminhtml_Stamptemplate_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    { 
        parent::__construct();
        $this->setId('stamptemplate_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('text2image')->__('Stamptemplate Information'));
    }
}