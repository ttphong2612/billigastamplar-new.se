<?php

class Giaiphapso_Text2image_Block_Adminhtml_Stamptemplate_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct(); 
		$this->setId('text2imageStamptemplateGrid');
		$this->setDefaultSort('template_id');
		$this->setDefaultDir('ASC');
	}

	protected function _prepareCollection()
	{
		$collection = Mage::getModel('text2image/stamptemplate')->getCollection();
		$collection->setFirstStoreFlag(true);
		$this->setCollection($collection);
		return parent::_prepareCollection(); 
	}

	protected function _prepareColumns()
	{ 
		$this->addColumn('template_id', array(
				'header'    => Mage::helper('text2image')->__('ID'),
				'align'     => 'center',
				'width'     => '50px',
				'index'     => 'template_id',
		));

		$this->addColumn('title', array(
				'header'    => Mage::helper('text2image')->__('Stamptemplate name'),
				'align'     =>'left',
				'index'     => 'title',
		));
		$this->addColumn('product_id', array(
		        'header'    => Mage::helper('text2image')->__('Textplate name'),
		        'index'     => 'product_id',
		        'type'      => 'options',
		        'width'     => '150px',
		        'options'   => Mage::getSingleton('text2image/stamptemplate')->getAllTextplateSelect(),
		        'filter_condition_callback'   => array($this, '_filterTextplateCondition'),
		));
		$this->addColumn('textplate_size', array(
		        'header'    => Mage::helper('text2image')->__('Stamptemplate size (Width x Height) mm'),
		        'index'     => 'textplate_size',
		        'type'      => 'options',
		        'width'     => '150px',
		        'options'   => Mage::getSingleton('text2image/stamptemplate')->getAvailableSize()
		));
	    /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) { 
            $this->addColumn('store_id', array(
                'header'        => Mage::helper('text2image')->__('Store View'),
                'index'         => 'store_id',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
                ));
        }
        $this->addColumn('weight', array(
                'header'    => Mage::helper('text2image')->__('Sort'),
                'index'     => 'weight',
                'type'      => 'text',
                'width'     => '50px',
                'renderer'  => 'text2image/adminhtml_stamptemplate_renderer_weight',
        ));
        $this->addColumn('is_active', array(
                'header'    => Mage::helper('text2image')->__('Status'),
                'index'     => 'is_active',
                'type'      => 'options',
                'options'   => Mage::getSingleton('text2image/stamptemplate')->getAvailableStatuses()
        ));
		$this->addColumn('action',
				array(
						'header'    =>  Mage::helper('text2image')->__('Action'),
						'width'     => '100',
						'type'      => 'action',
						'getter'    => 'getId',
						'actions'   => array(
								array(
										'caption'   => Mage::helper('text2image')->__('Edit'),
										'url'       => array('base'=> '*/*/edit'),
										'field'     => 'template_id'
								)
						),
						'filter'    => false,
						'sortable'  => false,
						'index'     => 'stores',
						'is_system' => true,
				));
		 
		return parent::_prepareColumns();
	}
	protected function _afterLoadCollection()
	{
	    $this->getCollection()->walk('afterLoad');
	    parent::_afterLoadCollection();
	}
    protected function _filterStoreCondition($collection, $column)
    {   
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addStoreFilter($value);
    }
    protected function _filterTextplateCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
    
        $this->getCollection()->addTextplateFilter($value);
    }
	protected function _prepareMassaction()
	{
		$this->setMassactionIdField('template_id');
		$this->getMassactionBlock()->setFormFieldName('stamptemplate');

		$this->getMassactionBlock()->addItem('delete', array(
				'label'    => Mage::helper('text2image')->__('Delete'),
				'url'      => $this->getUrl('*/*/massDelete'),
				'confirm'  => Mage::helper('text2image')->__('Are you sure?')
		));
        
		$this->getMassactionBlock()->addItem('sort', array(
		        'label'    => Mage::helper('text2image')->__('Sort template'),
		        'url'      => $this->getUrl('*/*/massSort'),
		        'confirm'  => Mage::helper('text2image')->__('Are you sure?')
		));
		
 		$statuses = Mage::getSingleton('text2image/status')->getOptionArray();
		$this->getMassactionBlock()->addItem('status', array(
				'label'=> Mage::helper('text2image')->__('Change status'),
				'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
				'additional' => array(
						'visibility' => array(
								'name' => 'status',
								'type' => 'select',
								'class' => 'required-entry',
								'label' => Mage::helper('text2image')->__('Status'),
								'values' => $statuses
						)
				)
		));
		return $this;
	}

	public function getRowUrl($row)
	{
		return $this->getUrl('*/*/edit', array('template_id' => $row->getId()));
	}

}