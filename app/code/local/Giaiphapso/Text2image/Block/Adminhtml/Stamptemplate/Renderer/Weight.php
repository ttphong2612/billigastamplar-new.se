<?php 
class Giaiphapso_Text2image_Block_Adminhtml_Stamptemplate_Renderer_Weight extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract{
     
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
        $id = $row->getId();
        $html = '<input id="weight'.$id.'" type="text" size="5" name="weights" onchange="updateSort(this,'.$id.');" value="'.$value.'" />';
        return $html;
    }
}
