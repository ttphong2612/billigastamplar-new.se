<?php
 
class Giaiphapso_Text2image_Block_Adminhtml_Tabs extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs
{
    private $parent;
 
    protected function _prepareLayout()
    {
        //get all existing tabs
        $this->parent = parent::_prepareLayout();
        //add new tab
        $product = $this->getProduct();
        $attributeSetName = Mage::getModel('eav/entity_attribute_set')->load($product->getAttributeSetId())->getAttributeSetName();
        if(strtolower($attributeSetName) == 'textplate'){
            $this->addTab('template', array(
                    'label'     => Mage::helper('catalog')->__('Associate template'),
                    'content'   => $this->getLayout()
                    ->createBlock('text2image/adminhtml_tabs_template')->toHtml(),
            ));
        }
        
        return $this->parent; 
    }
}