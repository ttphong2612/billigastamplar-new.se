<?php
 
class Giaiphapso_Text2image_Block_Adminhtml_Tabs_Template extends Mage_Adminhtml_Block_Widget
{
    public function __construct()
    {
        parent::__construct();
        $product = $this->getProduct();
        $this->getTemplateList();
        $productId = $product->getId();
        $this->setTemplate('text2image/tabs/template.phtml');
    }
    public function getProduct()
    {
        if (!($this->getData('product') instanceof Mage_Catalog_Model_Product)) {
            $this->setData('product', Mage::registry('product'));
        }
        return $this->getData('product');
    }
    public function getTemplateList(){
        $textplateArray = Mage::getModel('text2image/stamptemplate')
                        ->getCollection()
                        ->addStoreFilter($this->getRequest()->getParam('store', 30));
    }
}