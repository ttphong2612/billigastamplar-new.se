<?php 
class Giaiphapso_Text2image_Block_Stamptemplate extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {   
		return parent::_prepareLayout();
    }
    
     public function getStamptemplate()     
     { 
        if (!$this->hasData('stamptemplate')) {
            $this->setData('stamptemplate', Mage::registry('text2image_stamptemplate')); 
        }
        return $this->getData('stamptemplate');
        
    }
}