<?php 
    class Giaiphapso_Text2image_Block_Text2image extends Mage_Catalog_Block_Product_Abstract /*Mage_Core_Block_TemplateMage_Catalog_Block_Product_Abstract*/
    {
        public function _prepareLayout()
        {
            $headBlock = $this->getLayout()->getBlock('head');
            if ($headBlock) { 
                $product = $this->getProduct();
                $title = $product->getMetaTitle();
                if ($title) {
                    $headBlock->setTitle($title);
                }
                $keyword = $product->getMetaKeyword();
                $currentCategory = Mage::registry('current_category');
                if ($keyword) {
                    $headBlock->setKeywords($keyword);
                } elseif($currentCategory) {
                    $headBlock->setKeywords($product->getName());
                }
                $description = $product->getMetaDescription();
                if ($description) {
                    $headBlock->setDescription( ($description) );
                } else {
                    $headBlock->setDescription(Mage::helper('core/string')->substr($product->getDescription(), 0, 255));
                }
                if ($this->helper('catalog/product')->canUseCanonicalTag()) {
                    $params = array('_ignore_category'=>true);
                    $headBlock->addLinkRel('canonical', $product->getUrlModel()->getUrl($product, $params));
                }
            }
            
			$textplateDesigned = $this->getTextplateDesinged();
            return parent::_prepareLayout();
        }
		public function getTextplateDesinged()
		{
			$textplateDesigned = $this->getData('data');
			if(empty($textplateDesigned)){
				$textplateDesigned = $this->getRequest()->getParam('data');
				$textplateDesigned = Mage::helper('text2image')->convertSerialize2Array($textplateDesigned);
				return $textplateDesigned;
			}
		}
		protected function _initTextPlate()
		{
		    $color = array(144 => 'red', 143=>'blue', 142 => 'black', 145 => 'red-blue');
			
		    if($this->getProduct()->getTypeId() == 'bundle'){
		        $_options = $this->getProduct()->getTypeInstance(true)
		        ->getSelectionsCollection($this->getProduct()->getTypeInstance(true)->getOptionsIds($this->getProduct()), $this->getProduct());
		
		        $textplateId = $this->getRequest()->getParam('textplateId');
		
		        foreach($_options as $option){
		            $attributeSetName = Mage::getModel("eav/entity_attribute_set")
		            ->load($option->getAttributeSetId())
		            ->getAttributeSetName();
		
		            if(strtolower($attributeSetName) == 'textplate' || strcmp($attributeSetName,'Stockstamps Textplate') == 0){
		                if( $textplateId ){
		                    $productTextplate = Mage::getModel('catalog/product')->load($textplateId);
		                    $data['textplate_id'] = $textplateId;
		                }else{
		                    $data['textplate_id'] = $option->getId();
		                    $productTextplate = $option;
		                }
		            }elseif(strtolower($attributeSetName) == 'inkpad'){
		                $data['inkpad_id'][$color[$option->getData('color')]] = $option->getId();
		            }elseif(strtolower($attributeSetName) == 'machine'){
		                $data['machine_id'] = $option->getId();
		            }
		        }
		    }elseif($this->getProduct()->getTypeId() == 'simple' && in_array($this->getProduct()->getAttributeSetId(), unserialize(TEXTPLATE_ATTRIBUTE_SET_ID))){
		        $productTextplate = $this->getProduct();
		    }else{
		        return null;
		    }
		    Mage::register('current_textplate', $productTextplate);
			if(!Mage::registry('current_data')){
				 Mage::register('current_data', $data);
			}
		    return $productTextplate;
		}
		/**
		 * Retrieve current product model
		 *
		 * @return Mage_Catalog_Model_Product
		 */
		public function getProduct()
		{
		    if (!Mage::registry('product') && $this->getProductId()) {
		        $product = Mage::getModel('catalog/product')->load($this->getProductId());
		        Mage::register('product', $product);
		    }
		    return Mage::registry('product');
		}
        
		public function getTextplate()
		{
		    if (!Mage::registry('current_textplate') ) {
		        $this->_initTextplate();
		    }
		    return Mage::registry('current_textplate');
		}
	
		/* public function getTextplate()
		{
			$_product = $this->getProduct();
			$productType = $_product->getTypeID();
			if($productType == 'bundle'){
				
			}
				
			if($_product->getAttributeSetId() == SEALS_ATTRIBUTE_SET_ID){ 
				$textplateId      = $this->getRequest()->getParam('textplateId');
				$textplateProduct = Mage::getModel('catalog/product')->load($textplateId);
				$this->shape = $textplateProduct->getResource()->getAttribute('shape')->getSource()->getOptionText($textplateProduct->getData('shape'));
				$model = Mage::getSingleton('text2image/stamptemplate');
				$availabeSize = $model->getAvailableSize();
				$textplateProduct->setData('textplate_size', $availabeSize[$textplateProduct->getData('textplate_size')]);
				$this->setData('productTextplate',$textplateProduct); 
			}else{
				$textplate_id = Mage::registry('textplate_id');
				$textplateProduct = Mage::getSingleton('catalog/product')->load($textplate_id);
				$model = Mage::getSingleton('text2image/stamptemplate');
				$availabeSize = $model->getAvailableSize();
				$textplateProduct->setData('textplate_size', $availabeSize[$textplateProduct->getData('textplate_size')]);
				$this->setData('productTextplate',$textplateProduct); 
			}
			return $textplateProduct;
		} */
        
    }