<?php
    class Giaiphapso_Text2image_Block_Textplate_Confirm extends Mage_Catalog_Block_Product_Abstract
    {
        public $seal  = 0;
        public $shape = 0;

        public function _prepareLayout()
        {
            return parent::_prepareLayout();
        }

        public function __construct()
        {
            $product_id  = $this->getRequest()->getParam('product_id');
            $textplateId = $this->getRequest()->getParam('textplate_id');
            $_product = Mage::getSingleton('catalog/product')->load($product_id);

            $elements = array();
            $elements = $this->getRequest()->getParam('data');
            $elements['auto_size'] = $this->getRequest()->getParam('auto_size');
            $elements['auto_width'] = $this->getRequest()->getParam('auto_width');
            $elements['has_border'] = $this->getRequest()->getParam('has_border');
            $elements['border_style'] = $this->getRequest()->getParam('border_style');
            $elements['border_width'] = $this->getRequest()->getParam('border_width');
            $elements['inkpad_color'] = $this->getRequest()->getParam('inkpad_color');
            $elements['zoom_size'] = 0;
            $elements['template_height'] = $this->getRequest()->getParam('template_height');
            $elements['template_width'] = $this->getRequest()->getParam('template_width');
            $elements['templateType'] = $this->getRequest()->getParam('templateType');
            $elements['textplate_id'] = $this->getRequest()->getParam('textplate_id');
            $elements['template_id'] = $this->getRequest()->getParam('template_id');
            $elements['shape'] = $this->getRequest()->getParam('shape');
            $elements['line_height'] = $this->getRequest()->getParam('line_height');
            //$encryptElement = base64_encode(serialize($elements)); // comment by phong.tran
            $encryptElement = base64_encode(gzcompress(serialize($elements),9)); // add by phong.tran to tiny url when send to outlook (outlook limit character in url)
            $data           = str_replace(array('/', '+'), array('_', '-'), $encryptElement);
            //$data = urlencode($encryptElement); // comment by phong.tran

            // save template designed to cookies
            /*$period = 7*24*60*60;
            Mage::getSingleton('core/cookie')->set('textplateDesigned'.$elements['textplate_id'], $data, $period); */
            
            if(in_array($_product['attribute_set_id'], unserialize(TEXTPLATE_ATTRIBUTE_SET_ID)))
            {
                Mage::register('textplate_id', $_product->getId());
                $this->setData('productTextplate',$_product);
            }
            else
            {   
                $_options = $_product->getTypeInstance(true)
                ->getSelectionsCollection($_product->getTypeInstance(true)->getOptionsIds($_product), $_product);

                if($_product->getData('attribute_set_id') == SEALS_ATTRIBUTE_SET_ID){
                    $this->seal = 1;
                    $this->repareSealData($_options);
                }else{
                    $this->repareData($_options, $product_id, $textplateId);
                }
            }
            $redesignUrl = $product_id. '.'. $textplateId . '.'. $_product->getUrlKey(). '.html/'. $data;
            $this->setData('dataurl',$data);
            $this->setData('product',$_product);
            $this->setData('redesignurl',$redesignUrl);
        }

        public function repareData($_options, $productId, $textplateId)
        {
            $inkpad_id = $this->getRequest()->getParam('inkpad_id');
            $data      = array();
            $color     = array(26 => 'red', 25=>'blue', 24 => 'black');
            $productTextplate = '';

            foreach($_options as $option){
                $attributeSetName = Mage::getModel("eav/entity_attribute_set")
                ->load($option->getAttributeSetId())
                ->getAttributeSetName();

                if((strtolower($attributeSetName) == 'textplate' || strcmp($attributeSetName,'Stockstamps Textplate') == 0) && empty($productTextplate)){
                    if( $productId == STOCKSTAMP_BUNDLE_ID && $option->getId() == $textplateId){
                        $productTextplate = $option;
                        $data['textplate_id'] = $textplateId;
                    }elseif(strtolower($attributeSetName) == 'textplate'){
                        $data['textplate_id'] = $option->getId();    
                        $productTextplate = $option;
                    }
                    $this->setData('productTextplate',$productTextplate);
                    Mage::unregister('textplate_id');
                    Mage::register('textplate_id', $data['textplate_id']);
                }elseif(strtolower($attributeSetName) == 'inkpad' && $option->getId() == $inkpad_id){
                    $this->setData('productInkpad',$option);
                }elseif(strtolower($attributeSetName) == 'machine'){
                    $this->setData('productMachine',$option);
                }
            }  
        }

        public function repareSealData($_options)
        {
            $textplate_id = $this->getRequest()->getParam('textplate_id');
            $stickers_id  = $this->getRequest()->getParam('stickers_id');

            $stickers_id  = trim($stickers_id, ',');
            $stickers_id  = explode(',', $stickers_id);

            $productStickers = array();

            foreach($_options as $option)
            { 
                $attributeSetName = Mage::getModel("eav/entity_attribute_set")
                ->load($option->getAttributeSetId())
                ->getAttributeSetName(); 

                if(strtolower($attributeSetName) == 'textplate' && $option->getId() == $textplate_id){
                    $this->setData('productTextplate',$option);
                }elseif(strtolower($attributeSetName) == 'machine'){
                    $this->setData('productMachine',$option);
                }elseif(strtolower($attributeSetName) == 'stickers' && in_array($option->getId(), $stickers_id)){
                    $productStickers[] = $option;
                }
            }

            $this->setData('productStickers', $productStickers);    
        }
}