<?php 
    class Giaiphapso_Text2image_Block_Textplate_Product extends Giaiphapso_Text2image_Block_Text2image
    {
        /* public function __construct()
        {test
        	
        } */

        /* public function __construct()
        {
            parent::__construct();

            $productId    = $this->getRequest()->getParam('id');
            $textplateId  = $this->getRequest()->getParam('textplateId');
            $inkpad       = $this->getRequest()->getParam('inkpad');
            $inkpad       = ($inkpad)?$inkpad:'black';
            
            Mage::register('inkpad', $inkpad);
            Mage::register('textplate_id', $textplateId);

            $model = Mage::getSingleton('text2image/stamptemplate');
            $availabeSize = $model->getAvailableSize();
            $this->setData('availabeSize',$availabeSize);

            $_product = Mage::getSingleton('catalog/product')->load($productId);
            $this->product_id = $productId;

            if($_product['attribute_set_id'] == 63) // Texplate product
            {
                Mage::unregister('textplate_id');
                Mage::register('textplate_id', $productId);
                $this->setData('productTextplate',$_product);
            }
            elseif($_product['type_id'] == 'bundle')  // Bundle product
            {
                $_options = $_product->getTypeInstance(true)
                ->getSelectionsCollection($_product->getTypeInstance(true)->getOptionsIds($_product), $_product);

                if($_product->getData('attribute_set_id') == SEALS_ATTRIBUTE_SET_ID)
                {
                    $this->seal = 1;
                    $this->repareSealData($textplateId);
                }else{
                    $this->repareData($_options, $textplateId);
                }   
            }  
        } */
    	
    	public function getProductSetType()
    	{
    		if(!$this->getData('product_set_type')){
    			$this->setData('product_set_type', $this->helper('text2image')->productSetType($this->getProduct()->getAttributeSetId()) );
    		}
    		return $this->getData('product_set_type');
    	}
    	
    	public function getTextplateShap()
    	{
    	    if(!$this->getData('product_shape')){
    	    	$shape = $this->getTextPlate()->getResource()->getAttribute('shape')->getSource()->getOptionText($this->getTextPlate()->getData('shape'));
    	    	$shape = $shape?$shape:'square';
    	        $this->setData('product_shape', $shape);
    	    }
    	    return $this->getData('product_shape');
    	}
    	public function getTextplateData()
    	{
    		if (!Mage::registry('current_data')) {
    			if($this->getProduct()->getTypeId() == 'bundle'){
    			    $_options = $this->getProduct()->getTypeInstance(true)
    			    ->getSelectionsCollection($this->getProduct()->getTypeInstance(true)->getOptionsIds($this->getProduct()), $this->getProduct());
    			    $data = $this->repareData($_options);
    			}
		        
		        Mage::register('current_data', $data);
    		}
    		return Mage::registry('current_data');
    	}
    	public function _toHtml()
    	{
    		if($this->getProductSetType() === 'seal')
    	   		 $this->setTemplate('text2image/textplate/require/seal.phtml');
    		else
    			$this->setTemplate('text2image/textplate/require/other.phtml');
    	    return parent::_toHtml();
    	}
        public function repareData($_options)
        {
            $data = array();
            $productTextplate = '';
            $color = array(144 => 'red', 143=>'blue', 142 => 'black', 145 => 'red-blue');
            foreach($_options as $option){ 
                $attributeSetName = Mage::getModel("eav/entity_attribute_set")
                ->load($option->getAttributeSetId())
                ->getAttributeSetName(); 
                
                if(strtolower($attributeSetName) == 'textplate' || strcmp($attributeSetName,'Stockstamps Textplate') == 0){                    
                    if( $textplateId ){
                        $productTextplate = Mage::getModel('catalog/product')->load($textplateId);
                        $data['textplate_id'] = $textplateId;
                    }else{ 
                        $data['textplate_id'] = $option->getId();
                        $productTextplate = $option;
                    }
                }elseif(strtolower($attributeSetName) == 'inkpad'){ 
                    $data['inkpad_id'][$color[$option->getData('color')]] = $option->getId();
                }elseif(strtolower($attributeSetName) == 'machine'){
                    $data['machine_id'] = $option->getId();
                }
            }
            return json_encode($data);        
        }

        public function repareSealData($textplateId)
        {
            $productTextplate = Mage::getModel('catalog/product')->load($textplateId);
            $this->shape = $productTextplate->getResource()->getAttribute('shape')->getSource()->getOptionText($productTextplate->getData('shape'));
            $this->setData('productTextplate',$productTextplate); 
        }
}