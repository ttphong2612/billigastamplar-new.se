<?php
class Giaiphapso_Text2image_Block_Textplate_Sticker extends Mage_Core_Block_Template
{
	function getStickers()
	{
	    $products = Mage::getModel('catalog/product')
	    ->getCollection()
	    ->addAttributeToSelect(array('id', 'name', 'price'))
	    ->addStoreFilter()
	    ->addWebsiteFilter()
	    ->addAttributeToFilter("attribute_set_id", STICKERS_ATTRIBUTE_SET_ID)
	    ->addAttributeToFilter('type_id', 'simple')
	    ->addAttributeToFilter("status", 1)
	    ->addFieldToFilter("visibility", 4)
	    ->addWebsiteFilter();
	
	    return $products;
	}
}