<?php 
    class Giaiphapso_Text2image_Block_Textplate_Template extends Mage_Core_Block_Template
    {
        public function __construct()
        {
            parent::__construct();
        }
		public function getTemplateDesigned()
		{
			$data = $this->getRequest()->getParam('data');
			
		}
        public function loadData()
        {
            $productId    = $this->getRequest()->getParam('productId'); // Get from ajax url /text2image/ajax/getTemplate?textplate_id=value&productId=value
            $oRiginalTextplateId = $textplateId = $this->getRequest()->getParam('textplate_id'); // Get from ajax url /text2image/ajax/getTemplate?textplate_id=value&productId=value
            if($this->getRequest()->getParam('data')){
            	$textplateDesigned = Mage::helper('text2image')->convertSerialize2Array($this->getRequest()->getParam('data'));
            	$this->setData('textplateDesigned', $textplateDesigned);
            }
            if( $productId == STOCKSTAMP_BUNDLE_ID ){
                $textplateId = 271; // Trodat4911's TextplateId
            }
            $data         = array();
            $availabeSize = array();
            $model        = Mage::getSingleton('text2image/stamptemplate');
            if(!empty($textplateId)){
                $collection = $model->getCollection()
                ->addStoreFilter(Mage::app()->getStore()->getId() )
                ->addProductFilter($textplateId)
                ->setOrder('weight', 'asc');

                $availabeSize = $model->getAvailableSize();
                $data =  $collection->getData(); 
                if( $productId == STOCKSTAMP_BUNDLE_ID ){
                    // upload product textplate image to media/tmp/textplate
                    $product   =  Mage::getModel("catalog/product")->load($oRiginalTextplateId);
                    $productImagePath = Mage::getBaseDir('base') . DS . 'media' . DS . 'catalog' . DS . 'product' . $product->getImage();
                    $fileName  = basename($product->getImage());
                    $extension = end(explode(".", $fileName));
                    $fileNameWithoutExt = basename($fileName, '.'.$extension);
                    $basePathIcon       = Mage::getBaseDir('base') . DS . 'media' . DS . 'tmp' . DS . 'textplate' . DS;
                    if (!copy($productImagePath, $basePathIcon . $fileName)) {
                        Mage::log('can not coppy: '.$productImagePath . ' to: '. $basePathIcon . $fileName, null, 'text2image.log', true);
                    }
                    if( $extension != 'png' ){
                        imagepng(imagecreatefromstring(file_get_contents($basePathIcon . $fileName)), $basePathIcon . $fileNameWithoutExt . '.png');    
                    }
                    $sizeImage = getimagesize($productImagePath);
                    
                    // Calculator size
                    $size = strtolower($availabeSize[$data[0]['textplate_size']]);
                    $size = explode('x',$size);
                    $dimension['w'] = $size[0];
                    $dimension['h'] = $size[1];
                    $tyle = $dimension['w']/$dimension['h'];
                    $tyle2 = $sizeImage[0]/$sizeImage[1];

                    $scaleHeight = $dimension['w']/$tyle2;
                    if($scaleHeight <= $dimension['h']){
                    	$siseIcon = $dimension['w'];
                    }else{
                    	$siseIcon = $dimension['h']*$tyle2;
                    }
                    
                    $newData = '[{"text":"'. $fileNameWithoutExt .'","size":"'. $siseIcon .'","font":"","type":"image","left":"0","top":"0","style":"","color":"","alignment":"centermiddle"}]';
                    foreach( $data as $key=>$item ){
                        if( $key == 0 ){
                            $data[$key]['elements'] = $newData;
                        }else{
                            $data[$key]['elements'] = '[]';
                        }
                    }
                }
            }

            $this->setData('textplate_id',$textplateId);
            $this->setData('data',$data);
            $this->setData('availabeSize',$availabeSize);
        }

        public function _prepareLayout()
        {   
            return parent::_prepareLayout();
        }

}