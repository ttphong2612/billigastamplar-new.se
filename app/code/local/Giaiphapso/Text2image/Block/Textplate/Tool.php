<?php 
/*
 * Block display editor texplate
 */
class Giaiphapso_Text2image_Block_Textplate_Tool extends Giaiphapso_Text2image_Block_Text2image
{
	//get textplate shape
	public function getTextplateShap()
	{
	    if(!$this->getData('product_shape')){
	        $shape = $this->getTextPlate()->getResource()->getAttribute('shape')->getSource()->getOptionText($this->getTextPlate()->getData('shape'));
	        $shape = $shape?$shape:'square';
	        $this->setData('product_shape', $shape);
	    }
	    return $this->getData('product_shape');
	}
	
	public function getTextplateSize()
	{
		$model = Mage::getSingleton('text2image/stamptemplate');
		$availabeSize = $model->getAvailableSize();
		return $availabeSize[$this->getTextplate()->getTextplateSize()];
	}
	//get font select box
	public function getFontSelect($name, $position, $selected = '')
	{
		$selectHtml = '<select class="font_text" name="font" id="font" onchange="fontChange(this,\''.$position.'\')">';
		$arrFont = Mage::helper('text2image')->getFontList();
		foreach($arrFont as $font => $label){
			$selectHtml .= '<option value="'.$font.'">'. $label .'</option>';
		}
		$selectHtml .= '</select>';
		return $selectHtml;
	}
	//get size select box
	public function getSizeSelect($name, $position, $selected = 8)
	{
	    $selectHtml = '<select name="'.$name.'" id="'.$name.'" onchange="fontSizeChange(this,\''.$position.'\')" >';
	    $arrSize = Mage::helper('text2image')->getSizeList();
	    foreach($arrSize as $size => $label){
	    	if($size == $selected){
	    		$selectHtml .= '<option value="'.$size.'" selected="selected">'. $label .'</option>';
	    	}else{
	    		$selectHtml .= '<option value="'.$size.'">'. $label .'</option>';
	    	}
	    }
	    $selectHtml .='<option value="">Other...</option>';
	    $selectHtml .='<input type="text" onkeyup="otherSizeKeyup(this,\''.$position.'\')" class="otherSize" style="display: none;width:24px;height:22px">';
	    $selectHtml .= '</select>';
	    return $selectHtml;
	}
	
}
