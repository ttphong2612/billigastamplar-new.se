<?php
require_once(Mage::getBaseDir('lib') . '/Giaiphapso/Text2image/Text2ImageDraw.php');
require_once(Mage::getBaseDir('lib') . '/Giaiphapso/FPDF/tfpdf.php');
require_once(Mage::getBaseDir('lib') . '/Giaiphapso/FPDI/fpdi.php');

class Giaiphapso_Text2image_Helper_Data extends Mage_Core_Helper_Abstract {

    protected $rate_px_mm = 3.77952775905;
    protected $ftp_username = 'vns-factory';
    protected $ftp_password = 'rDfje#23q';
    protected $ftp_host = 'ftp.serac.nl';
    protected $MAXTEXTWIDTH = 200000;

    public function convertSerialize2Array($serialize) {
        // add by phong.tran
        $serialize = str_replace(array('_', '-'), array('/', '+'), $serialize);
        $serialize = base64_decode($serialize);
        if (@gzuncompress($serialize) != FALSE) { // if data were compressed
            $serialize = gzuncompress($serialize);
        }
        // end by phong.tran
        $serialize = (array) unserialize($serialize);
        $result = array();
        foreach ($serialize as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $item => $option) {
                    $result[$item][$key] = $option;
                }
            }
        }
        $serialize['elements'] = $result;
        return $serialize;
    }

    public function getRateMM2Pixcel() {
        return $this->rate_px_mm;
    }

//        public function getDataLines($infoTemplate,  $shape = 'SQUARE', $autoWidth = 1, $autoSize = 1, $hasBorder = 0, $borderWidth = 0.3, $borderStyle = '', $inkpadColor ='black', $lineHeight = 0.1, $zoom_size = 0, $storeId = null)
//        {
//        	$template = new Text2ImageDraw($infoTemplate['imgWidth'], $infoTemplate['imgHeight'], $infoTemplate['templateType'], $shape, $storeId);
//        	//add border
//        	if($hasBorder){
//        	    if($borderWidth && $borderStyle){
//        	        $template->addBorder($borderWidth, $borderStyle);
//        	    }
//        	}
//        	//add lineHeight
//        	$template->addLineHeight($lineHeight);
//        	//add color
//        	if(strpos($inkpadColor, '-') !== false){
//        	    $arrColor = explode('-', $inkpadColor);
//        	    $firstColor = $arrColor[0];
//        	    $secondColor = $arrColor[1];
//        	    $template->addTextColor($secondColor);
//        	    $template->addDaterColor($firstColor);
//        	}else{
//        	    $firstColor = $secondColor = $inkpadColor;
//        	    $template->addTextColor($inkpadColor);
//        	    if($template->isDater()){
//        	        $template->addDaterColor($inkpadColor);
//        	    }
//        	}
//        	//add element text & image
//        	
//        	$template->addLineElements($infoTemplate['elements']);
//        	if($autoSize){
//        	    $template->processAuto();
//        	}
//        	return $template;
//        }
    public function getDataLines($infoTemplate, $shape = 'SQUARE', $autoWidth = 1, $autoSize = 1, $hasBorder = 0, $borderWidth = 0.3, $borderStyle = '', $inkpadColor = 'black', $lineHeight = 0.1, $zoom_size = 0, $storeId = null) {
        $template = new Text2ImageDraw($infoTemplate['imgWidth'], $infoTemplate['imgHeight'], $infoTemplate['templateType'], $shape, $storeId);
        if (!in_array($inkpadColor, array('red', 'blue', 'black', 'red-blue'))) {
            $inkpadColor = 'black';
        }
        if (!is_numeric($lineHeight)) {
            $lineHeight = 0.3;
        }
        //add border
        if ($hasBorder) {
            if ($borderWidth && $borderStyle) {
                if (!in_array($borderStyle, array('BASIC', 'DBL', 'DBL_THICK', 'DBL_THICK_THIN', 'DBL_THIN_THICK', 'DASHED', 'DOTTED'))) {
                    $borderStyle = 'BASIC';
                }
                if (!is_numeric($borderWidth)) {
                    $borderWidth = 0.3;
                }
                $template->addBorder($borderWidth, $borderStyle);
            }
        }
        //add lineHeight
        $template->addLineHeight($lineHeight);

        if (strpos($inkpadColor, '-') !== false) {
            $arrColor = explode('-', $inkpadColor);
            $firstColor = $arrColor[0];
            $secondColor = $arrColor[1];
            $template->addTextColor($secondColor);
            $template->addDaterColor($firstColor);
        } else {
            $firstColor = $secondColor = $inkpadColor;
            $template->addTextColor($inkpadColor);
            if ($template->isDater()) {
                $template->addDaterColor($inkpadColor);
            }
        }
        //add element text & image

        $template->addLineElements($infoTemplate['elements']);
        if ($autoSize) {
            $template->processAuto();
        }
        return $template;
    }

    public function getTemplateimage($infoTemplate, $shape = 'SQUARE', $autoWidth = 1, $autoSize = 1, $hasBorder = 0, $borderWidth = 0.3, $borderStyle = '', $inkpadColor = 'black', $lineHeight = 0.1, $zoom_size = 0, $storeId = null) {
        $template = $this->getDataLines($infoTemplate, $shape, $autoWidth, $autoSize, $hasBorder, $borderWidth, $borderStyle, $inkpadColor, $lineHeight, $zoom_size, $storeId);
        //add zoom
        if ($zoom_size) {
            $template->addZoomSize($zoom_size);
        }
        //generate template
        $template->generateTemplate();
        //add border
        if ($hasBorder) {
            $template->drawBorder();
        }
        //output image
        $template->previewImage('png');
    }

    //use for download template 
    public function getTemplatePdf($infoTemplate, $shape = 'SQUARE', $autoWidth = 1, $autoSize = 1, $hasBorder = 0, $borderWidth = 0.3, $borderStyle = '', $inkpadColor = 'black', $lineHeight = 0.1, $zoom_size = 0, $storeId = null) {
        $template = new Text2ImageDraw($infoTemplate['imgWidth'], $infoTemplate['imgHeight'], $infoTemplate['templateType'], $shape, $storeId);
        //add border
        if ($hasBorder) {
            if ($borderWidth && $borderStyle) {
                $template->addBorder($borderWidth, $borderStyle);
            }
        }
        //add lineHeight
        $template->addLineHeight($lineHeight);
        //add color
        if (strpos($inkpadColor, '-') !== false) {
            $arrColor = explode('-', $inkpadColor);
            $firstColor = $arrColor[0];
            $secondColor = $arrColor[1];
            $template->addTextColor($secondColor);
            $template->addDaterColor($firstColor);
        } else {
            $firstColor = $secondColor = $inkpadColor;
            $template->addTextColor($inkpadColor);
            if ($template->isDater()) {
                $template->addDaterColor($inkpadColor);
            }
        }
        //add element text & image
        $template->addLineElements($infoTemplate['elements']);
        if ($autoSize) {
            $template->processAuto();
        }
        //add zoom
        if ($zoom_size) {
            $template->addZoomSize($zoom_size);
        }
        //generate template
        $template->generateTemplate();
        //add border
        if ($hasBorder) {
            $template->drawBorder();
        }
        //output image
        $template->download('pdf', 'template.pdf');
    }

    public function microtime_float() {
        list($usec, $sec) = explode(" ", microtime());
        $float = ((float) $usec + (float) $sec);
        return str_replace('.', '', $float);
    }

    public function getUrlPathTemplate($template_id) {
        $path = '';
        $file = $template_id . '.png';
        $pathDir = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'textplate';
        if (!empty($template_id)) {
            if (strlen($template_id) < 3) {
                while (strlen($template_id) < 3) {
                    $template_id = '0' . $template_id;
                }
            }
            $path1 = substr($template_id, 0, 1);
            $path2 = substr($template_id, 1, 1);
            $path = $path1 . '/' . $path2 . '/' . $file;
            $pathDir = $pathDir . DS . $path1 . DS . $path2 . DS . $file;
        }
        if (file_exists($pathDir)) {
            return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB) . 'media/catalog/textplate/' . $path;
        } else {
            return '';
        }
    }

    //get folder path by template_id
    public function getFolderPathTemplate($template_id) {
        $path = '';
        if (empty($template_id)) {
            $path = Mage::getBaseDir('base') . DS . "media" . DS . "tmp" . DS . "textplate";
        } else {
            if (strlen($template_id) < 3) {
                while (strlen($template_id) < 3) {
                    $template_id = '0' . $template_id;
                }
            }
            if (is_numeric($template_id)) {
                $path = Mage::getBaseDir('base') . DS . "media" . DS . "catalog" . DS . "textplate" . DS . substr($template_id, 0, 1) . DS . substr($template_id, 1, 1);
                if (!is_dir($path)) {
                    if (!is_dir(Mage::getBaseDir('base') . DS . "media" . DS . "catalog" . DS . "textplate" . DS . substr($template_id, 0, 1))) {
                        if (!is_dir(Mage::getBaseDir('base') . DS . "media" . DS . "catalog" . DS . "textplate")) {
                            @mkdir(Mage::getBaseDir('base') . DS . "media" . DS . "catalog" . DS . 'textplate', 0777, true);
                        }
                        @mkdir(Mage::getBaseDir('base') . DS . "media" . DS . "catalog" . DS . 'textplate' . DS . substr($template_id, 0, 1), 0777, true);
                    }
                    @mkdir($path, 0777, true);
                }
            }
        }
        return $path;
    }

    protected function _hex2rgb($hex) {
        $color = str_replace('#', '', $hex);
        $rgb = array('r' => hexdec(substr($color, 0, 2)),
            'g' => hexdec(substr($color, 2, 2)),
            'b' => hexdec(substr($color, 4, 2)));
        return $rgb;
    }

    public function getFontList() {
        return array('arial' => 'Arial',
            'acaslon' => 'Acaslon',
            'timesnewroman' => 'Times New Roman',
            'verdana' => 'Verdana',
            'helvetica' => 'Helvetica',
            'calibri' => 'Calibri',
            'tirantisolid' => 'Tiranti Solid',
            'babelsans' => 'Babelsans',
            'bauhaus' => 'Bauhaus',
            'casablanca' => 'Casablanca',
            //   'coolvetica-regular' => 'Coolvetica Regular',
            'decker' => 'Decker',
            'dreamerone' => 'Dreamerone',
            //    'eurof35' => 'Eurof35',
            //   'eurof55' => 'Eurof55',
            //    'eurof56' => 'Eurof56',
            //    'eurof75' => 'Eurof75',
            //    'eurof76' => 'Eurof76',
            'erika' => 'Erika',
            'garamond' => 'Garamond',
            //     'helvetica' => 'Helvetica',
            //     'manksans' => 'Manksans',
            //     'manksans-medium' => 'Manksans Medium',
            //     'nilland' => 'Nilland',
            //     'nilland-black' => 'Nilland Black',
            'walkway' => 'Walkway',
            'dejavusansmono' => 'Dejavusansmono',
            'dejavuserif' => 'Dejavuserif',
            'dejavusanscondensed' => 'Dejavusanscondensed',
            'garuda' => 'Garuda',
            'kinnari' => 'Kinnari',
            'norasi' => 'Norasi',
            'purisa' => 'Purisa',
            'sawasdee' => 'Sawasdee',
            'waree' => 'Waree',
        );
    }

    public function getData($property) {
        return $this->{$property};
    }

    public function getSizeList() {
        return array(
            '6' => '6',
            '7' => '7',
            '8' => '8',
            '9' => '9',
            '10' => '10',
            '12' => '12',
            '13' => '13',
            '14' => '14',
            '15' => '15',
            '16' => '16',
            '17' => '17',);
    }

    public function _convertToMilimet($pixcel) {
        return $pixcel / self::$rate_px_mm;
    }

    public function saveTemplateImage($id, $data) {

        $elements = Mage::helper('core')->jsonDecode($data);

        $destination = $this->getFolderPathTemplate($id);
        $target = Mage::getBaseDir('media') . DS . 'tmp' . DS . 'textplate';
        //move file to textplate folder
        if (!empty($elements)) {
            foreach ($elements as $element) {
                if ($element['type'] == 'image') {
                    if (file_exists($target . DS . $element['text'] . '.pdf')) {
                        if (!copy($target . DS . $element['text'] . '.pdf', $destination . DS . $element['text'] . '.pdf')) {
                            Mage::throwException($this->__("Canot copy file to {$destination} folder"));
                        } else {
                            @unlink($target . DS . $element['text'] . '.pdf');
                        }
                    }
                }
            }
        }
    }

    /* phong.tran add $shape parameter */

    public function generateImage($items, $template_size, $type = 'jpg', $imagename = '', $_product, $shape) {

        $folder = $this->getFolderPathTemplate($items['template_id']);
        if (empty($imagename)) {
            $imagename = $items['template_id'];
        }

        if (file_exists($folder . DS . $imagename . '.pdf')) {

            $tmp_image = new Imagick();
            $tmp_image->setResolution(600, 600);
            $tmp_image->readimage($folder . DS . $imagename . '.pdf');

            $tmp_width = $template_size[0] * self::$rate_px_mm;
            $tmp_height = $template_size[1] * self::$rate_px_mm;

            $tmp_image->thumbnailImage($tmp_width, $tmp_height);
            $tmp_image->setimageformat($type);

            if ($shape == 'Oval') {
                $border = 1;

                $draw = new ImagickDraw();
                $draw->setFillColor(new ImagickPixel('transparent'));
                $draw->setFillOpacity(0); // very important                
                $draw->setStrokeColor("#cccccc");
                $draw->setStrokeWidth($border);
                $draw->setStrokeOpacity(1); // very important                

                $draw->ellipse($tmp_width / 2, $tmp_height / 2, ($tmp_width / 2) - $border - 1, ($tmp_height / 2) - $border - 1, 0, 360);
                $tmp_image->drawImage($draw);
            } elseif ($shape == 'Circle') {
                $border = 1;

                $draw = new ImagickDraw();
                $draw->setFillColor(new ImagickPixel('transparent'));
                $draw->setFillOpacity(0); // very important                
                $draw->setStrokeColor("#cccccc");
                $draw->setStrokeWidth($border);
                $draw->setStrokeOpacity(1); // very important                

                $draw->circle(($tmp_width / 2), ($tmp_height / 2), ($tmp_width / 2) - $border - 2, $tmp_height - 2);
                $tmp_image->drawImage($draw);
            } else {
                $border = 1;

                $draw = new ImagickDraw();
                $draw->setFillColor(new ImagickPixel('transparent'));
                $draw->setFillOpacity(0); // very important                
                $draw->setStrokeColor("#cccccc");
                $draw->setStrokeWidth($border);
                $draw->setStrokeOpacity($border); // very important                

                $draw->rectangle($border, $border, $tmp_width - $border - 1, $tmp_height - $border - 1);
                $tmp_image->drawImage($draw);
            }

            $tmp_image->writeImage($folder . DS . $imagename . '.' . $type);
        }
    }

    public function downloadOrdersPDF($inputs) {
        $pdf = new FPDI();
        foreach ($inputs as $input) {
            $storeId = $input[0];
            $incrementId = $input[2];
            $dataOfTemplates = $input[1];
            foreach ($dataOfTemplates as $key => $data) {
                if (!empty($data)) {
                    if (strlen($data) > 50) {// new OPD
                        $data = Mage::helper('text2image')->convertSerialize2Array($data);
                        $autoSize = $data['auto_size'];
                        $autoWidth = $data['auto_width'];
                        $hasBorder = $data['has_border'];
                        $borderStyle = $data['border_style'];
                        $borderWidth = $data['border_width'];
                        $inkpadColor = $data['inkpad_color'];
                        $zoom_size = $data['zoom_size'];
                        $width = $data['template_width'];
                        $height = $data['template_height'];
                        $line_height = $data['line_height'];
                        $templateType = $data['templateType'];
                        $shape = $data['shape'];
                        $elements = $data['elements'];
                        $infoTemplate['elements'] = $elements;
                        $infoTemplate['imgWidth'] = $width;
                        $infoTemplate['imgHeight'] = $height;
                        $infoTemplate['templateType'] = $templateType;
                        $zoomSize = (900 / 72 - 1) * 100;

                        $basePathIcon = Mage::getBaseDir('media') . DS . 'order' . DS . $storeId . DS;
                        $fileName = $incrementId . '-' . $key . '.pdf';
                        $template = $this->createTemplate($infoTemplate, $basePathIcon, $shape, $autoWidth, $autoSize, $hasBorder, $borderWidth, $borderStyle, $inkpadColor, $line_height, $zoomSize, $storeId);

                        // Save template on server
                        $templatePath = Mage::getBaseDir() . DS . 'var' . DS . 'tmp' . DS . $fileName;
                        $template->saveTemplateOnServer('pdf', $templatePath);

                        $pdf->AddPage();
                        $pdf->setSourceFile($templatePath);
                        $tplidx = $pdf->importPage(1);
                        $size = $pdf->getTemplateSize($tplidx);
                        $pdf->SetMargins(0, 0, 0);
                        $pdf->Ln();
                        $pdf->useTemplate($tplidx, null, null, $size['w'], $size['h'], true);
                    } else {// old OPD
                        $pdfPath = Mage::getBaseDir('media') . DS . 'order' . DS . $storeId . DS . $data . '.pdf';
                        if (file_exists($pdfPath)) {
                            $pdf->AddPage();
                            $pdf->setSourceFile($pdfPath);
                            $tplidx = $pdf->importPage(1);
                            $size = $pdf->getTemplateSize($tplidx);
                            $pdf->SetMargins(0, 0, 0);
                            $pdf->Ln();
                            $pdf->useTemplate($tplidx, null, null, $size['w'], $size['h'], true);
                        }
                    }
                }
            }
        }
        return $pdf;
    }

    public function downloadOrdersTIFF($inputs, $ftp_download = false) {
        $templates = array();
        foreach ($inputs as $input) {
            $storeId = $input[0];
            $incrementId = $input[2];
            $dataOfTemplates = $input[1];

            foreach ($dataOfTemplates as $key => $data) {
                if (!empty($data)) {
                    if (strlen($data) > 50) {// new OPD
						$fileName = $incrementId . '-' . $key .'.tiff';
						$templatePath = Mage::getBaseDir() . DS . 'var' . DS . 'tmp' . DS . $fileName;
						$templates[$fileName] = $templatePath;
						if(file_exists($templatePath)){
							continue;
						}
                        $data = Mage::helper('text2image')->convertSerialize2Array($data);
                        $autoSize = $data['auto_size'];
                        $autoWidth = $data['auto_width'];
                        $hasBorder = $data['has_border'];
                        $borderStyle = $data['border_style'];
                        $borderWidth = $data['border_width'];
                        $inkpadColor = $data['inkpad_color'];
                        $zoom_size = $data['zoom_size'];
                        $width = $data['template_width'];
                        $height = $data['template_height'];
                        $line_height = $data['line_height'];
                        $templateType = $data['templateType'];
                        $shape = $data['shape'];
                        $elements = $data['elements'];
                        $infoTemplate['elements'] = $elements;
                        $infoTemplate['imgWidth'] = $width;
                        $infoTemplate['imgHeight'] = $height;
                        $infoTemplate['templateType'] = $templateType;
                        //$zoomSize = (600 / 72 - 1)*100;
                        $zoomSize = (600 / 96 - 1) * 100;
						
                        $basePathIcon = Mage::getBaseDir('media') . DS . 'order' . DS . $storeId . DS;
                        
                        $template = $this->createTemplate($infoTemplate, $basePathIcon, $shape, $autoWidth, $autoSize, $hasBorder, $borderWidth, $borderStyle, $inkpadColor, $line_height, $zoomSize, $storeId);

                        // Save template on server
                        $template->saveTemplateOnServer('tiff', $templatePath, array(600, 600));
                        
                    } else {// old OPD
                        $pdfPath = Mage::getBaseDir('media') . DS . 'order' . DS . $storeId . DS . $data . '.pdf';
                        $jpgPath = Mage::getBaseDir('media') . DS . 'order' . DS . $storeId . DS . $data . '.jpg';
                        $tiffPath = Mage::getBaseDir('media') . DS . 'order' . DS . $storeId . DS . $data . '.tiff';

                        if (file_exists($pdfPath)) {
                            //create tiff file
                            if (!file_exists($tiffPath) || true) {
                                $im = new Imagick();
                                $im->setResolution(600, 600);
                                $im->readImage($pdfPath);
                                $im->setImageColorSpace(Imagick::COLORSPACE_GRAY);
                                $im->setimagedepth(2);
                                $max = $im->getQuantumRange();
                                $max = $max["quantumRangeLong"];
                                $im->thresholdImage(0.77 * $max);
                                $im->setImageFormat("tiff");
                                $im->writeImage($tiffPath);
                                $im->clear();
                            }
                            $templates[$data] = $tiffPath;
                        }
                    }
                }
            }
        }

        if (count($templates) > 0) {
            //download to FTP
            if ($ftp_download == true) {
                $ftp = new Varien_Io_Ftp();
                $args = array('host' => $this->ftp_host, 'user' => $this->ftp_username, 'password' => $this->ftp_password);
                if ($ftp->open($args)) {
                    $ftp->cd('/textplates');
                    foreach ($templates as $file => $template) {
                        $ftp->write($file . '.tiff', $template);
                    }
                }
                $ftp->close();
                return false;
                //download to zip file
            } else {
                $zip = new ZipArchive;
				$zipName = Mage::getSingleton('core/date')->date('Y-m-d_H-i-s') . '.zip';
                $zipPath = Mage::getBaseDir() . DS . 'var' . DS . 'export' . DS . 'order' . DS . $zipName;
                $zip->open($zipPath, ZipArchive::CREATE);
                foreach ($templates as $file => $template) {
                    $zip->addFile($template, $file);
                }
                $zip->close();
                return $zipName; 
            }
        }
        return false;
    }

	
    /* Code add by phong.tran@giaiphapso.com */
    public function createTemplate($infoTemplate, $basePathIcon = '', $shape = 'SQUARE', $autoWidth = 1, $autoSize = 1, $hasBorder = 0, $borderWidth = 0.3, $borderStyle = '', $inkpadColor = 'black', $lineHeight = 0.1, $zoomSize = 0, $storeId = null) {
        $template = new Text2ImageDraw($infoTemplate['imgWidth'], $infoTemplate['imgHeight'], $infoTemplate['templateType'], $shape, $storeId);
        $template->setPathIcon($basePathIcon);
        //add border
		if($hasBorder){
			if($borderWidth && $borderStyle){
				if(!in_array($borderStyle,array('BASIC','DBL','DBL_THICK','DBL_THICK_THIN','DBL_THIN_THICK', 'DASHED', 'DOTTED'))){
					$borderStyle = 'BASIC';
				}
				if(!is_numeric($borderWidth)){
					$borderWidth = 0.3;
				}
		   
				$template->addBorder($borderWidth, $borderStyle);
			}
		}
        //add lineHeight
        $template->addLineHeight($lineHeight);
        //add color
        if (strpos($inkpadColor, '-') !== false) {
            $arrColor = explode('-', $inkpadColor);
            $firstColor = $arrColor[0];
            $secondColor = $arrColor[1];
            $template->addTextColor($secondColor);
            $template->addDaterColor($firstColor);
        } else {
            $firstColor = $secondColor = $inkpadColor;
            $template->addTextColor($inkpadColor);
            if ($template->isDater()) {
                $template->addDaterColor($inkpadColor);
            }
        }
        //add element text & image
        $template->addLineElements($infoTemplate['elements']);
        if ($autoSize) {
            $template->processAuto();
        }
        //add zoom
        if ($zoomSize) {
            $template->addZoomSize($zoomSize);
        }
        //generate template
        $template->image->setResolution(600, 600);
        $template->generateTemplate();
        //add border
        if ($hasBorder) {
            $template->drawBorder();
        }

        return $template;
    }

    /*
     * Merge Element
     */

    public function mergeElements(Array $oldElements, Array $newElements) {
        foreach ($oldElements as $key => $element) {
            if (in_array($key, array_keys($newElements))) {
                $oldElements[$key] = $newElements[$key];
            }
        }
        return $oldElements;
    }

    // add by phong.tran
    function tinyUrl($url) {
        return file_get_contents('http://tinyurl.com/api-create.php?url=' . $url);
    }

    function productSetType($attributeSetId) {
        if (in_array($attributeSetId, unserialize(TEXTPLATE_ATTRIBUTE_SET_ID))) {
            return 'textplate';
        } elseif ($attributeSetId == SEALS_ATTRIBUTE_SET_ID) {
            return 'seal';
        } else {
            return 'stamp';
        }
        return false;
    }

    public function getAvailableSize() {
        $model = Mage::getSingleton('text2image/stamptemplate');
        $availabeSize = $model->getAvailableSize();
        return $availabeSize;
    }

    public function myLog($str) {
        //return false;
        Mage::log($str, null, 'text2image.log', true);
    }

    function myDebug($obj) {
        if ($_SERVER['REMOTE_ADDR'] == '1.54.84.199') {
            echo '<pre>';
            print_r($obj);
            //die();
        }
    }

}
