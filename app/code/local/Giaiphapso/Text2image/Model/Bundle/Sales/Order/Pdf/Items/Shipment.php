<?php
    /**
    * Magento
    *
    * NOTICE OF LICENSE
    *
    * This source file is subject to the Open Software License (OSL 3.0)
    * that is bundled with this package in the file LICENSE.txt.
    * It is also available through the world-wide-web at this URL:
    * http://opensource.org/licenses/osl-3.0.php
    * If you did not receive a copy of the license and are unable to
    * obtain it through the world-wide-web, please send an email
    * to license@magentocommerce.com so we can send you a copy immediately.
    *
    * DISCLAIMER
    *
    * Do not edit or add to this file if you wish to upgrade Magento to newer
    * versions in the future. If you wish to customize Magento for your
    * needs please refer to http://www.magentocommerce.com for more information.
    *
    * @category    Mage
    * @package     Mage_Bundle
    * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
    * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
    */

    /**
    * Sales Order Shipment Pdf items renderer
    *
    * @category   Mage
    * @package    Mage_Bundle
    * @author     Magento Core Team <core@magentocommerce.com>
    */
    class Giaiphapso_Text2image_Model_Bundle_Sales_Order_Pdf_Items_Shipment extends Mage_Bundle_Model_Sales_Order_Pdf_Items_Shipment
    {
        /**
        * Draw item line
        *
        */
        public function draw()
        {
            $item   = $this->getItem();
            $pdf    = $this->getPdf();
            $page   = $this->getPage();

            $this->_setFontRegular();

            $shipItems = $this->getChilds($item);
            $items = array_merge(array($item->getOrderItem()), $item->getOrderItem()->getChildrenItems());

            $_prevOptionId = '';
            $drawItems = array();
            $options = $this->getItemOptions();
            foreach ($items as $_item) {
                $line   = array();

                $attributes = $this->getSelectionAttributes($_item); 

                if (is_array($attributes)) {
                    $optionId   = $attributes['option_id'];
                }
                else {
                    $optionId = 0;
                }

                if (!isset($drawItems[$optionId])) {
                    $drawItems[$optionId] = array(
                        'lines'  => array(),
                        'height' => 15
                    );
                }

                if ($_item->getParentItem()) {
                    if ($_prevOptionId != $attributes['option_id']) {
                        $line[0] = array(
                            'font'  => 'italic',
                            'text'  => Mage::helper('core/string')->str_split($attributes['option_label'], 60, true, true),
                            'feed'  => 60
                        );

                        $drawItems[$optionId] = array(
                            'lines'  => array($line),
                            'height' => 15
                        );

                        $line = array();

                        $_prevOptionId = $attributes['option_id'];
                    }
                }

                if (($this->isShipmentSeparately() && $_item->getParentItem())
                    || (!$this->isShipmentSeparately() && !$_item->getParentItem())
                ) {
                    if (isset($shipItems[$_item->getId()])) {
                        $qty = $shipItems[$_item->getId()]->getQty()*1;
                    } else if ($_item->getIsVirtual()) {
                        $qty = Mage::helper('bundle')->__('N/A');
                    } else {
                        $qty = 0;
                    }
                } else {
                    $qty = '';
                }

                $line[] = array(
                    'text'  => $qty,
                    'feed'  => 35
                );

                // draw Name
                if ($_item->getParentItem()) {
                    $feed = 65;
                    $name = $this->getValueHtml($_item);
                } else {
                    $feed = 60;
                    $name = $_item->getName();
                }
                $text = array();
                foreach (Mage::helper('core/string')->str_split($name, 60, true, true) as $part) {
                    $text[] = $part;
                }
                $line[] = array(
                    'text'  => $text,
                    'feed'  => $feed
                );
                //draw template
                if(in_array($attributes['attribute_set_id'], unserialize(TEXTPLATE_ATTRIBUTE_SET_ID)))
                {
                    if($options[0]['label'] == 'design_image'){
                        $orderId = $this->getOrder()->getRealOrderId();
                        $storeId = $this->getOrder()->getStoreId();
                        $data    = $options[0]['value'];
                        if( strlen($data) < 50 ){// Old version
                            $imagePath = Mage::getBaseDir('media') . DS . 'order' . DS . $storeId . DS . $data. '.png'; 
                        }else{// New version
                            $data = Mage::helper('text2image')->convertSerialize2Array($data);
                            $autoSize = $data['auto_size'];
                            $autoWidth = $data['auto_width'];
                            $hasBorder = $data['has_border'];
                            $borderStyle = $data['border_style'];
                            $borderWidth = $data['border_width'];
                            $inkpadColor = $data['inkpad_color'];
                            $zoom_size = $data['zoom_size'];
                            $width = $data['template_width'];
                            $height = $data['template_height'];
                            $line_height = $data['line_height'];
                            $templateType = $data['templateType'];
                            $shape = $data['shape'];
                            $elements = $data['elements'];
                            $infoTemplate['elements'] = $elements;
                            $infoTemplate['imgWidth'] = $width;
                            $infoTemplate['imgHeight'] = $height;
                            $infoTemplate['templateType'] = $templateType;
                            $zoomSize = 1;

                            $basePathIcon = Mage::getBaseDir('media') . DS . 'order' . DS . $storeId . DS; 
                            $fileName     = $orderId . '-' . $item->getSku(); 
                            $template     = Mage::helper('text2image')->createTemplate($infoTemplate, $basePathIcon, $shape ,$autoWidth, $autoSize, $hasBorder, $borderWidth, $borderStyle, $inkpadColor, $line_height, $zoomSize, $storeId);

                            // Save template on server
                            $templatePath = Mage::getBaseDir() . DS . 'var' . DS . 'tmp' . DS . $fileName . '.png'; 
                            $template->saveTemplateOnServer('png', $templatePath, array(72, 72));
                            $imagePath = $templatePath;
                        }

                        $line[] = array(
                            'image'  => $imagePath,
                            'feed'  => 250
                        );	
                        $drawItems[$optionId]['height'] = 25;
                    }else{
                        $line[] = array(
                            'text'  => '',
                            'feed'  => 250
                        );
                    }

                }
                // draw SKUs
                $text = array();
                foreach (Mage::helper('core/string')->str_split($_item->getSku(), 25) as $part) {
                    $text[] = $part;
                }
                $line[] = array(
                    'text'  => $text,
                    'feed'  => 440
                );

                $drawItems[$optionId]['lines'][] = $line;
            }


            // custom options
            $options = $item->getOrderItem()->getProductOptions(); 
            if ($options) {
                if (isset($options['options'])) {
                    foreach ($options['options'] as $option) {
                        $lines = array();
                        $lines[][] = array(
                            'text'  => Mage::helper('core/string')->str_split(strip_tags($option['label']), 70, true, true),
                            'font'  => 'italic',
                            'feed'  => 60
                        );

                        if ($option['value']) {
                            $text = array();
                            $_printValue = isset($option['print_value'])
                            ? $option['print_value']
                            : strip_tags($option['value']);
                            $values = explode(', ', $_printValue);
                            foreach ($values as $value) {
                                foreach (Mage::helper('core/string')->str_split($value, 50, true, true) as $_value) {
                                    $text[] = $_value;
                                }
                            }

                            $lines[][] = array(
                                'text'  => $text,
                                'feed'  => 65
                            );
                        }

                        $drawItems[] = array(
                            'lines'  => $lines,
                            'height' => 15
                        );
                    }
                }
            }

            $page = $pdf->drawLineBlocks($page, $drawItems, array('table_header' => true));
            $this->setPage($page);
        }
    }
