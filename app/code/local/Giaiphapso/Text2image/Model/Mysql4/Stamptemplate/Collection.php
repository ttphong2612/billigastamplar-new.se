<?php 
class Giaiphapso_Text2image_Model_Mysql4_Stamptemplate_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('text2image/stamptemplate');
    }
    /**
     * Set first store flag
     *
     * @param bool $flag
     * @return Giaiphapso_Text2image_Model_Mysql4_Stamptemplate_Collection
     */
    public function setFirstStoreFlag($flag = false)
    {
        $this->_previewFlag = $flag;
        return $this;
    }
}