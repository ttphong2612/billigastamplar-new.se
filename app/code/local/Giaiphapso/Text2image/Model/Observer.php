<?php
//
class Giaiphapso_Text2image_Model_Observer {
	
	/**
	 * Add design image to text plate product
	 * @param Varien_Event_Observer $observer
	 */
    public function catalogProductLoadAfter(Varien_Event_Observer $observer)
    {
        // set the additional options on the product
        $action = Mage::app()->getFrontController()->getAction();
        if ($action->getFullActionName() == 'checkout_cart_add')
        {
            // assuming you are posting your custom form values in an array called extra_options...
            if ($options = $action->getRequest()->getParam('extra_options'))
            {
                $product = $observer->getProduct();
    
                // add to the additional options array
                $additionalOptions = array();
                if ($additionalOption = $product->getCustomOption('additional_options'))
                {
                    $additionalOptions = (array) unserialize($additionalOption->getValue());
                }
				
                foreach ($options as $key => $value)
                {
                    $additionalOptions[] = array(
                        'label' => $key,
                        'value' => $value,
                    );
                }
                // add the additional options array with the option code additional_options
                $observer->getProduct()
                    ->addCustomOption('additional_options', serialize($additionalOptions));
            }
        }
    }
    
    public function salesConvertQuoteItemToOrderItem(Varien_Event_Observer $observer)
    {   
        $quoteItem = $observer->getItem();
        if ($additionalOptions = $quoteItem->getOptionByCode('additional_options')) {
            $orderItem = $observer->getOrderItem();
            $options = $orderItem->getProductOptions();
            $options['additional_options'] = unserialize($additionalOptions->getValue()); 
            $orderItem->setProductOptions($options);
        }
    }
    
    public function checkoutCartProductAddAfter(Varien_Event_Observer $observer)
    {   
        $action = Mage::app()->getFrontController()->getAction();
        if ($action->getFullActionName() == 'sales_order_reorder')
        {   
            $item = $observer->getQuoteItem();
            $buyInfo = $item->getBuyRequest();
            if ($options = $buyInfo->getExtraOptions())
            {
                $additionalOptions = array();
                if ($additionalOption = $item->getOptionByCode('additional_options'))
                {
                    $additionalOptions = (array) unserialize($additionalOption->getValue());
                }
                foreach ($options as $key => $value)
                {
                    $additionalOptions[] = array(
                            'label' => $key,
                            'value' => $value,
                    );
                }
                $item->addOption(array(
                        'code' => 'additional_options',
                        'value' => serialize($additionalOptions)
                ));
            }
        }
    }
    
    public function saleOrderPlaceAfter(Varien_Event_Observer $observer) {
        
        $order = $observer->getOrder();
        $itemCollection = $order->getItemsCollection();
        $store_id = $order->getStoreId();
        $storeOrderPath = Mage::getBaseDir('media') . DS . 'order' . DS . $store_id ;
        if(!is_dir($storeOrderPath)){
            @mkdir($storeOrderPath, 0777, true);
        }
        $order_id = $order->getRealOrderId();
		
        foreach($itemCollection as $item)
		{
            $additional_options =  $item->getProductOptionByCode('additional_options') ; 
			
			if(!empty($additional_options)){ 
				foreach($additional_options as $additional)
				{  
					if(!empty($additional['label']) && $additional['label'] == 'design_image'){ 
						$target = Mage::getBaseDir('media') . DS . 'tmp' . DS . 'textplate';
						$destination = $storeOrderPath ;
						if(!empty($additional['value'])){
							$data = Mage::helper('text2image')->convertSerialize2Array($additional['value']);
							foreach($data['elements'] as $element)
							{
								if($element['type'] == 'image'){
									if(file_exists($target .  DS . $element['text'] .'.pdf')){
										if (!copy( $target .  DS . $element['text'] .'.pdf' , $destination . DS . $element['text'] .'.pdf')) {
											Mage::throwException($this->__("Canot copy file to {$destination} folder"));
										}else{
											@unlink($target . DS . $element['text'] .'.pdf');
										}
									}
									if(file_exists($target .  DS . $element['text'] .'.png' )){
										if (!copy( $target .  DS . $element['text'] .'.png' , $destination . DS . $element['text'] .'.png')) {
											Mage::throwException($this->__("Canot copy file to {$destination} folder"));
										}else{
											@unlink($target . DS . $element['text'] .'.png'); 
										} 
									}
									if(file_exists($target .  DS . $element['text'] .'.tiff' )){
									    if (!copy( $target .  DS . $element['text'] .'.tiff' , $destination . DS . $element['text'] .'.tiff')) {
									        Mage::throwException($this->__("Canot copy file to {$destination} folder"));
									    }else{
									        @unlink($target . DS . $element['text'] .'.tiff');
									    }
									}
								}
							}
							break;
						}
					}
				}
            }
        }
    }
	/**
	 * Manipulate the custom product options
	 *
	 * @param Varien_Event_Observer $observer        	
	 * @return void
	 */
	
	public function salesConvertQuoteItemToOrderItem_new(Varien_Event_Observer $observer) {
		$quoteItem = $observer->getItem ();
		$orderItem = $observer->getOrderItem();
		$order = Mage::getModel('sales/order')->load($orderItem->getOrderId());
		$orderIncrementId = $order->getIncrementId();
		$options = $quoteItem->getOptionByCode ( 'design_image' );
		if ($options) {
			$orderItem = $observer->getOrderItem();
			$data = $options->getData();
			$path_file = $data['value'];
            
			if(is_file(MAGENTO_ROOT.DS.$path_file))
            {
				$filename = explode('/', $path_file);
				$filename = end($filename);
				$new_name = $orderIncrementId.'_'.$orderItem->getId().'.png';
	
				$path_file_dest = str_replace('stamp_design', 'stamp_order', $path_file);
				$path_file_dest = str_replace($filename, $new_name, $path_file_dest);
	
				rename(MAGENTO_ROOT.DS.$path_file, MAGENTO_ROOT.DS.$path_file_dest);
				$options = $orderItem->getProductOptions();
				$options ['design_image'] = $path_file_dest;
				$orderItem->setProductOptions( $options );
			}
		}
	
		$options = $quoteItem->getOptionByCode ( 'design_pdf' );
		if ($options) {
			$orderItem = $observer->getOrderItem();
			$data = $options->getData();
			$path_file = $data['value'];
			if(is_file(MAGENTO_ROOT.DS.$path_file)){
	
				$filename = explode('/', $path_file);
				$filename = end($filename);
				$new_name = $orderIncrementId.'_'.$orderItem->getId().'.pdf';
	
				$path_file_dest = str_replace('stamp_design', 'stamp_order', $path_file);
				$path_file_dest = str_replace($filename, $new_name, $path_file_dest);
	
				rename(MAGENTO_ROOT.DS.$path_file, MAGENTO_ROOT.DS.$path_file_dest);
				$options = $orderItem->getProductOptions();
				$options ['design_pdf'] = $path_file_dest;
				$orderItem->setProductOptions( $options );
			}
		}
	
	}
	
	public function catalogProductSaveBefore(Varien_Event_Observer $observer){
		$product = $observer->getProduct();
		$groupedLinkData = $product->getGroupedLinkData();
		if($groupedLinkData){
			$existIds = array();
			foreach ($groupedLinkData as  $product_id=>$data){
				$existIds[]=$product_id;
			}
			$collection = Mage::getModel('catalog/product')->getCollection()
						->addAttributeToSelect('*')
						->addAttributeToFilter('attribute_set_id', MACHINE_ATTRIBUTE_SET_ID)
						->addAttributeToFilter('entity_id', array('in'=>$existIds))
			;
			
			foreach ($collection as $machine){
				$textplateId =  $machine->getMachineTextplate();
				if(!in_array($textplateId, $existIds)){
					$pro = Mage::getModel('catalog/product')->load($textplateId);
					if($pro && $pro->getId()){
						$groupedLinkData[$textplateId]=array('qty'=>0,'position'=>0);
					}
					
				}
				$inkpadIds = $machine->getMachineInkpads();
				if($inkpadIds){
					if(!is_array($inkpadIds)){
						$inkpadIds = explode(',', $inkpadIds);
					}
					foreach($inkpadIds as $id){
						if(!in_array($id, $existIds)){
							$pro = Mage::getModel('catalog/product')->load($id);
							if($pro && $pro->getId()){
								$groupedLinkData[$id]=array('qty'=>0,'position'=>0);
							}
						}
					}
				}
			}
			
			$product->setGroupedLinkData($groupedLinkData);	
		}
	}
	
	
	public function orderCancelAfter(Varien_Event_Observer $observer){
		$order = $observer->getOrder();
		$order->sendOrderCancelEmail();
	}
	
	public function addMassAction($observer) {
	    $block = $observer->getEvent()->getBlock();
	    //baotn edit fixbug conflict with xtento
	    if($block instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction
	            && strstr( $block->getRequest()->getControllerName(), 'sales_order') )
	    {
	        $block->addItem('downloadpdfTextplate', array(
	                'label' => Mage::helper('text2image')->__('Download Textplate PDF'),
	                'url' => Mage::app()->getStore()->getUrl('text2image/adminhtml_sales_order_export/pdfexport'),
	        ));
	        $block->addItem('downloadtiffTextplate', array(
	                'label' => Mage::helper('text2image')->__('Download Textplate TIFF'),
	                'url' => Mage::app()->getStore()->getUrl('text2image/adminhtml_sales_order_export/tiffexport'),
	        ));
	        $block->addItem('downloadftpTextplate', array(
	                'label' => Mage::helper('text2image')->__('FTP Textplate TIFF'),
	                'url' => Mage::app()->getStore()->getUrl('text2image/adminhtml_sales_order_export/tiffftpexport'),
	        ));
	        $block->addItem('printPackBeforeShip', array(
	                'label' => Mage::helper('text2image')->__('Print Packing Slip Before Shipping'),
	                'url' => Mage::app()->getStore()->getUrl('text2image/adminhtml_sales_order_export/massPrintSlip'), 
	        ));
	    }
	}
	//daily clear temp file
	public function dailyClearTempFile($observer){
		$tempDir = Mage::getBaseDir('base') . DS . "media" . DS . "tmp" . DS ."textplate";
		$tempFile = scandir($tempDir);
		foreach($tempFile as $file){
		   if(time() > filemtime($tempDir. DS . $file) + 7*24*60*60){
		       unlink($tempDir. DS . $file);
		   }
		}
		return true;
	//	exit;
	}
}