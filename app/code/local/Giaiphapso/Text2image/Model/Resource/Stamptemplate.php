<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Cms
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Cms page mysql resource
 *
 * @category    Mage
 * @package     Mage_Cms
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Giaiphapso_Text2image_Model_Resource_Stamptemplate extends Mage_Core_Model_Resource_Db_Abstract
{
    const ATT_TEXTPLATE_SIZE_CODE = 'textplate_size';
    /**
     * Store model
     *
     * @var null|Mage_Core_Model_Store
     */
    protected $_store  = null;

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {    
        $this->_init('text2image/stamptemplate', 'template_id');
    }

    /**
     * Process stamptemplate data before deleting
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Giaiphapso_Text2image_Model_Resource_Stamptemplate
     */
    protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
        $condition = array(
            'template_id = ?'     => (int) $object->getId(),
        );

        $this->_getWriteAdapter()->delete($this->getTable('text2image/stamptemplate_store'), $condition);
        $this->_getWriteAdapter()->delete($this->getTable('text2image/stamptemplate_textplate'), $condition);
        
        return parent::_beforeDelete($object);
    }


    /**
     * Assign template to store views
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Giaiphapso_Text2image_Model_Resource_Stamptemplate
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {    
        $oldStores = $this->lookupStoreIds($object->getId());
        $newStores = (array)$object->getStores();
        
        $oldProducts = $this->lookupProductIds($object->getId()); 
        $newProducts = (array)$object->getData('product_id'); 
        
        if (empty($newStores)) {
            $newStores = (array)$object->getStoreId();
        }
        
        $table  = $this->getTable('text2image/stamptemplate_store'); 
        $insert = array_diff($newStores, $oldStores);
        $delete = array_diff($oldStores, $newStores);
        
        $table_product  = $this->getTable('text2image/stamptemplate_textplate');
        $insert_product = array_diff($newProducts, $oldProducts);
        $delete_product = array_diff($oldProducts, $newProducts);
        if ($delete) {
            $where = array(
                'template_id = ?'     => (int) $object->getId(),
                'store_id IN (?)' => $delete
            );

            $this->_getWriteAdapter()->delete($table, $where);
        }

        if ($insert) {
            $data = array();

            foreach ($insert as $storeId) {
                $data[] = array(
                    'template_id'  => (int) $object->getId(),
                    'store_id' => (int) $storeId
                );
            }

            $this->_getWriteAdapter()->insertMultiple($table, $data);
        }
        
        if ($delete_product) {
            $where = array(
                    'template_id = ?'     => (int) $object->getId(),
                    'product_id IN (?)' => $delete_product
            );
        
            $this->_getWriteAdapter()->delete($table_product, $where);
        }
        
        if ($insert_product) {
            $data = array();
        
            foreach ($insert_product as $productId) {
                $data[] = array(
                        'template_id'  => (int) $object->getId(),
                        'product_id' => (int) $productId
                );
            }
        
            $this->_getWriteAdapter()->insertMultiple($table_product, $data);
        }

        return parent::_afterSave($object);
    }

   
    /**
     * Perform operations after object load
     *
     * @param Mage_Core_Model_Abstract $object
     * @return Giaiphapso_Text2image_Model_Resource_Stamptemplate
     */
    protected function _afterLoad(Mage_Core_Model_Abstract $object)
    {
        if ($object->getId()) {
            $stores = $this->lookupStoreIds($object->getId());
            $products = $this->lookupProductIds($object->getId());
            $template_size = $this->lookupTemplateSize($object->getData('textplate_size'));
            $object->setData('width', $template_size['width']);
            $object->setData('height', $template_size['height']);
            $object->setData('store_id', $stores);
            $object->setData('product_id', $products);
        }

        return parent::_afterLoad($object);
    }

    /**
     * Retrieve select object for load object data
     *
     * @param string $field
     * @param mixed $value
     * @param Giaiphapso_Text2image_Model_Stamptemplate $object
     * @return Zend_Db_Select
     */
    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);
        
        if ($object->getStoreId()) {
            $storeIds = array(Mage_Core_Model_App::ADMIN_STORE_ID, (int)$object->getStoreId());
            $select->join(
                array('text2image_template_store' => $this->getTable('text2image/stamptemplate_store')),
                $this->getMainTable() . '.template_id = text2image_template_store.template_id',
                array())
                ->where('is_active = ?', 1)
                ->where('text2image_template_store.store_id IN (?)', $storeIds)
                ->order('text2image_template_store.store_id DESC')
                ->limit(1);
        }

        return $select;
    }

    /**
     * Retrieve load select with filter by identifier, store and activity
     *
     * @param string $identifier
     * @param int|array $store
     * @param int $isActive
     * @return Varien_Db_Select
     */
    protected function _getLoadByIdentifierSelect($identifier, $store, $isActive = null)
    {
        $select = $this->_getReadAdapter()->select()
            ->from(array('cp' => $this->getMainTable()))
            ->join(
                array('cps' => $this->getTable('text2image/stamptemplate_store')),
                'cp.template_id = cps.template_id',
                array())
            ->where('cp.identifier = ?', $identifier)
            ->where('cps.store_id IN (?)', $store);

        if (!is_null($isActive)) {
            $select->where('cp.is_active = ?', $isActive);
        }

        return $select;
    }

    /**
     * Check for unique of identifier of page to selected store(s).
     *
     * @param Mage_Core_Model_Abstract $object
     * @return bool
     */
    public function getIsUniqueStamptemplateToStores(Mage_Core_Model_Abstract $object)
    {
        if (Mage::app()->isSingleStoreMode() || !$object->hasStores()) {
            $stores = array(Mage_Core_Model_App::ADMIN_STORE_ID);
        } else {
            $stores = (array)$object->getData('stores');
        }

        $select = $this->_getLoadByIdentifierSelect($object->getData('identifier'), $stores);

        if ($object->getId()) {
            $select->where('cps.template_id <> ?', $object->getId());
        }

        if ($this->_getWriteAdapter()->fetchRow($select)) {
            return false;
        }

        return true;
    }

    /**
     *  Check whether page identifier is numeric
     *
     * @date Wed Mar 26 18:12:28 EET 2008
     *
     * @param Mage_Core_Model_Abstract $object
     * @return bool
     */
    protected function isNumericPageIdentifier(Mage_Core_Model_Abstract $object)
    {
        return preg_match('/^[0-9]+$/', $object->getData('identifier'));
    }

    /**
     *  Check whether page identifier is valid
     *
     *  @param    Mage_Core_Model_Abstract $object
     *  @return   bool
     */
    protected function isValidPageIdentifier(Mage_Core_Model_Abstract $object)
    {
        return preg_match('/^[a-z0-9][a-z0-9_\/-]+(\.[a-z0-9_-]+)?$/', $object->getData('identifier'));
    }



    /**
     * Check if page identifier exist for specific store
     * return page id if page exists
     *
     * @param string $identifier
     * @param int $storeId
     * @return int
     */
    public function checkIdentifier($identifier, $storeId)
    {
        $stores = array(Mage_Core_Model_App::ADMIN_STORE_ID, $storeId);
        $select = $this->_getLoadByIdentifierSelect($identifier, $stores, 1);
        $select->reset(Zend_Db_Select::COLUMNS)
            ->columns('cp.template_id')
            ->order('cps.store_id DESC')
            ->limit(1);

        return $this->_getReadAdapter()->fetchOne($select);
    }

    /**
     * Get product ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupTemplateSize($template_size = '')
    {
        $options = Mage::registry('textplate_size_array');
        if(empty($options)){
            $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product',self::ATT_TEXTPLATE_SIZE_CODE);
            /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
            $attributeOptions = $attribute->getSource()->getAllOptions();
            $options = array();
            foreach($attributeOptions as $value){
                if($value['value'] != '')
                    $options[$value['value']] = $value['label'];
            }
        }
        if(!empty($template_size)){
            $size = explode('x',strtolower($options[$template_size]));
            return array('width'=>$size[0], 'height' => $size[1]);
        }else{
            return array('width' => 0, 'height' => 0);
        }
    }
    /**
     * Get product ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupProductIds($templateId)
    {
        $adapter = $this->_getReadAdapter();
    
        $select  = $adapter->select()
        ->from($this->getTable('text2image/stamptemplate_textplate'), 'product_id')
        ->where('template_id = ?',(int)$templateId);
    
        return $adapter->fetchCol($select);
    }
    /**
     * Get store ids to which specified item is assigned
     *
     * @param int $id
     * @return array
     */
    public function lookupStoreIds($pageId)
    { 
        $adapter = $this->_getReadAdapter();

        $select  = $adapter->select()
            ->from($this->getTable('text2image/stamptemplate_store'), 'store_id')
            ->where('template_id = ?',(int)$pageId);

        return $adapter->fetchCol($select);
    }

    /**
     * Set store model
     *
     * @param Mage_Core_Model_Store $store
     * @return Mage_Cms_Model_Resource_Page
     */
    public function setStore($store)
    {
        $this->_store = $store;
        return $this;
    }

    /**
     * Retrieve store model
     *
     * @return Mage_Core_Model_Store
     */
    public function getStore()
    {
        return Mage::app()->getStore($this->_store);
    }
}
