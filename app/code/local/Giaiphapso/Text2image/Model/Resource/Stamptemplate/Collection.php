<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Cms
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * CMS page collection
 *
 * @category    Mage
 * @package     Mage_Cms
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Giaiphapso_Text2image_Model_Resource_Stamptemplate_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Load data for preview flag
     *
     * @var bool
     */
    protected $_previewFlag;


    /**
     * Define resource model
     *
     */
    protected function _construct()
    {   
        $this->_init('text2image/stamptemplate');
        $this->_map['fields']['template_id'] = 'main_table.template_id';
        $this->_map['fields']['store']   = 'store_table.store_id';
        $this->_map['fields']['product']   = 'product_table.product_id';
    }

    /**
     * deprecated after 1.4.0.1, use toOptionIdArray()
     *
     * @return array
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray('identifier', 'title');
    }

    /**
     * Returns pairs identifier - title for unique identifiers
     * and pairs identifier|page_id - title for non-unique after first
     *
     * @return array
     */
    public function toOptionIdArray()
    {   
        $res = array();
        $existingIdentifiers = array();
        foreach ($this as $item) {
            $identifier = $item->getData('identifier');

            $data['value'] = $identifier;
            $data['label'] = $item->getData('title');

            if (in_array($identifier, $existingIdentifiers)) {
                $data['value'] .= '|' . $item->getData('template_id');
            } else {
                $existingIdentifiers[] = $identifier;
            }

            $res[] = $data;
        }

        return $res;
    }
    /**
     * Perform operations after collection load
     *
     * @return Giaiphapso_Text2image_Model_Resource_Stamptemplate_Collection
     */
    protected function _afterLoad()
    {
        if ($this->_previewFlag) {
            $items = $this->getColumnValues('template_id');
            $connection = $this->getConnection();
            if (count($items)) {
                $select = $connection->select()
                        ->from(array('cps'=>$this->getTable('text2image/stamptemplate_store')))
                        ->where('cps.template_id IN (?)', $items);
                
                if ($result = $connection->fetchPairs($select)) {
                    foreach ($this as $item) {
                        if (!isset($result[$item->getData('template_id')])) {
                            continue;
                        }
                        if ($result[$item->getData('template_id')] == 0) {
                            $stores = Mage::app()->getStores(false, true);
                            $storeId = current($stores)->getId();
                            $storeCode = key($stores);
                        } else {
                            $storeId = $result[$item->getData('template_id')];
                            $storeCode = Mage::app()->getStore($storeId)->getCode();
                        }
                       
                        $item->setData('_first_store_id', $storeId);
                        $item->setData('store_code', $storeCode);
                    }
                }
            }
        }

        return parent::_afterLoad();
    }
    /**
     * Add filter by textplate product
     *
     * @param int|Mage_Core_Model_Store $store
     * @param bool $withAdmin
     * @return Giaiphapso_Text2image_Model_Resource_Stamptemplate_Collection
     */
    public function addTextplateFilter($textplate, $withAdmin = true)
    {
        if (!$this->getFlag('textplate_filter_added')) {
            if ($textplate instanceof Mage_Catalog_Model_Product) {
                $textplate = array($textplate->getId());
            }
    
            if (!is_array($textplate)) {
                $textplate = array($textplate);
            }
    
            $this->addFilter('product', array('in' => $textplate), 'public');
        }
        return $this;
    }
    /**
     * Add filter by store
     *
     * @param int|Mage_Core_Model_Store $store
     * @param bool $withAdmin
     * @return Giaiphapso_Text2image_Model_Resource_Stamptemplate_Collection
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            if ($store instanceof Mage_Core_Model_Store) {
                $store = array($store->getId());
            }

            if (!is_array($store)) {
                $store = array($store);
            }

            if ($withAdmin) {
                $store[] = Mage_Core_Model_App::ADMIN_STORE_ID;
            }

            $this->addFilter('store', array('in' => $store), 'public');
        }
        return $this;
    }
    //add filter by product
    public function addProductFilter($product, $withAdmin = true)
    {
        if (!$this->getFlag('store_product_added')) {
            if ($product instanceof Mage_Catalog_Model_Product) {
                $product = array($product->getId());
            }
    
            if (!is_array($product)) {
                $product = array($product);
            }
            $this->addFilter('product', array('in' => $product), 'public');
        }
        return $this;
    }
    /**
     * Set first store flag
     *
     * @param bool $flag
     * @return Mage_Cms_Model_Resource_Page_Collection
     */
    public function setFirstStoreFlag($flag = false)
    {
        $this->_previewFlag = $flag;
        return $this;
    }
    /**
     * Join store relation table if there is store filter
     */
    protected function _renderFiltersBefore()
    {
        if ($this->getFilter('store')) {
            $this->getSelect()->join(
                array('store_table' => $this->getTable('text2image/stamptemplate_store')),
                'main_table.template_id = store_table.template_id',
                array()
            )->group('main_table.template_id');

            /*
             * Allow analytic functions usage because of one field grouping
             */
            $this->_useAnalyticFunction = true;
        }
        if ($this->getFilter('product')) {
            $this->getSelect()->join(
                    array('product_table' => $this->getTable('text2image/stamptemplate_textplate')),
                    'main_table.template_id = product_table.template_id',
                    array()
            )->group('main_table.template_id');
        
            /*
             * Allow analytic functions usage because of one field grouping
            */
            $this->_useAnalyticFunction = true;
        }
        return parent::_renderFiltersBefore();
    }


    /**
     * Get SQL for get record count.
     * Extra GROUP BY strip added.
     *
     * @return Varien_Db_Select
     */
    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();

        $countSelect->reset(Zend_Db_Select::GROUP);

        return $countSelect;
    }
}
