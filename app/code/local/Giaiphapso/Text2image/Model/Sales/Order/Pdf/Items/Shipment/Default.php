<?php 
    /**
    * Sales Order Shipment Pdf default items renderer
    *
    * @category   Mage
    * @package    Mage_Sales
    * @author     Magento Core Team <core@magentocommerce.com>
    */
    class Giaiphapso_Text2image_Model_Sales_Order_Pdf_Items_Shipment_Default extends Mage_Sales_Model_Order_Pdf_Items_Abstract
    {
        /**
        * Draw item line
        */
        public function draw()
        {   
            $item   = $this->getItem();
            $pdf    = $this->getPdf();
            $page   = $this->getPage();
            $lines  = array();

            // draw Product name
            $lines[0] = array(array(
                'text' => Mage::helper('core/string')->str_split($item->getName(), 60, true, true),
                'feed' => 100,
            ));

            $oitem = $item->getOrderItem();
            $options = $oitem->getProductOptions();	
            if($options['additional_options'][0]['label'] == 'design_image'){ 

                $orderId = $this->getOrder()->getRealOrderId();
                $storeId = $this->getOrder()->getStoreId();
                $data    = $options['additional_options'][0]['value'];
                if( strlen($data) < 50 ){// Old version
                    $imagePath = Mage::getBaseDir('media') . DS . 'order' . DS . $storeId . DS . $options['additional_options'][0]['value']. '.png';     
                }else{// New version
                    $data = Mage::helper('text2image')->convertSerialize2Array($data);
                    $autoSize = $data['auto_size'];
                    $autoWidth = $data['auto_width'];
                    $hasBorder = $data['has_border'];
                    $borderStyle = $data['border_style'];
                    $borderWidth = $data['border_width'];
                    $inkpadColor = $data['inkpad_color'];
                    $zoom_size = $data['zoom_size'];
                    $width = $data['template_width'];
                    $height = $data['template_height'];
                    $line_height = $data['line_height'];
                    $templateType = $data['templateType'];
                    $shape = $data['shape'];
                    $elements = $data['elements'];
                    $infoTemplate['elements'] = $elements;
                    $infoTemplate['imgWidth'] = $width;
                    $infoTemplate['imgHeight'] = $height;
                    $infoTemplate['templateType'] = $templateType;
                    $zoomSize = 1;

                    $basePathIcon = Mage::getBaseDir('media') . DS . 'order' . DS . $storeId . DS; 
                    $fileName     = $orderId . '-' . $item->getSku(); 
                    $template     = Mage::helper('text2image')->createTemplate($infoTemplate, $basePathIcon, $shape ,$autoWidth, $autoSize, $hasBorder, $borderWidth, $borderStyle, $inkpadColor, $line_height, $zoomSize, $storeId);
                    
                    // Save template on server
                    $templatePath = Mage::getBaseDir() . DS . 'var' . DS . 'tmp' . DS . $fileName . '.png'; 
                    $template->saveTemplateOnServer('png', $templatePath, array(72, 72));
                    $imagePath = $templatePath;
                }

                $lines[0][] = array(
                    'image'  => $imagePath,
                    'feed'  => 250
                );
                $line_height = 100;
            }else{
                $lines[0][] = array(
                    'text'  => '',
                    'feed'  => 250
                );
                $line_height = 20;
            }

            // draw QTY
            $lines[0][] = array(
                'text'  => $item->getQty()*1,
                'feed'  => 35
            );

            // draw SKU
            $lines[0][] = array(
                'text'  => Mage::helper('core/string')->str_split($this->getSku($item), 25),
                'feed'  => 565,
                'align' => 'right'
            );

            // Custom options
            $options = $this->getItemOptions();
            if ($options) {
                foreach ($options as $option) {
                    // draw options label
                    if($option['label'] == 'design_image' && !empty($option['value'])){

                    }else{
                        $lines[][] = array(
                            'text' => Mage::helper('core/string')->str_split(strip_tags($option['label']), 70, true, true),
                            'font' => 'italic',
                            'feed' => 110
                        );
                    }


                    // draw options value
                    if ($option['value'] && $option['label'] != 'design_image') {
                        $_printValue = isset($option['print_value'])
                        ? $option['print_value']
                        : strip_tags($option['value']);
                        $values = explode(', ', $_printValue);
                        foreach ($values as $value) {
                            $lines[][] = array(
                                'text' => Mage::helper('core/string')->str_split($value, 50, true, true),
                                'feed' => 115
                            );
                        }
                    }
                }
            }

            $lineBlock = array(
                'lines'  => $lines,
                'height' => 50
            );

            $page = $pdf->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
            $this->setPage($page);
        }
}