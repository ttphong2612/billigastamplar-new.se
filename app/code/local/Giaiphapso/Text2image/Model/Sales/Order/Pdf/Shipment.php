<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Sales Order Shipment PDF model
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Giaiphapso_Text2image_Model_Sales_Order_Pdf_Shipment extends Mage_Sales_Model_Order_Pdf_Shipment
{
    /**
     * Draw table header for product items
     *
     * @param  Zend_Pdf_Page $page
     * @return void
     */
    protected function _drawHeader(Zend_Pdf_Page $page)
    {
        /* Add table head */
        $this->_setFontRegular($page, 10);
        $page->setFillColor(new Zend_Pdf_Color_RGB(0.93, 0.92, 0.92));
        $page->setLineColor(new Zend_Pdf_Color_GrayScale(0.5));
        $page->setLineWidth(0.5);
        $page->drawRectangle(25, $this->y, 570, $this->y-15);
        $this->y -= 10;
        $page->setFillColor(new Zend_Pdf_Color_RGB(0, 0, 0));

        //columns headers
        $lines[0][] = array(
            'text' => Mage::helper('sales')->__('Products'),
            'feed' => 80,
        );
        
        $lines[0][] = array(
        		'text' => Mage::helper('sales')->__('Textplate'),
        		'feed' => 250,
        );

        $lines[0][] = array(
            'text'  => Mage::helper('sales')->__('Qty'),
            'feed'  => 35
        );

        $lines[0][] = array(
            'text'  => Mage::helper('sales')->__('SKU'),
            'feed'  => 565,
            'align' => 'right'
        );

        $lineBlock = array(
            'lines'  => $lines,
            'height' => 10
        );
        $this->drawLineBlocks($page, array($lineBlock), array('table_header' => true));
        $page->setFillColor(new Zend_Pdf_Color_GrayScale(0));
        $this->y -= 20;
    }
    
    /**
     * Draw lines
     *
     * draw items array format:
     * lines        array;array of line blocks (required)
     * shift        int; full line height (optional)
     * height       int;line spacing (default 10)
     *
     * line block has line columns array
     *
     * column array format
     * text         string|array; draw text (required)
     * feed         int; x position (required)
     * font         string; font style, optional: bold, italic, regular
     * font_file    string; path to font file (optional for use your custom font)
     * font_size    int; font size (default 7)
     * align        string; text align (also see feed parametr), optional left, right
     * height       int;line spacing (default 10)
     *
     * @param  Zend_Pdf_Page $page
     * @param  array $draw
     * @param  array $pageSettings
     * @throws Mage_Core_Exception
     * @return Zend_Pdf_Page
     */
    public function drawLineBlocks(Zend_Pdf_Page $page, array $draw, array $pageSettings = array())
    {
    	foreach ($draw as $itemsProp) {
    		if (!isset($itemsProp['lines']) || !is_array($itemsProp['lines'])) {
    			Mage::throwException(Mage::helper('sales')->__('Invalid draw line data. Please define "lines" array.'));
    		}
    		$lines  = $itemsProp['lines'];
    		$height = isset($itemsProp['height']) ? $itemsProp['height'] : 10;
    		if (empty($itemsProp['shift'])) {
    			$shift = 0;
    			foreach ($lines as $line) {
    				$maxHeight = 0;
    				foreach ($line as $column) {
    					if (isset($column['text'])){
    						$lineSpacing = !empty($column['height']) ? $column['height'] : $height;
    						if (!is_array($column['text'])) {
    							$column['text'] = array($column['text']);
    						}
    						$top = 0;
    						foreach ($column['text'] as $part) {
    							$top += $lineSpacing;
    						}
    
    						$maxHeight = $top > $maxHeight ? $top : $maxHeight;
    					}
    				}
    				$shift += $maxHeight;
    			}
    			$itemsProp['shift'] = $shift;
    		}
    
    		if ($this->y - $itemsProp['shift'] < 15) {
    			$page = $this->newPage($pageSettings);
    		}
    
    		foreach ($lines as $line) {
    			$maxHeight = 0;
    			foreach ($line as $column) {
    
    				if (isset($column['text'])){
    					$fontSize = empty($column['font_size']) ? 10 : $column['font_size'];
    					if (!empty($column['font_file'])) {
    						$font = Zend_Pdf_Font::fontWithPath($column['font_file']);
    						$page->setFont($font, $fontSize);
    					} else {
    						$fontStyle = empty($column['font']) ? 'regular' : $column['font'];
    						switch ($fontStyle) {
    							case 'bold':
    								$font = $this->_setFontBold($page, $fontSize);
    								break;
    							case 'italic':
    								$font = $this->_setFontItalic($page, $fontSize);
    								break;
    							default:
    								$font = $this->_setFontRegular($page, $fontSize);
    								break;
    						}
    					}
    
    					if (!is_array($column['text'])) {
    						$column['text'] = array($column['text']);
    					}
    
    					$lineSpacing = !empty($column['height']) ? $column['height'] : $height;
    					$top = 0;
    					foreach ($column['text'] as $part) {
    						if ($this->y - $lineSpacing < 15) {
    							$page = $this->newPage($pageSettings);
    						}
    
    						$feed = $column['feed'];
    						$textAlign = empty($column['align']) ? 'left' : $column['align'];
    						$width = empty($column['width']) ? 0 : $column['width'];
    						switch ($textAlign) {
    							case 'right':
    								if ($width) {
    									$feed = $this->getAlignRight($part, $feed, $width, $font, $fontSize);
    								}
    								else {
    									$feed = $feed - $this->widthForStringUsingFontSize($part, $font, $fontSize);
    								}
    								break;
    							case 'center':
    								if ($width) {
    									$feed = $this->getAlignCenter($part, $feed, $width, $font, $fontSize);
    								}
    								break;
    						}
    						$page->drawText($part, $feed, $this->y-$top, 'UTF-8');
    						$top += $lineSpacing;
    					}
    						
    						
    				}elseif(isset($column['image'])){
    					$image =  $column['image'];
    					if (is_file($image)) {
    
    						$image = $this->png2jpg($image, str_replace('.png', '.jpg', $image), 100);
    
    						$image       = Zend_Pdf_Image::imageWithPath($image);
    						//$top         = 830; //top border of the page
    						$widthLimit  = 270; //half of the page width
    						$heightLimit = 270; //assuming the image is not a "skyscraper"
    						$width       = $image->getPixelWidth();
    						$height      = $image->getPixelHeight();
    
    						//preserving aspect ratio (proportions)
    						$ratio = $width / $height;
    						if ($ratio > 1 && $width > $widthLimit) {
    							$width  = $widthLimit;
    							$height = $width / $ratio;
    						} elseif ($ratio < 1 && $height > $heightLimit) {
    							$height = $heightLimit;
    							$width  = $height * $ratio;
    						} elseif ($ratio == 1 && $height > $heightLimit) {
    							$height = $heightLimit;
    							$width  = $widthLimit;
    						}
    
    						$r = 0.60;
    						$width = ceil($width * $r);
    						$height = ceil($height* $r);
    
    						$y1 = $this->y - $height;
    						$y2 = $y1+$height;
    						$x1 = 250;
    						$x2 = $x1 + $width;
    
    						//coordinates after transformation are rounded by Zend
    						$page->drawImage($image, $x1, $y1, $x2, $y2);
    						//$top += $height;
    					}
    				}
    
    				$maxHeight = $top > $maxHeight ? $top : $maxHeight;
    			}
    			$this->y -= $maxHeight;
    		}
    	}
    
    	return $page;
    }
    
    protected function png2jpg($originalFile, $outputFile, $quality=80) {
    	if(is_file($outputFile)){
    		@unlink($outputFile);
    	}
    		
    	$image = imagecreatefrompng($originalFile);
    	$bg = imagecreatetruecolor(imagesx($image), imagesy($image));
    	imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
    	imagealphablending($bg, TRUE);
    	imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
    	imagedestroy($image);
    		
    	$color_black = imagecolorallocate($bg, 0, 0, 0);
    	$this->drawBorder($bg, $color_black);
    		
    	imagejpeg($bg, $outputFile, $quality);// the 50 is to set the quality, 0 = worst-smaller file, 100 = better-bigger file
    	imagedestroy($bg);
    		
    	return $outputFile;
    }
    
    // Draw a border
    protected  function drawBorder(&$img, &$color, $thickness = 1)
    {
    	$x1 = 0;
    	$y1 = 0;
    	$x2 = imagesx($img) - 1;
    	$y2 = imagesy($img) - 1;
    
    	for($i = 0; $i < $thickness; $i++)
    	{
    	imagerectangle($img, $x1++, $y1++, $x2--, $y2--, $color);
    	}
    }
}
