<?php
class Giaiphapso_Text2image_Model_Sales_Order_Shipment extends Mage_Sales_Model_Order_Shipment
{
	public function setOrderNew(Mage_Sales_Model_Order $order)
	{
		parent::setOrder($order);
		$this->_items = array();
		$items = $order->getAllItems();
		foreach($items as $item)
		{
			if($item->getQtyToShip() >0){
				$shipmentItem = Mage::getModel('sales/order_shipment_item');
				$shipmentItem->setShipment($this);
				$shipmentItem->setOrderItem($item);
				$shipmentItem->setQty($item->getQtyToShip());
				$shipmentItem->setSku($item->getSku());
				if(!$shipmentItem->getName())
					$shipmentItem->setName($item->getName());
				$this->_items[] = $shipmentItem;
			}
		}
		return $this;
	}
	
	
}
