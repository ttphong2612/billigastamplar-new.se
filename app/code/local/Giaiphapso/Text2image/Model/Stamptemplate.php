<?php

    class Giaiphapso_Text2image_Model_Stamptemplate extends Mage_Core_Model_Abstract
    {
        const NOROUTE_PAGE_ID = 'no-route';
        const STATUS_ENABLED = 1;
        const STATUS_DISABLED = 0;
        const TEXTPLATE_SET_ID = 63;
        //const ARRAY_TEXTPLATE_ATTRIBUTE_SET_ID = array(63,70);
        private static $textplateAttributeSetIds = array(10,20);
        const ATT_TEXTPLATE_SIZE_CODE = 'textplate_size';
        public function _construct()
        {
            parent::_construct();
            $this->_init('text2image/stamptemplate'); 
        }  
        /**
        * Load object data
        *
        * @param mixed $id
        * @param string $field
        * @return Giaiphapso_Text2image_Model_Stamptemplate
        */
        public function load($id, $field=null)
        {
            if (is_null($id)) {
                return $this->noRoutePage();
            }
            return parent::load($id, $field);
        }
        
        public function exportTemplates($orders,$type='pdf', $download_ftp = false)
        {   
            $designs   = array();
            
            foreach ($orders as $key=>$order) {
                $order = Mage::getModel('sales/order')->load($order);
                // Start change order status to "In production"
                if( $type == 'tiff' ){
                    if( $order->getState() == 'processing' ){
                        $order->setState('processing', 'in_production', 'Downloaded TIFF file', false);
                        $order->save();   
                    }
                }
                // End change order status to "In production"
                
                $tempArray    = array(); // not move out foreach
                $tempArray[0] = $order->getStoreId(); 
                $tempArray[2] = $order->getIncrementId(); 
                //$realOrderId = $order->getRealOrderId();     
                $items = $order->getItemsCollection();
                foreach($items as $key => $item){
                    $additional_options =  $item->getProductOptionByCode('additional_options') ; 
                    if(!empty($additional_options)){
                        foreach($additional_options as $option){
                            if($option['label'] == 'design_image'){
                                $tempArray[1][] = $option['value']; 
                            }
                        }
                    }
                }
                $designs[] = $tempArray;
            }     

            if(!empty($designs)){
                if($type == 'pdf')
                    $files = Mage::helper('text2image')->downloadOrdersPDF($designs);
                if($type == 'tiff'){
                    $files = Mage::helper('text2image')->downloadOrdersTIFF($designs, $download_ftp);
                }
                return $files;
            }
            return null;
        }
        /**
        * Check if page identifier exist for specific store
        * return page id if page exists
        *
        * @param string $identifier
        * @param int $storeId
        * @return int
        */
        public function checkIdentifier($identifier, $storeId)
        {
            return $this->_getResource()->checkIdentifier($identifier, $storeId);
        }
        /**
        * Load No-Route Page
        *
        * @return Mage_Cms_Model_Page
        */
        public function noRoutePage()
        {
            return $this->load(self::NOROUTE_PAGE_ID, $this->getIdFieldName());
        }
        /**
        * Prepare stamptemplate's statuses.
        *
        * @return array
        */
        public function getAvailableStatuses()
        { 
            $statuses = new Varien_Object(array(
                self::STATUS_ENABLED => Mage::helper('text2image')->__('Enabled'),
                self::STATUS_DISABLED => Mage::helper('text2image')->__('Disabled'),
            ));

            return $statuses->getData();
        }
        /**
        * Prepare stamptemplate's size.
        *
        * @return array
        */
        public function getAvailableSize()
        {   
            $options = Mage::registry('textplate_size_array');
            if(empty($options)){
                $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product',self::ATT_TEXTPLATE_SIZE_CODE);
                /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
                $attributeOptions = $attribute->getSource()->getAllOptions();
                $options = array();
                foreach($attributeOptions as $value){ 
                    if($value['value'] != '')
                        $options[$value['value']] = $value['label'];
                } 
            }
            return $options;
        }
        public function getAllTextplateSelect(){
            $options = array();
            $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('textplate_size')
            ->addAttributeToSelect('textplage_line_limit')
            ->addAttributeToSort('name', 'ASC');
            //->addFieldToFilter('attribute_set_id', self::$textplateAttributeSetIds);
            foreach($products as $product){
                $options[$product->getId()] = $product->getName();
            } 
            return $options;
        }
        public function getAllTextplateForStore(){

            $storeId = Mage::app()->getStore()->getId();
            $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addStoreFilter($storeId)
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('textplate_size')
            ->addAttributeToSelect('textplage_line_limit')
            ->addAttributeToFilter('status', array('eq' => 1))
            ->addFieldToFilter('attribute_set_id', self::$textplateAttributeSetIds);

            //return all products name with attribute set 'my_custom_attribute'
            return $products;
        }
        public function getProductTextplate(){
            $products = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addFieldToFilter('attribute_set_id', self::$textplateAttributeSetIds);
            //process your product collection as per your bussiness logic
            $productsID = array();
            foreach($products as $p){
                $productsID[] = $p->getId();
            }
            //return all products name with attribute set 'my_custom_attribute'
            return $productsID;
        }
        public function getProductTextplateForForm($textplate_size){
            //Load product model collecttion filtered by attribute set id
            if(!empty($textplate_size)){
                $products = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToSelect('name')
                //->addFieldToFilter('attribute_set_id', self::$textplateAttributeSetIds)
                ->addFieldToFilter(array(
                    array('attribute'=>'textplate_size','eq'=>$textplate_size)));
            }else{
                $products = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToSelect('name')
                ->addFieldToFilter('attribute_set_id', self::$textplateAttributeSetIds);
            }

            //process your product collection as per your bussiness logic
            $productsName = array();
            foreach($products as $p){
                $productsName[] = array('value'=>$p->getId(),'label'=>$p->getName());
            }
            //return all products name with attribute set 'my_custom_attribute'
            return $productsName; 
        }

        // Add by phong.tran
        function myDebug($obj)
        {
            if( $_SERVER['REMOTE_ADDR'] == '115.77.54.118' ){
                echo '<pre>';
                print_r($obj);
                die();
            }
        }
}