<?php 
class Giaiphapso_Text2image_Model_Textplate
{
    public $size = 12;
    public $font = 'arial';
    public $id = 0;
    public $afs = 1.2;
    public $bold = false;
    public $italic = false;
    public $underline = false;
    public $text = 'Type your text';
    public $width = 100;//px
    public $height = 19;
    public $left = '';
    public $top = '';
    public $color = '000000';
    public $base64Text = '';
    
    private $_instance;
    
    public function _construct($agrs){
        $this->_init();
    }
    
    public function _init(){
        /* if(!empty($this->modelTemplate->getData('elements'))){
            $instance = unserialize($this->modelTemplate->getData('elements'));
            if($instance instanceof Giaiphapso_Text2image_Model_Textplate){
                $this->_instance = $instance;
            }else{
                $this->_instance = new Giaiphapso_Text2image_Model_Textplate();
            }
        } */
    }
 
    public function getData($type){
        return $this->$type;
    }
    public function setData($type, $value){
        $this->$type = $value;
    }
}