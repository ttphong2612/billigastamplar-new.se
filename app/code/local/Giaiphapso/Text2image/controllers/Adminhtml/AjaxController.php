<?php 
class Giaiphapso_Text2image_Adminhtml_AjaxController extends Mage_Adminhtml_Controller_Action
{
    public function gettextplateAction() {
        $id = $this->getRequest()->getParam('textplate_size');
        $textplateArray = Mage::getModel('text2image/stamptemplate')->getProductTextplateForForm($id);
        $json = json_encode($textplateArray);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($json);
    }
    public function gettextlineAction(){
        $id = $this->getRequest()->getParam('number_line');
        $objectLine = Mage::getSingleton('text2image/textplate');
        $objectLine->setData('id',$id);
        $objectLine->setData('base64Text', base64_encode($objectLine->getData('text')));
        $json = json_encode(get_object_vars($objectLine));
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($json);
    }
    public function getbase64encodeAction(){
        $text =  $this->getRequest()->getParam('text');
        if(!empty($text)){
            $text = base64_encode($text);
        }
        $this->getResponse()->setHeader('Content-type', 'application/html');
        $this->getResponse()->setBody($text);
    }
    
    public function uploadAction(){
        if (!empty($_FILES))
        {
            $result = array();
            try
            {
                $template_id = $this->getRequest()->getParam('template_id');
                $uploader = new Varien_File_Uploader("filename");
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);
                $uploader->setAllowCreateFolders(true);
                //$path = Mage::getBaseDir('base') . DS . "media" . DS . "tmp" . DS . "textplate" . DS;//ex. Mage::getBaseDir('base') . DS ."my_uploads" . DS
                $path = Mage::getBaseDir('media') . DS . "template_icon_default" . DS;
                $uploader->setAllowedExtensions(array('pdf','png','gif','tiff','jpg','bmp','eps','tiff')); //server-side validation of extension
                $new_name = Mage::helper('text2image')->microtime_float();
                $uploadSaveResult = $uploader->save($path, $new_name.'.'.$uploader->getFileExtension());
                $tmp_image = new Imagick();
                $tmp_image->setResolution(600,600);
                $tmp_image->readimage($path . DS . $new_name.'.'.$uploader->getFileExtension());
                //create pdf file
                if($uploader->getFileExtension() != 'pdf'){
                    //write to pdf
                    $tmp_image->setImageFormat("pdf");
                    $tmp_image->writeImage($path . DS . $new_name.'.pdf');
                }
                //write to png
                $tmp_image->setImageFormat("png");
                $tmp_image->writeImage($path . DS . $new_name.'.png');
                
                $result['file'] = $new_name;
                $result['error'] = '';
                $result['status'] = 'success';
            }
            catch(Exception $e)
            {
                $result = array(
                    "error" => $e->getMessage(),
                    "errorCode" => $e->getCode(),
                    "status" => "error"
                );
            }      
//             
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }
}