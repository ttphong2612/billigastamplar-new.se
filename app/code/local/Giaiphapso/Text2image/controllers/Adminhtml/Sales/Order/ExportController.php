<?php
/**
 * @category   Giaiphapso
 * @package    Export Order
 * @copyright  Copyright (c) 2013 Wimbolt Ltd (http://www.giaiphapso.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @author     Tran Ngoc Bao <bao.tran@giaiphapso.com>
 **/
class Giaiphapso_Text2image_Adminhtml_Sales_Order_ExportController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Exports orders defined by id in post param "order_ids" to csv and offers file directly for download
     * when finished.
     */
    public function pdfexportAction() 
    {
    	$orders = $this->getRequest()->getPost('order_ids', array());
		$files = Mage::getModel('text2image/stamptemplate')->exportTemplates($orders,'pdf');
		if($files){
		    return $this->_prepareDownloadResponse(
		            'Template'.Mage::getSingleton('core/date')->date('Y-m-d_H-i-s').'.pdf', $files->getBuffer(),
		            'application/pdf'
		    );
		}
		$this->_redirect('admin/sales_order');
    }
    /**
     * Print packing slips before shipping
     */
    public function massPrintSlipAction()
    {   
        $order_ids = $this->getRequest()->getParam('order_ids');
        $order = Mage::getModel('sales/order');
    
        $pdf = new Zend_Pdf(); // the one big pdf with all the orders that need packing slips
		
        foreach($order_ids as $order_id)
        {
            $order = Mage::getModel('sales/order');
            $order->load($order_id);
            $fauxShipment = Mage::getModel('sales/order_shipment');
            $fauxShipment->setOrderNew($order); 
            $shipments = array();
            $shipments[] = $fauxShipment;
            $tmp_pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf($shipments);
            $pdf->pages = array_merge($pdf->pages, $tmp_pdf->pages);
        }
		     
		$this->_prepareDownloadResponse('packingslip.pdf',
                $pdf->render(), 'application/pdf'
        );
    }
    /**
     * Exports orders defined by id in post param "order_ids" to csv and offers file directly for download
     * when finished.
     */
    public function tiffexportAction()
    {
        $orders = $this->getRequest()->getPost('order_ids', array()); 
        $zipName = Mage::getModel('text2image/stamptemplate')->exportTemplates($orders,'tiff');
        if($zipName){
			$zipPath = Mage::getBaseDir() . DS . 'var' . DS . 'export' . DS . 'order' . DS . $zipName;
            return $this->_prepareDownloadResponse(
                    'Template'.$zipName, 
					array('type' => 'filename', 'value' => $zipPath),
                    'Content-Type: application/zip'
            );
        }
        // Redirect
        $this->_redirect('./admin/sales_order/index/key');
    }
    /**
     * Exports orders defined by id in post param "order_ids" to csv and offers file directly for download
     * when finished.
     */
    public function tiffftpexportAction()
    {
        $orders = $this->getRequest()->getPost('order_ids', array());
        $files = Mage::getModel('text2image/stamptemplate')->exportTemplates($orders,'tiff', true);
        $this->_redirect('./admin/sales_order/index/key');
    }
}
?>