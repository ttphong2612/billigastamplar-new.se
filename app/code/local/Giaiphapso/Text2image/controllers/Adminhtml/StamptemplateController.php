<?php
class Giaiphapso_Text2image_Adminhtml_StamptemplateController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Init actions
     *
     * @return Giaiphapso_Text2image_Adminhtml_StamptemplateController
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('text2image/stamptemplate')
            ->_addBreadcrumb(Mage::helper('text2image')->__('Text2Image'), Mage::helper('text2image')->__('Text2Image'))
            ->_addBreadcrumb(Mage::helper('text2image')->__('Manage Stamptemplate'), Mage::helper('text2image')->__('Manage Stamptemplate'))
            ;
             $req  = Mage::app()->getRequest();
            $o = Mage::app();
            $info = sprintf(
                    "\nRequest: %s\nFull Action Name: %s_%s_%s\nHandles:\n\t%s\nUpdate XML:\n%s",
                    $req->getRouteName(),
                    $req->getRequestedRouteName(),      //full action name 1/3
                    $req->getRequestedControllerName(), //full action name 2/3
                    $req->getRequestedActionName(),     //full action name 3/3
                    implode("\n\t",$o->getLayout()->getUpdate()->getHandles()),
                    $o->getLayout()->getUpdate()->asString()
            );
            
            // Force logging to var/log/layout.log
            Mage::log($info, Zend_Log::INFO, 'layout.log', true); 
        return $this;
    }
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->_title($this->__('Text2Image'))
             ->_title($this->__('Stamptemplate'))
             ->_title($this->__('Manage Stamptemplates'));

        $this->_initAction();
        $this->renderLayout();
    }
    
    public function newAction() {
        // the same form is used to create and edit
        $this->_forward('edit');
    }
    public function editAction() {
        $this->_title($this->__('Text2Image'))
             ->_title($this->__('Stamptemplate'))
             ->_title($this->__('Manage Stamptemplate'));
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('template_id');
        $model = Mage::getModel('text2image/stamptemplate');
        
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (! $model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('text2image')->__('This page no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }
        
        $this->_title($model->getId() ? $model->getTitle() : $this->__('New Stamptemplate'));
        
        // 3. Set entered data if was error when we do save
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (! empty($data)) {
            $model->setData($data);
        }
      
        // 4. Register model to use later in blocks
        Mage::register('text2image_stamptemplate', $model);
        // 5. Build edit form
        $this->_initAction()
        ->_addBreadcrumb(
                $id ? Mage::helper('text2image')->__('Edit Stamptemplate')
                : Mage::helper('text2image')->__('New Stamptemplate'),
                $id ? Mage::helper('text2image')->__('Edit Stamptemplate')
                : Mage::helper('text2image')->__('New Stamptemplate'));
        
        $this->renderLayout();
    }
    //mass delete stamptemplate
    public function massDeleteAction() {
        $stamptemplateIds = $this->getRequest ()->getParam ( 'stamptemplate');
        if (! is_array ( $stamptemplateIds )) {
            Mage::getSingleton ( 'adminhtml/session' )->addError ( Mage::helper ( 'adminhtml' )->__ ( 'Please select item(s)' ) );
        } else {
            try {
                foreach ( $stamptemplateIds as $stamptemplateId ) {
                    $stamtemplate = Mage::getModel ( 'text2image/stamptemplate' )->load ( $stamptemplateId );
                    $stamtemplate->delete ();
                }
                Mage::getSingleton ( 'adminhtml/session' )->addSuccess ( Mage::helper ( 'adminhtml' )->__ ( 'Total of %d record(s) were successfully deleted', count ( $stamptemplateIds ) ) );
            } catch ( Exception $e ) {
                Mage::getSingleton ( 'adminhtml/session' )->addError ( $e->getMessage () );
            }
        }
        $this->_redirect ( '*/*/index' );
    }
    //mass change status action
    public function massStatusAction()
    {
        $stamptemplateIds = $this->getRequest()->getParam('stamptemplate');
        if(!is_array($stamptemplateIds)) {
            // No products selected
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select template(s).'));
        } else {
            try {
                foreach ($stamptemplateIds as $stamptemplateId) {
                    $stamptemplate = Mage::getModel('text2image/stamptemplate')
                        ->load($stamptemplateId)
                        ->setIs_active($this->getRequest()->getParam('status'));
                     $stamptemplate->save();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('Total of %d record(s) have been updated.', count($stamptemplateIds))
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $ret = $this->getRequest()->getParam('ret') ? $this->getRequest()->getParam('ret') : 'index';
        $this->_redirect('*/*/'.$ret);
    }
    //mass change sort action
    public function updateSortAction()
    {
        $stamptemplateId = $this->getRequest()->getParam('stamptemplate');
        $weight = $this->getRequest()->getParam('weight'); 
        try {
            if(isset($weight) && is_numeric($stamptemplateId)){
                $stamptemplate = Mage::getModel('text2image/stamptemplate')
                                ->load($stamptemplateId)
                                ->setWeight(intval($weight));
                $stamptemplate->save();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(
                $this->__('Total of %d record(s) have been updated.', count($stamptemplateIds))
            );
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }
    /**
     * prepare post data
     */
    protected function _preparePostData($data){
        
        $result = array();
        foreach($data['data'] as $key => $value){
            foreach($value as $item => $option){
                $result[$item][$key] = $option; 
                if($key == 'text'){
                    $result[$item]['base64Text'] = base64_encode($option);
                }         
            }
        }
        $data['elements'] = Mage::helper('core')->jsonEncode($result);
        return $data;
    }
    /**
     * generate PDF / Image
     */
    protected function _generatePdf($data, $template_size){
        
        Mage::helper('text2image')->generatePdf($data, $template_size);
    }
    /**
     * Save action
     */
    public function saveAction()
    {
        // check if data sent
        if ($data = $this->getRequest()->getPost()) {
            $data = $this->_filterPostData($data); 
            //init model and set data
            $model = Mage::getModel('text2image/stamptemplate');
            $data = $this->_preparePostData($data);
            if ($id = $this->getRequest()->getParam('template_id')) {
                $model->load($id);
            }
            $this->model = clone $model;
            $model->setData('textplate_size', $data['textplate_size']);
			$model->setData('title', $data['title']);
			$model->setData('elements', $data['elements']);
			$model->setData('datefield', $data['datefield']);
			$model->setData('is_active', $data['is_active']);
			$model->setData('stores', $data['stores']);
			$model->setData('product_id', $data['product_id']);
            // try to save it
            try {
                // save the data
                $model->save();
                //generate pdf file
                $data['template_id'] = $model->getId(); 
                $width_textplate = $model->getData('width');
                $height_textplate = $model->getData('height');
                
                if(empty($width_textplate) || empty($height_textplate)){
                    $template_size = $model->getAvailableSize();
                    $current_size = $template_size[$data['textplate_size']];
                    $current_size = explode('x',$current_size); 
                    $model->setData('width',$current_size[0]);
                    $model->setData('height',$current_size[1]);
                }
                
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('text2image')->__('The stamp template has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('template_id' => $model->getId(), '_current'=>true));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/');
                return;
    
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
            catch (Exception $e) {
                $this->_getSession()->addException($e,
                        Mage::helper('text2image')->__('An error occurred while saving the page.'));
            }
    
            $this->_getSession()->setFormData($data);
            $this->_redirect('*/*/edit', array('template_id' => $this->getRequest()->getParam('template_id')));
            return;
        }
        $this->_redirect('*/*/');
    }
    /**
     * Filtering posted data. Converting localized data if needed
     *
     * @param array
     * @return array
     */
    protected function _filterPostData($data)
    {  
        $data = $this->_filterDates($data, array('custom_theme_from', 'custom_theme_to'));
        
        return $data;
    }
    //get textplate by size
    public function getTextplateAction(){
        
    }
}
