<?php
class Giaiphapso_Text2image_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function indexAction ()
    {
        $this->loadLayout();
        $req  = Mage::app()->getRequest();
        $this->renderLayout();
    }
    public function uploadAction(){
        if (!empty($_FILES))
        {
            $result = array();
            try
            {
                $textplateId = $this->getRequest()->getParam('textplate_id');
                $textplateProduct = Mage::getSingleton('catalog/product')->load($textplateId);
                $this->shape = $textplateProduct->getResource()->getAttribute('shape')->getSource()->getOptionText($textplateProduct->getData('shape'));
                
                $model = Mage::getSingleton('text2image/stamptemplate');
                $availabeSize = $model->getAvailableSize();
                $textplateSize = $availabeSize[$textplateProduct->getData('textplate_size')];
                $arrSize = explode('x',$textplateSize);
                $width = $arrSize[0]*Mage::helper('text2image')->getRateMM2Pixcel();
                $height = $arrSize[1]*Mage::helper('text2image')->getRateMM2Pixcel();
				
                $uploader = new Varien_File_Uploader("uploadfile");
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);
                $uploader->setAllowCreateFolders(true);
                $path = Mage::getBaseDir('base') . DS . "media" . DS . "tmp" . DS . "textplate" . DS;//ex. Mage::getBaseDir('base') . DS ."my_uploads" . DS
                $uploader->setAllowedExtensions(array('pdf','png','gif','tiff','jpg','bmp','eps', 'jpeg', 'svg')); //server-side validation of extension
				
                $new_name = Mage::helper('text2image')->microtime_float();
                $uploadSaveResult = $uploader->save($path, $new_name.'.'.$uploader->getFileExtension());
				
                //trim white space and write to tiff file
                $tmp_image = new Imagick();
				$checkImage = clone $tmp_image;
				$checkImage->readimage($path . DS . $new_name.'.'.$uploader->getFileExtension());
         	    if($checkImage->getImageWidth() > $width*6 || $checkImage->getImageHeight() > $height*6){
					$addResolution = false;
				}else{
					$addResolution = true;
				}
				$checkImage->clear();
                $checkImage->destroy();
				if($addResolution){
					$tmp_image->setResolution(600,600);
				}
				$tmp_image->readimage($path . DS . $new_name.'.'.$uploader->getFileExtension());
				$tmp_image->setImageBackgroundColor('white');
				$tmp_image = $tmp_image->flattenImages();
                $tmp_image->borderImage(new ImagickPixel( 'white' ), 1, 1);
                $tmp_image->trimImage(0);
				
				if ($tmp_image->getImageColorspace() == Imagick::COLORSPACE_CMYK) {
					$tmp_image->quantizeImage(64, Imagick::COLORSPACE_RGB, 0, false, false );
                	$tmp_image->setImageDepth(8);
				}
				
                //create pdf file
                $tmp_image->setImageFormat("tiff");
                $tmp_image->writeImage($path . DS . $new_name.'.tiff');
				
                $result['widthOriginal'] = $tmp_image->getImageWidth();
                $result['heightOriginal'] = $tmp_image->getImageHeight();
				$tmp_image->clear();
                $tmp_image->destroy();
                $tyle = $result['widthOriginal'] / $result['heightOriginal'];
                
                $new_width = $width/3;
                $new_height = $height/3;
                
                if ($tyle > 1){
                  if($result['widthOriginal'] / 3 >= $width) { 
                    $new_width = $width;
                    $new_height = (int) $width / $tyle;
                    if($new_height > $height){
                      $new_height = $height/2;
                      $new_width =  $new_height * $tyle;
                    }
                  }
                } else{
                  if($result['heightOriginal']/3 >= $height) {
                    $new_height = $height;
                    $new_width = (int) $new_height * $tyle;
                  }
                }
                $new_height = $new_height / Mage::helper('text2image')->getRateMM2Pixcel();
                $new_width = $new_width / Mage::helper('text2image')->getRateMM2Pixcel();
               
                $result['width'] = $new_width;
                $result['height'] = $new_height;
                $result['file'] = $new_name;
                $result['error'] = '';
                $result['status'] = 'success';
            }
            catch(Exception $e)
            {
                $result = array(
                    "error" => $e->getMessage(),
                    "errorCode" => $e->getCode(),
                    "status" => "error"
                );
            }
            //
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }
    protected function _preparePostData($data){
    
        $result = array();
        foreach($data as $key => $value){
            foreach($value as $item => $option){
                $result[$item][$key] = $option;
                if($key == 'text'){
                    $result[$item]['base64Text'] = base64_encode($option);
                }
            }
        }
        $data['elements'] = Mage::helper('core')->jsonEncode($result);
        return $data;
    }
    public function previewAction(){
                     
        $elements = array();
        $elements = $this->getRequest()->getParam('data');
        $data = $this->_preparePostData($elements);
        $template_width = $this->getRequest()->getParam('template_width');
        $template_height = $this->getRequest()->getParam('template_height');
        /*phong.tran*/
        $shape = $this->getRequest()->getParam('shape');
        $has_border = $this->getRequest()->getParam('has_border');
		
        if(!empty($template_width) && !empty($template_height)){
            if(count($elements) > 0){
                try{
                    $helper = Mage::helper('text2image');
                    $new_name = $helper->microtime_float();
                    /*phong.tran*/
                    //$helper->generatePdf($data, array($helper->_convertToMilimet($template_width), $helper->_convertToMilimet($template_height)), null, $new_name );
                    $helper->generatePdf($data, array($template_width, $template_height), null, $new_name, null , $shape, $has_border );
                    $result['file'] = $new_name;
                    $result['product_url'] = '';
                    $result['error'] = '';
                    $result['status'] = 'success';
                }
                catch(Exception $e)
                {
                    $result = array(
                            "error" => $e->getMessage(),
                            "errorCode" => $e->getCode(),
                            "status" => "error"
                    );
                }
            }
        }
        
        $json = json_encode($result); 
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($json);
    }
    
    public function templateAction(){
        $elements = array();
        $elements = $this->getRequest()->getParam('data');
        $elements['auto_size'] = $this->getRequest()->getParam('auto_size');
        $elements['auto_width'] = $this->getRequest()->getParam('auto_width');
        $elements['has_border'] = $this->getRequest()->getParam('has_border');
        $elements['border_style'] = $this->getRequest()->getParam('border_style');
        $elements['border_width'] = $this->getRequest()->getParam('border_width');
        $elements['inkpad_color'] = $this->getRequest()->getParam('inkpad_color');
        $elements['zoom_size'] = $this->getRequest()->getParam('zoom_size');
        $elements['template_height'] = $this->getRequest()->getParam('template_height');
        $elements['template_width'] = $this->getRequest()->getParam('template_width');
        $elements['line_height'] = $this->getRequest()->getParam('line_height');
        $elements['templateType'] = $this->getRequest()->getParam('templateType');
        $elements['shape'] = $this->getRequest()->getParam('shape');
        
        $infoTemplate['elements'] = $this->getRequest()->getParam('data');
        $infoTemplate['imgWidth'] = $elements['template_width'];
        $infoTemplate['imgHeight'] = $elements['template_height'];
        $infoTemplate['templateType'] = $elements['templateType'];
        //scale template if auto size
        $template  = Mage::helper('text2image')->getDataLines($infoTemplate, $elements['shape'], $elements['auto_width'], $elements['auto_size'], $elements['has_border'], $elements['border_width'], $elements['border_style'], $elements['inkpad_color'], $elements['line_height'] );
        $resizeElements = $template->getElements();
     
        //merge element
        $elements = Mage::helper('text2image')->mergeElements($elements, $resizeElements);
        //encode element
        $encryptElement = base64_encode(serialize($elements));
        $result['url'] = urlencode($encryptElement);
        $result['status'] = 'success';
        $json = json_encode($result);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($json);
    }
    
    public function getTemplateAction()
    {
        $this->getResponse()->setHeader('Content-type', 'application/html');
        $this->getResponse()->setBody($this->getLayout()
                                           ->createBlock('text2image/textplate_template')
                                           ->setTemplate('text2image/template/list.phtml')
                                           ->toHtml());
    }

    public function getbase64encodeAction(){
        $text =  $this->getRequest()->getParam('text');
        if(!empty($text)){
            $text = base64_encode($text);
        }
        $this->getResponse()->setHeader('Content-type', 'application/html');
        $this->getResponse()->setBody($text);
    }
}