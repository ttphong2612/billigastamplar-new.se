<?php
    class Giaiphapso_Text2image_ImageController extends Mage_Core_Controller_Front_Action
    {
        protected function _preparePostData($data){

            $result = array();
            foreach($data as $key => $value){
                foreach($value as $item => $option){
                    $result[$item][$key] = $option;
                    if($key == 'text'){
                        $result[$item]['base64Text'] = base64_encode($option);
                    }
                }
            }
            $data['elements'] = Mage::helper('core')->jsonEncode($result);
            return $data;
        }

        public function templateAction ()
        {
            $data = $this->getRequest()->getParam('data');
            $template_id = $this->getRequest()->getParam('template_id');
            $storeId =  $this->getRequest()->getParam('sid');
            $infoTemplate = array();
            if($template_id){
                $template = Mage::getModel('text2image/stamptemplate')->load($template_id);
                $elements = $template->getElements();
                $width = $template->getWidth();
                $height = $template->getHeight();
                if($template->getDatefield()){
                    $templateType = 'DATER';
                }else{
                    $templateType = 'STAMP';
                }

                $line_height = 0.1;
                $autoSize = 1;
            }
            if(!empty($data)){
                $data = Mage::helper('text2image')->convertSerialize2Array($data);
                $autoSize = $data['auto_size'];
                $autoWidth = $data['auto_width'];
                $hasBorder = $data['has_border'];
                $borderStyle = $data['border_style'];
                $borderWidth = $data['border_width'];
                $inkpadColor = $data['inkpad_color'];
                $zoom_size = $data['zoom_size'];
                $width = $data['template_width'];
                $height = $data['template_height'];
                $line_height = $data['line_height'];
                $templateType = $data['templateType'];
                $shape = $data['shape'];
                $elements = $data['elements'];
            }
            $infoTemplate['elements'] = $elements;
            $infoTemplate['imgWidth'] = $width;
            $infoTemplate['imgHeight'] = $height;
            $infoTemplate['templateType'] = $templateType;
            Mage::helper('text2image')->getTemplateimage($infoTemplate, $shape ,$autoWidth, $autoSize, $hasBorder, $borderWidth, $borderStyle, $inkpadColor, $line_height, $zoom_size, $storeId);

        }

        function downloadAction()
        {
            $incrementId = $this->getRequest()->getParam('incrementid');
            $storeId     = $this->getRequest()->getParam('sid');
            $data        = $this->getRequest()->getParam('data');
            
                if(!empty($data)){
                    $data = Mage::helper('text2image')->convertSerialize2Array($data);
                    $autoSize = $data['auto_size'];
                    $autoWidth = $data['auto_width'];
                    $hasBorder = $data['has_border'];
                    $borderStyle = $data['border_style'];
                    $borderWidth = $data['border_width'];
                    $inkpadColor = $data['inkpad_color'];
                    $zoomSize = $data['zoom_size'];
                    $width = $data['template_width'];
                    $height = $data['template_height'];
                    $lineHeight = $data['line_height'];
                    $templateType = $data['templateType'];
                    $shape = $data['shape'];
                    $elements = $data['elements'];
                }
            $infoTemplate['elements'] = $elements;
            $infoTemplate['imgWidth'] = $width;
            $infoTemplate['imgHeight'] = $height;
            $infoTemplate['templateType'] = $templateType;
            $zoomSize = (600 / 96 - 1)*100;

            $basePathIcon = Mage::getBaseDir('media') . DS . 'order' . DS . $storeId . DS; 
            $template = Mage::helper('text2image')->createTemplate($infoTemplate, $basePathIcon, $shape ,$autoWidth, $autoSize, $hasBorder, $borderWidth, $borderStyle, $inkpadColor, $lineHeight, $zoomSize, $storeId);

            $type     = 'pdf';
            $fileName = (isset($incrementId))? $incrementId.'.pdf' : 'design.pdf';
            $template->download($type, $fileName);
        }

		public function pdfimageAction()
		{
			$file = $this->getRequest()->getParam('file');
			$color = $this->getRequest()->getParam('color');
			$size = $this->getRequest()->getParam('size');
			if(!empty($file)){
				$filePath = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'product' . base64_decode($file);
				
				if(file_exists($filePath)){
					$pathParts = pathinfo($filePath);
					if(!file_exists($pathParts['dirname']. DS . $pathParts['filename'].'.tiff')){
						$tmp_image = new Imagick();
						$tmp_image->setResolution(600,600);
						$tmp_image->readimage($filePath);
						$tmp_image->setImageBackgroundColor('white');
						$tmp_image = $tmp_image->flattenImages();
						$tmp_image->borderImage(new ImagickPixel( 'white' ), 1, 1);
						$tmp_image->trimImage(0);
						
						if ($tmp_image->getImageColorspace() == Imagick::COLORSPACE_CMYK) {
							$tmp_image->quantizeImage(64, Imagick::COLORSPACE_RGB, 0, false, false );
							$tmp_image->setImageDepth(8);
						}
						//create pdf file
						$tmp_image->setImageFormat("tiff");
						$tmp_image->writeImage($pathParts['dirname']. DS . $pathParts['filename'].'.tiff');
					}
					
					$size = $size ? $size : 105;
					$color = $color ? $color : 'black';
					$src = $pathParts['dirname']. DS . $pathParts['filename'].'.tiff';					
					$iconImage = new Imagick();
					$iconImage->setResolution(600,600);
					$iconImage->readimage($src);
					$image_dimension = $iconImage->getImageGeometry();
					$tyle = $image_dimension['width'] / $size;
					
					$iconImage->paintTransparentImage($iconImage->getImageBackgroundColor(), 0, 10000);
					$iconImage->floodfillPaintImage($color, 1, 'white', 1, 1, true);
					$iconImage->scaleimage($image_dimension['width']/$tyle, $image_dimension['height']/$tyle);
					$iconImage->setImageUnits(Imagick::RESOLUTION_PIXELSPERINCH);
					$iconImage->resizeImage($image_dimension['width']/$tyle, $image_dimension['height']/$tyle);

					$iconImage->setimageformat("png");
					header("Content-type: image/png");
					echo $iconImage->getimageblob();
					exit;
				}
			}
			echo '0';exit;
		}
		
        function iconthumbAction(){
            $size = $this->getRequest()->getParam('size');
            $name = $this->getRequest()->getParam('name');
            $template_id = $this->getRequest()->getParam('template_id');

            $src = Mage::getBaseDir('base') . DS . 'media' .DS . 'template_icon_default' . DS . $name . '.png';
            if( !file_exists($src) ){
                if(!empty($template_id)){
                    $src = Mage::helper('text2image')->getFolderPathTemplate($template_id) . DS . $name .'.png';
                }else{
                	$src = Mage::getBaseDir('base') . DS . 'media' .DS . 'tmp' . DS . 'textplate' . DS . $name .'.tiff';
                    if( !file_exists($src) ){
                    	$src = Mage::getBaseDir('base') . DS . 'media' .DS . 'tmp' . DS . 'textplate' . DS . $name .'.png';
                    }
                }
            }
            //$src = WWW_ROOT.$src;
            $tmp_image = new Imagick();
            $tmp_image->readimage($src);
            $tmp_image->setImageResolution(96, 96);
            $image_dimension = $tmp_image->getImageGeometry();
            $tmp_width = $image_dimension["width"];
            $tmp_height = $image_dimension["height"];
            $tyle = $tmp_width / $tmp_height;
            $new_width = 0;
            $new_height = 0;
            if ($tyle > 1) {
                $new_width = $size;
                $new_height = (int) $size / $tyle;
            } else {
                $new_height = $size;
                $new_width = (int) $size * $tyle;
            }
         
            $tmp_image->scaleimage($new_width,$new_height);

            /* Create an empty canvas */
            $canvas = new Imagick();
            $width = 128;
            $height = 116;
            $canvas->newImage($width, $height, new ImagickPixel("transparent"));
            $canvas->setImageColorspace($tmp_image->getImageColorspace() );
            $canvas->compositeImage($tmp_image, imagick::COMPOSITE_DEFAULT, 0, 0);
            $canvas->setimageformat("png");
            header("Content-type: image/png");
            //imagepng($img);
            echo $canvas->getimageblob();
            exit;
        }
        function thumbnailAction() {
            $size = $this->getRequest()->getParam('size');
            $size = $size * Mage::helper('text2image')->getRateMM2Pixcel();
            $name = $this->getRequest()->getParam('name');
            $template_id = $this->getRequest()->getParam('template_id');

            $src = Mage::getBaseDir('base') . DS . 'media' .DS . 'template_icon_default' . DS . $name . '.png';
            if( !file_exists($src) ){
                if(!empty($template_id)){
                    $src = Mage::helper('text2image')->getFolderPathTemplate($template_id) . DS . $name .'.png';
                }else{
                	$src = Mage::getBaseDir('base') . DS . 'media' .DS . 'tmp' . DS . 'textplate' . DS . $name .'.tiff';
                    if( !file_exists($src) ){
                    	$src = Mage::getBaseDir('base') . DS . 'media' .DS . 'tmp' . DS . 'textplate' . DS . $name .'.png';
                    }
                }    
            }

            //$src = WWW_ROOT.$src;
            $tmp_image = new Imagick();
            $tmp_image->readimage($src);
            $tmp_image->setImageResolution(96, 96);
            $image_dimension = $tmp_image->getImageGeometry();
            $tmp_width = $image_dimension["width"];
            $tmp_height = $image_dimension["height"];
            $tyle = $tmp_width / $tmp_height;
            $new_width = 0;
            $new_height = 0;
            if ($tyle > 1) {
                $new_width = $size;
                $new_height = (int) $size / $tyle;
            } else {
                $new_height = $size;
                $new_width = (int) $size * $tyle;
            }
            $tmp_image->thumbnailImage($new_width,$new_height);
            $tmp_image->setimageformat("jpg");
            header("Content-type: image/jpg");
            //imagepng($img);
            echo $tmp_image;
            exit;
        }

        public function myLog($str){
            //return false;
            Mage::log($str,null,'text2image.log',true);
        }

        function myDebug($obj)
        {
            if( $_SERVER['REMOTE_ADDR'] == '1.54.83.7' ){
                echo '<pre>';
                print_r($obj);
                die();
            }
        }
}