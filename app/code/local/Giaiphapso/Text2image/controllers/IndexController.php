<?php
class Giaiphapso_Text2image_IndexController extends Mage_Core_Controller_Front_Action
{
	protected function _initProduct()
	{
	    $productId  = (int) $this->getRequest()->getParam('id');
	    $params = new Varien_Object();
	    return Mage::helper('catalog/product')->initProduct($productId, $this, $params);
	}
	protected function getProduct()
	{
	    if (!Mage::registry('product') && $this->getProductId()) {
	        $product = Mage::getModel('catalog/product')->load($this->getProductId());
	        Mage::register('product', $product);
	    }
	    return Mage::registry('product');
	}
	protected function _initTextplate()
	{
		$color = array(144 => 'red', 143=>'blue', 142 => 'black', 145 => 'red-blue');
		
	    if($this->getProduct()->getTypeId() == 'bundle'){
	        $_options = $this->getProduct()->getTypeInstance(true)
	        ->getSelectionsCollection($this->getProduct()->getTypeInstance(true)->getOptionsIds($this->getProduct()), $this->getProduct());
	         
	        $textplateId = $this->getRequest()->getParam('textplateId');
	         
	        foreach($_options as $option){
	            $attributeSetName = Mage::getModel("eav/entity_attribute_set")
	            ->load($option->getAttributeSetId())
	            ->getAttributeSetName();
	            
	            if(strtolower($attributeSetName) == 'textplate' || strcmp($attributeSetName,'Stockstamps Textplate') == 0){
	                if( $textplateId ){
	                    $productTextplate = Mage::getModel('catalog/product')->load($textplateId);
	                    $data['textplate_id'] = $textplateId;
	                }else{
	                    $data['textplate_id'] = $option->getId();
	                    $productTextplate = $option;
	                }
	            }elseif(strtolower($attributeSetName) == 'inkpad'){
	                $data['inkpad_id'][$color[$option->getData('color')]] = $option->getId();
	            }elseif(strtolower($attributeSetName) == 'machine'){
	                $data['machine_id'] = $option->getId();
	            }
	        }
	    }elseif($this->getProduct()->getTypeId() == 'simple' && in_array($this->getProduct()->getAttributeSetId(), unserialize(TEXTPLATE_ATTRIBUTE_SET_ID))){
	        $productTextplate = $this->getProduct();
	    }else{
			return null;
	    }
	    Mage::register('current_textplate', $productTextplate);
	    Mage::register('current_data', json_encode($data));
	    return $productTextplate;
	}
	
    public function indexAction()
    {
    	if (!$this->_initProduct() || !$this->_initTextplate())
    	{
    		$this->_redirect('/');
			return;
    	}
		
        $this->loadLayout();
        $this->renderLayout();
    }
    
    //download template textplate
    public function downloadAction(){
        $storeId = $this->getRequest()->getParam('sid');
        $code = $this->getRequest()->getParam('code');
        $code = base64_decode($code);
        $path = Mage::getBaseDir('base') . DS . "media" . DS . "order" . DS .$storeId;
        $file_name = $code.'.pdf';
        $file = $path. DS . $code .'.pdf';  
        if(is_file($file)){
            $mime = 'application/force-download';
            header('Pragma: public');   // required
            header('Expires: 0');    // no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($file)).' GMT');
            header('Cache-Control: private',false);
            header('Content-Type: '.$mime);
            header('Content-Disposition: attachment; filename="'.$file_name.'"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.filesize($file));  // provide file size
            header('Connection: close');
            readfile($file);    // push it out
            exit();
        }else{
            $data = $this->getRequest()->getParam('code');
            $storeId =  $this->getRequest()->getParam('sid');
            if(!empty($data)){
                $data = Mage::helper('text2image')->convertSerialize2Array($data);
                $autoSize = $data['auto_size'];
                $autoWidth = $data['auto_width'];
                $hasBorder = $data['has_border'];
                $borderStyle = $data['border_style'];
                $borderWidth = $data['border_width'];
                $inkpadColor = $data['inkpad_color'];
                $zoom_size = $data['zoom_size'];
                $width = $data['template_width'];
                $height = $data['template_height'];
                $line_height = $data['line_height'];
                $templateType = $data['templateType'];
                $shape = $data['shape'];
                $elements = $data['elements'];
            }
            $infoTemplate['elements'] = $elements;
            $infoTemplate['imgWidth'] = $width;
            $infoTemplate['imgHeight'] = $height;
            $infoTemplate['templateType'] = $templateType;
            Mage::helper('text2image')->getTemplatePdf($infoTemplate, $shape ,$autoWidth, $autoSize, $hasBorder, $borderWidth, $borderStyle, $inkpadColor, $line_height, $zoom_size, $storeId);
            $this->_redirect('404');
        }
    }
    
    public function confirmAction(){
        $product_id = $this->getRequest()->getParam('product_id');
        if($this->getRequest()->getParams() && !empty($product_id)){
            $this->loadLayout();
            $this->renderLayout();
        }else{
            $this->_redirect('/'); 
        }
        
    }
    
    protected function _preparePostData($data){
    
        $result = array();
        foreach($data as $key => $value){
            foreach($value as $item => $option){
                $result[$item][$key] = $option;
                if($key == 'text'){
                    $result[$item]['base64Text'] = base64_encode($option);
                }
            }
        }
        $data['elements'] = Mage::helper('core')->jsonEncode($result);
        return $data;
    }
    
    public function saveAction(){
        
        $template_file = $this->getRequest()->getParam('template_file');
        $maxline_textplate = $this->getRequest()->getParam('maxline_textplate');
        $template_width = $this->getRequest()->getParam('template_width');
        $template_height = $this->getRequest()->getParam('template_height');
        $elements = $this->getRequest()->getParam('data');
        $product_id = $this->getRequest()->getParam('product_id');
        $_product = Mage::getSingleton('catalog/product')->load($product_id);
        
        if(empty($template_file) && count($elements) > 0){
            try{
                $data = $this->_preparePostData($elements);
                $helper = Mage::helper('text2image');
                $new_name = $helper->microtime_float();
                $helper->generatePdf($data, array($helper->_convertToMilimet($template_width), $helper->_convertToMilimet($template_height)), null, $new_name );
            }
            catch(Exception $e)
            {
                Mage::throwException($e->getMessage());
            }
        }
        $this->_redirect($_product->getProductUrl());
    }
    
}