<?php
    class Giaiphapso_Textplate_Block_Textplate extends Mage_Core_Block_Template 
    {
        public function __construct()
        {
            $brand_name = $this->getRequest()->getParam('brand_name');

            if( !empty($brand_name) ){
                $brand_name = str_replace(array('.html', '-'), '', $brand_name);    
                $brand_id   = constant(strtoupper($brand_name)."_BRAND_ID");    
            }else{
                $brand_name = 'alpo';    
                $brand_id   = ALPO_BRAND_ID;    
            }

            $this->setData('brand_name', $brand_name);
            $this->setData('brand_id', $brand_id);
        }

        public function getProducts($category)
        {
            if(is_object($category)){
                $product = Mage::getModel('catalog/category')
                    ->load($category->getId())
                    ->getProductCollection()
                    ->addAttributeToSelect(array('name', 'price', 'id', 'url_path'))
                    ->addStoreFilter()
                    ->addWebsiteFilter()
                    ->addFieldToFilter("visibility",4)
                    ->addFieldToFilter("status", 1)
                    ->addAttributeToSort('position', 'asc');
                    
                return $product;
            }        
        }
}