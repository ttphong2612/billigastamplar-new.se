<?php
class  Idev_OneStepCheckout_Model_Tax_Calculation extends Mage_Tax_Model_Calculation
{

    /**
     * Collect totals patched for magento issue #26145
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getRate($request)
    {
        if (!$request->getCountryId() || !$request->getCustomerClassId() || !$request->getProductClassId()) {
            return 0;
        }
		
        $cacheKey = $this->_getRequestCacheKey($request);
        if (!isset($this->_rateCache[$cacheKey])) {
            $this->unsRateValue();
            $this->unsCalculationProcess();
            $this->unsEventModuleId();
            Mage::dispatchEvent('tax_rate_data_fetch', array('request'=>$request));
            if (!$this->hasRateValue()) {
                $rateInfo = $this->_getResource()->getRateInfo($request);
                $this->setCalculationProcess($rateInfo['process']);
                $this->setRateValue($rateInfo['value']);
				
				
                //baotn
				/*if($thisDiscountCode == "0000" && $thisProductClassCode == "5"){
					$this->setRateValue(0);
				}	
				else{    
					$this->setRateValue($rateInfo['value']);
				}  */ 
				//end
            } else {
                $this->setCalculationProcess($this->_formCalculationProcess());
            }
            $this->_rateCache[$cacheKey] = $this->getRateValue();
            $this->_rateCalculationProcess[$cacheKey] = $this->getCalculationProcess();
        }
        return $this->_rateCache[$cacheKey];
    }
	 /**
     * Get request object with information necessary for getting tax rate
     * Request object contain:
     *  country_id (->getCountryId())
     *  region_id (->getRegionId())
     *  postcode (->getPostcode())
     *  customer_class_id (->getCustomerClassId())
     *  store (->getStore())
     *
     * @param   null|false|Varien_Object $shippingAddress
     * @param   null|false|Varien_Object $billingAddress
     * @param   null|int $customerTaxClass
     * @param   null|int $store
     * @return  Varien_Object
     */
    public function getRateRequest(
        $shippingAddress = null,
        $billingAddress = null,
        $customerTaxClass = null,
        $store = null)
    {
        if ($shippingAddress === false && $billingAddress === false && $customerTaxClass === false) {
            return $this->getRateOriginRequest($store);
        }
        $address    = new Varien_Object();
        $customer   = $this->getCustomer();
        $basedOn    = Mage::getStoreConfig(Mage_Tax_Model_Config::CONFIG_XML_PATH_BASED_ON, $store);

        if (($shippingAddress === false && $basedOn == 'shipping')
            || ($billingAddress === false && $basedOn == 'billing')) {
            $basedOn = 'default';
        } else {
            if ((($billingAddress === false || is_null($billingAddress) || !$billingAddress->getCountryId())
                && $basedOn == 'billing')
                || (($shippingAddress === false || is_null($shippingAddress) || !$shippingAddress->getCountryId())
                && $basedOn == 'shipping')
            ){
                if ($customer) {
                    $defBilling = $customer->getDefaultBillingAddress();
                    $defShipping = $customer->getDefaultShippingAddress();

                    if ($basedOn == 'billing' && $defBilling && $defBilling->getCountryId()) {
                        $billingAddress = $defBilling;
                    } else if ($basedOn == 'shipping' && $defShipping && $defShipping->getCountryId()) {
                        $shippingAddress = $defShipping;
                    } else {
                        $basedOn = 'default';
                    }
                } else {
                    $basedOn = 'default';
                }
            }
        }

        switch ($basedOn) {
            case 'billing':
                $address = $billingAddress;
                break;
            case 'shipping':
                $address = $shippingAddress;
                break;
            case 'origin':
                $address = $this->getRateOriginRequest($store);
                break;
            case 'default':
                $address
                    ->setCountryId(Mage::getStoreConfig(
                        Mage_Tax_Model_Config::CONFIG_XML_PATH_DEFAULT_COUNTRY,
                        $store))
                    ->setRegionId(Mage::getStoreConfig(Mage_Tax_Model_Config::CONFIG_XML_PATH_DEFAULT_REGION, $store))
                    ->setPostcode(Mage::getStoreConfig(
                        Mage_Tax_Model_Config::CONFIG_XML_PATH_DEFAULT_POSTCODE,
                        $store));
                break;
        }

        if (is_null($customerTaxClass) && $customer) {
            $customerTaxClass = $customer->getTaxClassId();
        } elseif (($customerTaxClass === false) || !$customer) {
            $customerTaxClass = $this->getDefaultCustomerTaxClass($store);
        }
		
        $request = new Varien_Object();
		$request
            ->setCountryId($address->getCountryId())
            ->setRegionId($address->getRegionId())
            ->setPostcode($address->getPostcode())
            ->setStore($store);
		//baotn add
		//    ->setCustomerClassId($customerTaxClass);
		$eutax = $customer['taxvat'];
		if(Mage::getSingleton('core/session')->getZeroVat() === true){
			
		}else{
		
            if(empty($eutax)){
                $request ->setCustomerClassId($customerTaxClass);
            }else{
               // $requesterCountryCode = 'DK';
               //  $requesterVatNumber = '31011337';
			  
                $result = Mage::helper('customer')->checkVatNumber(
                        $billingAddress['country_id'],
                        $eutax
                );
                $isValid = $result->getIsValid(); 
				
                if($isValid === true){
					$zero_rate = false;
					$write = Mage::getSingleton('core/resource')->getConnection('core_write');
					$readresult = $write->query("SELECT `main_table`.*, `region_table`.`code` AS `region_name` 
												 FROM `tax_calculation_rate` AS `main_table`
												 LEFT JOIN `directory_country_region` AS `region_table` ON main_table.tax_region_id = region_table.region_id 
												 WHERE (main_table.tax_country_id = '{$billingAddress['country_id']}') 
												 ORDER BY region_table.code ASC LIMIT 20"); 
													   
					while ($row = $readresult->fetch(PDO::FETCH_ASSOC)){
						if($row['rate'] == 0){
							$zero_rate = true;
							$rero_rate_id = $row['tax_calculation_rate_id'];
							break;    
						}
					}
					if($zero_rate === false)
						$request ->setCustomerClassId($customerTaxClass);
				}else{
					$request ->setCustomerClassId($customerTaxClass);
				} 
           
			} 
		}
        //end
        return $request;
    }
}