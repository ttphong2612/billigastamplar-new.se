<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Block_Adminhtml_Economic extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_controller = 'adminhtml_economic';
		$this->_blockGroup = 'economic';
		$this->_headerText = Mage::helper('economic')->__('e-conomic integration');
		parent::__construct();
		$this->removeButton('add');
	}
}