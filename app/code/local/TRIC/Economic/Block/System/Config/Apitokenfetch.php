<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Block_System_Config_Apitokenfetch extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
    	$html = '';    	
    	$appId = urlencode('YH9ushxvxSgu5fmsKB6JihiBXCP3077meiJ5HzWyOXc1');
    	
    	$scope = '';
    	if($website = $this->getRequest()->getParam('website')){
	    	$scope .= 'website/'.$website.'/';
    	}
    	if($store = $this->getRequest()->getParam('store')){
	    	$scope .= 'store/'.$store.'/';
    	}
    	
    	$redirectUrl = Mage::helper("adminhtml")->getUrl('adminhtml/adminhtml_economic/token',array()).$scope.'magekey/'.$this->getRequest()->getParam('key');
    	
    	$tokenUrl = 'https://secure.e-conomic.com/secure/api1/requestaccess.aspx?role=superuser&appId='.$appId.'&redirectUrl='.$redirectUrl;
    	
    	$html .= $this->getLayout()->createBlock('adminhtml/widget_button')
    		->setLabel(Mage::helper('economic')->__('Get token'))
            ->setOnClick('setLocation(\''.$tokenUrl.'\')')
            ->setType('button')
            ->setClass('scalable')
            ->setId('economic_general_api_token_fetch')
            ->toHtml();

    	return $html;	
    }

}
