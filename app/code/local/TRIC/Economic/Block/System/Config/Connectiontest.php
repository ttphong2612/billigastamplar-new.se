<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Block_System_Config_Connectiontest extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
    	$html = '';
    	$testActionUrl = Mage::helper("adminhtml")->getUrl('adminhtml/adminhtml_economic/testconnection');
    	
    	$javascript = "
    		$(this).up('tr').down('td.label').setStyle({color:'#FF8D00',fontStyle:'italic',fontWeight:'bold'});
    		$(this).up('tr').down('td.label').update('".Mage::helper('economic')->__('Testing accessibility...')."');
    		var self = this;
    		new Ajax.Request('$testActionUrl', {
    			method: 'post',
    			parameters: {
    				type: $('economic_general_connection_type').value,
    				token: $('economic_general_api_token').value,
    				agreementnumber: $('economic_general_agreementnumber').value,
    				userid: $('economic_general_userid').value,
    				password: $('economic_general_password').value,
    				admin_agreementnumber: $('economic_general_admin_agreementnumber').value,
    				admin_userid: $('economic_general_admin_userid').value,
    				admin_password: $('economic_general_admin_password').value,
    				admin_client_agreementnumber: $('economic_general_admin_client_agreementnumber').value
    			},
    			onComplete: function(transport) {
	    			if(transport.responseText == 'true'){
	    				$(self).up('tr').down('td.label').setStyle({color:'#22C400',fontStyle:'italic',fontWeight:'bold'});
	    				$(self).up('tr').down('td.label').update('".Mage::helper('economic')->__('Connection established. Save settings before continuing!')."');
	    			}
	    			else{
	    				$(self).up('tr').down('td.label').setStyle({color:'#FF0000',fontStyle:'italic',fontWeight:'bold'});
	    				$(self).up('tr').down('td.label').update('".Mage::helper('economic')->__('Unable to connect!').'<br/>'.Mage::helper('economic')->__('Check your information and API access in e-conomic!')."');
	    			}
    			}
    		});
    	";
    	
    	$html .= $this->getLayout()->createBlock('adminhtml/widget_button')
    		->setLabel(Mage::helper('economic')->__('Test connection'))
            ->setOnClick('javascript: '.$javascript)
            ->setType('button')
            ->setClass('scalable')
            ->toHtml();

    	return $html;	
    }

}
