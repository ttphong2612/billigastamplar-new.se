<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Block_System_Config_MagentoPaymentTypes extends Mage_Core_Block_Html_Select
{
    private $_paymentTypes;

    private function _getPaymentTypes(){
    	
		$paymentMethods = Mage::getSingleton('payment/config')->getActiveMethods();
    	if(is_null($this->_paymentTypes)){
			foreach ($paymentMethods as $paymentCode => $paymentCode){
				$label = Mage::getStoreConfig('payment/'.$paymentCode.'/title');
				if($label){
					$this->_paymentTypes[$paymentCode] = $label;
					$this->_getPaymentCardTypes($paymentCode);
				}
			}
    	}
    	return $this->_paymentTypes;
    }
    
    private function _getPaymentCardTypes($paymentCode){

		if($paymentCode == 'epay_standard'){
			$epayCardTypes = array('Dankort, Visa-Dankort, eDankort','VISA, MasterCard, JCB, Maestro', 'American Express, Diners','PayPal', 'Klarna, ViaBill', 'Paii', 'MobilePay Online');
			$this->_paymentTypes[$paymentCode] = 'ePay';
			foreach($epayCardTypes as $card){
				$this->_paymentTypes[$paymentCode.'#'.$card] = " · ePay ($card)";
			}
		}
		elseif($paymentCode == 'quickpaypayment_payment'){
			$epayCardTypes = array('Dankort, Visa-Dankort, eDankort','VISA, MasterCard, JCB, Maestro', 'American Express, Diners','PayPal', 'ViaBill');
			$this->_paymentTypes[$paymentCode] = 'QuickPay';
			foreach($epayCardTypes as $card){
				$this->_paymentTypes[$paymentCode.'#'.$card] = " · QuickPay ($card)";
			}
		}
    }
    
    public function setInputName($value)
    {
        return $this->setName($value);
    }
	
    public function _toHtml()
    {
    	$this->_getPaymentTypes();
        
    	if (!$this->getOptions()) {
    		foreach ($this->_getPaymentTypes() as $key => $label) {
                $this->addOption($key, $label);
            }
        }
        
        return parent::_toHtml();
    }
}
