<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Block_System_Config_WebhookurlCustomer extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
    	$html = '';
    	
    	$agreementnumber = Mage::helper('economic')->getConfig('general/agreementnumber');
    	$userid = Mage::helper('economic')->getConfig('general/userid');
    	$api_token = Mage::helper('economic')->getConfig('general/api_token');
    	
    	if((!$agreementnumber || !$userid) && !$api_token){
	    	$webhookUrl = Mage::helper('economic')->__('Please connect to e-conomic first...');
    	}
    	else{
	    	$key = md5($agreementnumber.$userid.$api_token);
	    	$webhookUrl = $this->getBaseUrl()."economic/customer/update/key/$key/new/[NEWNUMBER]/old/[OLDNUMBER]/";
    	}

    	$html .= '<input id="economic_debtor_settings_economic_webhook_url_customer" value="'.$webhookUrl.'" class="input-text" readonly />';
    	return $html;
	}
}
