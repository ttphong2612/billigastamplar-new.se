<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_Customer extends TRIC_Economic_Model_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('economic/customer');
	}

	public function syncCustomerToEconomic($customer=false,$order=false)
	{
		$customerGroupId = false;
		
		if($customer){
			$customerAddress = Mage::getModel('customer/address')->load($customer->getDefaultBilling());
			$customerGroupId = $customer->getGroupId();
			/*if($invoiceEmail = $customerAddress->getData('invoice_email')){
				$customer->setData('email',$invoiceEmail);
			}*/
		}
		elseif($order && !$customer){
			$customerAddress = $order->getBillingAddress();

			$customer = Mage::getModel('customer/customer');
			$customer->setData('id',false);
			$customer->setData('firstname',$customerAddress->getData('firstname'));
			$customer->setData('lastname',$customerAddress->getData('lastname'));
			$customer->setData('email',$order->getCustomerEmail());
		}
		else{
			$this->log('customer debug - return false');
			return false;
		}
		
		if(!$customerAddress->getData('country_id') && $order){
			$customerAddress = $order->getBillingAddress();
		}
		
		$customerId = $customer->getId();
		$customerName = trim($customer->getData('firstname')).' '.trim($customer->getData('lastname'));
		if($order){
			$storeId = $order->getStoreId();
		}
		else{
			$storeId = $customer->getStoreId();
		}
		
		$telephone = intval(str_replace(' ','',trim($customerAddress->getData('telephone'))));
		
		$createNewDebtor = true;
		$debtorData = new StdClass();		
		
		if($order && $this->getConfig('debtor_settings/use_order_number_as_debtor_number')){
			$orderNumber = explode('-', $order->getIncrementId());
			$orderNumber = $orderNumber[0];
			$customerId = $orderNumber;
			$relation = Mage::getModel('economic/customer')->load($customerId, 'magento_customer_id');
			
			if($debtorData->Handle = $this->api->Debtor_FindByNumber($orderNumber)){
				$debtorData->Number = $orderNumber;
				$createNewDebtor = false;
			}
			else{
				$debtorData->Number = $orderNumber;
			}
		}
		elseif($telephone && $this->getConfig('debtor_settings/use_telephone_as_debtor_number')){
			$relation = Mage::getModel('economic/customer')->load($telephone, 'magento_customer_id');
			
			if($debtorData->Handle = $this->api->Debtor_FindByNumber($telephone)){
				$debtorData->Number = $telephone;
				$createNewDebtor = false;
			}
			else{
				$debtorData->Number = $telephone;
			}
		}
		else{
			$relation = Mage::getModel('economic/customer')->load($customerId, 'magento_customer_id');
			$this->log('$relation->economic_customer_id:'.$relation->economic_customer_id);
			
			if($relation->economic_customer_id){
				if($debtorData->Handle = $this->api->Debtor_FindByNumber($relation->economic_customer_id)){
					$debtorData->Number = $relation->economic_customer_id;
					$createNewDebtor = false;
				}
			}
		
			if($createNewDebtor){
				if($debtorData->Handle = $this->api->Debtor_FindByEmail($customer->getData('email'))){
					if($this->api->Debtor_GetIsAccessible($debtorData->Handle->Number)){
						$debtorData->Number = $debtorData->Handle->Number;
						$createNewDebtor = false;
					}
				}
			}
			
			if($createNewDebtor){
				$debtorData->Number = $this->api->Debtor_GetNextAvailableNumber();
			}
		}		
		
		$debtor = ($createNewDebtor) ? null : $debtorData;
		$paymentMethod = ($order) ? $order->getPayment()->getMethod() : null;
		$orderIncrementId = ($order) ? $order->getIncrementId() : null;
		
		$termOfPaymentHandle = new stdClass();
		if($termOfPaymentHandle->Id = $this->__getTermsOfPaymentId($paymentMethod,$orderIncrementId,$debtor)){
			$debtorData->TermOfPaymentHandle = $termOfPaymentHandle;
		}

		$layoutHandle = new stdClass();
		$debtorGroupHandle = new stdClass();

		if(!$vatNumber = trim($customer->getData('taxvat'))){
			if($order){
				if(!$vatNumber = $order->getData('customer_taxvat')){
					$vatNumber = $order->getBillingAddress()->getData('vat_id');
				}
			}
		}
		$euCountries = explode(',', $this->getConfig('general/eu_countries'));
		
		$customerAddress->setData('country_id',strtoupper($customerAddress->getData('country_id'))); //Fix to make sure it's always uppercase
		if ($customerAddress->getData('country_id') == $this->getConfig('general/country_default')) {
			$debtorData->VatZone = 'HomeCountry';
			$layoutHandle->Id = $this->getConfig('order_settings/layout_own_country',$storeId);
			$debtorGroupHandle->Number = $this->getConfig('debtor_settings/debtor_group_own_country',$storeId);
		}
		elseif ($vatNumber && in_array($customerAddress->getData('country_id'),$euCountries)) {
			$debtorData->VatZone = 'EU';
			$layoutHandle->Id = $this->getConfig('order_settings/layout_eu',$storeId);
			if ($this->getConfig('debtor_settings/debtor_group_eu_with_vatnumber',$storeId)) {
				$debtorGroupHandle->Number = $this->getConfig('debtor_settings/debtor_group_eu_with_vatnumber',$storeId);
			}
			else {
				$debtorGroupHandle->Number = $this->getConfig('debtor_settings/debtor_group_eu',$storeId);
			}
		}
		elseif (in_array($customerAddress->getData('country_id'),$euCountries)) {
			$debtorData->VatZone = 'HomeCountry';
			$layoutHandle->Id = $this->getConfig('order_settings/layout_eu',$storeId);
			if ($vatNumber && $this->getConfig('debtor_settings/debtor_group_eu_with_vatnumber',$storeId)) {
				$debtorGroupHandle->Number = $this->getConfig('debtor_settings/debtor_group_eu_with_vatnumber',$storeId);
			}
			else {
				$debtorGroupHandle->Number = $this->getConfig('debtor_settings/debtor_group_eu',$storeId);
			}
		}
		else {
			$debtorData->VatZone = 'Abroad';
			$layoutHandle->Id = $this->getConfig('order_settings/layout_other',$storeId);
			$debtorGroupHandle->Number = $this->getConfig('debtor_settings/debtor_group_other',$storeId);
		}

		if ($this->getConfig('debtor_settings/debtor_group_per_country',$storeId) != '') {
			$debtorGroupCountries = explode(',',$this->getConfig('debtor_settings/debtor_group_per_country',$storeId));

			$debtorGroupCountry = array();
			foreach ($debtorGroupCountries as $groupCountry) {
				$groupCountry = explode('=',$groupCountry);
				$debtorGroupCountry[trim($groupCountry[0])] = trim($groupCountry[1]);
			}
			if (isset($debtorGroupCountry[$customerAddress->getData('country_id')])) {
				$debtorGroupHandle->Number = $debtorGroupCountry[$customerAddress->getData('country_id')];
			}
		}
		
		/*if ($customerGroupId && $this->getConfig('debtor_settings/debtor_group_mapping_from_magento',$storeId) != '') {
			$debtorGroupMappings = explode(',',$this->getConfig('debtor_settings/debtor_group_mapping_from_magento',$storeId));

			$debtorGroupMapping = array();
			foreach ($debtorGroupMappings as $groupIds) {
				$groupIds = explode('=',$groupIds);
				$debtorGroupMapping[trim($groupIds[0])] = trim($groupIds[1]);
			}
			if (isset($debtorGroupMapping[$customerGroupId])) {
				$debtorGroupHandle->Number = $debtorGroupMapping[$customerGroupId];
			}
		}*/


		$debtorData->DebtorGroupHandle = $debtorGroupHandle;
		$debtorData->LayoutHandle = $layoutHandle;

		$currencyCode = Mage::getStoreConfig('currency/options/base',$storeId);
		$currencyHandle = new stdClass();
		$currencyHandle->Code = $currencyCode;
		$debtorData->CurrencyHandle = $currencyHandle;
		$debtorAtt = "";
		if ($customerAddress->getData('company') && trim($customerAddress->getData('company')) != '') {
			$debtorData->Name = trim($customerAddress->getData('company'));
			$debtorAtt = trim($customerAddress->getData('firstname'))." ".trim($customerAddress->getData('lastname'));
		}
		else {
			$debtorData->Name = $customerName;
		}

		$debtorData->Address = trim($customerAddress->getData('street'));
		$debtorData->PostalCode = trim($customerAddress->getData('postcode'));
		$debtorData->City = trim($customerAddress->getData('city'));
		$debtorData->Country = trim($customerAddress->getCountryModel()->getName());
		$debtorData->Email = trim($customer->getData('email'));
		$debtorData->TelephoneAndFaxNumber = trim($customerAddress->getData('telephone'));
		if($customerId){
			$debtorData->Website = 'x';
		}
		$debtorData->IsAccessible = 1;

		if ($vatNumber != NULL && $vatNumber != '') {
			$debtorData->CINumber = $vatNumber;
		}

		$debtor = false;
		if($createNewDebtor){
			$debtor = $this->api->Debtor_CreateFromData($debtorData);
		}
		elseif($this->getConfig('debtor_settings/debtor_sync_to_economic')){
			$debtor = $this->api->Debtor_UpdateFromData($debtorData);
		}
		else{
			$debtor = $debtorData;
		}

		if($debtor){
			if($economicCustomerId = $debtor->Number){
				$relation->setData('magento_customer_id',$customerId);
				$relation->setData('economic_customer_id',$economicCustomerId);
				$relation->setData('update_time',date('Y-m-d H:i:s'));
				if($createNewDebtor){
					$relation->setData('created_time',date('Y-m-d H:i:s'));
				}
				$relation->save();
			}
		}

		return $debtor;
	}

	public function syncCustomerFromEconomic($customerId)
	{
		if(!$this->getConfig('debtor_settings/debtor_sync_from_economic')){
			return false;
		}
		elseif($debtor = $this->api->Debtor_GetData($customerId)){
			
			$sync = false;
			
			/*
			if(isset($debtor->DebtorGroupHandle)){
				if($debtor->DebtorGroupHandle->Number == 1){
					$sync = true;
				}
			}
			*/

			/*
			if(isset($debtor->PriceGroupHandle)){
				if($debtor->PriceGroupHandle->Number == 1){
					$sync = true;
				}
			}
			*/
			
			$websiteCharacterToSync = $this->getConfig('debtor_settings/debtor_sync_from_economic_website_character');
			
			if(!$websiteCharacterToSync){
				$sync = true;
			}
			elseif(isset($debtor->Website) && strtolower($debtor->Website) == $websiteCharacterToSync){
				$sync = true;
			}

			$this->log('syncCustomerFromEconomic: '.serialize($sync));

			if(!$sync){
				return false;
			}

			$createNewCustomer = true;
			$customer = Mage::getModel('customer/customer');
			$customerAddress = Mage::getModel('customer/address');

			$relation = Mage::getModel('economic/customer')->load($customerId, 'economic_customer_id');
			if($relation->magento_customer_id){
				if($customer = $customer->load($relation->magento_customer_id)){
					if($customer->getId()){
						$customerAddress = Mage::getModel('customer/address')->load($customer->getDefaultBilling());
						$createNewCustomer = false;
					}
					else{
						$customer = Mage::getModel('customer/customer');
					}
				}
			}

			$customer->setEmail($debtor->Email);

			$name = explode(' ',$debtor->Name);
			if(sizeof($name) == 1){
				$firstname = $name[0];
				$lastname = '';
			}
			else{
				$lastname = array_pop($name);
				$firstname = implode(' ', $name);
			}

			$customer->setFirstname($firstname);
			$customer->setLastname($lastname);

			if($createNewCustomer){
				$customer->setWebsiteId($this->__getDefaultWebsiteId());
				$password = (isset($debtor->TelephoneAndFaxNumber)) ? $debtor->TelephoneAndFaxNumber : $customer->generatePassword();
				$customer->setPassword($password);
			}

			if(isset($debtor->CINumber) && $vatNumber = $debtor->CINumber){
				$customer->setTaxvat($vatNumber);
				$customerAddress->setData('vat_id',$vatNumber);
			}

			try {
				$customer->save();
				$customerAddress->setData('firstname',$firstname);
				$customerAddress->setData('lastname',$lastname);
				$customerAddress->setData('street',$debtor->Address);
				$customerAddress->setData('postcode',$debtor->PostalCode);
				$customerAddress->setData('city',$debtor->City);
				$customerAddress->setData('country_id',$this->__getCountryCodeByName($debtor->Country));
				$customerAddress->setData('telephone',(isset($debtor->TelephoneAndFaxNumber)) ? $debtor->TelephoneAndFaxNumber : '');
				if($createNewCustomer){
					$customerAddress->setCustomerId($customer->getId());
					$customerAddress->setIsDefaultBilling(1);
					$customerAddress->setIsDefaultShipping(1);
					$customerAddress->setSaveInAddressBook(1);
					if($this->getConfig('debtor_settings/new_customer_welcome_email')){
						$customer->sendNewAccountEmail('registered');
					}
				}
				$customerAddress->save();

				$relation->setData('magento_customer_id',$customer->getId());
				$relation->setData('economic_customer_id',$customerId);
				$relation->setData('update_time',date('Y-m-d H:i:s'));
				if($createNewCustomer){
					$relation->setData('created_time',date('Y-m-d H:i:s'));
				}
				$relation->save();

			}
			catch (Exception $e) {
				$this->log($e->getMessage());
			}


			if ($customer->getId()){
				$subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($customer->getEmail());

				if (!$subscriber->getId()
					|| $subscriber->getStatus() == Mage_Newsletter_Model_Subscriber::STATUS_UNSUBSCRIBED
					|| $subscriber->getStatus() == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {

					$subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED);
					$subscriber->setSubscriberEmail($customer->getEmail());
					$subscriber->setSubscriberConfirmCode($subscriber->RandomSequence());
				}

				$subscriber->setStoreId($customer->getStoreId());
				$subscriber->setCustomerId($customer->getId());

				try {
					$subscriber->save();
				}
				catch (Exception $e) {
					$this->log($e->getMessage());
				}
			}
		}
	}

	private function __getCountryCodeByName($countryName)
	{
		$countryList = Mage::getResourceModel('directory/country_collection')
		->loadData()
		->toOptionArray(false);

		foreach ($countryList as $key => $val)
		{
			if (strtolower($val['label']) === strtolower($countryName)) {
				return $val['value'];
			}
		}

		return '';
	}

	private function __getDefaultWebsiteId()
	{
		$websites = Mage::app()->getWebsites();
		foreach($websites as $website){
			return $website->getId();
		}
	}
}
