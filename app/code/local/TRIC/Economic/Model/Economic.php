<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_Economic extends TRIC_Economic_Model_Abstract
{
	protected $api;
	private $connected;
	
	
	private $_client = false;
	private $_config = false;
	private $_debtor = false;
	public $salesLines = array();
	public $invoiceId = false;
	public $ean = false;
	public $type;
	public $eu;

	public function _construct() {
		$this->_init('economic/economic');
	}
	
	

}
