<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_Observer
{
	protected function getConfig($path,$store=0)
	{
		return Mage::helper('economic')->getConfig($path,$store);
	}
	
	public function salesOrderAfterSave($observer)
	{
		$order = $observer->getEvent()->getOrder();
		$storeId = $order->getStoreId();
		
		if(!$this->getConfig('general/active',$storeId)){
			return false;
		}
		
		if($this->getConfig('order_settings/transfer_type',$storeId) != 'order'){
			return false;
		}
		
		if(!$order->getStatus()){
			return false;
		}
		
		// Start store emulation process
		$appEmulation = Mage::getSingleton('core/app_emulation');
		$initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
		
		try{
			Mage::getSingleton('economic/order')->processOrderToEconomic($order);
		}
	    catch(Exception $e){
	    	Mage::log($e->getMessage(),null,'economic.log',true);
	    }
		
		// Stop store emulation process
		$appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
	}
	
	public function salesOrderInvoiceAfterSave($observer)
	{
		$invoice = $observer->getEvent()->getInvoice();
		$storeId = $invoice->getStoreId();
		
		if(!$this->getConfig('general/active',$storeId)){
			return false;
		}
		
		if($this->getConfig('order_settings/transfer_type',$storeId) != 'invoice'){
			return false;
		}
		
		// Start store emulation process
		$appEmulation = Mage::getSingleton('core/app_emulation');
		$initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
		
		try{
			Mage::getSingleton('economic/invoice')->processInvoiceToEconomic($invoice,0);
		}
	    catch(Exception $e){
		    Mage::log($e->getMessage(),null,'economic.log',true);
	    }
		
		// Stop store emulation process
		$appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
	}

	public function salesOrderCreditmemoAfterSave($observer)
	{
		$creditmemo = $observer->getEvent()->getCreditmemo();
		$storeId = $creditmemo->getStoreId();
		
		if(!$this->getConfig('general/active',$storeId)){
			return false;
		}
		
		// Start store emulation process
		
		$appEmulation = Mage::getSingleton('core/app_emulation');
		$initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
		
		try{
			Mage::getSingleton('economic/invoice')->processInvoiceToEconomic($creditmemo,1);
		}
	    catch(Exception $e){
		    Mage::log($e->getMessage(),null,'economic.log',true);
	    }
		
		// Stop store emulation process
		$appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
	}
	
	public function customerAfterSave($observer)
    {
    	$customer = $observer->getEvent()->getCustomer();
    	$storeId = $customer->getStoreId();
    	
    	if(!$this->getConfig('general/active',$storeId)){
			return false;
		}
    	
    	if(!$this->getConfig('debtor_settings/debtor_sync_to_economic',$storeId)){
			return false;
		}
    	
    	$moduleName = Mage::app()->getRequest()->getModuleName();
    	$controllerName = Mage::app()->getRequest()->getControllerName();
		$actionName = Mage::app()->getRequest()->getActionName();
		
		if(!$controllerName && !$actionName){
			return false;
		}
		elseif($moduleName == 'economic' && $actionName == 'update'){
			return false;
		}
	    	    
	    try{
		    Mage::getSingleton('economic/customer')->syncCustomerToEconomic($customer);
	    }
	    catch(Exception $e){
		    Mage::log($e->getMessage(),null,'economic.log',true);
	    }
    }
    
    public function productAfterSave($observer)
	{
		if(!$this->getConfig('product_settings/product_sync_to_economic')){
			return false;
		}
		
		if(!$this->getConfig('general/active')){
			return false;
		}
			
		$controllerName = Mage::app()->getRequest()->getControllerName();
		$actionName = Mage::app()->getRequest()->getActionName();
		
		$preventProductActions = array('duplicate');
		
		if($controllerName == 'catalog_product' && in_array($actionName,$preventProductActions)){
			return false;
		}
		
		try{
			if($_product = $observer->getEvent()->getProduct()){
				if($this->getConfig('product_settings/product_sync_to_economic_only_new')){
					//Only sync new products
					$createdAt = strtotime($_product->getData('created_at'));
					$now = time();
					$diffInSeconds = $now-$createdAt;
					if($diffInSeconds < 60){
						Mage::getModel('economic/product')->syncProductToEconomic($_product);
					}
				}
				else{
					Mage::getModel('economic/product')->syncProductToEconomic($_product);
				}
				
			}
		}
		catch(Exception $e){
		    Mage::log($e->getMessage(),null,'economic.log',true);
	    } 
	}
    
    
    /* MASS ACTIONS */
	
	public function addMassAction($observer)
	{
		if(!$this->getConfig('general/active')){
			return false;
		}
		
		$block = $observer->getEvent()->getBlock();
		if(preg_match('/_Widget_Grid_Massaction$/',get_class($block))){
			// Product grid
			if(strpos($block->getRequest()->getControllerName(),'catalog_product') !== false){
				$block->addItem('economic_add_product_to_economic', array(
						'label' => Mage::helper('economic')->__('Send products to e-conomic'),
						'url'   => Mage::helper('adminhtml')->getUrl('adminhtml/adminhtml_economic/addProductToEconomic', array('_current'=>true)),
					)
				);
				$block->addItem('economic_update_product_from_economic', array(
						'label' => Mage::helper('economic')->__('Update products from e-conomic'),
						'url'   => Mage::helper('adminhtml')->getUrl('adminhtml/adminhtml_economic/updateProductFromEconomic', array('_current'=>true)),
					)
				);
			}
			// Order grid
			elseif($this->getConfig('order_settings/transfer_type') == 'order' && strpos($block->getRequest()->getControllerName(),'sales_order') !== false){
				$block->addItem('economic_orders', array(
						'label' => Mage::helper('economic')->__('e-conomic: Create orders in e-conomic (regardless of status)'),
						'url'   => Mage::helper('adminhtml')->getUrl('adminhtml/adminhtml_economic/addOrderToEconomic', array('_current'=>true)),
					)
				);
			}
			// Invoice grid
			elseif($this->getConfig('order_settings/transfer_type') == 'invoice' && strpos($block->getRequest()->getControllerName(),'sales_invoice') !== false){
				$block->addItem('economic_invoices', array(
						'label' => Mage::helper('economic')->__('Send invoices to e-conomic'),
						'url'   => Mage::helper('adminhtml')->getUrl('adminhtml/adminhtml_economic/addInvoiceToEconomic', array('_current'=>true)),
					)
				);
			}
			// Creditmemo grid
			elseif($this->getConfig('order_settings/transfer_type') == 'invoice' && strpos($block->getRequest()->getControllerName(),'sales_creditmemo') !== false){
				$block->addItem('economic_creditmemos', array(
						'label' => Mage::helper('economic')->__('Send creditmemos to e-conomic'),
						'url'   => Mage::helper('adminhtml')->getUrl('adminhtml/adminhtml_economic/addCreditmemoToEconomic', array('_current'=>true)),
					)
				);
			}
			
		}
	}

}