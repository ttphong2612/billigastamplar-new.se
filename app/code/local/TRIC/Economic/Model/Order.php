<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_Order extends TRIC_Economic_Model_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('economic/economic');
	}

	public function processOrderToEconomic($order,$forceNewOrder=false)
	{
		$this->log($order->getIncrementId().' - processOrderToEconomic - order status: '.$order->getStatus().' - force: '.serialize($forceNewOrder));
		$orderStatus = $order->getStatus();

		$this->log('try relation: '.$order->getIncrementId());
		try{
			$relation = Mage::getModel('economic/order')->load($order->getIncrementId(), 'ord_num');
		}
		catch(Exception $e){
			$this->log('e: '.$e->getMessage());
		}
		
		if(!$relation->ord_num){
			try{
				$relation = Mage::getModel('economic/order')
					->setEntityId($order->getId())
					->setOrdNum($order->getIncrementId())
					->setBillTo($order->getCustomerName())
					->setCreatedTime($order->getCreatedAt())
					->setType(-1)
					->setStatus(1)
					->save();
			}
			catch(Exception $e){
				$this->log($e->getMessage());
			}
		}
		
		$this->log('relation: '.serialize($relation->getData()));
		
		if($forceNewOrder){
			if($economicOrderNumber = $this->createOrderInEconomic($order)){
				$relation->setData('economic_order_num',$economicOrderNumber);
				$relation->setData('update_time',date('Y-m-d H:i:s'));
				$relation->save();
				$this->log('Order created in e-conomic with number: '.$economicOrderNumber);
			}
		}
		else{
			if($orderStatus == $this->getConfig('order_settings/order_create_status')){
				if(!$relation->economic_order_num){
					if($economicOrderNumber = $this->createOrderInEconomic($order)){
						$relation->setData('economic_order_num',$economicOrderNumber);
						$relation->setData('update_time',date('Y-m-d H:i:s'));
						$relation->save();
						$this->log('Order created in e-conomic with number: '.$economicOrderNumber);
					}
				}
			}
			elseif($orderStatus == $this->getConfig('order_settings/invoice_create_status')){
				if($relation->economic_order_num && !$relation->economic_invoice_num){
					if($economicInvoiceNumber = $this->upgradeOrderToInvoiceInEconomic($order,$relation->economic_order_num)){
						$invoice = $order;
						if ($order->hasInvoices()) {
						    foreach ($order->getInvoiceCollection() as $invoice) {
						    	$relation->setData('inv_num',$invoice->getIncrementId());
						    }
						}
						$relation->setData('economic_invoice_num',$economicInvoiceNumber);
						$relation->setData('update_time',date('Y-m-d H:i:s'));
						$relation->save();
						$this->log('Order upgraded to invoice with number: '.$economicInvoiceNumber);
						
						if($this->getConfig('order_settings/auto_book')){
							$this->__bookInvoiceAutomatic($invoice,$economicInvoiceNumber);
							$this->log('Invoiced booked automatically');
						}
					}
				}
			}
			elseif($orderStatus == $this->getConfig('order_settings/order_delete_status')){
				if($relation->economic_order_num){
					$this->cancelOrderInEconomic($relation->economic_order_num);
					$this->log('Order canceled!');
				}
			}
		}
		
		return true;
	}

	public function upgradeOrderToInvoiceInEconomic($order,$economicOrderNumber)
	{
		$economicInvoiceId = false;
		if($result = $this->api->Order_UpgradeToInvoice($economicOrderNumber)){
			$economicInvoiceId = $result->Id;
		}
		
		return $economicInvoiceId;
	}

	public function cancelOrderInEconomic($economicOrderNumber)
	{
		return $this->api->Order_Delete($economicOrderNumber);
	}

}
