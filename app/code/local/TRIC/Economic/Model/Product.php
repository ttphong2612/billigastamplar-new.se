<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_Product extends TRIC_Economic_Model_Abstract
{
	public function _construct()
	{
		parent::_construct();
		$this->_init('economic/product');
	}

	public function syncProductToEconomic($product,$type=false)
	{
		$this->log('syncProductToEconomic');
		if(is_array($product)){
			$sku = (isset($product['sku'])) ? $product['sku'] : false;
			$price = (isset($product['price'])) ? $product['price'] : 0;
			$cost = (isset($product['cost'])) ? $product['cost'] : 0;
			$name = (isset($product['name'])) ? $product['name'] : false;
			
			if(!$sku || !$name){
				return false;
			}
			
			$sku = trim(Mage::helper('core/string')->truncate($sku, 25));
			$name = trim(Mage::helper('core/string')->truncate($name, 300));
			$barcode = false;
		}
		else{
			
			$productId = $product->getId();
			$product = Mage::getModel('catalog/product')->load($productId);
			$sku = trim(Mage::helper('core/string')->truncate($product->getSku(), 25));
			
			$price = $product->getPrice();
			if(Mage::helper('tax')->priceIncludesTax()){
				$price = Mage::helper('tax')->getPrice($product, $price, false, null, null, null, null, true);
			}
			
			if ($product->getCost()) {
				$cost = $product->getCost();
			}
			else {
				$cost = 0;
			}
			
			$name = trim(Mage::helper('core/string')->truncate($product->getName(), 300));
			$barcode = false;
			if($barcodeAttributeCode = trim($this->getConfig('product_settings/product_barcode_attribute'))){
				$barcode = trim($product->getData($barcodeAttributeCode));
			}
		}
		
		$createNewProduct = true;

		if($existingEconomicProduct = $this->api->Product_FindByNumber($sku)){
			$createNewProduct = false;
		}

		$productData = new stdClass();

		$handle = new stdClass();
		$handle->Number = $sku;
		$productData->Handle = $handle;

		$productData->Number = $handle->Number;

		if($barcode){
			$productData->BarCode = $barcode;
		}

		if($this->getConfig('product_settings/unit_type')){
			$unitHandle = new stdClass();
			$unitHandle->Number = $this->getConfig('product_settings/unit_type');
			$productData->UnitHandle = $unitHandle;
		}

		$productGroupHandle = new stdClass();
		
		
		if($type == 'shipping'){
			$productGroupHandle->Number = $this->getConfig('shipping_settings/shipping_product_group');
		}
		elseif($type == 'giftvoucher'){
			$productGroupHandle->Number = $this->getConfig('giftvoucher_settings/giftvoucher_product_group');
		}
		elseif($type == 'rewardpoint'){
			$productGroupHandle->Number = $this->getConfig('rewardpoint_settings/rewardpoint_product_group');
		}
		elseif($type == 'surcharge'){
			$productGroupHandle->Number = $this->getConfig('surcharge_settings/surcharge_product_group');
		}
		elseif($type == 'paymentfee'){
			$productGroupHandle->Number = $this->getConfig('payment_fee_settings/payment_fee_product_group');
		}
		else{
			$productGroupHandle->Number = $this->getConfig('product_settings/product_group');
		}

		$productData->ProductGroupHandle = $productGroupHandle;
		
		$productData->SalesPrice = $this->convertPrice($price);
		$productData->RecommendedPrice = 0;

		$productData->CostPrice = $this->convertPrice($cost);
		
		$productData->Name = $name;

		$productData->IsAccessible = true;
		$productData->Volume = 0;
				
		if($createNewProduct){
			$economicProduct = $this->api->Product_CreateFromData($productData);
		}
		elseif($this->getConfig('product_settings/product_sync_to_economic' && !$type)){
			if(!$economicProduct = $this->api->Product_UpdateFromData($productData)){
				// could not update - then just return the existing (not updated) product
				return $existingEconomicProduct;
			}
		}
		else{
			$economicProduct = $existingEconomicProduct;
		}		
		
		return $economicProduct;
	}

	public function syncProductFromEconomic($sku,$type='sku')
	{
		$this->log('syncProductFromEconomic: '.$sku);
		
		if($type == 'id'){
			$productId = $sku;
		}
		else{
			$productId = Mage::getModel('catalog/product')->getIdBySku($sku);
		}
		
		if(!$productId){
			return false;
		}
		$product = Mage::getModel('catalog/product')->load($productId);
		
		if($product && $product->getId()){
			$sku = $product->getSku();
			$productData = array();
			if($economicProduct = $this->api->Product_GetData($sku)){
				
				if($this->getConfig('product_settings/product_sync_from_economic_price')){
					$productData['price'] = $economicProduct->SalesPrice;
					if(Mage::helper('tax')->priceIncludesTax()){
						$productData['price'] = Mage::helper('tax')->getPrice($product, $economicProduct->SalesPrice, true, null, null, true, null, false);
					}
				}
				
				if($this->getConfig('product_settings/product_sync_from_economic_cost')){
					$productData['cost'] = $economicProduct->CostPrice;
				}
				
				if($this->getConfig('product_settings/product_sync_from_economic_stock')){
					$typeId = $product->getTypeId();
					if($typeId != 'configurable' && $typeId != 'bundle' && $typeId != 'grouped' && $typeId != 'virtual' && $typeId != 'downloadable'){
						
						$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
						$oldQty = $stockItem->getQty();
						
						$availableQty = $economicProduct->Available;
						
						$save = false;
						
						if($oldQty != $availableQty){
							$stockItem->setQty($availableQty);
							if($availableQty > $stockItem->getMinQty()){
					            $stockItem->setIsInStock(1);
				            }
				            $save = true;
						}
						elseif($availableQty > $stockItem->getMinQty() && !$stockItem->getIsInStock()){
				            $stockItem->setIsInStock(1);
				            $save = true;
			            }
			            
			            if($save){
				            try{
				            	$stockItem->save();
				            }
				            catch (Exception $e) {
								$this->log($e->getMessage());
							}
			            }						
					}
				}
				
				if(!empty($productData)){
					try{
						Mage::getSingleton('catalog/product_action')->updateAttributes(array($product->getId()), $productData, Mage_Core_Model_App::ADMIN_STORE_ID);
						$product->clearInstance();
						return true;
					}
					catch (Exception $e) {
						$this->log($e->getMessage());
					}
				}
			}
			
			$product->clearInstance();
		}
		return false;
	}
	
	public function syncCompleteProductStockFromEconomic()
	{
		if(!$this->getConfig('product_settings/product_sync_from_economic_stock')){
			return false;
		}
		
		$products = Mage::getModel('catalog/product')->getCollection()
						->addAttributeToFilter('type_id','simple');
						
		foreach($products as $product){
			if($economicProduct = $this->api->Product_GetData($product->getSku())){
				
				$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
				$oldQty = $stockItem->getQty();
				
				$availableQty = $economicProduct->Available;
				
				$save = false;
				
				if($oldQty != $availableQty){
					$stockItem->setQty($availableQty);
					if($availableQty > $stockItem->getMinQty()){
			            $stockItem->setIsInStock(1);
		            }
		            $save = true;
				}
				elseif($availableQty > $stockItem->getMinQty() && !$stockItem->getIsInStock()){
		            $stockItem->setIsInStock(1);
		            $save = true;
	            }
	            
	            if($save){
		            try{
		            	$stockItem->save();
		            }
		            catch (Exception $e) {
						$this->log($e->getMessage());
					}
	            }
			}
		}
		
		return false;
	}

	private function __getDefaultWebsiteId()
	{
		$websites = Mage::app()->getWebsites();
		foreach($websites as $website){
			return $website->getId();
		}
	}
}
