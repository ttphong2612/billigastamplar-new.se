<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_System_Config_Source_Bookwithnumber
{

	public function toOptionArray()
	{
		return array(
			array('value' => 'economic', 'label'=>Mage::helper('economic')->__('E-conomic controls the invoice / credit number')),
			array('value' => 'magento', 'label'=>Mage::helper('economic')->__('Magento invoice / credit numbers are used in e-conomic')),
		);
	}

}
