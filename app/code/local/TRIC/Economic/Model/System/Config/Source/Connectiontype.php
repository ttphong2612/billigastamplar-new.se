<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_System_Config_Source_Connectiontype
{

	public function toOptionArray()
	{
		return array(
			array('value' => 'token', 'label'=>Mage::helper('economic')->__('Token-based login')),
			array('value' => 'usercredentials', 'label'=>Mage::helper('economic')->__('Standard login with user/pass')),
			array('value' => 'administrator', 'label'=>Mage::helper('economic')->__('Login as administrator with client agreement no.')),
		);
	}

}
