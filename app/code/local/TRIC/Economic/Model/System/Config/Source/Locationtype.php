<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_System_Config_Source_Locationtype
{

	public function toOptionArray()
	{
		return array(
			array('value' => '', 'label'=>Mage::helper('economic')->__('Do not use locations')),
			array('value' => 'store', 'label'=>Mage::helper('economic')->__('Global / Per store')),
			array('value' => 'product_attribute', 'label'=>Mage::helper('economic')->__('Product attribute')),
		);
	}

}
