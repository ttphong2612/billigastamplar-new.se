<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_System_Config_Source_Reftext
{
	public function toOptionArray()
	{
		return array(
			array('value' => '', 'label'=>Mage::helper('economic')->__('None')),
			array('value' => 'order_invoice', 'label'=>Mage::helper('economic')->__('Order no. / Invoice no.')),
			array('value' => 'invoice_order', 'label'=>Mage::helper('economic')->__('Invoice no. / Order no.')),
			array('value' => 'invoice', 'label'=>Mage::helper('economic')->__('Invoice no.')),
			array('value' => 'order', 'label'=>Mage::helper('economic')->__('Order no.')),
		);
	}

}
