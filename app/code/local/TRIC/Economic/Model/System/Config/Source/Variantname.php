<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_System_Config_Source_Variantname
{

	public function toOptionArray()
	{
		return array(
			array('value' => 'config', 'label'=>Mage::helper('economic')->__('Name of the configurable product')),
			array('value' => 'simple', 'label'=>Mage::helper('economic')->__('Name from the simple product (product variant)')),
		);
	}

}
