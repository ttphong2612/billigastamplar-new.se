<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_CustomerController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$this->loadLayout();
		$this->renderLayout();
	}
	
	protected function getConfig($path,$store=0)
	{
		return Mage::helper('economic')->getConfig($path,$store);
	}
	
	public function updateAction()
	{
		$storeId = Mage::app()->getStore()->getId();
		if(!$this->getConfig('general/active',$storeId)){
			return false;
		}
		
		if(!$this->getConfig('debtor_settings/debtor_sync_from_economic',$storeId)){
			return false;
		}
		
		$params = $this->getRequest()->getParams();
		
		$agreementnumber = $this->getConfig('general/agreementnumber');
		$userid = $this->getConfig('general/userid');
		$token = $this->getConfig('general/api_token');
		
    	$key = md5($agreementnumber.$userid.$token);
		
		if(!$params['key'] || ($params['key'] != $key)){
			return false;
		}
				
		if(isset($params['new']) && $customerId = $params['new']){
			Mage::getModel('economic/customer')->syncCustomerFromEconomic($customerId);
			return true;
		}
		
		return false;
	}
}