<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_ProductController extends Mage_Core_Controller_Front_Action
{
	public function indexAction()
	{
		$this->loadLayout();
		$this->renderLayout();
	}
	
	protected function getConfig($path,$store=0)
	{
		return Mage::helper('economic')->getConfig($path,$store);
	}
	
	public function updateAction()
	{
		$storeId = Mage::app()->getStore()->getId();
		if(!$this->getConfig('general/active',$storeId)){
			return false;
		}
		
		if(!$this->getConfig('product_settings/product_sync_from_economic')){
			return false;
		}
		
		$params = $this->getRequest()->getParams();
		
		$agreementnumber = $this->getConfig('general/agreementnumber');
		$userid = $this->getConfig('general/userid');
		$token = $this->getConfig('general/api_token');
		
    	$key = md5($agreementnumber.$userid.$token);
		
		if(!$params['key'] || ($params['key'] != $key)){
			return false;
		}
				
		$sku = $params['new'];
		if(isset($params['number']) && $params['number'] != '[NUMBER]'){
			$sku = $params['number'];
		}
		
		if($sku && $sku != '[NEWNUMBER]' && $sku != '[OLDNUMBER]' && $sku != '[NUMBER]'){
			Mage::getModel('economic/product')->syncProductFromEconomic($sku);
			return true;
		}
		
		return false;
	}
}