<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

$installer = $this;
$installer->startSetup();

$installer->run("ALTER TABLE {$this->getTable('economic')} DROP INDEX `UNIQUE`");

Mage::getConfig()->cleanCache();

$installer->endSetup();
