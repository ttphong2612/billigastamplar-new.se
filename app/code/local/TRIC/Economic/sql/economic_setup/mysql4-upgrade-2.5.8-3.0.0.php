<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

$installer = $this;
$installer->startSetup();

$installer->run("ALTER TABLE  {$this->getTable('economic')} ADD  `economic_invoice_num` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
ADD  `economic_order_num` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL");

$installer->run("
  -- DROP TABLE IF EXISTS {$this->getTable('economic_customer')};
  CREATE TABLE IF NOT EXISTS {$this->getTable('economic_customer')} (
    `id` int(11) unsigned NOT NULL auto_increment,
    `magento_customer_id` varchar(255) NOT NULL default '',
    `economic_customer_id` varchar(255) NOT NULL default '',
    `created_time` datetime NULL,
    `update_time` datetime NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
      ");

Mage::getConfig()->cleanCache();

$installer->endSetup();
