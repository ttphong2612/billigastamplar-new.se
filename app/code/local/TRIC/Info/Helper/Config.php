<?php
/**
* Magento Extension by TRIC Solutions
*
* @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
* @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
* @store       http://store.tric.dk
*/

class TRIC_Info_Helper_Config extends Mage_Core_Helper_Abstract
{
    const EXTENSIONS_FEED_URL = 'https://services.tric.dk/info/extensions.xml';
    const UPDATES_FEED_URL = 'https://services.tric.dk/info/updates.xml';
}
