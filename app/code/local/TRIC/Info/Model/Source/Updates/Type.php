<?php
/**
* Magento Extension by TRIC Solutions
*
* @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
* @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
* @store       http://store.tric.dk
*/

class TRIC_Info_Model_Source_Updates_Type extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    const TYPE_NEW_RELEASE = 'NEW_RELEASE';
    const TYPE_UPDATE_RELEASE = 'UPDATE_RELEASE';
    const TYPE_INFO = 'INFO';
    
    public function toOptionArray()
    {
        return array(
            array('value' => self::TYPE_UPDATE_RELEASE, 'label' => Mage::helper('tric_info')->__('My extensions updates')),
            array('value' => self::TYPE_NEW_RELEASE, 'label' => Mage::helper('tric_info')->__('New releases')),
            array('value' => self::TYPE_INFO, 'label' => Mage::helper('tric_info')->__('Other information'))
        );
    }

    public function getAllOptions()
    {
        return $this->toOptionArray();
    }

    public function getLabel($value)
    {
        $options = $this->toOptionArray();
        foreach ($options as $v) {
            if ($v['value'] == $value) {
                return $v['label'];
            }
        }
        return '';
    }

    public function getGridOptions()
    {
        $items = $this->getAllOptions();
        $out = array();
        foreach ($items as $item) {
            $out[$item['value']] = $item['label'];
        }
        return $out;
    }
}
