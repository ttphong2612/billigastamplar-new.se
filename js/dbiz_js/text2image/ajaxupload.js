﻿function $m(theVar){
	return document.getElementById(theVar)
}
function remove(theVar){
	var theParent = theVar.parentNode;
	theParent.removeChild(theVar);
}
function addEvent(obj, evType, fn){
	if(obj.addEventListener)
	    obj.addEventListener(evType, fn, true)
	if(obj.attachEvent)
	    obj.attachEvent("on"+evType, fn)
}
function removeEvent(obj, type, fn){
	if(obj.detachEvent){
		obj.detachEvent('on'+type, fn);
	}else{
		obj.removeEventListener(type, fn, false);
	}
}
function isWebKit(){
	return RegExp(" AppleWebKit/").test(navigator.userAgent);
}
function ajaxUpload(form,url_action,id_element){
	var detectWebKit = isWebKit();
	form = typeof(form)=="string"?$m(form):form;
	var erro="";
	if(form==null || typeof(form)=="undefined"){
		erro += "The form of 1st parameter does not exists.\n";
	}else if(form.nodeName.toLowerCase()!="form"){
		erro += "The form of 1st parameter its not a form.\n";
	}
	if($m(id_element)==null){
		erro += "The element of 3rd parameter does not exists.\n";
	}
	if(erro.length>0){
		alert("Error in call ajaxUpload:\n" + erro);
		return;
	}
	var oldActionUrl = form.action;
	var iframe = document.createElement("iframe");
	iframe.setAttribute("id","ajax-temp");
	iframe.setAttribute("name","ajax-temp");
	iframe.setAttribute("width","0");
	iframe.setAttribute("height","0");
	iframe.setAttribute("border","0");
	iframe.setAttribute("style","width: 0; height: 0; border: none;");
	form.parentNode.appendChild(iframe);
	window.frames['ajax-temp'].name="ajax-temp";
	var doUpload = function(){
		removeEvent($m('ajax-temp'),"load", doUpload);
		var ret = frames['ajax-temp'].document.getElementsByTagName("body")[0].innerHTML;
		var data = eval("("+ret+")");
		if(detectWebKit){
        	remove($m('ajax-temp'));
        }else{
        	setTimeout(function(){ remove($m('ajax-temp'))}, 250);
        }
		toggleSelectsUnderBlock($('loading-mask'), true);
	    Element.hide('loading-mask');
	    setLoaderPosition();
	    if(data.status == 'success'){
	    	form.action = oldActionUrl;
	    	form.target = '';
	    	var numline = parseInt($('rowNum').value);
	    	$('rowNum').value = numline+1;
            var id = numline;
	    	var objImg = '<p><img id="temp_'+id+'" src="'+baseUrl+'text2image/image/thumbnail/'+width_template+'/'+data.file+'" style="border:0" /></p>';
	    	var dialogWindow = Dialog.confirm(objImg, {
	            closable:true,
	            resizable:true,
	            className:'magento',
	            width:400,
	            minHeight:200,
	            minWidth:300,
	            zIndex:1000,
	            recenterAuto:true,
	            draggable:true,
	            autoWidthHeight:true,
	            onOk:function(win){
	               var width = $('temp_'+id).getWidth();
	               var height = $('temp_'+id).getHeight();
	               var obj = {};
	               obj.text = data.file;
	               obj.width = width;
	               obj.height = height;
	               obj.top = 2;
	               obj.left = 2;
	               obj.type = 'image';
	               generateTextLine(id, obj);
	               return true;
                }
	        });
	    }
	    
    }
	addEvent($m('ajax-temp'),"load", doUpload);
	form.setAttribute("target","ajax-temp");
	form.setAttribute("action",url_action);
	form.setAttribute("method","post");
	form.setAttribute("enctype","multipart/form-data");
	form.setAttribute("encoding","multipart/form-data");
	toggleSelectsUnderBlock($('loading-mask'), false);
    Element.show('loading-mask');
    setLoaderPosition();
	form.submit();
}