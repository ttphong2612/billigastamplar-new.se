var TextLineRow = Class.create();
TextLineRow.prototype = {
	initialize: function(id, obj) 
	{
		if(typeof id == 'undefined'){
			id = rownum;
		}
		if(typeof obj != 'undefined'){
			this.params = obj;
		}else{
			this.params = {size: 12, font: 'arial', type: 'text', style: '', text:'Type your text', left: 0, top:0, color:'000000', alignment :'center'};
		}
		
		this.domElement = document.getElementById('main_editor');
		this.id = id;
		this._generateEditorLine();
		this._generateHiddenLine();
	},
	_getFontSelect: function(){
		this.fontSelect = new Element('select', {'name': 'font', 'id': this.id, 'class': 'font-text', 'title':'Font for line '+this.id});
		objFontSelect = this.fontSelect;
		params = this.params;
		$H(jsonFont).each(function(element){
			if(element.key == params.font)
				objFontSelect.insert(new Element('option', {value: element.key, 'selected':'selected'}).update(element.value));
			else
				objFontSelect.insert(new Element('option', {value: element.key}).update(element.value));
		});
		objFontSelect.observe('change', function(event) {
			var id = this.getAttribute('id');console.log(id);
			$('hidden_font'+id).value = this.value;
		});
		this.fontSelect = objFontSelect;
		this.elementRowDom.insert({
            bottom:  new Element('div',{'class':'selectfont'}).insert(this.fontSelect),
        });
	},
	_getSizeSelect: function(){
		this.sizeSelect = new Element('select', {'name': 'size', 'id':'size'+ this.id, 'class': 'size-text', 'title':'Size for line '+this.id});
		objSizeSelect = this.sizeSelect;
		params = this.params;
		$H(jsonSize).each(function(element){
		if(element.key == params.size)
			objSizeSelect.insert(new Element('option', {value: element.key, 'selected':'selected'}).update(element.value));
		else
			objSizeSelect.insert(new Element('option', {value: element.key}).update(element.value));
		});
		this.sizeSelect.observe('change', function(event) {
			var id = this.getAttribute('id');
			$('hidden_size'+id).value = this.value;
		});
		this.sizeSelect = objSizeSelect;
		this.elementRowDom.insert({
            bottom:  new Element('div',{'class':'selectsize'}).insert(this.sizeSelect),
        });
	},
	_getFontStyleCheckbox: function(){
		if(this.params.style.indexOf('b') > -1){
			styleBold = new Element('span', {'class':'bold active'});
		}else{
			styleBold = new Element('span', {'class':'bold'});
		}
		if(this.params.style.indexOf('i') > -1){
			styleItalic = new Element('span', {'class':'italic active'});
		}else{
			styleItalic = new Element('span', {'class':'italic'});
		}
		if(this.params.style.indexOf('u') > -1){
			styleUnderline = new Element('span', {'class':'underline active'});
		}else{
			styleUnderline = new Element('span', {'class':'underline'});
		}
		styleText = new Element('div',{'class':'styletext'});
		
		styleText.insert({
            bottom: styleBold,
        });
		styleText.insert({
            bottom:  styleItalic,
        });
		styleText.insert({
            bottom:  styleUnderline,
        });
		styleText.insert({
            bottom:  new Element('span', {'class':'clear-both'}),
        });
		this.elementRowDom.insert({
            bottom:  styleText,
        });
	},
	_getTextAlignButton: function(){
		if(this.params.alignment == 'left'){
			alignmentLeft = new Element('span', {'class':'left active'});
		}else{
			alignmentLeft = new Element('span', {'class':'left'});
		}
		if(this.params.alignment == 'center'){
			alignmentCenter = new Element('span', {'class':'center active'});
		}else{
			alignmentCenter = new Element('span', {'class':'center'});
		}
		if(this.params.alignment == 'right'){
			alignmentRight = new Element('span', {'class':'right active'});
		}else{
			alignmentRight = new Element('span', {'class':'right'});
		}
		
		alignmentText = new Element('div',{'class':'alignmentbutton'});
		
		alignmentText.insert({
            bottom:  alignmentLeft,
        });
		alignmentText.insert({
            bottom:  alignmentCenter,
        });
		alignmentText.insert({
            bottom:  alignmentRight,
        });
		this.elementRowDom.insert({
            bottom:  alignmentText,
        });
	},
	_generateEditorLine: function(){
		this.elementRowDom = new Element('div', {'class': 'row-editor','id':this.id});
		this._getRemoveButton();
        this._getTextInput();
        this._getFontSelect();
		this._getSizeSelect();
		this._getFontStyleCheckbox();
		this._getTextAlignButton();

		this.elementRowDom.insert({
            bottom:   new Element('div', {'class': 'clear-both'}),
        });
		this.domElement.insert({
            bottom:  this.elementRowDom,
        });
    },
	_getRemoveButton: function(){
        this.removeBt = new Element('a', {'id': 'remove'+this.params.id, 'href':'javascript:void(0)', 'title': 'Remove'});
        this.removeBt.observe('click', function(event) {
            //@TODO something
        });
        this.elementRowDom.insert({
            bottom:  new Element('div',{'class':'remove'}).insert(this.removeBt),
        });
    },
	_getTextInput: function(){
        this.textInput = new Element('input', {'type':'text','name': 'text', 'id':this.id, 'title':'Text line '+this.params.id, 'value': this.params.text});
        this.textInput.observe('change', function(event) {
			var id = this.getAttribute("id");
            $('hidden_text'+id).value = this.value;
        });
        this.elementRowDom.insert({
            bottom:  new Element('div',{'class':'inputtext'}).insert(this.textInput),
        });
    },
	_generateHiddenLine: function()
	{
		this.hiddenElement = document.getElementById('hidden_field');
		hiddenRow = new Element('div',{'id':'hidden'+this.id});
		//insert text
		hiddenRow.insert({
            bottom:  new Element('input',{'type':'hidden','name':'data[text][]', 'id':'hidden_text'+this.id, 'value':this.params.text}),
        });
		//insert font
		hiddenRow.insert({
            bottom:  new Element('input',{'type':'hidden','name':'data[font][]', 'id':'hidden_font'+this.id, 'value':this.params.font}),
        });
		//insert size
		hiddenRow.insert({
            bottom:  new Element('input',{'type':'hidden','name':'data[size][]', 'id':'hidden_size'+this.id, 'value':this.params.size}),
        });
		
		//insert type
		hiddenRow.insert({
            bottom:  new Element('input',{'type':'hidden','name':'data[type][]', 'id':'hidden_type'+this.id, 'value':this.params.type}),
        });
		//insert left
		hiddenRow.insert({
            bottom:  new Element('input',{'type':'hidden','name':'data[left][]', 'id':'hidden_left'+this.id, 'value':0}),
        });
		//insert top
		hiddenRow.insert({
            bottom:  new Element('input',{'type':'hidden','name':'data[top][]', 'id':'hidden_top'+this.id, 'value':0}),
        });
		//insert style
		hiddenRow.insert({
            bottom:  new Element('input',{'type':'hidden','name':'data[style][]', 'id':'hidden_style'+this.id, 'value':this.params.style}),
        });
		//insert color
		hiddenRow.insert({
            bottom:  new Element('input',{'type':'hidden','name':'data[color][]', 'id':'hidden_color'+this.id, 'value':this.params.color}),
        });
		//insert alignment
		hiddenRow.insert({
            bottom:  new Element('input',{'type':'hidden','name':'data[alignment][]', 'id':'hidden_alignment'+this.id, 'value':this.params.alignment}),
        });
		this.hiddenElement.insert({
            bottom:  hiddenRow,
        });
	}
};
var xhrQueue = [];
var xhrCount = 0;
function updateTemplate(){
	var oOptions = {  
		method: "POST",  
		parameters: $("edit_form").serialize(),  
		asynchronous: true,  
		onFailure: function (oXHR) {  
			$('feedback').update(oXHR.statusText);  
		},  
		onLoading: function (oXHR) {  
			$("template_image").attr('src',loadingImgUrl);
		},                            
		onSuccess: function(oXHR) {
			var json = oXHR.responseText.evalJSON();
			if(json.status == 'success'){
			   if(json.url != ''){
				   var src = baseUrl+'text2image/image/template?template_id='+ template_id +'&data='+json.url;
				   $("template_image").src = src;
				}
			}
		},
	};  
 	xhrQueue.push(xhrCount);
	setTimeout(function(){
		xhrCount = ++xhrCount;
		if (xhrCount === xhrQueue.length) {
			new Ajax.Request(previewUrl, oOptions);
		}
	}, 600);
}

function generateTextLine(){
	var elementTemplate = new TextLineRow(rownum);
	rownum++;
}
