<?php
$type = 'TrueType';
$name = 'Bauhaus-BoldItalic';
$desc = array('Ascent'=>716,'Descent'=>-236,'CapHeight'=>716,'Flags'=>96,'FontBBox'=>'[-121 -236 1209 969]','ItalicAngle'=>23853,'StemV'=>120,'MissingWidth'=>316);
$up = -80;
$ut = 50;
$cw = array(
	chr(0)=>316,chr(1)=>316,chr(2)=>316,chr(3)=>316,chr(4)=>316,chr(5)=>316,chr(6)=>316,chr(7)=>316,chr(8)=>316,chr(9)=>316,chr(10)=>316,chr(11)=>316,chr(12)=>316,chr(13)=>316,chr(14)=>316,chr(15)=>316,chr(16)=>316,chr(17)=>316,chr(18)=>316,chr(19)=>316,chr(20)=>316,chr(21)=>316,
	chr(22)=>316,chr(23)=>316,chr(24)=>316,chr(25)=>316,chr(26)=>316,chr(27)=>316,chr(28)=>316,chr(29)=>316,chr(30)=>316,chr(31)=>316,' '=>316,'!'=>260,'"'=>371,'#'=>599,'$'=>599,'%'=>930,'&'=>709,'\''=>260,'('=>426,')'=>426,'*'=>544,'+'=>638,
	','=>316,'-'=>316,'.'=>316,'/'=>316,'0'=>599,'1'=>599,'2'=>599,'3'=>599,'4'=>599,'5'=>599,'6'=>599,'7'=>599,'8'=>599,'9'=>599,':'=>316,';'=>316,'<'=>638,'='=>638,'>'=>638,'?'=>481,'@'=>843,'A'=>599,
	'B'=>599,'C'=>544,'D'=>654,'E'=>544,'F'=>426,'G'=>820,'H'=>654,'I'=>260,'J'=>371,'K'=>599,'L'=>426,'M'=>820,'N'=>654,'O'=>820,'P'=>544,'Q'=>820,'R'=>599,'S'=>544,'T'=>481,'U'=>599,'V'=>599,'W'=>930,
	'X'=>599,'Y'=>544,'Z'=>544,'['=>426,'\\'=>316,']'=>426,'^'=>638,'_'=>544,'`'=>316,'a'=>599,'b'=>599,'c'=>426,'d'=>599,'e'=>599,'f'=>316,'g'=>599,'h'=>544,'i'=>316,'j'=>260,'k'=>481,'l'=>260,'m'=>764,
	'n'=>544,'o'=>599,'p'=>599,'q'=>599,'r'=>316,'s'=>481,'t'=>371,'u'=>544,'v'=>481,'w'=>764,'x'=>426,'y'=>544,'z'=>426,'{'=>426,'|'=>260,'}'=>426,'~'=>638,chr(127)=>316,chr(128)=>316,chr(129)=>316,chr(130)=>316,chr(131)=>190,
	chr(132)=>426,chr(133)=>1040,chr(134)=>544,chr(135)=>544,chr(136)=>316,chr(137)=>1040,chr(138)=>540,chr(139)=>371,chr(140)=>1040,chr(141)=>316,chr(142)=>316,chr(143)=>316,chr(144)=>316,chr(145)=>260,chr(146)=>260,chr(147)=>426,chr(148)=>426,chr(149)=>544,chr(150)=>544,chr(151)=>1040,chr(152)=>316,chr(153)=>1032,
	chr(154)=>540,chr(155)=>371,chr(156)=>985,chr(157)=>316,chr(158)=>316,chr(159)=>540,chr(160)=>316,chr(161)=>260,chr(162)=>599,chr(163)=>599,chr(164)=>599,chr(165)=>599,chr(166)=>316,chr(167)=>544,chr(168)=>316,chr(169)=>843,chr(170)=>371,chr(171)=>544,chr(172)=>316,chr(173)=>540,chr(174)=>843,chr(175)=>316,
	chr(176)=>316,chr(177)=>638,chr(178)=>316,chr(179)=>316,chr(180)=>316,chr(181)=>544,chr(182)=>638,chr(183)=>316,chr(184)=>316,chr(185)=>316,chr(186)=>371,chr(187)=>544,chr(188)=>316,chr(189)=>875,chr(190)=>316,chr(191)=>481,chr(192)=>599,chr(193)=>599,chr(194)=>599,chr(195)=>599,chr(196)=>599,chr(197)=>599,
	chr(198)=>930,chr(199)=>544,chr(200)=>544,chr(201)=>544,chr(202)=>544,chr(203)=>544,chr(204)=>260,chr(205)=>260,chr(206)=>260,chr(207)=>260,chr(208)=>654,chr(209)=>654,chr(210)=>316,chr(211)=>820,chr(212)=>820,chr(213)=>820,chr(214)=>820,chr(215)=>316,chr(216)=>820,chr(217)=>599,chr(218)=>599,chr(219)=>599,
	chr(220)=>599,chr(221)=>316,chr(222)=>544,chr(223)=>654,chr(224)=>599,chr(225)=>599,chr(226)=>599,chr(227)=>599,chr(228)=>599,chr(229)=>599,chr(230)=>930,chr(231)=>426,chr(232)=>599,chr(233)=>599,chr(234)=>599,chr(235)=>599,chr(236)=>316,chr(237)=>316,chr(238)=>316,chr(239)=>316,chr(240)=>599,chr(241)=>544,
	chr(242)=>599,chr(243)=>599,chr(244)=>599,chr(245)=>599,chr(246)=>599,chr(247)=>316,chr(248)=>599,chr(249)=>544,chr(250)=>544,chr(251)=>544,chr(252)=>544,chr(253)=>316,chr(254)=>599,chr(255)=>544);
$enc = 'cp1252';
$file = 'bauhaus-bold-italic.z';
$originalsize = 53759;
?>
