<?php
$type = 'TrueType';
$name = 'Eurofurencebolditalic';
$desc = array('Ascent'=>826,'Descent'=>-176,'CapHeight'=>649,'Flags'=>96,'FontBBox'=>'[-187 -176 1235 826]','ItalicAngle'=>-15,'StemV'=>120,'MissingWidth'=>364);
$up = -87;
$ut = 124;
$cw = array(
	chr(0)=>364,chr(1)=>364,chr(2)=>364,chr(3)=>364,chr(4)=>364,chr(5)=>364,chr(6)=>364,chr(7)=>364,chr(8)=>364,chr(9)=>364,chr(10)=>364,chr(11)=>364,chr(12)=>364,chr(13)=>364,chr(14)=>364,chr(15)=>364,chr(16)=>364,chr(17)=>364,chr(18)=>364,chr(19)=>364,chr(20)=>364,chr(21)=>364,
	chr(22)=>364,chr(23)=>364,chr(24)=>364,chr(25)=>364,chr(26)=>364,chr(27)=>364,chr(28)=>364,chr(29)=>364,chr(30)=>364,chr(31)=>364,' '=>311,'!'=>241,'"'=>427,'#'=>692,'$'=>441,'%'=>692,'&'=>557,'\''=>223,'('=>262,')'=>262,'*'=>513,'+'=>513,
	','=>218,'-'=>343,'.'=>198,'/'=>514,'0'=>513,'1'=>513,'2'=>513,'3'=>513,'4'=>513,'5'=>513,'6'=>513,'7'=>513,'8'=>513,'9'=>513,':'=>198,';'=>233,'<'=>513,'='=>513,'>'=>513,'?'=>400,'@'=>637,'A'=>692,
	'B'=>498,'C'=>616,'D'=>643,'E'=>498,'F'=>498,'G'=>636,'H'=>595,'I'=>241,'J'=>294,'K'=>598,'L'=>498,'M'=>749,'N'=>614,'O'=>692,'P'=>498,'Q'=>692,'R'=>499,'S'=>440,'T'=>498,'U'=>556,'V'=>672,'W'=>955,
	'X'=>577,'Y'=>556,'Z'=>576,'['=>381,'\\'=>519,']'=>381,'^'=>401,'_'=>518,'`'=>227,'a'=>517,'b'=>498,'c'=>467,'d'=>518,'e'=>425,'f'=>386,'g'=>458,'h'=>459,'i'=>241,'j'=>241,'k'=>510,'l'=>275,'m'=>692,
	'n'=>459,'o'=>517,'p'=>518,'q'=>518,'r'=>370,'s'=>366,'t'=>405,'u'=>439,'v'=>517,'w'=>750,'x'=>461,'y'=>459,'z'=>459,'{'=>343,'|'=>241,'}'=>343,'~'=>397,chr(127)=>364,chr(128)=>641,chr(129)=>364,chr(130)=>218,chr(131)=>452,
	chr(132)=>393,chr(133)=>664,chr(134)=>401,chr(135)=>401,chr(136)=>344,chr(137)=>1012,chr(138)=>440,chr(139)=>253,chr(140)=>984,chr(141)=>364,chr(142)=>586,chr(143)=>364,chr(144)=>364,chr(145)=>218,chr(146)=>218,chr(147)=>393,chr(148)=>393,chr(149)=>513,chr(150)=>401,chr(151)=>634,chr(152)=>400,chr(153)=>715,
	chr(154)=>365,chr(155)=>253,chr(156)=>774,chr(157)=>364,chr(158)=>470,chr(159)=>556,chr(160)=>364,chr(161)=>241,chr(162)=>467,chr(163)=>520,chr(164)=>519,chr(165)=>556,chr(166)=>241,chr(167)=>401,chr(168)=>372,chr(169)=>691,chr(170)=>402,chr(171)=>458,chr(172)=>513,chr(173)=>513,chr(174)=>691,chr(175)=>518,
	chr(176)=>401,chr(177)=>513,chr(178)=>395,chr(179)=>400,chr(180)=>286,chr(181)=>439,chr(182)=>560,chr(183)=>513,chr(184)=>332,chr(185)=>313,chr(186)=>430,chr(187)=>458,chr(188)=>938,chr(189)=>977,chr(190)=>1018,chr(191)=>400,chr(192)=>692,chr(193)=>692,chr(194)=>692,chr(195)=>692,chr(196)=>692,chr(197)=>692,
	chr(198)=>808,chr(199)=>616,chr(200)=>498,chr(201)=>498,chr(202)=>498,chr(203)=>498,chr(204)=>241,chr(205)=>241,chr(206)=>241,chr(207)=>241,chr(208)=>673,chr(209)=>653,chr(210)=>692,chr(211)=>692,chr(212)=>692,chr(213)=>692,chr(214)=>692,chr(215)=>513,chr(216)=>692,chr(217)=>556,chr(218)=>556,chr(219)=>556,
	chr(220)=>556,chr(221)=>556,chr(222)=>556,chr(223)=>520,chr(224)=>517,chr(225)=>518,chr(226)=>518,chr(227)=>517,chr(228)=>517,chr(229)=>518,chr(230)=>736,chr(231)=>467,chr(232)=>425,chr(233)=>425,chr(234)=>430,chr(235)=>445,chr(236)=>241,chr(237)=>241,chr(238)=>241,chr(239)=>241,chr(240)=>517,chr(241)=>459,
	chr(242)=>517,chr(243)=>517,chr(244)=>517,chr(245)=>517,chr(246)=>518,chr(247)=>513,chr(248)=>517,chr(249)=>439,chr(250)=>439,chr(251)=>439,chr(252)=>439,chr(253)=>459,chr(254)=>518,chr(255)=>459);
$enc = 'cp1252';
$file = 'eurof76.z';
$originalsize = 57036;
?>
