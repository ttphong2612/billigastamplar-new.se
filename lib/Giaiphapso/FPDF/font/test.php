<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>jQuery UI Resizable - Default functionality</title>
	<link rel="stylesheet" href="/stempelfabrikken/text2image/css/jquery-ui-1.8.24.custom.css">
	<script src="/stempelfabrikken/text2image/js/jquery-1.8.2.min.js"></script>
	<script src="/stempelfabrikken/text2image/js/jquery-ui-1.8.24.custom.min.js"></script>
	
	<style>
	#resizable { width: 150px; height: 150px; padding: 0.5em; }
	#resizable h3 { text-align: center; margin: 0; }
	</style>
	<script>
	$(function() {
		$( "#img" ).resizable();
	});
	</script>
</head>
<body>

<div class="demo">

<img id="img" src="/stempelfabrikken/text2image/tmp/images/1349163803.png" style="position: absolute;"/>

</div><!-- End demo -->



<div class="demo-description">
<p>Enable any DOM element to be resizable.  With the cursor grab the right or bottom border and drag to the desired width or height.</p>
</div><!-- End demo-description -->

</body>
</html>
