<?php
abstract class Text2ImageDraw_Abstract
{
    public $borderArray = array('BASIC','DBL','DBL_THICK','DBL_THICK_THIN','DBL_THIN_THICK', 'DASHED', 'DOTTED');
    public $colorArray = array('red','blue','black','red-blue','pink','green');
    public $imageTypeArray = array('gif','jpg','png', 'jpeg');
    public $templateTypeArray = array('DATER','STAMP', 'SEAL');
    public $templateShapArray = array('CIRCLE','SQUARE');
    protected $imageType = 'png';
    protected $rate_px_mm = 3.77952775905;
    protected $MAXTEXTWIDTH = 200000;
    protected $tyle ;
    protected $borderWidth = 0;
    protected $widthBorder = 0;
    protected $textColor;
    protected $textLineNum = 0;
    protected $currentHeight = 0;
    protected $imgWidth;//template width
    protected $imgHeight;//template height
    protected $borderStyle;
    protected $daterColor;
    protected $tempateType;
    public $image;//imagick main element
    protected $background;//background template
    protected $textElement = array();
    protected $imageElement = array();
    protected $pathIcon = '';
    protected $storeId = null;
    protected $limitTextWidth;
    protected $limitTextHeight;
	protected $topLineHeight;

    protected function __construct($imgWidth, $imgHeight, $tempateType)
    {
        if(!$imgWidth || !$imgHeight || !in_array($tempateType,$this->templateTypeArray)){
            throw new Exception('Invalid Template Dimension OR Stamp Type');
        }
        $this->imgWidth = $imgWidth;
        $this->imgHeight = $imgHeight;
        
        $this->image = new Imagick();
        $this->image->setresolution($this->resolution, $this->resolution);
        $this->addTextColor('black');
        $this->tempateType = $tempateType;
        $this->background = new ImagickPixel('white');
        $this->tyle = 8/6;
        $this->_convertToNewResolution();
    }
    
    protected function _convertToNewResolution()
    {
    	$this->imgWidth = round($this->imgWidth*$this->rate_px_mm * $this->resolution/96);
    	$this->imgHeight = round($this->imgHeight*$this->rate_px_mm * $this->resolution/96);
    	$this->MAXTEXTWIDTH = round($this->MAXTEXTWIDTH*$this->rate_px_mm * $this->resolution/96);
    	$this->baseLineHeight = round($this->baseLineHeight*$this->rate_px_mm * $this->resolution/96);
    	$this->textplatePadding = round($this->textplatePadding * $this->resolution/96);
    	$this->spaceBorder = round($this->spaceBorder * $this->resolution/96);
    	$this->underline = round($this->underline * $this->resolution/96);
    	$this->roundSpace = round($this->roundSpace * $this->resolution/96);
    }

    protected function _getFontPt2Px($pt)
    {
        $px = $pt * $this->tyle;//convert size pt to px
        return $px;
    }

    protected function _drawCircle()
    {
        $firstWidth = $this->borderWidth;
        if(strpos( $this->borderStyle, 'DBL_THICK_THIN') !== false){
            $firstWidth = $this->borderWidth + round($this->borderWidth / 2);
        }
        $border = new ImagickDraw();
        // Set fill color to transparent
        $border->setFillColor( new ImagickPixel( 'transparent' ) );
        // Set border format
        $border->setStrokeColor($this->textColor  );
        $border->setStrokeWidth( $firstWidth );
        $border->setStrokeAntialias( false );
        $border->setStrokeOpacity(1); // very important
    	if($this->borderStyle == 'DASHED'){
            $border->setStrokeDashArray(array(40, 40, 40));
        }elseif($this->borderStyle == 'DOTTED'){
            $border->setStrokeDashArray(array(10, 10 , 10));
        }
        $border->circle($this->imgWidth/2,  $this->imgHeight/2,  $this->imgWidth/2, $this->imgWidth - $this->roundSpace);
        if(strpos( $this->borderStyle, 'DBL') !== false){
            $secondWidth = $this->borderWidth;
            switch( $this->borderStyle){
                case 'DBL_THIN_THICK': 
                    $secondWidth = $this->borderWidth + round($this->borderWidth / 2);
                    break;
                case 'DBL_THICK':
                    $secondWidth = $firstWidth;
                    break;
            }
            $radius = ($firstWidth + $this->roundSpace) + $secondWidth + $this->roundSpace;
            $radius = $this->imgHeight - $radius;
            $border->setStrokeWidth( $secondWidth );
            $border->setStrokeAntialias( false );
            $border->setStrokeOpacity(1);
            $border->circle($this->imgWidth/2,  $this->imgHeight/2,  $this->imgWidth/2, $this->imgWidth - $radius);
            $this->image->drawImage($border);
        }
        $this->image->drawImage($border);
        $border->clear();
        $border->destroy();
    }
    protected function _drawOval()
    {
        $firstWidth = $this->borderWidth;
        if(strpos( $this->borderStyle, 'DBL_THICK_THIN') !== false){
            $firstWidth = $this->borderWidth + round($this->borderWidth / 2);
        }
        $border = new ImagickDraw();
        // Set fill color to transparent
        $border->setFillColor( new ImagickPixel( 'transparent' ) );
        // Set border format
        $border->setStrokeColor($this->textColor  );
        $border->setStrokeWidth( $firstWidth );
        $border->setStrokeAntialias( false );
        $border->setStrokeOpacity(1); // very important
        if($this->borderStyle == 'DASHED'){
            $border->setStrokeDashArray(array(40, 40, 40));
        }elseif($this->borderStyle == 'DOTTED'){
            $border->setStrokeDashArray(array(10, 10 , 10));
        }
        $radius = $this->firstWidth + $this->roundSpace;
        $border->ellipse( $this->imgWidth/2 , $this->imgHeight/2, ($this->imgWidth/2) - $radius, ($this->imgHeight/2) - $radius, 0, 360 );
        if(strpos( $this->borderStyle, 'DBL') !== false){
            $secondWidth = $this->borderWidth;
            switch( $this->borderStyle){
                case 'DBL_THIN_THICK':
                    $secondWidth = $this->borderWidth + round($this->borderWidth / 2);
                    break;
                case 'DBL_THICK':
                    $secondWidth = $firstWidth;
                    break;
            }
            $radius += $secondWidth + $this->spaceBorder; 
            $border->setStrokeWidth( $secondWidth );
            $border->setStrokeAntialias( false );
            $border->setStrokeOpacity(1);
            
            $border->ellipse( $this->imgWidth/2 , $this->imgHeight/2, ($this->imgWidth/2) - $radius, ($this->imgHeight/2)- $radius, 0, 360 );
            
            $this->image->drawImage($border);
        }
        $this->image->drawImage($border);
        $border->clear();
        $border->destroy();
    }
    protected function _drawRectangle($top =0, $left =0, $bottom=0, $right=0 , $secondWidth)
    {
        $line = new ImagickDraw();
        $line->setStrokeColor( $this->textColor);
        $line->setStrokeWidth( $secondWidth );
        $line->setStrokeAntialias( false );
        if($secondWidth == 2){
            //top
            $line->line($left + 1, $top + ($secondWidth / 2 - 0.5) , $right - 2, $top + ($secondWidth / 2 - 0.5) );
            //left
            $line->line($left + ($secondWidth/2) - 0.5, $top + 1, $left + ($secondWidth/2) - 0.5 , $bottom - 2);
            //bottom
            $line->line($left + 1, $bottom - ($secondWidth / 2 + 0.5), $right - 2 , $bottom - ($secondWidth / 2 + 0.5));
            //right
            $line->line($right - ($secondWidth/2) - 0.5 , $top + 1 , $right - ($secondWidth/2) - 0.5, $bottom -2);
        }else{
            //top
            $line->line($left , $top + ($secondWidth / 2 - 0.5) , $right -1, $top + ($secondWidth / 2 - 0.5) );
            //left
            $line->line($left + ($secondWidth/2) - 0.5, $top, $left + ($secondWidth/2) - 0.5 , $bottom -1);
            //bottom
            $line->line($left, $bottom - ($secondWidth / 2 + 0.5), $right -1 , $bottom - ($secondWidth / 2 + 0.5));
            //right
            $line->line($right - ($secondWidth/2) - 0.5 , $top , $right - ($secondWidth/2) - 0.5, $bottom -1);
        }
        $this->image->drawImage( $line );
        $line->clear();
        $line->destroy();
    }
    protected function _autoHeightTextOval()
    {
    	$this->_calculateLimitSizeOval();
        if($this->middleIconHeight > 0){
        	$this->_autoHeightText($this->limitTextHeightTop, 0, floor($this->textLineNum /2));
        	$this->_autoHeightText($this->limitTextHeightBottom, floor($this->textLineNum /2), $this->textLineNum - floor($this->textLineNum /2));
        }else{
        	$this->_autoHeightText($this->limitTextHeight, 0, $this->textLineNum);
        }
    }
    /*
     * int $distance: distance between round and text area
     * $distance = 0: square template
     * $distance > 0: oval, circle template
     */
    protected function _calculateLimitSize($limitTextWidth, $limitTextHeight, $distanceWidth = 0, $distanceHeight=0)
    {
    	$this->limitTextWidth = $limitTextWidth;
    	$this->limitTextHeight = $limitTextHeight;
    	if($this->iconHeight + ($this->textplatePadding + $this->widthBorder)*2 >= $this->imgHeight)
    	{
    	    return false;
    	}
    	$topMargin = $this->topIconHeight + $this->textplatePadding + $this->widthBorder;
    	$bottomMargin = $this->bottomIconHeight + $this->textplatePadding + $this->widthBorder;
    	$leftMargin = $this->leftIconWidth + $this->textplatePadding + $this->widthBorder;
    	$rightMargin = $this->rightIconWidth + $this->textplatePadding + $this->widthBorder;
    	if($topMargin > $distanceHeight){
    	    $this->limitTextHeight -= $topMargin - $distanceHeight;
    	}else{
    	    $topMargin = $distanceHeight;
    	}
    	if($bottomMargin > $distanceHeight){
    	    $this->limitTextHeight -= $bottomMargin - $distanceHeight;
    	}else{
    	    $bottomMargin = $distanceHeight;
    	}
    	 
    	if($leftMargin > $distanceWidth){
    	    $this->limitTextWidth -= $leftMargin - $distanceWidth;
    	}else{
    	    $leftMargin = $distanceWidth;
    	}
    	if($rightMargin > $distanceWidth){
    	    $this->limitTextWidth -= $rightMargin - $distanceWidth;
    	}else{
    	    $rightMargin = $distanceWidth;
    	}
    	 
    	if($this->middleIconHeight > 0){
    	    $this->limitTextHeightTop = $this->imgHeight/2 - ($topMargin + $this->middleIconHeight / 2);
    	    $this->limitTextHeightBottom = $this->imgHeight/2 - ($bottomMargin + $this->middleIconHeight / 2);
    	}
    	 
    	$this->topMargin = $topMargin;
    	$this->bottomMargin = $bottomMargin;
    	$this->leftMargin = $leftMargin;
    	$this->rightMargin = $rightMargin;
    }
    protected function _calculateLimitSizeCircle()
    {
    	$distance = $this->imgHeight*(sqrt(2) - 1)/(2*sqrt(2));
    	$rSquare = ($this->imgWidth - $this->textplatePadding) / sqrt(2);
    	$this->_calculateLimitSize($rSquare, $rSquare, $distance, $distance);
    }
    protected function _calculateLimitSizeOval()
    {
        $limitTextWidth = $this->imgWidth/sqrt(2);
    	$limitTextHeight = $this->imgHeight/sqrt(2);
    	
    	$distanceHeight = ($this->imgHeight - $limitTextHeight)/2;
    	$distanceWidth = ($this->imgWidth - $limitTextWidth)/2;
        $this->_calculateLimitSize($limitTextWidth, $limitTextHeight, $distanceWidth, $distanceHeight);
    }
    protected function _autoHeightTextCircle()
    {
    	$this->_calculateLimitSizeCircle();
        if($this->middleIconHeight > 0){
        	$this->_autoHeightText($this->limitTextHeightTop, 0, floor($this->textLineNum /2));
        	$this->_autoHeightText($this->limitTextHeightBottom, floor($this->textLineNum /2), $this->textLineNum - floor($this->textLineNum /2));
        }else{
        	$this->_autoHeightText($this->limitTextHeight, 0, $this->textLineNum);
        }
    }
    protected function _autoHeightTextRectangle()
    {
       	$this->_calculateLimitSize($this->imgWidth, $this->imgHeight, 0, 0);
        if($this->middleIconHeight > 0){
        	if($this->isDater()){
        	    $topLine = ceil($this->textLineNum / 2);
        	}else{
        	    $topLine = floor($this->textLineNum / 2);
        	}
            $this->_autoHeightText($this->limitTextHeightTop, 0, $topLine);
            $this->_autoHeightText($this->limitTextHeightBottom, $topLine, $this->textLineNum - $topLine);
        }else{
            $this->_autoHeightText($this->limitTextHeight, 0, $this->textLineNum);
        }
    }
    protected function _autoHeightText($maxTextHeight = 0, $position, $length)
    {
        $fixHeight = false;
		$size_lower = false;
        $elements = $this->textElement;
        $i = -1;
        if($maxTextHeight > 0 )
        {
            while(!$fixHeight && $i < 100)
            {
            	$boundBoxY1 = 0;
                $maxWidth = $this->maxWidth;
                $currentHeight = 0;
                $i++;
                for($key = $position; $key < $length + $position; $key++)
                {
                    $item = $elements[$key];
                    //get font information
                    $font_size = $this->_getFontPt2Px( $item['size'])*$this->resolution/96;
                    $font = $item['font'];
                    $current_encoding = mb_detect_encoding($item['text'], 'auto');
                    $content = iconv($current_encoding, 'UTF-8', $item['text']);
                    $style = $item['style'];
                    if(!empty($style)){
                        if (preg_match("/^(bui|biu|iub|ibu|ubi|uib)$/i", $style)) {
                            $font = $font.'bi';
                        }
                        if (preg_match("/^(bi|ib)$/i", $style)) {
                            $font = $font.'bi';
                        }
                        if (preg_match("/^(b|bu|ub)$/i", $style)) {
                            $font = $font.'b';

                        }
                        if (preg_match("/^(i)$/i", $style)) {
                            $font = $font.'i';
                        }
                    }
                    //draw font
                    if(empty($font)){
                        $font = 'arial';
                    }
                    $font_name = Mage::getBaseDir('lib') . '/Giaiphapso/FPDF/font/unifont/'.$font.'.ttf';
                    $draw = new ImagickDraw();
                   // $draw->setResolution($this->resolution,$this->resolution);
                    $draw->setFont($font_name);
                    $draw->setFontSize($font_size);
                    $draw->setFillColor($this->textColor);
                    if(strpos($style,'b') !== false){ 
                        $draw->setFontWeight(900);
                    }
                    
                    //get query font metrics
                    $metrics = $this->image->queryFontMetrics($draw, $content); 
                    $textWidth =  $metrics['textWidth'] + 2 * $metrics['boundingBox']['x1'];
                    if($textWidth > $this->MAXTEXTWIDTH){
                        $textWidth = $metrics['originX'];
                    }

                    if($textWidth > $maxWidth){
                        $maxWidth = $textWidth;
                    }
                    if($boundBoxY1 > $metrics['boundingBox']['y1']){
                    	$boundBoxY1 = $metrics['boundingBox']['y1'];
                    }
                    
                    $textheight = $metrics['boundingBox']['y2'] - $metrics['boundingBox']['y1'];
					/* if($_SERVER['REMOTE_ADDR'] == '115.79.40.232')
					{
						var_dump($metrics);exit;
					} */
                    $baseline = floatval($metrics['boundingBox']['y2']); // add by phong.tran
                    //$currentHeight += $textheight;
                    $currentHeight += $baseline; // add by phong.tran
                    
                    //clear variable 
                    $draw->clear();
                    $draw->destroy();
                }
                //check last text have boundingbox y
                
                if($metrics['boundingBox']['y1'] < -1){
                	$currentHeight -= $metrics['boundingBox']['y1'];
                }
               
                //check top line called
                if(!$this->topLineHeight){
                	$this->lineHeight = $this->baseLineHeight - $boundBoxY1;
                }
                if($this->middleIconHeight > 0 || $this->isDater()){
                	for($key = $position; $key < $length + $position; $key++)
                	{
                	    if($elements[$key]["size"] > 0){
                	    	$currentHeight += $this->lineHeight;
                		}
            		}
                }else{
                	for($key = $position; $key < $length + $position - 1; $key++)
                	{
                	    if($elements[$key]["size"] > 0){
                	    	$currentHeight += $this->lineHeight;
                		}
            		}
                }
                
                
                $textHeightTemplate = $currentHeight;
                
                $arrHeights[$i] = $textHeightTemplate;
                $arrElements[$i] = $elements;
                $arrMaxWidth[$i] = $maxWidth;
                $arrBoundBoxY1[$i] = $boundBoxY1; 
                //check max current height
                if($currentHeight > $maxTextHeight){
                	$size_lower = true;
                    for($key = $position; $key < $length + $position; $key++)
                    {
                        $elements[$key]['size'] -= 1;
                        if($elements[$key]["size"] < 0){
                            $fixHeight = true;
                            break;
                        }
                    }
                }else{
                    $size_greater = true;
                    for($key = $position; $key < $length + $position; $key++)
                    {
                        $elements[$key]["size"] += 1;
                    }
                }
                
                if($size_lower && $size_greater){
                    $fixHeight = true;
                }
            }
           
            $actualHeight = 0;
            for($i =0; $i< count($arrHeights); $i++){
                if($arrHeights[$i] <= $maxTextHeight && $arrHeights[$i] >= $actualHeight){
                    $actualHeight = $arrHeights[$i];
                    $actualKey = $i;
                }
            }
            $this->maxWidth = $arrMaxWidth[$actualKey];
            $this->textElement = $arrElements[$actualKey];
            $this->boundBoxY1 = $arrBoundBoxY1[$actualKey];
            $this->topLineHeight = $this->lineHeight;
            
            if(!$this->isDater() && empty($this->middleIconHeight)){
            	$this->currentHeight = $arrHeights[$actualKey];
            }else{
                if(empty($this->topTextHeight)){
                	$this->topTextHeight = $arrHeights[$actualKey];
                }elseif(empty($this->bottomTextHeight)){
                	$this->bottomTextHeight = $arrHeights[$actualKey];
                }
                $this->currentHeight = $this->topTextHeight + $this->bottomTextHeight;
            }
        }
    }
    
    protected function _autoHeightTextDater()
    {
        $currentDate = Mage::helper('core')->formatDate(date(), 'medium', false); 
        if( Mage::app()->getWebsite()->getId() == FR_WEBSITE_ID ){
            $currentDate = date('d.m.Y');
        }
        $font_size = $this->_getFontPt2Px( $this->dateFontSize)*$this->resolution/96;
        $font_name = Mage::getBaseDir('lib') . '/Giaiphapso/FPDF/font/unifont/'.$this->dateFontName.'.ttf';
        $draw = new ImagickDraw();
        $draw->setFont($font_name);
        $draw->setFontSize($font_size);
        $draw->setFontWeight(900);
        //$draw->setTextKerning (1);
        $draw->setFillColor($this->daterColor);
        //get query font metrics
        $metrics = $this->image->queryFontMetrics($draw, $currentDate); //var_dump($metrics );exit;
        $textheightDate = $metrics['boundingBox']['y2'] - $metrics['boundingBox']['y1'];
        $textwidthDate = $metrics['textWidth'] + 2 * $metrics['boundingBox']['x1'];
        
        $textwidthDatemm = $textwidthDate /( $this->resolution/96 * $this->rate_px_mm);
        $textheightDatemm = $textheightDate /( $this->resolution/96 * $this->rate_px_mm);
        
        if($this->textLineNum > 0){
        	$this->middleIconHeight = $textheightDate;
        	if($this->dateHeightSpace){
        		$this->middleIconHeight = round($this->dateHeightSpace*$this->resolution/96 * $this->rate_px_mm);
        	}//echo $this->middleIconHeight;exit;
            $this->_autoHeightTextRectangle();
        }
    }

    protected function _calculateIconWidthHeight(){
        $iconWidth = 0;
        $leftWidth = 0;
        $rightWidth = 0;
        $topHeight = 0;
        $bottomHeight = 0;
        $middleHeight = 0;
        foreach($this->imageElement as $image)
        {
            $name = $image["text"];
            $src = $this->pathIcon . $name .'.tiff';
            if( !file_exists($src) ){
                $src = $this->pathIcon . $name .'.png';
            }
            $iconImage = new Imagick();
            $iconImage->readimage($src); 
            $image_dimension = $iconImage->getImageGeometry();
            $tmp_width = $image_dimension["width"];
            $tmp_height = $image_dimension["height"];
            $tyle = $tmp_width / $tmp_height;
            $new_width = round($image["size"] * $this->getRateMM2Pixcel());
            $new_height =  round($new_width / $tyle);
            if($image['style'] != 'background'){
                if(strpos($image['alignment'],'left') !== false  && $new_width > $leftWidth ){
                    $leftWidth = $new_width;
                }elseif(strpos($image['alignment'],'right') !== false  && $new_width > $rightWidth ){
                    $rightWidth = $new_width;
                }elseif(strpos($image['alignment'],'centertop') !== false  && $new_height > $topHeight ){
                    $topHeight = $new_height;
                }elseif(strpos($image['alignment'],'centerbottom') !== false  && $new_height > $bottomHeight ){
                    $bottomHeight = $new_height;
                }elseif(strpos($image['alignment'],'centermiddle') !== false  && $new_height > $middleHeight ){
                    $middleHeight = $new_height;
                }
            }
        }
        $this->leftIconWidth = $leftWidth;
        $this->rightIconWidth = $rightWidth;
        $this->topIconHeight = $topHeight;
        $this->bottomIconHeight = $bottomHeight;
        $this->middleIconHeight = $middleHeight;
        
        $this->iconWidth = $leftWidth + $rightWidth;
        $this->iconHeight = $topHeight + $bottomHeight + $middleHeight; 
    }
    
    protected function _generateTopBottomText($middleHeight = 0)
    {
    	$elements = $this->textElement;
    	if($this->isDater()){
    		$topLine = ceil($this->textLineNum / 2);
    	}else{
    		$topLine = floor($this->textLineNum / 2);
    	}
    	$bottomLine = $this->textLineNum - $topLine;
    	if($this->textLineNum > 0){
    		//draw top text
    		$yTop = ($this->imgHeight - $middleHeight)/2;
    		$yTopDater = $yTop;
    		
    		if($this->limitTextHeightTop > 0){
    			for($key = $topLine - 1; $key >= 0; $key--){
    			    if($elements[$key]['size'] > 0){
						$font_size = $this->_getFontPt2Px($elements[$key]['size'])*$this->resolution/96;
    			    	$font = $elements[$key]['font'];
    			    	$content = $elements[$key]['text'];
    			    				 
    			    	$current_encoding = mb_detect_encoding($content, 'auto');
    			    	$content = iconv($current_encoding, 'UTF-8', $content);
    			    	$content = trim($content);
    			    	$style = $elements[$key]['style'];
    			    				 
    			    	if(!empty($style)){
    			    		if (preg_match("/^(bui|biu|iub|ibu|ubi|uib)$/i", $style)) {
    			    			$font = $font.'bi';
    			    		}
							if (preg_match("/^(bi|ib)$/i", $style)) {
								$font = $font.'bi';
							}
							if (preg_match("/^(b|bu|ub)$/i", $style)) {
  								$font = $font.'b';
  							}
							if (preg_match("/^(i)$/i", $style)) {
									$font = $font.'i';
							}
						}
    			
						$font_name = Mage::getBaseDir('lib') . '/Giaiphapso/FPDF/font/unifont/'.$font.'.ttf';
						$draw = new ImagickDraw();
						//$draw->setResolution($this->resolution,$this->resolution);
						$draw->setFont($font_name);
						$draw->setFontSize($font_size);
						//$draw->setTextKerning (1);
						$draw->setFillColor($this->textColor);
						/* Get font metrics */
						$metrics = $this->image->queryFontMetrics($draw, $content);
						$baseline = $metrics['boundingBox']['y2'];
						$textheight = $metrics['boundingBox']['y2'] - $metrics['boundingBox']['y1'];
						$textwidth = $metrics['textWidth'] + 2 * $metrics['boundingBox']['x1'];
						
						if($key == $topLine - 1 && $this->isDater()){
							$yTopDater = $yTopDater -  $textheight;
						}else{
							$yTopDater = $yTopDater -  ($this->lineHeight + $baseline);
						}
						
						$y = $yTopDater + $baseline;
						switch($elements[$key]['alignment'])
						{
							case 'left':
								$x = $this->leftMargin;
								$draw->setTextAlignment(1);
								break;
							case 'right':
								$x = $this->imgWidth - ($this->rightMargin);
								$draw->setTextAlignment(3);
								break;
							case 'center':
							default:
								$x = $this->leftMargin + $this->limitTextWidth/2 ;
								$draw->setTextAlignment(2);
								break;
						}
						if($elements[$key]['left']){
							$x += $elements[$key]['left']*$this->getRateMM2Pixcel();
						}
						if($elements[$key]['top']){
							$y += $elements[$key]['top']*$this->getRateMM2Pixcel();
						}
						$y = ceil($y);
			
						/* Calculate the vertical starting position
						draw underline */
						if(strpos($style,'u') !== false){
							$line = new ImagickDraw();
							//$line->setResolution($this->resolution,$this->resolution);
							$line->setStrokeColor( $this->textColor  );
							$line->setStrokeWidth($this->underline);
							$line->setStrokeAntialias( false );
							$topUnderline = $y;
							if($elements[$key]['alignment'] == 'right'){
								$line->line($this->imgWidth - ($this->textplatePadding + $this->widthBorder + $this->rightIconWidth + $metrics["originX"]), $topUnderline , $this->imgWidth - ($this->textplatePadding + $this->widthBorder + $this->rightIconWidth) , $topUnderline );
							}elseif($elements[$key]['alignment'] == 'center'){
								$line->line($x - $textwidth/2, $topUnderline, $x + $textwidth/2 , $topUnderline);
							}elseif($elements[$key]['alignment'] == 'left'){
								$line->line($x, $topUnderline, $x + $textwidth , $topUnderline);
							}
							$this->image->drawImage( $line );
							$top += $this->underline;
						}
    			    	$this->image->annotateImage($draw, $x, $y, 0, $content);
    			    }
    			}	
    		}
    		
    		//draw dater bottom
			if($this->limitTextHeightBottom > 0){
				
				if($this->isDater())
					$yTop = ($this->imgHeight  - $middleHeight) / 2 + $middleHeight;
				else
					$yTop = ($this->imgHeight  - $middleHeight) / 2 + $middleHeight + $this->lineHeight  ;
				
				$yTopDater = $yTop;
				for($key = $topLine; $key < count($elements); $key++)
				{
					if($elements[$key]['size'] > 0){
						$font_size = $this->_getFontPt2Px($elements[$key]['size'])*$this->resolution/96;
						//$font_size = $font_size  * ($this->imgWidth / ((100*$this->imgWidth) /(100 + $this->zoomSize)));
						$font = $elements[$key]['font'];
						$content = $elements[$key]['text'];
				
						$current_encoding = mb_detect_encoding($content, 'auto');
						$content = iconv($current_encoding, 'UTF-8', $content);
						$content = trim($content);
						$style = $elements[$key]['style'];
				
						if(!empty($style)){
							if (preg_match("/^(bui|biu|iub|ibu|ubi|uib)$/i", $style)) {
								$font = $font.'bi';
							}
							if (preg_match("/^(bi|ib)$/i", $style)) {
								$font = $font.'bi';
							}
							if (preg_match("/^(b|bu|ub)$/i", $style)) {
								$font = $font.'b';
					
							}
							if (preg_match("/^(i)$/i", $style)) {
								$font = $font.'i';
							}
						}
				
						$font_name = Mage::getBaseDir('lib') . '/Giaiphapso/FPDF/font/unifont/'.$font.'.ttf';
						$draw = new ImagickDraw();
						//$draw->setResolution($this->resolution,$this->resolution);
						$draw->setFont($font_name);
						$draw->setFontSize($font_size);
						//$draw->setTextKerning (1);
						$draw->setFillColor($this->textColor);
						/* Get font metrics */
						$metrics = $this->image->queryFontMetrics($draw, $content);
						$baseline = $metrics['boundingBox']['y2'];
						$textwidth = $metrics['textWidth'] + 2 * $metrics['boundingBox']['x1'];
						
						$y = $yTopDater + $baseline;
						$yTopDater = $yTopDater + $this->lineHeight + $baseline;
						
						switch($elements[$key]['alignment'])
						{
							case 'left':
								$x = $this->leftMargin;
								$draw->setTextAlignment(1);
								break;
							case 'right':
								$x = $this->imgWidth - $this->rightMargin;
								$draw->setTextAlignment(3);
								break;
							case 'center':
							default:
								$x = $this->leftMargin + $this->limitTextWidth/2;
								$draw->setTextAlignment(2); 
								break;
						}
						if($elements[$key]['left']){
							 $x += $elements[$key]['left']*$this->getRateMM2Pixcel();
						}
						if($elements[$key]['top']){
							 $y += $elements[$key]['top']*$this->getRateMM2Pixcel();
						}
						//draw underline
						if(strpos($style,'u') !== false){
							$line = new ImagickDraw();
						//	$line->setResolution($this->resolution,$this->resolution);
							$line->setStrokeColor( $this->textColor  );
							$line->setStrokeWidth($this->underline);
							$line->setStrokeAntialias( false );
							$topUnderline = $y;
							if($elements[$key]['alignment'] == 'right'){
								$line->line($this->imgWidth - ($this->textplatePadding + $this->widthBorder + $this->rightIconWidth + $metrics["originX"]), $topUnderline , $this->imgWidth - ($this->textplatePadding + $this->widthBorder + $this->rightIconWidth) , $topUnderline );
							}elseif($elements[$key]['alignment'] == 'center'){
								$line->line($x - $textwidth/2, $topUnderline, $x + $textwidth/2 , $topUnderline);
							}elseif($elements[$key]['alignment'] == 'left'){
								$line->line($x, $topUnderline, $x + $textwidth , $topUnderline);
							}
							$this->image->drawImage( $line );
							$top += $this->underline;
						}
						$this->image->annotateImage($draw, $x, $y, 0, $content);
					}
				}
			}
		}
    }
    protected function _autoWidthText($maxTextWidth)
    {
        $currentHeight = 0;
        if($maxTextWidth > 0){
            foreach ($this->textElement as &$item)
            {
                //get font information
                $font_size = $this->_getFontPt2Px( $item['size'])*$this->resolution/96;
                $font = $item['font'];
                $current_encoding = mb_detect_encoding($item['text'], 'auto');
                $content = iconv($current_encoding, 'UTF-8', $item['text']);
                $style = $item['style'];
                if(!empty($style)){
                    if (preg_match("/^(bui|biu|iub|ibu|ubi|uib)$/i", $style)) {
                        $font = $font.'bi';
                    }
                    if (preg_match("/^(bi|ib)$/i", $style)) {
                        $font = $font.'bi';
                    }
                    if (preg_match("/^(b|bu|ub)$/i", $style)) {
                        $font = $font.'b';

                    }
                    if (preg_match("/^(i)$/i", $style)) {
                        $font = $font.'i';
                    }

                }
                //draw font
                if(empty($font)){
                    $font = 'arial';
                }
                $font_name = Mage::getBaseDir('lib') . '/Giaiphapso/FPDF/font/unifont/'.$font.'.ttf';

                $draw = new ImagickDraw();
             //   $draw->setResolution($this->resolution,$this->resolution);
                $draw->setFont($font_name);
                $draw->setFontSize($font_size);
                //$draw->setTextKerning (1);
                $draw->setFillColor($this->textColor);
                if(strpos($style,'b') !== false){ 
                    $draw->setFontWeight(900);
                }
                //get query font metrics
                $metrics = $this->image->queryFontMetrics($draw, $content); //var_dump($metrics );exit;
                $textWidth =  $metrics['textWidth'] + 2 * $metrics['boundingBox']['x1'];

                while($textWidth > $maxTextWidth)
                {
                    $item['size']--;
                    $font_size = $this->_getFontPt2Px($item['size'])*$this->resolution/96;
                    $draw->setFontSize($font_size);
                    $metrics = $this->image->queryFontMetrics($draw, $content); 
                    $textWidth =  $metrics['textWidth'] + 2 * $metrics['boundingBox']['x1'];
                }
                if($textWidth > $this->maxWidth){
                    $this->maxWidth = $textWidth;
                }
                $textheight = $metrics['boundingBox']['y2'] - $metrics['boundingBox']['y1'];
                $currentHeight += $metrics['boundingBox']['y2'] + $this->lineHeight;
                //clear variable 
                $draw->clear();
                $draw->destroy();
            }
            $currentHeight -= $this->lineHeight;
            if($metrics['boundingBox']['y1'] < 0){
            	$currentHeight -= $metrics['boundingBox']['y1'];
            }
            $this->currentHeight = $currentHeight;
        }else{
            foreach ($this->textElement as &$item)
            {
                $item['size'] = 0;
            }
            $this->currentHeight = 0;
            $this->maxWidth = 0;
        }
    }
    /*
     * draw text oval
    */
    protected function _generateOvalText()
    {/* if($_SERVER['REMOTE_ADDR'] == '115.79.40.232'){
					echo 'sessiontexttop<pre>';
					print_r(Mage::getSingleton('core/session')->getOvalTextTop())  ;
					echo '<br/>';
					echo 'sessiontextbottom<pre>';
					print_r(Mage::getSingleton('core/session')->getOvalTextBottom())  ;
					echo 'arrayovaltext<pre>';print_r($this->ovalTextElement);exit;
				} */
        if(count($this->ovalTextElement) > 0){
            foreach($this->ovalTextElement as $item)
            { 
                //generate oval text top
            	if(empty($item['text'])){
            	    continue;
            	}
                if($item['left'] > 30){
                	$item['left'] == 30;
                }elseif($item['left'] < -30){
                	$item['left'] = -30;
                }
            	
                //call to dbizgroup to create oval text
				$this->_getOvalTextImage($item);
               
            }
        }
    }
	
	protected function _getOvalTextImage($item)
	{
		$content = $item['text'];
		$current_encoding = mb_detect_encoding($content, 'auto');
		$content = iconv($current_encoding, 'UTF-8', $content);
		$content = trim($content);
		$style = $item['style'];
		$font = $item['font'];			 
		if(!empty($style)){
			if (preg_match("/^(bui|biu|iub|ibu|ubi|uib)$/i", $style)) {
				$font = $font.'bi';
			}
			if (preg_match("/^(bi|ib)$/i", $style)) {
				$font = $font.'bi';
			}
			if (preg_match("/^(b|bu|ub)$/i", $style)) {
				$font = $font.'b';
			}
			if (preg_match("/^(i)$/i", $style)) {
					$font = $font.'i';
			}
		}
		
		$font = Mage::getBaseDir('lib') . '/Giaiphapso/FPDF/font/unifont/'.$font.'.ttf';
		$text = $content;
		$font_size = $this->_getFontPt2Px($item['size'])*$this->resolution/96;
		$draw = new ImagickDraw();
		$draw->setFont($font);
		$draw->setFontSize($font_size);
		$draw->setFillColor($this->textColor);
		
		/* Get font metrics */
		$metrics = $this->image->queryFontMetrics($draw, $text);
		$baseline = $metrics['boundingBox']['y2'];
		$textwidth = $metrics['textWidth'] + 2 * $metrics['boundingBox']['x1'];
		$descender = $metrics['descender'];
		$space = 2*$this->resolution/96;
		$r = floatval($this->imgWidth/2) + $space;
		$degreesTotal = ($textwidth * 180) / (3.14 * $r);
		
		$calcuDescrees = (180 - ($degreesTotal + strlen($text)*2))/2 + ($item['left']*10);
		
		for ($i=0;$i<strlen($text);$i++) {
			$metrics = $this->image->queryFontMetrics($draw, $text[$i]);
			$charWidth = $metrics['textWidth'];
			$degrees = ($charWidth/$textwidth)*$degreesTotal;
			
			$a = $calcuDescrees + 90;
			$y = $this->imgWidth/2 - ($baseline);
			$x = 0;
			if($item['type'] != 'texttop'){
				$a = -$a;
				$y += $baseline + $descender;
			}
			
			$cos = cos(deg2rad($a));
			$sin = sin(deg2rad($a));
			
			$xt = round($cos*($x) - $sin*($y));
			$yt = round($sin*($x) + $cos*($y));
			if($item['type'] != 'texttop'){
				$xt = -$xt;
				$yt = -$yt;
			}
			$this->image->annotateImage($draw, $xt + $r, $yt + $r, 180 + $a, $text[$i]);
			
			$calcuDescrees += $degrees;
			if($text[$i] == ' '){
				$calcuDescrees = $calcuDescrees;
			}elseif(!in_array($text[$i],array('l','i','1','|',',','.'))){
				$calcuDescrees +=3;
			}else{
				$calcuDescrees += 2;
			}
		}
	}
    /*
     * generate text
    */
    protected function _generateText()
    {
        $top = $this->topMargin + ($this->limitTextHeight - $this->currentHeight)/2;
        if($this->limitTextHeight  < $this->imgHeight){
            $elements = $this->textElement;
            $currentHeight = 0;
            foreach ($elements as $item)
            {
                if($item['size'] > 0){
                    $font_size = $this->_getFontPt2Px($item['size'])*$this->resolution/96;
                    $font = $item['font'];
                    $content = $item['text'];
                    $current_encoding = mb_detect_encoding($content, 'auto');
                    $content = iconv($current_encoding, 'UTF-8', $content);
                    $content = trim($content);
                    $style = $item['style'];
                    if(!empty($style)){
                        if (preg_match("/^(bui|biu|iub|ibu|ubi|uib)$/i", $style)) {
                            $font = $font.'bi';
                        }
                        if (preg_match("/^(bi|ib)$/i", $style)) {
                            $font = $font.'bi';
                        }
                        if (preg_match("/^(b|bu|ub)$/i", $style)) {
                            $font = $font.'b';
    
                        }
                        if (preg_match("/^(i)$/i", $style)) {
                            $font = $font.'i';
                        }
                    }
                    if(empty($font)){
                        $font = 'arial';
                    }
                    $font_name = Mage::getBaseDir('lib') . '/Giaiphapso/FPDF/font/unifont/'.$font.'.ttf';
                    $draw = new ImagickDraw();
                    //    $draw->setResolution($this->resolution,$this->resolution);
                    $draw->setFont($font_name);
                    $draw->setFontSize($font_size);
                    //$draw->setTextKerning (1);
                    $draw->setFillColor($this->textColor);
                    /* Get font metrics */
                    $metrics = $this->image->queryFontMetrics($draw, $content);
                    $baseline = floatval($metrics['boundingBox']['y2']);
                    $textheight = $metrics['boundingBox']['y2'] - $metrics['boundingBox']['y1'];
                    $textwidth = floatval($metrics['textWidth'] + (2 * $metrics['boundingBox']['x1']));
                    if($textwidth > $this->MAXTEXTWIDTH){
                        $textwidth = floatval($metrics['originX']);
                    }
                    $y = $top + $baseline;
                    //$top += $textheight;
                    $top += $baseline;
                    $currentHeight += $baseline;
                    $isDraw = true;
                    switch($item['alignment'])
                    {
                        case 'left':
                            //set position if template have icon on left
                            $x = $this->leftMargin;
                            $draw->setTextAlignment(1);
                            break;
                        case 'right':
                            $x = $this->imgWidth - ($this->rightMargin );
                            $draw->setTextAlignment(3);
                            break;
                        case 'center':
                        default:
                            $x = $this->leftMargin + abs($this->limitTextWidth - $textwidth) / 2;
                            if($x >= $this->imgWidth){
                                $isDraw = false;
                            }
                            $draw->setTextAlignment(1);
                            break;
                    }
                    if(!$isDraw){
                        continue;
                    }
                    //set position left on own
                    if($item['left']){
                        $x += $item['left']*$this->getRateMM2Pixcel();
                    }
                    // Calculate the vertical starting position
                    if(strpos($style,'u') !== false){
                        $line = new ImagickDraw();
                        //   $line->setResolution($this->resolution,$this->resolution);
                        $line->setStrokeColor( $this->textColor  );
                        $line->setStrokeWidth($this->underline);
                        $line->setStrokeAntialias( false );
                        $topUnderline = $top + $item['top']*$this->getRateMM2Pixcel();
                        if($item['alignment'] == 'right'){
                            $line->line($this->imgWidth - ($this->textplatePadding + $this->widthBorder + $this->rightIconWidth + $metrics["originX"]), $topUnderline , $this->imgWidth - ($this->textplatePadding + $this->widthBorder + $this->rightIconWidth) , $topUnderline );
                        }elseif($item['alignment'] == 'center'){
                            $line->line($x, $topUnderline, $x + $textwidth , $topUnderline);
                        }elseif($item['alignment'] == 'left'){
                            $line->line($x, $topUnderline, $x + $textwidth , $topUnderline);
                        }
                        $this->image->drawImage( $line );
                        $top += $this->underline;
                        $line->clear();
                        $line->destroy();
                    }
                    if($this->lineHeight){
                        $top += $this->lineHeight;
                        $currentHeight += $this->lineHeight;
                    }
    
                    if($item['top']){
                        $y += $item['top']*$this->getRateMM2Pixcel();
                    }
                    $this->image->annotateImage($draw, $x, $y, 0, $content);
                    $draw->clear();
                    $draw->destroy();
                }
            }
        }
    }
    //generate icon stamp
    protected function _generateImage()
    {
        foreach($this->imageElement as $item)
        {
            $name = $item["text"];
            $src = $this->pathIcon . $name .'.tiff';
            if( !file_exists($src) ){
                $src = $this->pathIcon . $name .'.png';
            }
            if(file_exists($src)){
                $iconImage = new Imagick();
                $iconImage->readimage($src); 
                if($item["style"] != 'background'){
                    $image_dimension = $iconImage->getImageGeometry();
                    $tmp_width = $image_dimension["width"];
                    $tmp_height = $image_dimension["height"];
                    $tyle = $tmp_width / $tmp_height;
                    if($item["size"] > 0){
                        $new_width = round($item["size"] * $this->getRateMM2Pixcel());
                        $new_height =  round($new_width / $tyle);
                    }
                    //check icon image size >= template size, return template size - border size
                    if($new_width >= $this->imgWidth - $this->textplatePadding){
                        $new_width = $new_width - $this->widthBorder*2 - $this->textplatePadding*2;
                        $new_height = (int) $new_width / $tyle;
                    }

                    if($new_height >= $this->imgHeight - $this->textplatePadding){
                        $new_height = $new_height - $this->widthBorder*2 - $this->textplatePadding*2;
                        $new_width = (int) $new_height * $tyle;
                    }

                    // Identify icon's position
                    switch($item["alignment"]){
                        case "lefttop":
                            $imgleft = $this->widthBorder + $this->textplatePadding;
                            $imgtop = $this->widthBorder + $this->textplatePadding;
                            break;
                        case "centertop":
                            $imgleft = (int)($this->imgWidth - $new_width)/2;
                            $imgtop = $this->widthBorder + $this->textplatePadding;
                            break;
                        case "righttop":
                            $imgleft = (int)($this->imgWidth - $this->textplatePadding - ($new_width + $this->widthBorder ));
                            $imgtop = $this->widthBorder + $this->textplatePadding;
                            break;
                        case "leftmiddle":
                            $imgtop = (int)($this->imgHeight -  $new_height)/2;
                            $imgleft = $this->widthBorder + $this->textplatePadding;
                            break;	
                        case "centermiddle":
                            $imgtop = (int)($this->imgHeight - $new_height)/2;
                            $imgleft = (int)($this->imgWidth - $new_width)/2;
                            break;
                        case "rightmiddle":
                            $imgtop = (int)($this->imgHeight - $new_height)/2;
                            $imgleft = (int)($this->imgWidth  - ($new_width + $this->widthBorder + $this->textplatePadding));
                            break;		
                        case "leftbottom":
                            $imgtop = (int)($this->imgHeight - ($this->textplatePadding + $this->widthBorder + $new_height));
                            $imgleft = $this->widthBorder + $this->textplatePadding;
                            break;
                        case "centerbottom":
                            $imgtop = (int)($this->imgHeight - $this->textplatePadding - ($new_height + $this->widthBorder));
                            $imgleft = (int)($this->imgWidth - $new_width)/2;
                            break;	
                        case "rightbottom":
                            $imgtop = (int)($this->imgHeight - ($this->textplatePadding + $this->widthBorder + $new_height));
                            $imgleft = (int)($this->imgWidth - ($this->textplatePadding + $new_width + $this->widthBorder));
                            break;	
                    }
                    $addition_left = $item["left"] * $this->getRateMM2Pixcel();
                    $imgleft += $addition_left;
                    $addition_top = $item["top"] * $this->getRateMM2Pixcel();
                    $imgtop += $addition_top;

                }else{
                    $new_width = $this->imgWidth - 2*($this->textplatePadding + $this->widthBorder);
                    $new_height = $this->imgHeight - 2*($this->textplatePadding + $this->widthBorder);
                    $imgleft = $this->textplatePadding + $this->widthBorder;
                    $imgtop  = $this->textplatePadding + $this->widthBorder;
                }
                $iconImage->scaleimage($new_width,$new_height);
				$iconImage->paintTransparentImage($iconImage->getImageBackgroundColor(), 0, 10000);
				$iconImage->floodfillPaintImage($this->textColor, 1, $this->background, 1, 1, true);
               
                $this->image->compositeImage($iconImage, imagick::COMPOSITE_DEFAULT, $imgleft, $imgtop);
            }
        }
    }
    /*
     * draw circle mask, use for template circle
     */
    protected function _drawCircleMask()
    {
    	$circle = new Imagick();
    	$circle->newImage($this->imgWidth, $this->imgHeight, 'none' );
    	
    	$draw = new ImagickDraw();
    	$draw->setfillcolor('black');
    	$draw->circle($this->imgWidth/2,  $this->imgHeight/2,  $this->imgWidth/2, $this->imgWidth);
    	$circle->drawimage($draw);
    	$circle->setimageformat('png');
    	$circle->setimagematte(true);
    	$this->image->compositeImage($circle, Imagick::COMPOSITE_COPYOPACITY,0,0);
    }
    protected function _drawOvalMask()
    {
    	$circle = new Imagick();
    	$circle->newImage($this->imgWidth, $this->imgHeight, 'none' );
    	$draw = new ImagickDraw();
    	$draw->setfillcolor('black');
    	//Substract the border from the radius so it doesn't fall outside the screen.
    	$draw->ellipse( $this->imgWidth/2, $this->imgHeight/2, ($this->imgWidth/2) , ($this->imgHeight/2), 0, 360 );
    	$circle->drawimage($draw);
    	$circle->setimageformat('png');
    	$circle->setimagematte(true);
    	 
    	$this->image->compositeImage($circle, Imagick::COMPOSITE_COPYOPACITY,0,0);
    }
    public function __destruct()
    {
        $this->textColor->clear();
        $this->textColor->destroy();
        $this->image->clear();
        $this->image->destroy();
    }
    // Add by phong.tran
    public function myLog($str){
        //return false;
        Mage::log($str, null,'text2image.log', true);
    }

    function myDebug($obj)
    {
        if( $_SERVER['REMOTE_ADDR'] == '1.54.83.7' ){
            echo '<pre>';
            print_r($obj);
            die();
        }
    }
}
