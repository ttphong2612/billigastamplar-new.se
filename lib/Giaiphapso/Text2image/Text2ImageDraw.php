<?php
    define('TEXT2IMAGE_VERSION','0.2');
    if (!defined('TEXT2IMAGE_ROOT')) {
        define('TEXT2IMAGE_ROOT', dirname(__FILE__) . '/');
        require(TEXT2IMAGE_ROOT . '/Abstract.php');
    }
    class Text2ImageDraw extends Text2ImageDraw_Abstract
    {
        //contruct function
        //width , height in mm
        protected $resolution = 600;//600DPI
        protected $minPtNormalFont = 7;
        protected $minPtBoldFont = 9;
        protected $zoomSize = 0;
        protected $baseLineHeight = 0.3;
        protected $spaceBorder = 3;//for square border
        protected $dateFontSize = 11;//pt
        protected $dateHeightSpace = 11;//mm
        protected $dateFontName = 'arialb';
        protected $underline =  1;//underline text 1px
        protected $roundSpace =  2;//space between 2 border (for circle)
        protected $textplatePadding = 3; // 3px ~ 1 mm // distance from content to boundary
        
        public function __construct($imgWidth, $imgHeight, $tempateType, $templateShap = 'SQUARE', $storeId = null)
        {
            parent::__construct($imgWidth, $imgHeight, $tempateType);
            $this->templateShap = $templateShap;
            $this->colorArray = self::getColorList();
            if(is_numeric($storeId)){
                $this->storeId = $storeId;
                $this->pathIcon = Mage::getBaseDir('base') . DS . 'media' .DS . 'order' . DS . $storeId . DS;
            }else{
                $this->pathIcon = Mage::getBaseDir('base') . DS . 'media' .DS . 'tmp' . DS . 'textplate' . DS;
            }
        }
        public static function getColorList()
        {
            $color = array();
            $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'color');
            foreach ($attribute->getSource()->getAllOptions(true) as $option) {
                $color[$option['value']] = strtolower($option['label']);
            }
            return $color;
        }
        public function addZoomSize($zoom)
        {
        	$this->zoomSize = $zoom;
        }    
        public function addLineHeight($lineHeight)
        {
            $this->baseLineHeight = $lineHeight*$this->getRateMM2Pixcel();
        }

        public function isDater()
        {
            if($this->tempateType == 'DATER')
                return true;
            return false;
        }

        public function getRateMM2Pixcel()
        {
            return $this->rate_px_mm * $this->resolution/96;
        }

        public function addBorder($borderWidth = 0, $borderStyle)
        {
            if(in_array($borderStyle,$this->borderArray)){
                $this->borderStyle = $borderStyle;
            }else{
                throw new Exception('Invalid Border Style');
            }
            $borderWidth = $borderWidth*$this->getRateMM2Pixcel();
            $this->borderWidth = $borderWidth;
            $this->widthBorder = $borderWidth;
            if(strpos($borderStyle, 'DBL') !== false){
                switch($borderStyle){
                    case 'DBL':
                        $this->widthBorder = $borderWidth*2 + $this->spaceBorder; 
                        break;
                    case 'DBL_THICK':
                        $this->borderWidth = $borderWidth + ($borderWidth / 2);
                        $this->widthBorder = $this->borderWidth*2  + $this->spaceBorder;
                        break;
                    case 'DBL_THICK_THIN':
                    case 'DBL_THIN_THICK':
                        $this->widthBorder  +=  ($borderWidth + ($borderWidth / 2)) + $this->spaceBorder;
                        break;
                }
                if (strcasecmp($this->templateShap, 'CIRCLE') === 0) {
                	$this->widthBorder += $this->roundSpace;
                }
            }

            $this->borderWidth = round($this->borderWidth);
            $this->widthBorder = round($this->widthBorder);
        }

        public function addDaterColor($color)
        {
            if(in_array($color,$this->colorArray)){
                $this->daterColor =  new ImagickPixel($color);
            }else{
                throw new Exception('Invalid Color');
            }
        }

        public function addTextColor($color)
        {
            if(in_array($color,$this->colorArray)){
                $this->textColor =  new ImagickPixel($color);
            }else{
                throw new Exception('Invalid Color');
            }
        }

        public function addLineElements($elements)
        {
            if($elements && !is_array($elements)){
                $elements = Mage::helper('core')->jsonDecode($elements);
            }
            $this->elements  = $elements;
            $currentHeight = 0;
            $maxWidth = 0;
            $imageElement = array();
            $boundBoxY1 = 0;
			$tempElement = array();
			$ovalTextElement = array();
            for ($i = 0; $i < count($elements); $i++)
            {
                if ( $elements[$i]['type'] == 'text'){
                    $tempElement[] = $elements[$i];
                    $item = $elements[$i];
                    $font_size = $this->_getFontPt2Px( $item['size'])*$this->resolution/96;
                    $font = $item['font'];
                    $content = $item['text'];
                    $current_encoding = mb_detect_encoding($content, 'auto');
                    $content = iconv($current_encoding, 'UTF-8', $content);
                    $style = $item['style'];

                    if(!empty($style)){
                        if (preg_match("/^(bui|biu|iub|ibu|ubi|uib)$/i", $style)) {
                            $font = $font.'bi';
                        }
                        if (preg_match("/^(bi|ib)$/i", $style)) {
                            $font = $font.'bi';
                        }
                        if (preg_match("/^(b|bu|ub)$/i", $style)) {
                            $font = $font.'b';

                        }
                        if (preg_match("/^(i)$/i", $style)) {
                            $font = $font.'i';
                        }
                    }

                    $font_name = Mage::getBaseDir('lib') . '/Giaiphapso/FPDF/font/unifont/'.$font.'.ttf';
                    $draw = new ImagickDraw();
             //       $draw->setResolution($this->resolution, $this->resolution);
                    $draw->setFont($font_name);
                    $draw->setFontSize($font_size);
                    $draw->setFillColor($this->textColor);

                    /* Get font metrics */
                    $metrics = $this->image->queryFontMetrics($draw, $content);
                    if($boundBoxY1 > $metrics['boundingBox']['y1']){
                    	$boundBoxY1 = $metrics['boundingBox']['y1'];
                    }
                    $textheight = $metrics['boundingBox']['y2'] - $metrics['boundingBox']['y1'];
                    $baseline = floatval($metrics['boundingBox']['y2']); // add by phong.tran
                    //$currentHeight += $textheight;
                    $currentHeight += $baseline; // add by phong.tran

                    if($metrics['textWidth'] > $maxWidth){
                        $maxWidth = $metrics['textWidth'];
                    }
                    $this->textLineNum++;
                }elseif($elements[$i]['type'] == 'image'){
                    $imageElement[] = $elements[$i];
                }elseif(strtoupper($this->templateShap) == "CIRCLE" && ($elements[$i]['type'] == 'texttop' || $elements[$i]['type'] == 'textbottom') ){
                    $ovalTextElement[] = $elements[$i];
                }
            }
            //check last text have boundingBox y1
            if(isset($metrics) && $metrics['boundingBox']['y1'] < 0){
            	$currentHeight -= $metrics['boundingBox']['y1'];
            }
            $this->lineHeight = $this->baseLineHeight - $boundBoxY1;
            $currentHeight += ($this->textLineNum-1)*$this->lineHeight;
            $this->currentHeight = $currentHeight;
            $this->maxWidth = $maxWidth;
//             $this->maxWidth = 0;
            $this->textElement = $tempElement;
            $this->imageElement = $imageElement;
            $this->ovalTextElement = $ovalTextElement;
            $this->_calculateIconWidthHeight();
        }

        public function drawBorder()
        {
            if(strtoupper($this->templateShap) == "CIRCLE"){
                $this->_drawCircle();
            }elseif(strtoupper($this->templateShap) == "OVAL"){
                $this->_drawOval();
            }else{
                $firstWidth = $this->borderWidth;
                if(strpos( $this->borderStyle, 'DBL_THICK_THIN') !== false){
                    $firstWidth = $this->borderWidth + floor($this->borderWidth / 2);
                }
                $border = new ImagickDraw();
          //      $border->setResolution($this->resolution,$this->resolution);
                // Set fill color to transparent
                $border->setFillColor( new ImagickPixel( 'transparent' ) );
                // Set border format
                $border->setStrokeColor($this->textColor  );
                $border->setStrokeWidth( $firstWidth );
                $border->setStrokeAntialias( false );
                $border->setStrokeOpacity(1); // very important
                if($this->borderStyle == 'DASHED'){
                    $border->setStrokeDashArray(array(10*$this->resolution/96, 10*$this->resolution/96, 10*$this->resolution/96));
                }elseif($this->borderStyle == 'DOTTED'){
                    $border->setStrokeDashArray(array(2*$this->resolution/96, 2*$this->resolution/96 , 2*$this->resolution/96));
                }
                // Draw border
                $border->rectangle(
                    $firstWidth / 2,
                    $firstWidth / 2,
                    $this->imgWidth - ($firstWidth / 2) - 1,
                    $this->imgHeight - ($firstWidth / 2) - 1
                );
                $this->image->drawImage( $border );
                //clear object
                $border->clear();
                $border->destroy();
                if(strpos( $this->borderStyle, 'DBL') !== false){
                    $secondWidth = $this->borderWidth;
                    switch($this->borderStyle){
                        case 'DBL_THIN_THICK':
                            $secondWidth = $this->borderWidth + floor($this->borderWidth / 2);
                            break;
                        case 'DBL_THICK':
                            $secondWidth = $firstWidth;
                            break;
                    }
                    $top = $firstWidth + $this->spaceBorder;
                    $left = $top;
                    $right = $this->imgWidth - $left;
                    $bottom = $this->imgHeight - $top;
                    $this->_drawRectangle($top , $left, $bottom, $right, $secondWidth);
                }
            }
        }

        //auto size
        protected function _autoWidthHeight()
        {
            if($this->textLineNum > 0){
                if($this->tempateType == 'STAMP'){
                    if(strtoupper($this->templateShap) == 'CIRCLE'){
                        $this->_autoHeightTextCircle();
                    }elseif(strtoupper($this->templateShap) == 'OVAL'){
                    	$this->_autoHeightTextOval();
                    }else{
                    	$this->_autoHeightTextRectangle();
                    }
                    $this->_autoWidthText($this->limitTextWidth);
                }elseif($this->tempateType == 'DATER'){
                    $this->_autoHeightTextDater();
                    $limitTextWidth = $this->imgWidth - ($this->textplatePadding*2 + $this->iconWidth + $this->widthBorder*2);
                    if($this->maxWidth > $limitTextWidth){
                        $this->_autoWidthText($limitTextWidth);
                    }
                }
            }
        }

        public function processAuto()
        {
            $this->_autoWidthHeight();
        }
        /*
         * draw text circle
        */
        protected function _generateStampCircle()
        {
            //check calcute limit size circle
            if(empty($this->topMargin)){
                $this->_calculateLimitSizeCircle();
            }
       		if($this->middleIconHeight > 0){
            	$this->_generateTopBottomText($this->middleIconHeight);
            }else{
            	$this->_generateText();
            }
            $this->_generateOvalText();
        }
        
        
        /*
         * draw text oval
        */
        protected function _generateStampOval()
        {
            /* check calculate limit text width * height */
            if(empty($this->topMargin)){
                $this->_calculateLimitSizeOval();
            }
       		if($this->middleIconHeight > 0){
            	$this->_generateTopBottomText($this->middleIconHeight);
            }else{
            	$this->_generateText();
            }
        }
        
        /*
         * draw text retangle
        */
        protected function _generateStampRectangle()
        {
            //draw stamp
            if(empty($this->topMargin)){
                $this->_calculateLimitSize($this->imgWidth, $this->imgHeight, 0, 0);
            }
            if($this->middleIconHeight > 0){
            	$this->_generateTopBottomText($this->middleIconHeight);
            }else{
            	$this->_generateText();
            }
        }
        /*
         * draw text dater
        */
        protected function _generateDater()
        {
            $font_size = $this->dateFontSize;
            $font_size = $this->_getFontPt2Px($font_size)*$this->resolution/96;
        
            $font_name = Mage::getBaseDir('lib') . '/Giaiphapso/FPDF/font/unifont/arialb.ttf';
            $draw = new ImagickDraw();
            //$draw->setResolution($this->resolution,$this->resolution);
            $draw->setFont($font_name);
            $draw->setFontSize($font_size);
            $draw->setFontWeight(900);
            //$draw->setTextKerning (1);
            $draw->setFillColor($this->daterColor);
            /* Get font metrics */
            $currentDate = Mage::helper('core')->formatDate(date(), 'medium', false);
            if( Mage::app()->getWebsite()->getId() == FR_WEBSITE_ID ){
                $currentDate = date('d.m.Y');
            }
            $currentDate = strtoupper($currentDate);
            $metrics = $this->image->queryFontMetrics($draw, $currentDate);
            $baselineDater = $metrics['boundingBox']['y2'];
            $textheightDate = $metrics['boundingBox']['y2'] - $metrics['boundingBox']['y1'];
            $textwidthDate = $metrics['textWidth'] + 2 * $metrics['boundingBox']['x1'];
            $x = $this->imgWidth/2;
            $y = round($this->imgHeight - $textheightDate)/2 + $baselineDater;
            $draw->setTextAlignment(2);
            
            $dateHeightSpace = round($this->dateHeightSpace*$this->resolution/96 * $this->rate_px_mm);
            if(empty($this->topMargin)){
            	$this->middleIconHeight = $dateHeightSpace;
                $this->_calculateLimitSize($this->imgWidth, $this->imgHeight, 0, 0);
            }
            $this->image->annotateImage($draw, $x, $y, 0, $currentDate);
            $this->_generateTopBottomText($dateHeightSpace);
        }
        public function generateTemplate()
        {
            $this->image->newImage($this->imgWidth, $this->imgHeight, $this->background);
            /*draw icon image */
            $this->_generateImage();
            if($this->isDater()){
                $this->_generateDater();
            }else{
            	if(strtoupper($this->templateShap) == "CIRCLE"){
            		$this->_generateStampCircle();
            	}elseif(strtoupper($this->templateShap) == "OVAL"){
            		$this->_generateStampOval();
            	}else{
            		$this->_generateStampRectangle();
            	}
            }
            if(strtoupper($this->templateShap) == "CIRCLE"){
                $this->_drawCircleMask();
            }elseif(strtoupper($this->templateShap) == "OVAL"){
            	$this->_drawOvalMask();
            }
            // imagick::RESOLUTION_UNDEFINED  imagick::RESOLUTION_PIXELSPERINCH  imagick::RESOLUTION_PIXELSPERCENTIMETER
            $this->image->setImageUnits(Imagick::RESOLUTION_PIXELSPERINCH);
        }
		
		public function previewPdf()
		{
			header("Content-type: application/pdf");
			echo $this->image->getImageBlob();
            exit;
		}
		
        public function download($type='pdf', $fileName='design.pdf')
        {
            $this->image->setImageFormat($type);
            if($type == 'pdf' ){
            	header("Content-type: application/pdf");
            }elseif($type == 'svg' ){
            	header("Content-Type: image/svg+xml");
            }
            header('Content-Disposition: attachment; filename="'. $fileName .'"');
            echo $this->image->getImageBlob();
            exit;
        }
        
        public function previewImage($imageType)
        {
            if(in_array($imageType, $this->imageTypeArray)){
                $this->imageType = $imageType?$imageType:$this->imageType;
            }
            $zoom = 1;
            if($this->zoomSize){
            	$zoom = ($this->zoomSize + 100)/100;
            }
            $this->image->scaleImage(($this->imgWidth/$this->resolution)*96*$zoom, ($this->imgHeight/$this->resolution)*96*$zoom); 
           
            $this->image->setImageFormat($this->imageType);
            header('Content-type: image/'.$this->imageType);
            echo $this->image;
            exit;
        }

        /* Code add by phong.tran@giaiphapso.com */
        public function saveTemplateOnServer($type, $fileName, $resolution = array(900, 900))
        {
            try{
                if($type == 'tiff'){
                    $this->image->setImageMatte(false);
                    $max = $this->image->getQuantumRange();
                    $max = $max["quantumRangeLong"];
                    $this->image->thresholdImage(0.77 * $max);
                }
                $this->image->setImageFormat($type);
                $this->image->writeImage($fileName);    
            }catch( Exception $e ){
                echo $e->getMessage();
            }                         
        }

        /* Code add by phong.tran@giaiphapso.com */
        public function setPathIcon($path)
        {
            $this->pathIcon =  $path;
        }
        /*
         * get Elements
         */
        public function getElements()
        {
        	return $this->elements;
        }
}