/* Loading Script */
wdW = jQuery(window).width();
wdH = jQuery(window).height();
winresize = function(){ 
    
}

function autoCutStr(prefix) {
    if (!prefix || prefix === undefined) {
        prefix = "autoCutStr_";
    }
    jQuery('[class^="' + prefix + '"]').each(function () {
        if (jQuery(this).length > 0) {
            var str = jQuery(this).html();
            str = str.replace('<br/>','');
            str = str.replace('<br>','');
            jQuery(this).html(str);
            var len = parseInt(jQuery(this).attr("class").substr(jQuery(this).attr("class").lastIndexOf("_") + 1));
            var length = str.length;
            if (length > len) {
                if (str.charAt(len) == ' ') {
                    str = str.substr(0, len);
                }
                else {
                    str = str.substr(0, len);
                    str = str.substr(0, str.lastIndexOf(" "));
                }
                jQuery(this).html(str + "...");
            }
        }
        
    });
}



jQuery(document).ready(function(){
    winresize();
     autoCutStr();
    jQuery('.main-nav').click(function(event) {
          jQuery('.nav-main').slideToggle('slow');
    });
   
  /*SLIDER BY QUYEN*/    
      jQuery('.mn-con').mouseenter(function(){
          jQuery('.sub-menu').stop(true,true).fadeIn(200)
      }).mouseleave(function(){
          jQuery('.sub-menu').stop(true,true).fadeOut(200)
      })

      var slcrr = 0;
      var timeoutID;
      var sltotal = jQuery('.slider-item').length;
      var delaytimer = 0;
      
      function showNextItem(){
          if(!jQuery('.at-slider').hasClass('isPlay')){
              jQuery('.at-slider').addClass('isPlay');
              slcrr ++;
              if(slcrr == sltotal)(slcrr = 0)
              showItem(slcrr);
              
          }
      }
      
      jQuery('.at-nav li').click(function(){
          var nitem = jQuery(this).index()
          if(!jQuery('.at-slider').hasClass('isPlay')){
              jQuery('.at-slider').addClass('isPlay');
              
              showItem(nitem);
              jQuery('.at-nav li').removeClass('active');
              jQuery(this).addClass('active');
              
          }
          
      })

      function showItem(id){
          jQuery('.at-slider').addClass('isPlay');
          slcrr = id;
          if(jQuery('.slcrr').length){
              jQuery('.slcrr').fadeOut(1800,function(){
                  jQuery(this).find('.subtext').css('top',0)
              });
          }
          
          jQuery('.slider-item').removeClass('slcrr').eq(id).delay(delaytimer).fadeIn(1000,function(){
               jQuery('.at-nav li').removeClass('active');
               jQuery('.at-nav li').eq(id).addClass('active');
              
              jQuery(this).find('.subtext').each(function(index, val) {
                 jQuery(this).css('display','none').delay(index*300).fadeIn(500)
              })
              jQuery('.at-slider').removeClass('isPlay');
              jQuery('.control-wrap span').html((slcrr +1 )+' of ' + sltotal)
               jQuery('.subtext').hide();
              jQuery(this).addClass('slcrr');
          })

          delaytimer = 100;
          clearTimeout(timeoutID);
          if(sltotal>1){
              timeoutID = setTimeout(function(){showNextItem();} ,7000);
          }
      }

      jQuery('.btn-prev').click(function(){
          if(!jQuery('.at-slider').hasClass('isPlay')){
              jQuery('.at-slider').addClass('isPlay');
              slcrr --;
              if(slcrr == -1)(slcrr = sltotal-1)
              showItem(slcrr);
          }
          return false;
      });

      jQuery('.btn-next').click(function(){
          if(!jQuery('.at-slider').hasClass('isPlay')){
              jQuery('.at-slider').addClass('isPlay');
              slcrr ++;
              if(slcrr == sltotal)(slcrr = 0)
              showItem(slcrr);
          }
          return false;
      });

      //.subtext
      showItem(0);
      
      if(sltotal == 1){
          jQuery('.btn-prev,.btn-next').hide()
      }
      jQuery('.wrap-banner-list').append(jQuery('.banner-list'));

  // SLIDE BANNER TRANG TRONG
  jQuery('.flexslider').flexslider({
        animation: "slide",
        animationLoop: true,
        pauseOnHover: true,
        slideshowSpeed: 7000,
        itemMargin: 5,
        minItems: 2,
  });

  // DESIGN
  jQuery('.dropdown li').click(function(event) {
    var font = jQuery(this).find('span').css('font-family');
    jQuery('#dd > span').css('font-family',font);
  });

  // MOBILE CODE
  jQuery('.btn-mobi-top').click(function(event) {
    jQuery('.top-nav').slideToggle('slow');
  });

  //INKPADS
  jQuery('.option input:radio').click(function(){
    jQuery('.option').removeClass('active');
    jQuery('.option .color_box').addClass('hide');
    jQuery(this).parent('.option').addClass('active');
    jQuery('.option.active').find('.color_box').removeClass('hide');
  })
})

jQuery(window).load(function() {
    if(jQuery(window).width()<767){
      $('.language').insertBefore('nav.top-nav');
    }
});

jQuery(window).resize(function(){
    winresize()
    if(jQuery(window).width()<767){
      $('.language').insertBefore('nav.top-nav');
    }
})


   

