TextLineRow = function(params) {
    this.init(params);
}

jQuery.extend(TextLineRow.prototype, {
    // object variables
    widget_name: '',
    fonts: null,
    params: {size: '12', font: 'arial', afs: '1.2', style: '', underline: '', bold: '', text: 'Type your text', color: '000000', alignment: 'center', top: '0', left: '0', type: 'text', edit: false},
    sizes: null,
    id: '',
    init: function(params) {
        // do initialization here
        this.id = this.row_id;
        this.params.color = color;
        this.params.alignment = alignment;
        if (font != null)
            this.params.font = font;
        if (params != undefined)
            this.params = params;
    },
    createStyleChecked: function(type, value) {
        var id = this.id;
        var params = this.params;
        var temp = this;
        if (value)
            var checked = jQuery("<input/>").attr({/*class: 'fa fa-' + type,*/ type: 'checkbox', id: type + this.id, name: 'data[' + type + '][]', checked: 'checked'});
        else
            var checked = jQuery("<input/>").attr({/*class: 'fa fa-' + type,*/ type: 'checkbox', id: type + this.id, name: 'data[' + type + '][]'});
        //process event click on checkbox style font
        checked.on('change', function() {
            var currentStyle = jQuery('#hidden_style' + id).val();
            if (jQuery(this).is(':checked'))
            {
                jQuery(this).parent().addClass('active');
                if (currentStyle) {
                    if (currentStyle.indexOf(styleObj[type]) == -1) {
                        jQuery('#hidden_style' + id).val(currentStyle + styleObj[type]);
                    }
                } else {
                    currentStyle = styleObj[type];
                    jQuery('#hidden_style' + id).val(currentStyle);
                }
            }
            else
            {
                jQuery(this).parent().removeClass('active');
                if (currentStyle) {
                    currentStyle = currentStyle.sub(styleObj[type], '');
                    jQuery('#hidden_style' + id).val(currentStyle);
                } else {
                    jQuery('#hidden_style' + id).val('');
                }
            }
            temp.updateTemplate();
        });
        return checked;
    },
    createSizeList: function() {
        var id = this.id;
        var combo = jQuery("<select/>").attr({id: 'size' + this.id, name: 'size'});
        var params = this.params;
        var temp = this;
        // set size = active font size
        params.size = activeFontSize;

        jQuery.each(this.sizes, function(i, el) {
            if (params.size == i)
                combo.append("<option value='" + i + "' selected='selected'>" + el + "</option>");
            else {
                combo.append("<option value='" + i + "' >" + el + "</option>");
            }
        });
        // Add more a custom value
        combo.append("<option value='' >" + Translator.translate('Other...') + "</option>");
        combo.on('change', function(e) {
            // Validate
            if (this.value == '') {
                jQuery(this).css('width', '60%');
                jQuery(this).parent().find('.otherSize').val('').show().focus();
            } else {
                jQuery(this).css('width', '95%');
                jQuery(this).parent().find('.otherSize').hide();
                jQuery('#hidden_size' + id).val(jQuery(this).val());
                setTimeout(temp.updateTemplate(), 1000);
            }
        });
        return combo;
    },
    createInputOtherSize: function() {
        var id = this.id;

        var inputOtherSize = jQuery("<input>").attr({type: 'text', class: 'otherSize', style: 'display: none;width:24px;height:22px'});
        inputOtherSize.on('keyup', function() {
            // Validate
            if (!this.value || /^\s*$/.test(this.value)) { // Check not empty
                alert(Translator.translate('Please enter font size'));
                return;
            } else {
                jQuery('#hidden_size' + id).val(this.value);
                updateTemplate();
            }
        });

        return inputOtherSize;
    },
    createDistance2Character: function(left)
    {
        if (!jQuery.isNumeric(left)) {
            left = 0;
        }
        var id = this.id;
        //var input = jQuery("<input>").attr({type: "number", max: 10, min: 0, class: "distance2Ch", id: "distance2Ch" + id, value: left, name: "distance2Ch", onchange: "changeTexboxValue(this.value, 'hidden_left" + id + "')"});
        var input = jQuery("<input>").attr({type: "number", class: "distance2Ch", id: "distance2Ch" + id, value: left, name: "distance2Ch", onchange: "changeTexboxValue(this.value, 'hidden_left" + id + "')"});
        var span = jQuery("<span/>").attr({class: "cover"});
        span.append(jQuery("<span/>").attr({class: "asc", onclick: "plus_minus(this.parentNode.parentNode.getElementsByTagName('input')[0], '+', 1, 200, -200);", onmousedown: "changeBGPosition(this, 'down');", onmouseup: "changeBGPosition(this, 'up');"}));
        span.append(jQuery("<span/>").attr({class: "desc", onclick: "plus_minus(this.parentNode.parentNode.getElementsByTagName('input')[0], '-', 1, 200, -200);", onmousedown: "changeBGPosition(this, 'down');", onmouseup: "changeBGPosition(this, 'up');"}));

        return input.after(span);
    },
    createDistance2Line: function(top)
    {
        if (!jQuery.isNumeric(top)) {
            top = 0;
        }
        var id = this.id;
       // var input = jQuery("<input>").attr({type: "number", max: 10, min: 0, value: top, id: "distance2Li" + id, name: 'distance2Li', onchange: "changeTexboxValue(this.value, 'hidden_top" + id + "')"});
        var input = jQuery("<input>").attr({type: "number", value: top, id: "distance2Li" + id, name: 'distance2Li', onchange: "changeTexboxValue(this.value * -1, 'hidden_top" + id + "')"});
        var span = jQuery("<span/>").attr({class: "cover"});
        span.append(jQuery("<span/>").attr({class: "asc", onclick: "plus_minus(this.parentNode.parentNode.getElementsByTagName('input')[0], '-', 1, 200, -200);", onmousedown: "changeBGPosition(this, 'down');", onmouseup: "changeBGPosition(this, 'up');"}));
        span.append(jQuery("<span/>").attr({class: "desc", onclick: "plus_minus(this.parentNode.parentNode.getElementsByTagName('input')[0], '+', 1, 200, -200);", onmousedown: "changeBGPosition(this, 'down');", onmouseup: "changeBGPosition(this, 'up');"}));
        return input.after(span);
    },
    createFontList: function() {
        var params = this.params;
        temp = this;
        // set front = active font
        if (activeFont != "")
           // params.font = activeFont;
        var id = this.id;
        var combo = jQuery("<select/>").attr({'id': 'font' + this.id, 'name': 'font', 'class': 'font_text'});
        jQuery.each(this.fonts, function(i, el) {
            if (params.font == i)
                combo.append("<option value='" + i + "' selected='selected'>" + el + "</option>");
            else {
                combo.append("<option value='" + i + "'>" + el + "</option>");
            }
        });
        combo.on('change', function() {
            if (jQuery(this).val() != jQuery('#font_main').val() && jQuery('#apply_font').is(':checked')) {
                jQuery('#apply_font').prop("checked", false);
            }
            jQuery('#hidden_font' + id).val(jQuery(this).val());
            setTimeout(temp.updateTemplate(), 1000);
        });
        return combo;
    },
    generateHiddenField: function() {
        var params = this.params;
        if (params.alignment == 'center' && params.edit == false) {
            params.left = (template_width - params.width) / 2;
        }
        return jQuery("<div/>").attr("id", "hidden" + this.id)
                .append(jQuery("<input/>").attr({"type": "hidden", id: "hidden_text" + this.id, name: "data[text][]", value: this.params.text}))
                .append(jQuery("<input/>").attr({"type": "hidden", id: "hidden_font" + this.id, name: "data[font][]", value: this.params.font}))
                .append(jQuery("<input/>").attr({"type": "hidden", id: "hidden_size" + this.id, name: "data[size][]", value: this.params.size}))
                .append(jQuery("<input/>").attr({"type": "hidden", id: "hidden_type" + this.id, name: "data[type][]", value: this.params.type}))
                .append(jQuery("<input/>").attr({"type": "hidden", id: "hidden_left" + this.id, name: "data[left][]", value: this.params.left}))
                .append(jQuery("<input/>").attr({"type": "hidden", id: "hidden_top" + this.id, name: "data[top][]", value: this.params.top}))
                .append(jQuery("<input/>").attr({"type": "hidden", id: "hidden_style" + this.id, name: "data[style][]", value: this.params.style}))
                .append(jQuery("<input/>").attr({"type": "hidden", id: "hidden_color" + this.id, name: "data[color][]", value: this.params.color}))
                .append(jQuery("<input/>").attr({"type": "hidden", id: "hidden_alignment" + this.id, name: "data[alignment][]", value: this.params.alignment}));
    },
    createAlginmentButton: function(align) {
        var temp = this;
        var params = this.params;
        var active = '';
        if (params.alignment == align) {
            active = 'active';
        }
        return jQuery("<a/>").attr({'id': align + "align" + this.id, 'href': "javascript:void(0)", class: align + "align"})
                .append(jQuery("<i/>").attr({'class': "fa " + "fa-align-" + align})
                        .on('click', function() {
                            if (jQuery('#apply_align').is(':checked')) {
                                if (jQuery('.alginmentheader .alignment.active').hasClass(align) == false) {
                                    jQuery('#apply_align').prop("checked", false);
                                } else if (jQuery(this).hasClass('active')) {
                                    jQuery('#apply_align').prop("checked", false);
                                }
                            }

                            if (jQuery(this).hasClass('active')) {
                                jQuery(this).removeClass('active');
                                objRow[temp.id].params.alignment = undefined;
                                jQuery('#hidden_alignment' + temp.id).val('');
                            } else {
                                jQuery(this).closest("div").find(".alignment").removeClass('active');
                                jQuery(this).addClass('active');
                                objRow[temp.id].params.alignment = align;
                                jQuery('#hidden_alignment' + temp.id).val(align);
                                temp.updateTemplate();
                            }
                        })
                        )
    },
    updateTemplate: function() {
        var values = jQuery('#frmtemplate').serialize();
        jQuery.ajax({
            url: previewUrl,
            type: "post",
            data: values,
            dataType: "json",
            beforeSend: function() {
                jQuery("#template_image").attr('src', loadingImgUrl);
                jQuery("#img_preview").css({'background': 'none'});
            },
            success: function(objJson) {
                if (objJson.status == 'success') {
                    if (objJson.url != '') {
//                        var src = websiteURL+'media/tmp/textplate/'+objJson.file + '.png';
                        var template_id = jQuery('.box_choose .template.active').attr('id');
                        var src = websiteURL + 'text2image/image/template?template_id=' + template_id + '&data=' + objJson.url;
                        jQuery("#template_image").attr('src', src);
                    }
                }
            }
        });
    },
    generateEditorRow: function() {
        // an example object method
        var id = this.id;
        var temp = this;
        var bold = '';
        var underline = '';
        var italic = '';
        if (this.params.style.indexOf('b') != -1) {
            this.params.bold = 1;
            var bold = ' active';
        }
        if (this.params.style.indexOf('u') != -1) {
            this.params.underline = 1;
            var underline = ' active';
        }
        if (this.params.style.indexOf('i') != -1) {
            this.params.italic = 1;
            var italic = ' active';
        }
        return jQuery("<div/>").attr("class", "item-editer")
                .append(
                        jQuery("<div/>").attr("class", "textColumn")
                        .append(
                                jQuery("<div/>").attr("class", "deleteColumn")
                                .append(jQuery("<a/>").attr({'href': 'javascript:void(0)', 'id': 'remove' + this.id, 'title': 'Delete'})
                                        .bind('click', function() {
                                            jQuery(this).parent().parent().parent().remove();
                                            jQuery('#txt' + id).remove();
                                            jQuery('#hidden' + id).remove();
                                            updateTemplate();
                                            //edit by Hung Tran add action append button add new if max line > display line
                                            if (jQuery('.item-editer').length < jQuery('#maxline_textplate').val()) {
                                                jQuery('.editer-wrap-title .maximum-notice').remove();
                                                jQuery('.editer-wrap-title button.new_line').show();
                                               
                                            }
                                        }))
                                )
                        .append(
                                jQuery("<div/>").attr("class", "textinputColumn")
                                .append(jQuery("<input/>").attr({placeholder: 'Enter text here...', type: 'text', id: 'text' + this.id, name: 'text', value: this.params.text, tabindex: this.id + 1})
                                        .bind('keyup', function() {
                                            jQuery('#hidden_text' + id).val(jQuery(this).val());
                                            updateTemplate();
                                        })
                                        .bind('click', function() {
                                            jQuery(this).select();
                                        })
                                        )
                                )
                        .append(
                                jQuery("<div/>").attr("class", "clear")
                                )
                        )
                .append(jQuery("<div/>").attr("class", "applyfontstamp")
                        .append(this.createFontList()))
                .append(jQuery("<div/>").attr("class", "applyColumn fontsizeColmn")
                        .append(this.createSizeList())
                        .append(this.createInputOtherSize()))
                .append(jQuery("<div/>").attr("class", "styleColumn")
                        .append(jQuery("<span/>").attr("class", "fa fa-bold").append(this.createStyleChecked('bold', this.params.bold)))
                        .append(jQuery("<span/>").attr("class", "fa fa-italic").append(this.createStyleChecked('italic', this.params.italic)))
                        .append(jQuery("<span/>").attr("class", "fa fa-underline").append(this.createStyleChecked('underline', this.params.underline))))
                .append(jQuery("<div/>").attr("class", "distance2ChHeader")
                        .append(this.createDistance2Character(this.params.left)))
                .append(jQuery("<div/>").attr("class", "distance2LiHeader")
                        .append(this.createDistance2Line(this.params.top)))
                .append(jQuery("<div/>").attr({'class': 'alginmentheader'})
                        .append(this.createAlginmentButton('left'))
                        .append(this.createAlginmentButton('center'))
                        .append(this.createAlginmentButton('right')))
                .append(jQuery("<div/>").attr({'class': 'clear'}));
    }
});