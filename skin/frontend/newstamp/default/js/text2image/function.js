function changeBGPosition(obj, event)
{
    if (event == 'down') {
        if (obj.className == 'asc') {
            jQuery(obj).css('background-position', '40% 70%');
        } else if (obj.className == 'desc') {
            jQuery(obj).css('background-position', '40% 50%');
        } else if (obj.className == 'left') {
            jQuery(obj).css('background-position', '30% 45%');
        } else if (obj.className == 'right') {
            jQuery(obj).css('background-position', '65% 50%');
        }
    }
    else {
        if (obj.className == 'asc') {
            jQuery(obj).css('background-position', '40% 50%');
        } else if (obj.className == 'desc') {
            jQuery(obj).css('background-position', '40% 30%');
        } else if (obj.className == 'left') {
            jQuery(obj).css('background-position', '45% 45%');
        }
        else if (obj.className == 'right') {
            jQuery(obj).css('background-position', '50% 45%');
        }
    }

}
function changeBorderWidth(obj) {
    jQuery('#border_width').val(obj.value);
    updateTemplate();
}
function usePDF(temp) {
    jQuery('.box_choose .template').removeClass('active');
    if( jQuery('div.item-editer').length ){ 
        jQuery('div.item-editer').remove();        
    }
    jQuery('.text_design .item-editer').remove();
    if (temp) {
        var id = temp.attr('id').replace('imgUploadIcon', '');
        jQuery('#imageToolsCover').find('div[id^="imageTools"]').each(function(i, obj) {
            if (jQuery(obj).attr('id').replace('imageTools', '') != id) {
                jQuery(obj).remove();
            }
        });
        var temp = jQuery('#hidden_field' + ' #hidden' + id);
        jQuery('#hidden_field').find('div[id^="hidden"]').remove();
        jQuery('#hidden_field').append(temp);    

    } else {
        jQuery('#imageToolsCover').html('');
        jQuery('#hidden_field').find('div[id^="hidden"]').remove();

        //clear border
        jQuery('#has_border').val(0);
        jQuery('#border_style').val('');
        
        updateTemplate();
    }
}
var xhrQueue = [];
var xhrCount = 0;

function updateTemplate() {
    xhrQueue.push(xhrCount);
    setTimeout(function() {
        xhrCount = ++xhrCount;
        if (xhrCount === xhrQueue.length) {
            var values = jQuery('#frmtemplate').serialize();
            jQuery.ajax({
                url: previewUrl,
                type: "post",
                data: values,
                dataType: "json",
                beforeSend: function() {
                    jQuery("#template_image").attr('src', loadingImgUrl);
                    jQuery("#img_preview").css({'background': 'none'});
                },
                success: function(objJson) {
                    if (objJson.status == 'success') {
                        if (objJson.url != '') {
                            var template_id = jQuery('.box_choose .template.active').attr('id');
                            var src = websiteURL + 'text2image/image/template?template_id=' + template_id + '&data=' + objJson.url;
                            jQuery("#template_image").attr('src', src);
                        }
                    }
                }
            });
        }
    }, 600);
}


function addBorderToTextplate(obj) {
    if (jQuery(obj).is(':checked')) {
        jQuery('#has_border').val(1);
        if (jQuery('#border_style').val() != '') {
            updateTemplate();
        }
    } else {
        jQuery('#has_border').val(0);
        updateTemplate();
    }
}

function plus_minus(obj, sign, jumb, maxValue, minValue)
{
    var result;
    var val = parseFloat(obj.value);

    // Validate
    if ((val >= maxValue && sign == '+') || (val <= minValue && sign == '-'))
        return;

    // Processing
    if (sign == '+') {
        result = val + jumb;
    } else {
        result = val - jumb;
    }

    // Return
    obj.value = result.toFixed(1);
    if (jQuery(obj).attr('id') == 'frameThickness') {
        jQuery('#border_width').val(jQuery(obj).val());
    }
    if (jQuery(obj).attr('name') == 'demension') {
        var id = jQuery(obj).attr('id').replace('demension', '');
        jQuery('#hidden_size' + id).val(jQuery(obj).val());
    } else if (jQuery(obj).attr('name') == 'left') {
        var id = jQuery(obj).attr('id').replace('left', '');
        jQuery('#hidden_left' + id).val(jQuery(obj).val());
    } else if (jQuery(obj).attr('name') == 'top') {
        var id = jQuery(obj).attr('id').replace('top', '');
        jQuery('#hidden_top' + id).val(jQuery(obj).val());
    } else if (jQuery(obj).attr('name') == 'lineHeight') {
        jQuery('#line_height').val(jQuery(obj).val());
    } else if (jQuery(obj).attr('name') == 'distance2Ch') {
        var id = jQuery(obj).attr('id').replace('distance2Ch', '');
        jQuery('#hidden_left' + id).val(jQuery(obj).val());
    }
    else if (jQuery(obj).attr('name') == 'distance2Li') {
        var id = jQuery(obj).attr('id').replace('distance2Li', '');
        jQuery('#hidden_top' + id).val(jQuery(obj).val());
    } else if (jQuery(obj).attr('name') == 'distance2LiOval') {
        var id = jQuery(obj).attr('id').replace('distance2LiOval', '');
        jQuery('#hidden_left_' + id).val(jQuery(obj).val());
    }
    updateTemplate();
}

function chooseFileUpload(loadingImgUrl, obj)
{
    var src = jQuery('#img_uploaded').attr('src');
    var size = 128;
    if (src != loadingImgUrl) {
        var arrtemp = src.split('/');
        var filename = arrtemp[arrtemp.length - 1];
        var left = 0;
        var top = 0;
        var id = i;
        filename = filename.replace('.png', '');
        var thumb = baseUrl + 'text2image/image/iconthumb/' + size + '/' + filename;
        if (jQuery.isNumeric(obj)) {
            console.log(jQuery('#shape').val());
            if (jQuery('#shape').val() == 'Circle' || jQuery('#shape').val() == 'Oval') {
                var alignment = 'centermiddle';
            } else {
                var alignment = 'lefttop';
                if (imgUploaded.width > imgUploaded.height) {
                    var alignment = 'centertop';
                }
            }
            var objimage = {style: '', text: filename, type: 'image', alignment: alignment, top: top, left: left, edit: false, size: iconSize, src: src};
            var objRow = new TextLineRow(objimage);
            objRow.id = id;//add row id
            if (typeof jQuery('#hidden_text' + id).val() != 'undefined') {
                jQuery('#hidden_text' + id).val(filename);
            } else {
                var hiddenField = objRow.generateHiddenField();
                jQuery('#hidden_field').append(hiddenField);
                addNewImage(objimage);
            }
        } else {
            var id = jQuery(obj).attr('id').replace('imgUploadIcon', '');
            jQuery('#hidden_text' + id).val(filename);
            /*if(imgUploaded.width > imgUploaded.height){
             jQuery('#imageToolsCover #positions'+id+' .block.selected').removeClass('selected');
             jQuery('#imageToolsCover #positions'+id+' .block.centertop').addClass('selected');
             jQuery('#hidden_alignment'+id).val('centertop');
             }else{
             jQuery('#imageToolsCover #positions'+id+' .block.selected').removeClass('selected');
             jQuery('#imageToolsCover #positions'+id+' .block.lefttop').addClass('selected');
             jQuery('#hidden_alignment'+id).val('lefttop');
             }
             jQuery('#hidden_size'+id).val(imgUploaded.width);*/
             console.log(obj.closest(".edit-image"));
             obj.closest(".edit-image").find('img').attr("src", thumb);
         }

         jQuery("#dialog-confirm").dialog("close");
         updateTemplate();
     }
 }

 function addNewImage(element)
 {
    var uploadButton = generateuploadButton(element);
    var tools = generateImageTools(element);
    var position = generateImagePosition(element);
    jQuery("#imageToolsCover").append(
        jQuery('<div/>').attr({class: "content-edit", id: "imageTool" + i})
        .append(uploadButton)
        .append(tools)
        .append(position)
        .append(jQuery('<span/>').attr({class: "removeIcon", id:"removeIcon"+i})
            .html('&nbsp;')
            .bind("click",function(){
                var id = jQuery(this).attr('id').replace('removeIcon','');
                jQuery(this).parent().remove();
                jQuery("#hidden"+id).remove();
                updateTemplate();
            })
        )
        );
    i++;
}

function generateuploadButton(element)
{   
    var thumb = baseUrl + 'text2image/image/iconthumb/128/' + element.text;
    return jQuery("<div/>").attr({class: "edit-image"})
    .append(
        jQuery("<span/>").attr({class: "simg"})
        .append(jQuery("<img/>").attr({src: thumb, width: '100px'}))
        )
    .append(
        jQuery("<div/>").attr({class: "upload bottoncss3_1 rounded fileUpload btn btn-primary"})
        .append(
            jQuery("<form/>").attr({enctype: "multipart/form-data", method: "post", action: baseUrl + "text2image/ajax/upload?textplate_id=" + textplate_id})
            .append(jQuery("<input/>")
                .attr({type: 'file', id: "imgUploadIcon" + i, class: "upload", name: "uploadfile"})
                .bind("change", function() {
                    var temp = jQuery(this);
                    var options = {
                                            //target:     '#uploadOutput',
                                            beforeSubmit: function(arr, $form, options) {
                                                jQuery('#img_uploaded').attr('src', loadingImgUrl);
                                                jQuery("#dialog-confirm").dialog({
                                                    resizable: true,
                                                    height: "auto",
                                                    modal: true,
                                                    zIndex: 400,
                                                    buttons: [
                                                    {
                                                        'text': Translator.translate('Choose'),
                                                        'class': "choose-botton",
                                                        'click': function() {
                                                            jQuery(this).css('z-index', 0);
                                                            jQuery("#dialogConfirmClearText").dialog({
                                                                resizable: false,
                                                                width: 350,
                                                                modal: true,
                                                                buttons: [
                                                                {
                                                                    'text': Translator.translate('Yes'),
                                                                    'click': function() {
                                                                        usePDF(temp);
                                                                        chooseFileUpload(loadingImgUrl, temp);
                                                                        jQuery(this).dialog("close");
                                                                    }
                                                                },
                                                                {
                                                                    'text': Translator.translate('No'),
                                                                    'click': function() {
                                                                        chooseFileUpload(loadingImgUrl, temp);
                                                                        jQuery(this).dialog("close");
                                                                    }
                                                                }
                                                                ]
                                                            });
}
},
{
    'text': Translator.translate('Cancel'),
    'click': function() {
        jQuery(this).dialog("close");
    }
}
]
});
},
success: function(responseText, statusText, xhr, $form) {
    imgUploaded = jQuery.parseJSON(responseText);
    var size = template_width > template_height ? template_width : template_height;
    var src = baseUrl + 'text2image/image/thumbnail/' + size + '/' + imgUploaded.file;

    if (imgUploaded.status == 'success') {
        jQuery('#img_uploaded').attr('src', src);
        temp.closest(".uploadButton").next().find('.demension').first().val(imgUploaded.width);

        if (jQuery('#shape').val() == 'Circle') {
            temp.closest(".uploadButton").next().find('.positions .block').removeClass('selected');
            temp.closest(".uploadButton").next().find('.block.centermiddle').first().addClass('selected');
        } else {
            if (imgUploaded.width > imgUploaded.height) {
                temp.closest(".uploadButton").next().find('.positions .block').removeClass('selected');
                temp.closest(".uploadButton").next().find('.block.centertop').first().addClass('selected');
            }
        }
    } else {

    }
}
};
jQuery(this).closest("form").ajaxForm(options);
jQuery(this).closest("form").submit();
}))
.append(Translator.translate('Change image/PDF'))
)
)
}

function frameType(type, obj) {

    jQuery(obj).parent().addClass('selected');
    if (type == 'NONE') {
        jQuery('#has_border').val(0);
    } else {
        jQuery('#has_border').val(1);
    }
    jQuery('#border_style').val(type);
    updateTemplate();
}

function iconPosition(obj, position) {
    obj.parent().children().each(function(i, o) {
        jQuery(o).removeClass("selected");
    });
    obj.addClass("selected");
    var id = obj.parent().attr("id").replace("positions", "");
    jQuery("#hidden_alignment" + id).val(position);
    if (typeof jQuery("#hidden_text" + id).val() != "undefined" && !jQuery("#hidden_style" + id).val() != "background") {
        updateTemplate();
    }
}
function generateImagePosition(element)
{
    return	jQuery("<div/>").attr({class: "move-wrap"})
    .append(jQuery('<div/>').attr({class: "frm-row"})
        .append('<label for="">Move image/pdf left or right</label>')
        .append(jQuery('<input/>').attr({type: "number", id: "left" + i})
            .bind("change", function() {
                var id = jQuery(this).attr("id").replace("left", "");
                jQuery("#hidden_left" + id).val(jQuery(this).val());
                updateTemplate();
            }))

    )

    .append(
        jQuery('<div/>').attr({class: "frm-row"})
        .append('<label for="">Move image/pdf up or down</label>')        
        .append(jQuery('<input/>').attr({name:"top", type:"number", id:"top"+i, value:top,name:"top"})
            .bind("change", function() {                
                var id = jQuery(this).attr("id").replace("top", "");
                jQuery("#hidden_top" + id).val(jQuery(this).val()* -1);
                updateTemplate();
            })
            )
            .append(
                jQuery("<span/>").attr({class: "cover"})        
                  .append(jQuery("<span/>").attr({class: "asc", onclick: "plus_minus(this.parentNode.parentNode.getElementsByTagName('input')[0], '-', 1, 100, -100);", onmousedown: "changeBGPosition(this, 'down');", onmouseup: "changeBGPosition(this, 'up');"}))
                    .append(jQuery("<span/>").attr({class: "desc", onclick: "plus_minus(this.parentNode.parentNode.getElementsByTagName('input')[0], '+', 1, 100, -100);", onmousedown: "changeBGPosition(this, 'down');", onmouseup: "changeBGPosition(this, 'up');"}))
            )
     )


    .append(
            jQuery("<div/>").attr({class: "frm-row"})
             .append(
            jQuery("<label/>").attr({style: "float: left; margin-right: 5px;", for:"demension"+i}).html(Translator.translate("Change image/pdf dimension") + "&nbsp;")
            )      
            .append(
                jQuery("<span/>").attr({class: "bgCBB"})        
                .append(jQuery("<input/>").attr({type:"number",name:"demension", id:"demension"+i, value:element.size})
                    .bind("change",function(){
                      var id = jQuery(this).attr("id").replace("demension","");
                      jQuery("#hidden_size" + id).val(jQuery(this).val());
                      updateTemplate();
                  })
                    )
                .append(
                    jQuery("<span/>").attr({class: "cover"})        
                    .append(jQuery("<span/>").attr({class: "asc", onclick: "plus_minus(this.parentNode.parentNode.getElementsByTagName('input')[0], '+', 1, 100, -100);", onmousedown: "changeBGPosition(this, 'down');", onmouseup: "changeBGPosition(this, 'up');"}))
                    .append(jQuery("<span/>").attr({class: "desc", onclick: "plus_minus(this.parentNode.parentNode.getElementsByTagName('input')[0], '-', 1, 100, -100);", onmousedown: "changeBGPosition(this, 'down');", onmouseup: "changeBGPosition(this, 'up');"}))
                    )
                )
        )

    .append(
        jQuery("<div/>").attr({class: "frm-row frm-checkbox"})
        .append(
            jQuery("<input/>").attr({"id":"doBackground"+i, type:"checkbox", name:"doBackground",class:"pull-right checkupload"})
            .bind('click',function(){
                var id = jQuery(this).attr("id").replace("doBackground", "");
                if(jQuery(this).is(':checked')){
                  jQuery('#hidden_style'+id).val("background");
              }else{
                  jQuery('#hidden_style'+id).val("");
              }
              if(typeof jQuery('#hidden_text'+id).val() != "undefined"){
                  updateTemplate();
              }
          })
            )
        .append(
            jQuery("<label/>").attr({for:"doBackground"+i}).html("&nbsp;" + Translator.translate('Use this image as background'))
            )
        .append('<div class="separator1">&nbsp;</div>')
                                
        );

}
function generateImageTools(element)
{
    var lefttop = "";
    var centertop = "";
    var righttop = "";
    var leftmiddle = "";
    var centermiddle = "";
    var rightmiddle = "";
    var leftbottom = "";
    var centerbottom = "";
    var rightbottom = "";
    if (element) {
        var imgFile = element.text;
        imgFile = baseUrl + "text2image/image/iconthumb/70/" + imgFile;
        var size = element.size;
        var alignment = element.alignment;
        var left = element.left;
        var top = element.top;
    } else {
        imgFile = skinUrl + "images/text2image/no-image.png";
        var size = "";
        var alignment = "lefttop";
        var left = "0";
        var top = "0";
    }
    switch (alignment) {
        case "lefttop":
        lefttop = "selected";
        break;
        case "centertop":
        centertop = "selected";
        break;
        case "righttop":
        righttop = "selected";
        break;
        case "leftmiddle":
        leftmiddle = "selected";
        break;
        case "centermiddle":
        centermiddle = "selected";
        break;
        case "rightmiddle":
        lefttop = "selected";
        break;
        case "leftbottom":
        leftbottom = "selected";
        break;
        case "centerbottom":
        centerbottom = "selected";
        break;
        case "rightbottom":
        rightbottom = "selected";
        break;
    }
    return jQuery("<div/>").attr({class: "direction-wrap", id: "positions"+i})
    .append(jQuery('<span/>').attr({class: "direction dirleftop " + lefttop})
        .html('&nbsp;')
        .bind('click', function() {
            iconPosition(jQuery(this), 'lefttop');
        }))
    .append(jQuery('<span/>').attr({class: "direction dircentertop " + centertop})
        .html('&nbsp;')
        .bind('click', function() {
            iconPosition(jQuery(this), 'centertop');
        }))
    .append(jQuery('<span/>').attr({class: "direction dirrighttop " + righttop})
        .html('&nbsp;')
        .bind('click', function() {
            iconPosition(jQuery(this), 'righttop');
        }))
    .append(jQuery('<span/>').attr({class: "direction dirlefmiddle " + leftmiddle})
        .html('&nbsp;')
        .bind('click', function() {
            iconPosition(jQuery(this), 'leftmiddle');
        }))
    .append(jQuery('<span/>').attr({class: "direction dircentermiddle " + centermiddle})
        .html('&nbsp;')
        .bind('click', function() {
            iconPosition(jQuery(this), 'centermiddle');
        }))
    .append(jQuery('<span/>').attr({class: "direction dirrightmiddle " + rightmiddle})
        .html('&nbsp;')
        .bind('click', function() {
            iconPosition(jQuery(this), 'rightmiddle');
        }))
    .append(jQuery('<span/>').attr({class: "direction dirlefbottom " + leftbottom})
        .html('&nbsp;')
        .bind('click', function() {
            iconPosition(jQuery(this), 'leftbottom');
        }))
    .append(jQuery('<span/>').attr({class: "direction dircenterbottom " + centerbottom})
        .html('&nbsp;')
        .bind('click', function() {
            iconPosition(jQuery(this), 'centerbottom');
        }))
    .append(jQuery('<span/>').attr({class: "direction dirrightbottom " + rightbottom})
        .html('&nbsp;')
        .bind('click', function() {
            iconPosition(jQuery(this), 'rightbottom');
        }));

}

function chooseInkpadColor(obj) {
    jQuery('#inkpad_color').val(jQuery(obj).attr('color'));
    jQuery('#inkpad_id').val(jQuery(obj).attr('inkpad'));
    updateTemplate();
}

function changeTexboxValue(value, hiddenFieldId)
{
    jQuery("#" + hiddenFieldId).val(value);
    if (hiddenFieldId == 'border_width') {
        if (jQuery('#addBorder').is(':checked')) {
            updateTemplate();
        }
    } else {
        updateTemplate();
    }
}
function DropDown(el) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.dropdown > li');
    this.val = '';
    this.index = -1;
    this.initEvents();
}
DropDown.prototype = {
    initEvents: function() {
        var obj = this;

        obj.dd.on('click', function(event) {
            jQuery(this).toggleClass('active');
            return false;
        });

        obj.opts.on('click', function() {
            var opt = jQuery(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.val);
        });
    },
    getValue: function() {
        return this.val;
    },
    getIndex: function() {
        return this.index;
    }
}