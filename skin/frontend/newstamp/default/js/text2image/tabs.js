jQuery(document).ready(function(){
    jQuery('#t2iTabbed li.tab').click(function(){
        if(jQuery(this).attr('class')=='tab active'){
            return;
        }

        jQuery('li.active').removeClass('active');
        jQuery(this).addClass('active');              
        jQuery('.tabContent').hide();
        var content_show = jQuery(this).attr('title');
        jQuery('#'+ content_show).show();
    });
});