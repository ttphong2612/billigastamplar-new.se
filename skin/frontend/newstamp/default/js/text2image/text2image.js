var rate_px_mm = 3.77952775905;
var i = 0;
var colorObj = {red:'FF0000',blue:'0000FF',black:'000000'};
var styleObj = {bold:'b',italic:'i',underline:'u'};
var color = colorObj.black;
var alignment = 'center';
var font = null;
var objRow = new Array();
var iconSize = 0;
function createColorBox(color, inkpad_id){
  return jQuery('<span/>').attr({ color: color, inkpad: inkpad_id})
  			.append(jQuery('<input/>').attr({type: 'radio', id:'radio-'+ inkpad_id, class: "regular-radio", name:'radio-inkpad'}))
  			.append(jQuery('<label/>').attr({for: 'radio-'+ inkpad_id, class: color}).append(jQuery('<span/>')))
            .on('click',function(){
              chooseInkpadColor(this);
            });
}
jQuery(document).ready(function() {
    jQuery('#font_main').on('change',function(){
      if(jQuery('#apply_font').is(':checked')){
        jQuery('.font_text').each(function(){
            font = jQuery('#font_main').val();
            jQuery(this).val(font);
            var id = jQuery(this).attr('id').replace('font','');
            jQuery('#hidden_font'+id).val(font);
        });
        updateTemplate();
      }
    });
    jQuery('#apply_font').on('click',function(){
    	if(jQuery(this).is(':checked')){
    		jQuery('.font_text').each(function(){
    			font = jQuery('#font_main').val();
    			jQuery(this).val(font);
    			var id = jQuery(this).attr('id').replace('font','');
    			jQuery('#hidden_font'+id).val(font);
    		});
    		updateTemplate();
    	}
    });
	
jQuery('.textplate').click(function() {
	var data = {};
	if(jQuery(this).attr('data') != ''){
	    var data = jQuery.parseJSON(jQuery(this).attr('data'));
	}
	jQuery('#product_id').val(jQuery(this).attr('product'));
	/*phong.tran*/
	jQuery('#shape').val(jQuery(this).attr('shape'));
	if (data !== null) {
            if (data.inkpad_id !== undefined) {
                jQuery('#inkpadColor').html('');
				var currentColor = jQuery('#inkpad_color').val();
                for (var temp in data.inkpad_id) {
                    if (temp == currentColor) {
                        jQuery('#inkpad_id').val(data.inkpad_id[temp]);
                    }
					jQuery('#inkpadColor').append(createColorBox(temp, data.inkpad_id[temp]));
                }
            }
        }
	jQuery('.box_choose .textplate').removeClass('active');
	jQuery(this).addClass('active');
	var size = jQuery(this).attr('size');
	var shape = jQuery(this).attr('shape');
	size = jQuery.parseJSON(size);
	var notice_maxline = Translator.translate('This template has maximum %s lines');
	notice_maxline = notice_maxline.replace('%s',size.max );
	jQuery('#notice_textplate').html(notice_maxline);
	jQuery('#maxline_textplate').val(size.max); 
	/*console.log(this);*/
	jQuery.ajax({
  		url: baseUrl + 'text2image/ajax/getTemplate?textplate_id=' + jQuery(this).attr('id') + '&productId=' + jQuery(this).attr('product') + '&data=' + textplateDesigned,
  		dataType: "html",
  		success: function(data) { 
  			template_width = size.w; 
  			template_height = size.h;
  			jQuery('#template_width').val(template_width);
  			jQuery('#template_height').val(template_height);
  			jQuery('ul#texplate_list').html(data);
  		}
	})
});
//add new text editor line
jQuery('.new_line').on('click',function(){
   	var datefield = jQuery('.template.active').attr('datefield');
   	if (datefield == 1) {
            if (jQuery('.text_design_input').length >= (parseInt(jQuery('#maxline_textplate').val()))) {
                alert(Translator.translate('Maximum %s lines up and %s lines down').replace(/%s/g, parseInt(jQuery('#maxline_textplate').val() / 2)));
                return false;
            }
        }
//        else{
//            console.log(jQuery('.item-editer').length, jQuery('#maxline_textplate').val() );
//		if(jQuery('.item-editer').length >= jQuery('#maxline_textplate').val() ){
//			alert(Translator.translate('Maximum %s lines').replace(/%s/g, jQuery('#maxline_textplate').val()));
//			return false;
//		}
//   	}
   	objRow[i] = new TextLineRow();
   	objRow[i].fonts = jsonFont;
   	objRow[i].sizes = jsonSize; 
   	objRow[i].id = i;//add row id
   	var editorLine = objRow[i].generateEditorRow();
   	var hiddenField = objRow[i].generateHiddenField();
   	jQuery('#text-design').append(editorLine);
   	jQuery('#hidden_field').append(hiddenField);
   	objRow[i].updateTemplate();
   	i++;
        if (jQuery('.item-editer').length >= jQuery('#maxline_textplate').val()) {
            jQuery('.editer-wrap-title button.new_line').hide();
            jQuery('.editer-wrap-title').append('<p class="maximum-notice">' + Translator.translate('Maximum %s lines').replace(/%s/g, jQuery('#maxline_textplate').val()) + '</p>');
        }
});
	
jQuery('.choosecolor div .color').on('click',function(){
  jQuery('.choosecolor div').removeClass('active');
  jQuery(this).parent().addClass('active');
  jQuery('#inkpad_id').val();
  var colorSelected = jQuery(this).attr('id').split('-')[1];
  color = colorObj[colorSelected];
  
  jQuery('.text_image img').each(function(obj){
    var src = jQuery(this).attr('src');
    src = src.sub(baseUrl, '');
        var arr = src.split('/');
        arr[6] = color;
        srcURL = baseUrl + arr.join('/');
        jQuery(this).attr('src',srcURL);
  });
  jQuery("input[id^='hidden_color']").each(function(){ 
      jQuery(this).val(color);
  });
});
	
jQuery('.upload').change(function(){
  jQuery(this.form).submit();
  jQuery(this).val("");
});

var options = {
  //target:     '#uploadOutput',
  beforeSubmit: function(arr, $form, options){
    jQuery('#img_uploaded').attr('src',loadingImgUrl);
    jQuery( "#dialog-confirm" ).dialog({
      resizable: true,
      height: "auto",
      modal: true, 
      zIndex: 400,
      buttons: [
        {
          'text': Translator.translate('Choose'),
          'class': "choose-botton",
          'click': function() {
            jQuery(this).css('z-index', 0);   
            jQuery( "#dialogConfirmClearText" ).dialog({
              resizable: false,
              width: 350,
              modal: true,
              buttons: [
                {
                  'text': Translator.translate('Yes'),
                  'click': function() {
                    usePDF();
                    chooseFileUpload(loadingImgUrl, i);
                    jQuery( this ).dialog( "close" );
                  }
                },
                {
                  'text': Translator.translate('No'),
                  'click': function() {
                    chooseFileUpload(loadingImgUrl, i);
                    jQuery( this ).dialog( "close" );
                  }
                }
              ]
            });
          }
        },
        {
          'text': Translator.translate('Cancel'),
          'click': function() {
            jQuery( this ).dialog( "close" );
          }
        }
      ]
    });
  },
  success: function(responseText, statusText, xhr, $form) {
    imgUploaded = jQuery.parseJSON(responseText);
    var size = template_width > template_height ? template_width : template_height;
    var src = baseUrl+'text2image/image/thumbnail/'+size+'/'+imgUploaded.file;
    if(imgUploaded.status == 'success'){
      iconSize = imgUploaded.width;
      jQuery('#img_uploaded').attr('src', src);
    }
  }
};
	
	
jQuery( "#zoomTemplate" ).draggable({ axis: "x",
   containment: "#zoomFirst", 
   scroll: false, 
   stop: function( event, ui ) {
       left = ui.position.left;
       percent = parseInt(left*25/12);
       jQuery( "#zoom_size" ).val(percent);
       jQuery('#zoomRate').html(percent + 100);
       updateTemplate();
   },
   onmove: function (event) {
       x += event.dx;
       y += event.dy;
       event.target.style.webkitTransform =
       event.target.style.transform =
           'translate(' + x + 'px, ' + y + 'px)';
   }
});
  //check auto width
  jQuery('#autoWidth').on('click',function(){
    if(jQuery(this).attr('checked')) {
      jQuery("#auto_width").val(1);
    } else {
      jQuery("#auto_width").val(0);
    }
  });
  
  //check auto height
  jQuery('#autoSize').on('click',function(){   
    if(jQuery(this).is(':checked')) {
      jQuery("#auto_size").val(1);
    }else{
      jQuery("#auto_size").val(0);
    }
    updateTemplate();
  });
  jQuery('.submitButton').on('click',function(){
    jQuery('#frmtemplate').submit();
  });
  jQuery('.textplate:first').trigger('click');
	jQuery('.frm_upload').ajaxForm(options);
});
function otherSizeKeyup(obj, position){
    // Validate
    if( !obj.value || /^\s*$/.test(obj.value) ){ // Check not empty
        alert(Translator.translate('Please enter font size'));
        return;
    }else{
        jQuery('#hidden_size_' + position).val(obj.value);   
        updateTemplate();
    }
} 

function fontChange(obj,position){
    jQuery('#hidden_font_'+position).val(jQuery(obj).val());
    updateTemplate();
}

function fontSizeChange(obj,position){
    // Validate
    if( obj.value == '' ){
        jQuery(obj).css('width', '60%');
        jQuery(obj).parent().find('.otherSize').val('').show().focus();
    }else{
        jQuery(obj).css('width', '95%');
        jQuery(obj).parent().find('.otherSize').hide();
        jQuery('#hidden_size_' + position).val(jQuery(obj).val());
        updateTemplate();
    }
}
function styleChange(obj,type,position){
    var currentStyle = jQuery('#hidden_style_'+position).val();
    if(jQuery(obj).is(':checked'))
    {
      jQuery(obj).parent().addClass('active');
      if(currentStyle){ 
        if(currentStyle.indexOf(styleObj[type]) == -1){
          jQuery('#hidden_style_'+position).val(currentStyle + styleObj[type]) ;
        }
      }else{
        currentStyle = styleObj[type];
          jQuery('#hidden_style_'+position).val(currentStyle);
      }
    }
    else
    {
      jQuery(obj).parent().removeClass('active');
      if(currentStyle){
        currentStyle = currentStyle.sub(styleObj[type], '');
        jQuery('#hidden_style_'+position).val(currentStyle) ;
      }else{
        jQuery('#hidden_style_'+position).val('') ;
      }
    }
    updateTemplate();
 }
function textChange(obj,position){  
    jQuery('#hidden_text_' +position).val(jQuery(obj).val());
          updateTemplate();
  }

