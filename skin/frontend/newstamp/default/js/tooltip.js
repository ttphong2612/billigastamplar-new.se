jQuery(document).ready(function() {
	jQuery('#questions li > a').hover(function() {
			jQuery(this)
			.parent('li')
			.find('div.tooltip')
			.show()
			.stop()
			.animate({opacity: 1}, 'fast');
		}, function() {
			jQuery(this)
			.parent()
			.find('div.tooltip')
			.hide()
			.stop()
			.animate({opacity: 0}, 'fast');
	});
});